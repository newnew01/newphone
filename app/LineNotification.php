<?php

namespace App;

use GuzzleHttp\Client;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Auth;

class LineNotification extends Model
{

    static function reportNewInvoice(Invoice $invoice,$token=null)
    {
        //$token = 'GGFixOog1QZFEuV52be5Zlc5hKW6ScB9Qdy1TMJhnPF';
        $datetime = $invoice->created_at;

        $message = "\n";
        $message .= "สาขา: ".$invoice->branch->branch_name."\n";
        $message .= "เลขที่ใบเสร็จ: INV-".sprintf("%06d",$invoice->id)."\n";
        $message .= "วันที่: ".$datetime." \n\n";

        if($invoice->customer_fname != null){
            $message .= "ชื่อลูกค้า: ".$invoice->customer_fname." ".$invoice->customer_lname."\n";
            $message .= "ที่อยู่: ".$invoice->customer_address." ".$invoice->getDistrictName()." ".$invoice->getAmpherName()." ".$invoice->getProviceName()." ".$invoice->customer_zipcode."\n";
            $message .= "โทร: ".$invoice->customer_tel;
        }else{
            $message .= "ชื่อลูกค้า: สด";
        }

        $message .= "\n\n";

        foreach($invoice->detail as $index=>$inv_detail){
            if($inv_detail->product_id != null)
                $message .= "[".($index+1)."] ".$inv_detail->productInfo->product_name." ".$inv_detail->productInfo->brand->brand_name." ".$inv_detail->productInfo->model.($inv_detail->sn == null ? '':"\n   IMEI: ".$inv_detail->sn);
            else
                $message .= "[".($index+1)."] ".$inv_detail->serviceInfo->service_name;

            if(!$inv_detail->free_gift){
                $message .= ($inv_detail->discount > 0 ? " **ส่วนลด ".$inv_detail->discount.'.-':'');
            }else{
                $message .= '**แถมฟรี';
            }
            $message .= "\n";
            $message .= "   จำนวน: ".$inv_detail->amount."\n";
            $message .= "   ราคา: ".$inv_detail->price*$inv_detail->amount." (".$inv_detail->price."x".$inv_detail->amount.")\n";

        }

        $message .= "\nราคาสุทธิ: ".$invoice->getTotalPrice();

        if($invoice->remark != null)
            $message .= "\n\n*หมายเหตุ: ".$invoice->remark;

        $message .= "\nผู้ออกใบเสร็จ: ".$invoice->getEmployeeName();

        self::notify($token,$message);
    }

    static function reportCancelInvoice(Invoice $invoice,$token=null)
    {

        $message = "\n";
        $message .= "[ยกเลิกใบเสร็จ]\n";
        $message .= "สาขา: ".$invoice->branch->branch_name."\n";
        $message .= "เลขที่ใบเสร็จ: INV-".sprintf("%06d",$invoice->id)."\n";
        $message .= "เหตุผล: ".$invoice->getInvoiceCancelReason->reason."\n\n";

        $message .= "ผู้ยกเลิก: ".Auth::user()->name;

        self::notify($token,$message);
    }


    static function reportStockDefect(StockReference $stock_ref,$token=null)
    {
        $datetime = $stock_ref->created_at;

        $message = "\n";
        $message .= "[ตัดสินค้าเสีย]\n";
        $message .= "สาขา: ".$stock_ref->sourceBranch->branch_name."\n";
        $message .= "เลขที่อ้างอิง: ".$stock_ref->getReferenceId()."\n";
        $message .= "วันที่: ".$datetime." \n\n";



        foreach($stock_ref->detail as $index=>$stock_history){
            $message .= "[".($index+1)."] ".$stock_history->productInfo->product_name." ".$stock_history->productInfo->brand->brand_name." ".$stock_history->productInfo->model.($stock_history->sn == null ? '':"\n   IMEI: ".$stock_history->sn);

            $message .= "\n";
            $message .= "   จำนวน: ".$stock_history->amount."\n\n";
        }


            $message .= "*อาการเสีย: ".$stock_ref->getDefectInfo->info;

        $message .= "\nผู้ตัดสต๊อค: ".$stock_ref->user->name;

        self::notify($token,$message);
    }

    static public function notify($token, $message)
    {
        $headers = [
            'Authorization' => 'Bearer ' . $token
        ];

        $client = new Client();
        $response = $client->request('POST', 'https://notify-api.line.me/api/notify', [
            'form_params' => [
                'message' => $message
            ],
            'headers' => $headers
        ]);

        return $response;
    }
}
