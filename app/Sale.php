<?php

namespace App;

use function foo\func;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class Sale extends Model
{


    public $branch_id;
    public $emp_id;

    public $invoice_id;

    public $error_msg;
    public $transaction_result = false;

    private $products=[];
    private $customer_data;
    private $remark;

    public function addProduct($product_id,$product_price,$product_cost,$product_discount,$product_freegift,$product_amount=1,$product_type_sn,$product_sn=null,$item_type=1){
        if($product_type_sn == 1)
            $product_amount = 1;
        if($product_freegift == true)
            $product_discount = 0;

        $p = [
            'product_id' => $product_id,
            'product_amount' => $product_amount,
            'product_type_sn' => $product_type_sn,
            'product_sn' => $product_sn,
            'product_price' => $product_price,
            'product_cost' => $product_cost,
            'product_discount' => $product_discount,
            'product_freegift' => $product_freegift,
            'item_type' => $item_type,
        ];
        array_push($this->products,$p);
    }

    public function setCustomerData($customer_data)
    {
        $this->customer_data = $customer_data;
    }

    public function setRemark($remark)
    {
        $this->remark = $remark;
    }

    public function saveSale()
    {
        DB::transaction(function (){

            $this->invoice_id = $this->createInvoice(
                $this->customer_data['first_name'],
                $this->customer_data['last_name'],
                $this->customer_data['gender'],
                $this->customer_data['address'],
                $this->customer_data['district_id'],
                $this->customer_data['ampher_id'],
                $this->customer_data['province_id'],
                $this->customer_data['zip_code'],
                $this->customer_data['customer_tel'],
                $this->remark,
                $this->emp_id,
                $this->branch_id
            );

            foreach ($this->products as $product){
                if($product['item_type'] == 1)
                    $this->createInvoiceDetail($this->invoice_id,$product['product_id'],null,$product['product_price'],$product['product_cost'],$product['product_discount'],$product['product_sn'],$product['product_amount'],$product['product_freegift']);
                elseif ($product['item_type'] == 2)
                    $this->createInvoiceDetail($this->invoice_id,null,$product['product_id'],$product['product_price'],$product['product_cost'],$product['product_discount'],$product['product_sn'],$product['product_amount'],$product['product_freegift']);
            }

            $stock = new Stock();
            $stock->branch_id = $this->branch_id;
            $stock->emp_id = $this->emp_id;
            $stock->applySale($this);

            $this->transaction_result = $stock->transaction_result;
            $this->error_msg = $stock->error_msg;
        });

        return $this->transaction_result;
    }

    public function createInvoice($firs_name,$last_name,$gender,$address,$district_id,$ampher_id,$province_id,$zip_code,$tel,$remark,$emp_id,$branch_id)
    {
        $invoice = new Invoice();
        $invoice->customer_fname = $firs_name;
        $invoice->customer_lname = $last_name;
        $invoice->gender = $gender;
        $invoice->customer_address = $address;
        $invoice->customer_tumbol = $district_id;
        $invoice->customer_ampher = $ampher_id;
        $invoice->customer_province = $province_id;
        $invoice->customer_zipcode = $zip_code;
        $invoice->customer_tel = $tel;
        $invoice->remark = $remark;
        $invoice->employee_id = $emp_id;
        $invoice->branch_id = $branch_id;
        $invoice->save();
        $invoice_id = $invoice->id;

        return $invoice_id;
    }

    public function createInvoiceDetail($invoice_id,$product_id,$service_id=null,$price,$cost,$discount,$sn=null,$amount,$free_gift)
    {
        $invoice_detail = new InvoiceDetail();
        $invoice_detail->invoice_id = $invoice_id;
        $invoice_detail->product_id = $product_id;
        $invoice_detail->service_id = $service_id;
        $invoice_detail->price = $price;
        $invoice_detail->cost = $cost;
        $invoice_detail->discount = $discount;
        $invoice_detail->sn = $sn;
        $invoice_detail->amount = $amount;
        $invoice_detail->free_gift = $free_gift;
        $inv_detail_id = $invoice_detail->save();

        return $inv_detail_id;
    }

    public function getProducts()
    {
        return $this->products;
    }

    static public function cancelInvoice($invoice_id,$emp_id,$reason)
    {
        DB::transaction(function () use ($invoice_id,$emp_id,$reason){
            $stock = new Stock();
            $stock->emp_id = $emp_id;
            $result = $stock->returnStockCancelInvoice($invoice_id);

            if($result){
                $invoice = Invoice::find($invoice_id);
                $invoice->status = 4;
                $invoice->save();

                $inv_cancel_reason = new InvoiceCancelReason();
                $inv_cancel_reason->invoice_id = $invoice_id;
                $inv_cancel_reason->reason = $reason;
                $inv_cancel_reason->emp_id = $emp_id;
                $inv_cancel_reason->save();
            }

        });

    }

}
