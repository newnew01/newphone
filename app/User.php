<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Support\Facades\Hash;
use Spatie\Permission\Traits\HasRoles;

class User extends Authenticatable
{
    use Notifiable;
    use HasRoles;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password','username','branch_id','role_id','mobile_no'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    public function setPasswordAttribute($pass){

        $this->attributes['password'] = Hash::make($pass);

    }

    public function Branch()
    {
        return $this->belongsTo(Branch::class,'branch_id');
    }

    public function setPermissionBranch($branch_id)
    {
        $user_branch = UserBranch::where('user_id',$this->id)->where('branch_id',$branch_id);

        if(!$user_branch->exists()){
            $user_branch = new UserBranch();
            $user_branch->user_id = $this->id;
            $user_branch->branch_id = $branch_id;
            $user_branch->save();
        }
    }

    public function removePermissionBranch($branch_id)
    {
        $user_branch = UserBranch::where('user_id',$this->id)->where('branch_id',$branch_id);

        if($user_branch->exists()){
            $user_branch = $user_branch->first();
            $user_branch->delete();
        }
    }

    public function syncPermissionBranch($branch_ids)
    {
        $user_branch = UserBranch::where('user_id',$this->id);

        foreach ($branch_ids as $branch_id){
            $this->setPermissionBranch($branch_id);
        }

        foreach ($user_branch->get() as $u_branch){
            $has_permission = false;
            foreach ($branch_ids as $branch_id){
                if($u_branch->branch_id == $branch_id){
                    $has_permission = true;
                    break;
                }
            }

            if(!$has_permission){
                $this->removePermissionBranch($u_branch->branch_id);
            }
        }
    }

    public function hasPermissionBranch($branch_id)
    {
        $user_branch = UserBranch::where('user_id',$this->id)->where('branch_id',$branch_id);

        if($user_branch->exists())
            return true;
        else
            return false;
    }

    public function getUserBranch()
    {
        return $this->hasMany(UserBranch::class,'user_id');
    }

}
