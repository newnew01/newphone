<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class BuddyBranch extends Model
{
    protected $table  = 'buddy_branch';
}
