<?php

namespace App\Http\Controllers;

use App\TopupLog;
use Carbon\Carbon;
use Illuminate\Http\Request;

class TopupLogController extends Controller
{

    public function getTodayLog()
    {
        $start = (new Carbon('now'))->hour(0)->minute(0)->second(0);
        $end = (new Carbon('now'))->hour(23)->minute(59)->second(59);

        $logs = TopupLog::whereBetween('topup_logs.created_at', [$start , $end])
            ->join('branches','branches.id','=','topup_logs.branch_id')
            ->orderBy('topup_logs.created_at', 'desc')
            ->select('topup_logs.id',
                'topup_logs.created_at',
                'topup_logs.orderid',
                'topup_logs.branch_id',
                'branches.branch_name',
                'topup_logs.network',
                'topup_logs.network_code',
                'topup_logs.number',
                'topup_logs.cash',
                'topup_logs.status',
                'topup_logs.sms',
                'topup_logs.drawn_amount',
                'topup_logs.emp_id'
            )
            ->get();

        return $logs;
    }

    public function getTodayReport()
    {
        $start = (new Carbon('now'))->hour(0)->minute(0)->second(0);
        $end = (new Carbon('now'))->hour(23)->minute(59)->second(59);
        $reports = TopupLog::whereBetween('topup_logs.created_at', [$start , $end])->where('status','=','2')
            ->join('branches','branches.id','=','topup_logs.branch_id')
            ->groupBy('branch_id','network','branch_name')
            ->selectRaw('sum(cash) as sum, branch_id,branch_name,network')
            ->get();

        return $reports;
    }

    public function getTodayReportTotal()
    {
        $start = (new Carbon('now'))->hour(0)->minute(0)->second(0);
        $end = (new Carbon('now'))->hour(23)->minute(59)->second(59);
        $report_totals = TopupLog::whereBetween('topup_logs.created_at', [$start , $end])->whereIn('status',['2','6'])
            ->join('branches','branches.id','=','topup_logs.branch_id')
            ->groupBy('branch_id','branch_name')
            ->selectRaw('sum(cash) as sum,sum(drawn_amount) as sum_drawn_amount, branch_id,branch_name')->get();

        return $report_totals;
    }

    public function getMonthlyReport()
    {
        $monthStart = (new Carbon('now'))->startOfMonth();
        $monthEnd = (new Carbon('now'))->endOfMonth();
        $reports = TopupLog::whereBetween('topup_logs.created_at', [$monthStart , $monthEnd])->where('status','=','2')
            ->join('branches','branches.id','=','topup_logs.branch_id')
            ->groupBy('branch_name','network','branch_id')
            ->selectRaw('sum(cash) as sum, branch_name,network,branch_id')->get();

        return $reports;
    }

    public function getMonthlyReportTotal()
    {
        //$now = Carbon::now();
        $monthStart = (new Carbon('now'))->startOfMonth();
        $monthEnd = (new Carbon('now'))->endOfMonth();

        $report_totals = TopupLog::whereBetween('topup_logs.created_at', [$monthStart , $monthEnd])->where('status','=','2')
            ->join('branches','branches.id','=','topup_logs.branch_id')
            ->groupBy('branch_name','branch_id')
            ->selectRaw('sum(cash) as sum, branch_name,branch_id')->get();

        return $report_totals;
    }


    public function getEntireReportTotal()
    {

        $report_totals = TopupLog::where('status','=','2')->groupBy('branch_name','branch_id')
            ->join('branches','branches.id','=','topup_logs.branch_id')
            ->selectRaw('sum(cash) as sum, branch_name,branch_id')->get();

        return $report_totals;
    }

    public function getEntireReport()
    {
        $reports = TopupLog::where('status','=','2')->groupBy('branch_name','network','branch_id')
            ->join('branches','branches.id','=','topup_logs.branch_id')
            ->selectRaw('sum(cash) as sum, branch_name,network,branch_id')->get();

        return $reports;
    }
}
