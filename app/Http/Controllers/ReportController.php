<?php

namespace App\Http\Controllers;

use App\Invoice;
use App\Statistic;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class ReportController extends Controller
{
    public function view_dailyReport()
    {
        $total_income = 0;
        $total_outcome = 0;

        $date = Carbon::now()->format('d-m-Y');

        $total_mpay = Statistic::getTodayMpay(Auth::user()->branch_id);
        $total_topup = Statistic::getTodayTopup(Auth::user()->branch_id);
        $mpay_bill_amount = Statistic::getTodayMpayBillAmount(Auth::user()->branch_id);

        $invoice_amount = Statistic::getTodayInvoiceAmount(Auth::user()->branch_id);

        $branch_name = Auth::user()->Branch->branch_name;

        $total_income += $total_mpay+$total_topup;

        $invoices = Invoice::whereBetween('created_at',array(Carbon::today()->startOfDay(),Carbon::today()->endOfDay()))
            ->where('branch_id','=',Auth::user()->branch_id)
            ->get();

        foreach ($invoices as $invoice){
            if($invoice->status != 4)
                $total_income += $invoice->getTotalPrice();
        }


        return view('pages.daily-report')
            ->with(compact('invoices'))
            ->with(compact('total_income'))
            ->with(compact('total_outcome'))
            ->with(compact('total_mpay'))
            ->with(compact('total_topup'))
            ->with(compact('mpay_bill_amount'))
            ->with(compact('invoice_amount'))
            ->with(compact('date'))
            ->with(compact('branch_name'));
    }
}
