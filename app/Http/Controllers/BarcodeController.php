<?php

namespace App\Http\Controllers;

use App\PrintService;
use App\Product;
use Illuminate\Support\Facades\Session;
use Illuminate\Http\Request;
use Milon\Barcode\DNS1D;

class BarcodeController extends Controller
{
    public function view_PrintLabel()
    {
        $products = Product::all();
        $print_services = PrintService::all();

        return view('pages.barcode-print-label')
            ->with(compact('products'))
            ->with(compact('print_services'));
    }
    public function view_Custom()
    {
        return view('pages.barcode-custom');
    }

    public function view_PrintService()
    {
        $print_services = PrintService::all();
        return view('pages.barcode-print-service')->with(compact('print_services'));
    }

    public function addPrintService(Request $request)
    {
        $service_name = $request->input('service_name');
        $service_uri = $request->input('service_uri');
        $service_status_uri = $request->input('service_status_uri');

        $print_service = new PrintService();
        $print_service->service_name = $service_name;
        $print_service->service_uri = $service_uri;
        $print_service->service_status_uri = $service_status_uri;

        $print_service->service_enable = 1;
        if($print_service->save())
            Session::flash('flash_msg_success',['title'=>'สำเร็จ','text'=>'เพิ่มเซอร์วิสการพิมพ์แล้ว']);
        else
            Session::flash('flash_msg_danger',['title'=>'ผิดพลาด','text'=>'เพิ่มเซอร์วิสไม่สำเร็จ']);

        return redirect('/barcode/print-service');
    }


    public function view_Print(Request $request)
    {
        $barcode_data = $request->all();

        //dd($barcode_data['product_name'][1]);

        $barcode_img = [];
        for($i=0;$i<56;$i++){
            $barcode_img[$i] = '';
            if($barcode_data['barcode'][$i] != '')
                $barcode_img[$i] =  DNS1D::getBarcodePNG($barcode_data['barcode'][$i], "C128");
        }

        //$test_img =  DNS1D::getBarcodePNG('11112222000000', "C128");
        //position in cm
        $pos_x = [2.8,6.8,11,15.1];
        $pos_y = [0.2,1.8,3.4,5.0,
                    6.6,8.2,9.8,11.4,
                    13.0,14.6,16.2,17.9,
                    19.5,21.1];

        return view('template.barcode-print')
            ->with(compact('barcode_data'))
            ->with(compact('barcode_img'))
            ->with(compact('pos_x'))
            ->with(compact('pos_y'));
    }
}
