<?php

namespace App\Http\Controllers;

use App\Branch;
use App\Brand;
use App\Category;
use App\Export\StocksExport;
use App\Product;
use App\ProductAmount;
use App\ProductSN;
use Illuminate\Http\Request;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\DB;
use Maatwebsite\Excel\Facades\Excel;

class ReportStockController extends Controller
{
    public function view_reportStock()
    {
        $branches = Branch::all();
        $brands = Brand::all();
        $categories = Category::all();
        return view('pages.report-stock')->with(compact('branches'))
            ->with(compact('categories'))
            ->with(compact('brands'));
    }

    public function exportStock(Request $request)
    {

        //return $request->all();
        $brand_id = $request->input('brand_id');
        $branch_id = $request->input('branch_id');
        $category_id = $request->input('category_id');

        $option_show_imei = false;
        $option_show_zero_amount = false;
        $option_show_branch = false;
        $option_order_by_brand = false;

        if($request->has('option_show_imei'))
            $option_show_imei = true;
        if($request->has('option_show_zero_amount'))
            $option_show_zero_amount = true;
        if($request->has('option_show_branch'))
            $option_show_branch = true;
        if($request->has('option_order_by_brand'))
            $option_order_by_brand = true;


        $products = null;
        $collections = [];

        $branch_name = 'ทุกสาขา';
        if($branch_id != 0)
            $branch_name = Branch::find($branch_id)->branch_name;
        //show branch name
        array_push($collections,[
            'สาขา',
            $branch_name,
        ]);


        //insert gap
        array_push($collections,[' ']);

        //heading
        array_push($collections,[
            'รหัสสินค้า',
            'หมวดหมู่',
            'ชื่อสินค้า',
            'ยี่ห้อ',
            'รุ่น',
            'คงเหลือ',
        ]);

        if($brand_id == 0 && $category_id == 0){
            if($option_order_by_brand)
                $products = Product::orderBy('brand_id')->get();
            else
                $products = Product::all();
        }else{
            if($category_id != 0) {
                $products = Product::where('category_id',$category_id);
                if($option_order_by_brand)
                    $products->orderBy('brand_id');
            }

            if($brand_id != 0){
                if($products == null)
                    $products = Product::where('brand_id',$brand_id);
                else
                    $products->where('brand_id',$brand_id);
            }

            $products = $products->get();
            //return $products;

        }



        if($products->count() > 0){
            foreach ($products as $product){
                if ($product->type_sn == 1){
                    $product_sn = ProductSN::where('product_id',$product->id);

                    if($branch_id != 0) {
                        $product_sn->where('branch_id',$branch_id);
                        $amount = $product->getAmountInBranch($branch_id);
                    }else{
                        $amount = $product->getAmount();
                    }

                    if($amount > 0){
                        if($product_sn->exists()){
                            $product_sn = $product_sn->get();

                            array_push($collections,[
                                'product_id' => str_pad($product->id,6,'0',STR_PAD_LEFT),
                                'product_cate_name' => $product->category->cate_name,
                                'product_name' => $product->product_name,
                                'product_brand' => $product->brand->brand_name,
                                'product_model' => $product->model,
                                'product_amount' => $amount,

                            ]);

                            if($option_show_branch && $branch_id == 0){
                                $branches = Branch::all();
                                foreach ($branches as $branch){
                                    $amount_in_branch = $product->getAmountInBranch($branch->id);
                                    if($amount_in_branch > 0){
                                        array_push($collections,[
                                            'product_id' => '',
                                            'product_cate_name' => '',
                                            'product_name' => '',
                                            'product_brand' => '',
                                            'product_model' => '______'.$branch->branch_name,
                                            'product_amount' => $amount_in_branch,

                                        ]);

                                        if($option_show_imei){
                                            foreach ($product->getProductSN($branch->id) as $psn){
                                                array_push($collections,[
                                                    'product_id' => '',
                                                    'product_cate_name' => '',
                                                    'product_name' => '',
                                                    'product_brand' => '',
                                                    'product_model' => '['.$psn->sn.']',
                                                    'product_amount' => '',

                                                ]);
                                            }
                                        }
                                    }


                                }
                            }else{
                                if($option_show_imei){
                                    foreach ($product_sn as $p_sn){
                                        array_push($collections,[
                                            'product_id' => '',
                                            'product_cate_name' => '',
                                            'product_name' => '',
                                            'product_brand' => '',
                                            'product_model' => '['.$p_sn->sn.']',
                                            'product_amount' => '',

                                        ]);
                                    }
                                }
                            }




                        }else{
                            //no product_sn data found
                        }
                    }else{
                        //no product in this branch
                        if($option_show_zero_amount){
                            array_push($collections,[
                                'product_id' => str_pad($product->id,6,'0',STR_PAD_LEFT),
                                'product_cate_name' => $product->category->cate_name,
                                'product_name' => $product->product_name,
                                'product_brand' => $product->brand->brand_name,
                                'product_model' => $product->model,
                                'product_amount' => '0',

                            ]);
                        }
                    }
                }else{
                    $product_amount = ProductAmount::where('product_id',$product->id);

                    if($branch_id != 0) {
                        $product_amount->where('branch_id',$branch_id);
                        $amount = $product->getAmountInBranch($branch_id);
                    }else{
                        $amount = $product->getAmount();
                    }
                    if($amount > 0) {
                        array_push($collections,[
                            'product_id' => str_pad($product->id,6,'0',STR_PAD_LEFT),
                            'product_cate_name' => $product->category->cate_name,
                            'product_name' => $product->product_name,
                            'product_brand' => $product->brand->brand_name,
                            'product_model' => $product->model,
                            'product_amount' => $amount,

                        ]);
                        if($option_show_branch && $branch_id == 0){
                            $branches = Branch::all();
                            foreach ($branches as $branch){
                                $amount_in_branch = $product->getAmountInBranch($branch->id);
                                if($amount_in_branch > 0){
                                    array_push($collections,[
                                        'product_id' => '',
                                        'product_cate_name' => '',
                                        'product_name' => '',
                                        'product_brand' => '',
                                        'product_model' => '______'.$branch->branch_name,
                                        'product_amount' => $amount_in_branch,

                                    ]);
                                }


                            }
                        }

                    }else{
                        //no product_amount in this branch
                        if($option_show_zero_amount){
                            array_push($collections,[
                                'product_id' => str_pad($product->id,6,'0',STR_PAD_LEFT),
                                'product_cate_name' => $product->category->cate_name,
                                'product_name' => $product->product_name,
                                'product_brand' => $product->brand->brand_name,
                                'product_model' => $product->model,
                                'product_amount' => '0',

                            ]);
                        }
                    }


                }
            }
        }else{
            //no data found
        }



        return Excel::download(new StocksExport(Collection::make($collections)), 'stocks.xlsx');

        //return $request->all();
    }

    public function export()
    {
        $productsZZZ = Product::select(
            DB::raw("LPAD(products.id, 6, '0')"),
            'products.product_name',
            'brands.brand_name',
            'products.model'
        )
            ->join('brands','brands.id','=','products.brand_id');

        $products = Product::all();
        $collections = [];

        foreach ($products as $product){
            if ($product->type_sn == 1){
                $product_sn = ProductSN::where('product_id',$product->id);
                if($product_sn->exists()){
                    $product_sn = $product_sn->get();

                    array_push($collections,[
                        'product_id' => str_pad($product->id,6,'0',STR_PAD_LEFT),
                        'product_cate_name' => $product->category->cate_name,
                        'product_name' => $product->product_name,
                        'product_brand' => $product->brand->brand_name,
                        'product_model' => $product->model,
                        'product_amount' => $product->amount,

                    ]);

                    foreach ($product_sn as $p_sn){
                        array_push($collections,[
                            'product_id' => '',
                            'product_cate_name' => '',
                            'product_name' => $p_sn->sn,
                            'product_brand' => '',
                            'product_model' => '',
                            'product_amount' => '',

                        ]);
                    }
                }else{
                    //no product_sn data found
                }

            }else{
                array_push($collections,[
                    'product_id' => str_pad($product->id,6,'0',STR_PAD_LEFT),
                    'product_cate_name' => $product->category->cate_name,
                    'product_name' => $product->product_name,
                    'product_brand' => $product->brand->brand_name,
                    'product_model' => $product->model,
                    'product_amount' => $product->amount,

                ]);
            }
        }



        $posts = collect([
            ['id'=>1,'title'=>'Post one'],
            ['id'=>2,'title'=>'Post two'],
            ['id'=>3,'title'=>'Post three'],
            ['id'=>4,'title'=>'Post four'],
        ]);

        return Excel::download(new StocksExport(Collection::make($collections)), 'stocks.xlsx');

        $districts = District::where('district_name','like','%'.$district_name.'%')
            ->where('district_name','not like','%*%')
            ->join('amphers','districts.ampher_id','=','amphers.id')
            ->join('provinces','districts.province_id','=','provinces.id')
            ->join('zipcodes','districts.district_code','=','zipcodes.district_code')
            ->select(
                'districts.id as district_id',
                'districts.district_code',
                'districts.district_name',
                'amphers.id as ampher_id',
                'amphers.ampher_name',
                'provinces.id as province_id',
                'provinces.province_name',
                'zipcodes.zipcode'
            )
            ->get();
    }
}
