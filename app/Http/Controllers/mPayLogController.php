<?php

namespace App\Http\Controllers;

use App\mPayLog;
use Carbon\Carbon;
use Illuminate\Http\Request;

class mPayLogController extends Controller
{
    public function addLog(Request $request)
    {
        $transaction_datetime = Carbon::createFromFormat('d/m/Y H:i:s', $request->input('transaction_datetime'));
        $orderid = $request->input('orderid');
        $price = $request->input('price');
        $type = $request->input('type');
        $branch_id = $request->input('branch_id');

        $mpay_log = mPayLog::where('orderid','=',$orderid);
        if(!$mpay_log->exists()){
            $mpay_log = new mPayLog();
            $mpay_log->transaction_datetime = $transaction_datetime;
            $mpay_log->orderid = $orderid;
            $mpay_log->price = $price;
            $mpay_log->type = $type;
            $mpay_log->branch_id = $branch_id;
            $mpay_log->save();
        }

    }
}
