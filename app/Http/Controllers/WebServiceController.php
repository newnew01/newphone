<?php

namespace App\Http\Controllers;

use App\Ampher;
use App\District;
use App\InvoiceDetail;
use App\PrintService;
use App\Product;
use App\ProductSN;
use App\Province;
use App\Service;
use App\Statistic;
use App\StockHistory;
use App\Zipcode;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class WebServiceController extends Controller
{
    public function getPrinServices()
    {
        $print_services = PrintService::all();

        return $print_services;
    }



    public function getThaiLocationByDistrict($district_name)
    {
        $districts = District::where('district_name','like','%'.$district_name.'%')
            ->where('district_name','not like','%*%')
            ->join('amphers','districts.ampher_id','=','amphers.id')
            ->join('provinces','districts.province_id','=','provinces.id')
            ->join('zipcodes','districts.district_code','=','zipcodes.district_code')
            ->select(
                'districts.id as district_id',
                'districts.district_code',
                'districts.district_name',
                'amphers.id as ampher_id',
                'amphers.ampher_name',
                'provinces.id as province_id',
                'provinces.province_name',
                'zipcodes.zipcode'
            )
            ->get();



        return $districts;

        $results = [];
        array_push($results,['id' => 1, 'text' => 'test']);
        $test_data = [
            'results' => $districts
        ];
        return $test_data;
    }

    public function getThaiLocationByName($province_name,$ampher_name,$district_name)
    {
        $province = Province::where('province_name','like','%'.$province_name.'%')->first();
        $ampher = Ampher::where('ampher_name','like','%'.$ampher_name.'%')
            ->where('province_id','=',$province->id)
            ->where('ampher_name','not like','%*%')
            ->first();

        $district = District::where('district_name','like','%'.$district_name.'%')
            ->where('ampher_id','=',$ampher->id)
            ->where('district_name','not like','%*%')
            ->first();

        $zipcode = Zipcode::where('district_code','=',$district->district_code)->first();

        $data = [
            'district_id' => $district->id,
            'district_name' =>$district->district_name,
            'ampher_id' => $ampher->id,
            'ampher_name' => $ampher->ampher_name,
            'province_id' => $province->id,
            'province_name' => $province->province_name,
            'zipcode' => $zipcode->zipcode
        ];

        return $data;
    }

    public function getDashboardChartData($branch_id=null)
    {
        return Statistic::getThisMonthStatistic($branch_id);
    }

    public function getAllServices()
    {
        return Service::all();
    }

    public function searchSNbyMagicBarcode($sn)
    {
        //1.search in stock
        $product_sn = ProductSN::where('sn',$sn);
        if($product_sn->exists()){
            $product_sn = $product_sn->first();

            $product = Product::find($product_sn->product_id);
            $product['brand_name'] = $product->brand->brand_name;
            $product['cate_name'] = $product->category->cate_name;
            $product['sn'] = $product_sn->sn;
            $product['branch_name'] = $product_sn->branch->branch_name;
            $product['status'] = 1;
            $product['amount_in_branch'] = $product->getAmountInBranch(Auth::user()->branch_id);

            return $product;

        }else{
            //2.search in invocie and not cancel

            $isSold = false;
            $invoice_detail = InvoiceDetail::where('sn',$sn)->latest();

            if($invoice_detail->exists()){
                $invoice_detail = $invoice_detail->first();
                $invoice = $invoice_detail->invoice;
                if($invoice->status == 1){
                    $isSold = true;

                    $product = Product::find($invoice_detail->product_id);
                    $product['brand_name'] = $product->brand->brand_name;
                    $product['cate_name'] = $product->category->cate_name;
                    $product['sn'] = $invoice_detail->sn;
                    $product['branch_name'] = $invoice->branch->branch_name;
                    $product['status'] = 3;
                    $product['invoice_id'] = $invoice->id;
                    $product['amount_in_branch'] = $product->getAmountInBranch(Auth::user()->branch_id);


                    return $product;
                }
            }

            //3.search in stock histoy
            if(!$isSold){
                $stock_history = StockHistory::where('sn',$sn)->latest();

                if($stock_history->exists()){
                    $stock_history = $stock_history->first();
                    $stock_ref = $stock_history->stockReference;
                    //return $stock_history->stockReference;

                    if($stock_ref->ref_type == 1){
                        //stock in ,suppose to be in stock but not.

                        return 'none';

                    }else if($stock_ref->ref_type == 2){
                        //stock transfer
                        if($stock_ref->status_id == 3){
                            //transfer is being delivery

                            $product = $stock_history->productInfo;
                            $product['brand_name'] = $product->brand->brand_name;
                            $product['cate_name'] = $product->category->cate_name;
                            $product['sn'] = $stock_history->sn;
                            $product['branch_name'] = $stock_ref->sourceBranch->branch_name.'->'.$stock_ref->destinationBranch->branch_name;
                            $product['status'] = 2;
                            //$product['invoice_id'] = $invoice->id;
                            $product['amount_in_branch'] = $product->getAmountInBranch(Auth::user()->branch_id);


                            return $product;

                        }else{
                            //transfer success or unknown
                            return 'none';
                        }
                    }else if($stock_ref->ref_type == 3){
                        //sold, suppose to have invoice but not
                        return 'none';
                    }else if($stock_ref->ref_type == 4){
                        //stock in (cancel invoice) ,suppose to be in stock but not.
                        return 'none';

                    }else if($stock_ref->ref_type == 5){
                        //stock out defect

                        $product = $stock_history->productInfo;
                        $product['brand_name'] = $product->brand->brand_name;
                        $product['cate_name'] = $product->category->cate_name;
                        $product['sn'] = $stock_history->sn;
                        $product['branch_name'] = $stock_ref->sourceBranch->branch_name;
                        $product['status'] = 4;
                        //$product['invoice_id'] = $invoice->id;
                        $product['amount_in_branch'] = $product->getAmountInBranch(Auth::user()->branch_id);


                        return $product;
                    }

                }else{
                    return 'none';
                }
            }
        }


    }

    public function searchBarcodeByMagicBarcode($barcode)
    {
        $p = Product::where('barcode','=',$barcode);
        if($p->count() > 0){
            $product = $p->first();
            $product['brand_name'] = $product->brand->brand_name;
            $product['cate_name'] = $product->category->cate_name;
            $product['amount_in_branch'] = $product->getAmountInBranch(Auth::user()->branch_id);

            return $product;
        }

        else
            return 'none';
    }

    public function changeUserBranch($branch_id)
    {
        $user = Auth::user();

        if($user->hasPermissionBranch($branch_id)){
            $user->branch_id = $branch_id;
            $user->save();
        }
    }
}
