<?php

namespace App\Http\Controllers;

use App\Branch;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Session;
use Spatie\Permission\Models\Permission;

class UserController extends Controller
{
    public function view_userList()
    {
        $users = User::all();
        return view('pages.user-list')->with(compact('users'));
    }

    public function view_userNew()
    {
        $branches = Branch::all();

        return view('pages.user-new')->with(compact('branches'));
    }

    public function view_userDetail()
    {

    }

    public function view_editUser($id)
    {
        $user = User::find($id);
        $branches = Branch::all();

        return view('pages.user-edit')->with(compact('user'))
            ->with(compact('branches'));
    }

    public function view_Permission($id)
    {
        $user = User::find($id);
        $branches = Branch::all();
        $permissions = Permission::all();


        return view('pages.permission-set')->with(compact('user'))
            ->with(compact('permissions'))
            ->with(compact('branches'));
    }

    public function setPermission($id,Request $request)
    {
        //dd($request->all());
        $fields = Input::get();

        $permission_data = [];


        foreach($fields as $name => $value)
        {
            if($name != '_token' && $name != 'user-permission-branch')
                array_push($permission_data,$name);
        }

        $user = User::find($id);
        $user->syncPermissions($permission_data);


        if($request->has('user-permission-branch')){
            $permission_branch = $request->input('user-permission-branch');
            $user->syncPermissionBranch($permission_branch);
        }


        return redirect('/user/list');
    }

    public function editUser($id, Request $request)
    {
        $user = User::find($id);

        $user->name = $request->input('name');
        $user->username = $request->input('username');
        $user->email = $request->input('email');
        $user->mobile_no = $request->input('mobile_no');
        $user->branch_id = $request->input('branch_id');

        if($request->input('password') != '')
            $user->password = $request->input('password');

        $user->save();

        Session::flash('flash_msg_success',['title' => 'สำเร็จ','text' => 'แก้ไขผู้ใช้งานเรียบร้อยแล้ว']);

        return redirect('/user/list');

    }


    public function newUser(Request $request)
    {
        $r = User::create($request->all());

        $r->setPermissionBranch($request->input('branch_id'));

        Session::flash('flash_msg_success',['title' => 'สำเร็จ','text' => 'เพิ่มผู้ใช้งานเรียบร้อยแล้ว']);

        return redirect('/user/list');
    }
}
