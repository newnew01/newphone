<?php

namespace App\Http\Controllers;

use App\Branch;
use App\Brand;
use App\Product;
use App\ProductAmount;
use App\ProductSN;
use App\ProductTransfer;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class ServiceProductController extends Controller
{

    public function searchProduct($keyword)
    {
        $p = Product::where('id','=',(int)$keyword)->orWhere('product_name','like','%'.$keyword.'%')->orWhere('model','like','%'.$keyword.'%')->orWhere('price','like','%'.$keyword.'%');
        $brands = Brand::where('brand_name','like','%'.$keyword.'%');
        if($brands->exists()){
            $brands = $brands->get();
            foreach ($brands as $brand){
                //echo $brand->brand_name;
                $p = $p->orWhere('brand_id','=',$brand->id);
            }
        }

        $result_products = $p->get();

        foreach ($result_products as $product){
            $product['brand_name'] = $product->brand->brand_name;
            $product['cate_name'] = $product->category->cate_name;
            $product['amount_in_branch'] = $product->getAmountInBranch(Auth::user()->branch_id);
        }

        return $result_products;
    }
    public function findProductByBarcode($barcode)
    {
        $p = Product::where('barcode','=',$barcode);
        if($p->count() > 0){
            $product = $p->first();
            $product['brand_name'] = $product->brand->brand_name;
            $product['cate_name'] = $product->category->cate_name;

            return $product;
        }

        else
            return 'none';
    }

    public function findProductById($id)
    {
        $p = Product::find($id);
        if($p != null){
            $product = $p;
            $product['brand_name'] = $product->brand->brand_name;
            $product['cate_name'] = $product->category->cate_name;

            return $product;
        }

        else
            return 'null';
    }

    public function findProductSNbyIdSN($id,$sn)
    {
        //dd($id.' '.$sn);
        $p = ProductSN::where('product_id','=',$id)->where('sn','=',$sn);
        if($p->exists()){

            return $p->first();
        }

        else
            return 'null';
    }

    public function findProductSnInBranch($id,$sn,$branch_id)
    {
        //dd($id.' '.$sn);
        $p = ProductSN::where('product_id','=',$id)->where('sn','=',$sn)
            ->where('branch_id','=',$branch_id);
        if($p->exists()){

            return $p->first();
        }

        else
            return 'none';
    }

    public function findProductAmountInBranch($id, $branch_id)
    {
        $p = ProductAmount::where('product_id','=',$id)->where('branch_id','=',$branch_id);

        $amount = 0;
        if($p->exists()){
            $p = $p->first();
            $amount = $p->amount;
        }

        return ['amount' => $amount];
    }

    public function checkDuplicatedSN($id,$sn)
    {
        //dd($id.' '.$sn);
        $p = ProductSN::where('product_id','=',$id)->where('sn','=',$sn);
        if($p->exists()){

            return 'true';
        }

        else
            return 'false';
    }

    public function getGenBarcode()
    {
        $prefix = Carbon::now()->format('dmY');
        $barcode = $prefix.'00001';
        $p = Product::where('barcode','like',$prefix.'%')->orderBy('barcode','desc');
        if($p->count() > 0){
            $barcode_ = $p->first()->barcode;
            $number = explode($prefix,$barcode_)[1];
            $number = sprintf("%05d", ++$number);
            $barcode = $prefix.$number;
        }

        return $barcode;
    }

    public function getProductStock($product_id)
    {
        $result = Product::getProductStock($product_id);

        return $result;
    }

    public function findTransferringProduct($product_id)
    {
        $product_transfer = ProductTransfer::where('product_id',$product_id);

        $product_transfer = $product_transfer->get();

        return $product_transfer;

    }
}
