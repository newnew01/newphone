<?php

namespace App\Http\Controllers;

use App\Ampher;
use App\District;
use App\Invoice;
use App\InvoiceDetail;
use App\LineNotification;
use App\Product;
use App\Province;
use App\Sale;
use App\Service;
use App\StockReference;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Milon\Barcode\DNS1D;

class SaleController extends Controller
{
    public function view_NewOrder()
    {
        return view('pages.sale-neworder');
    }

    public function view_orderList()
    {
        $invoices = Invoice::where('branch_id','=',Auth::user()->branch_id)->latest()->get();

        return view('pages.sale-list')->with(compact('invoices'));
    }

    public function newOrder(Request $request)
    {
        //dd($request->all());
        //return 1;

        $branch_id = Auth::user()->branch_id;
        $emp_id = Auth::user()->id; //user id
        $invoice_full_price = $request->input('invoice_full_price');
        $no_customer_info = $request->input('$no_customer_info');

        $data = $request->all();

        $customer_data = [
            'first_name' => $request->input('first_name'),
            'last_name' => $request->input('last_name'),
            'gender' => $request->input('gender'),
            'address' => $request->input('address'),
            'district_id' => $request->input('tumbol'),
            'ampher_id' =>$request->input('ampher') ,
            'province_id' => $request->input('province'),
            'zip_code' => $request->input('zip_code'),
            'customer_tel' => $request->input('tel_no'),
        ];

        if($no_customer_info == 'on'){
            $customer_data['first_name'] = null;
            $customer_data['last_name'] = null;
            $customer_data['gender'] = null;
            $customer_data['address'] = null;
            $customer_data['district_id'] = null;
            $customer_data['ampher_id'] = null;
            $customer_data['province_id'] = null;
            $customer_data['zip_code'] = null;
            $customer_data['customer_tel'] = null;

        }

        $remark = $request->input('remark');

        $sale = new Sale();
        $sale->emp_id = $emp_id;
        $sale->branch_id = $branch_id;
        $sale->setCustomerData($customer_data);
        $sale->setRemark($remark);

        foreach ($data['product_id'] as  $index=>$product_id){
            $item_type = $data['item_type'][$index];
            if($item_type == 1){
                $product = Product::find($product_id);
                $price = $product->price;
                $cost = $product->cost;
            }else if($item_type == 2){
                //$service = Service::find($product_id);
                $price = $data['item_price'][$index];
                $cost = 0;
            }

            $amount = $data['count'][$index];
            $discount = $data['discount'][$index];
            $type_sn = $data['type_sn'][$index];
            $free_gift = $data['free_gift'][$index];
            $sn = $data['sn'][$index];


            if($free_gift == 'true')
                $free_gift = true;
            else
                $free_gift = false;


            $sale->addProduct($product_id,$price,$cost,$discount,$free_gift,$amount,$type_sn,$sn,$item_type);
        }

        $result = $sale->saveSale();
        if($result){
            $ref_id_barcode = DNS1D::getBarcodePNG('INV-'.sprintf('%06d', $sale->invoice_id), "C128");
            $invoice = Invoice::find($sale->invoice_id);



            if($customer_data['province_id'] == "1"){
                $prefix = [
                    'district' => 'แขวง',
                    'ampher' => 'เขต',
                    'province' => '',
                ];
            }else{
                $prefix = [
                    'district' => 'ต.',
                    'ampher' => 'อ.',
                    'province' => 'จ.',
                ];
            }

            if($invoice->customer_tumbol  == null || $invoice->customer_ampher == null || $invoice->customer_province == null){
                $address_data = [
                    'district' => null,
                    'ampher' => null,
                    'province' => null,
                ];
            }else{
                $address_data = [
                    'district' => District::find($invoice->customer_tumbol)->district_name,
                    'ampher' => Ampher::find($invoice->customer_ampher)->ampher_name,
                    'province' => Province::find($invoice->customer_province)->province_name,
                ];
            }

            //real LINE SYSTEM REPORT TOKEN = 6q8UvkLZ9yNlBFFyGWL7hVuiQ4C5ZIL802AJLlT7L6Z
            //New's TOKEN = 5UEc26Cw3eMDkFHiWXnJSV4vfUdM9rxKdMojtkUVWaj
            //LineNotification::reportNewInvoice($invoice,'5UEc26Cw3eMDkFHiWXnJSV4vfUdM9rxKdMojtkUVWaj');
            LineNotification::reportNewInvoice($invoice,env("LINE_NOTIFY_TOKEN"));

            return view('pages.sale-invoice-document')->with(compact('invoice'))
                ->with(compact('invoice_full_price'))
                ->with(compact('ref_id_barcode'))
                ->with(compact('prefix'))
                ->with(compact('address_data'));
        }else{
            Session::flash('flash_msg_danger',['title' => 'ผิดพลาด','text' => $sale->error_msg]);
            return redirect('/sale/neworder');
        }





    }

    public static function view_invoice($id){
        $data = explode('-',$id);

        //dd($data);
        if($data[0] == 'INV'){
            $invoice_id = (int)$data[1];
        }else{
            $invoice_id = (int)$id;
        }
        $invoice = Invoice::find($invoice_id);
        $ref_id_barcode = DNS1D::getBarcodePNG('INV-'.sprintf('%06d', $id), "C128");


        if($invoice->province == "1"){
            $prefix = [
                'district' => 'แขวง',
                'ampher' => 'เขต',
                'province' => '',
            ];
        }else{
            $prefix = [
                'district' => 'ต.',
                'ampher' => 'อ.',
                'province' => 'จ.',
            ];
        }

        if($invoice->customer_tumbol  == null || $invoice->customer_ampher == null || $invoice->customer_province == null){
            $address_data = [
                'district' => null,
                'ampher' => null,
                'province' => null,
            ];
        }else{
            $address_data = [
                'district' => District::find($invoice->customer_tumbol)->district_name,
                'ampher' => Ampher::find($invoice->customer_ampher)->ampher_name,
                'province' => Province::find($invoice->customer_province)->province_name,
            ];
        }





        return view('pages.sale-invoice-document')->with(compact('invoice'))
            ->with(compact('ref_id_barcode'))
            ->with(compact('prefix'))
            ->with(compact('address_data'));
    }

    public function cancelInvoice($invoice_id,Request $request)
    {
        $emp_id = Auth::user()->id;
        $reason = $request->input('reason_input');

        //dd($request->all());

        Sale::cancelInvoice($invoice_id,$emp_id,$reason);
        LineNotification::reportCancelInvoice(Invoice::find($invoice_id),env("LINE_NOTIFY_TOKEN"));

        return self::view_invoice($invoice_id);
    }
}
