<?php

namespace App\Http\Controllers;

use App\AisDeal;
use App\BuddyBranch;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;

class AisController extends Controller
{
    public function view_newAisDeal()
    {
        $buddy_branches = BuddyBranch::all();
        return view('pages.ais-new-ais-deal')->with(compact('buddy_branches'));
    }

    public function addAisDeal(Request $request)
    {

        $p = $request->all();

        //check if data is not empty
        if($request->has('product_id'))
        {
            for($i=0;$i<count($p['product_id']);$i++) {
                $tmp_ais_deal = AisDeal::where('product_id','=',$p['product_id'][$i])->where('product_sn','=',$p['sn'][$i]);
                if(!$tmp_ais_deal->exists()){
                    $ais_deal = new AisDeal();
                    $ais_deal->product_id = $p['product_id'][$i];
                    $ais_deal->product_sn = $p['sn'][$i];
                    $ais_deal->buddy_id = $p['buddy_branch_id'];
                    $ais_deal->save();
                }

            }

            Session::flash('flash_msg_success',['title' => 'สำเร็จ','text' => 'เพิ่มข้อมูลสำเร็จ']);



        }else{
            Session::flash('flash_msg_danger',['title' => 'ผิดพลาด','text' => 'ไม่มีข้อมูลนำเข้าสต๊อค']);
        }
        return redirect('/ais/new-ais-deal');
    }
}
