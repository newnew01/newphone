<?php

namespace App\Http\Controllers;

use App\Statistic;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class DashboardController extends Controller
{
    public function view_index()
    {
        $branch_id = Auth::user()->branch_id;
        $statistics = new Statistic();
        return view('pages.dashboard')->with(compact('statistics'))
            ->with(compact('branch_id'));
    }
}
