<?php

namespace App\Console;

use Illuminate\Console\Scheduling\Schedule;
use Illuminate\Foundation\Console\Kernel as ConsoleKernel;

class Kernel extends ConsoleKernel
{
    /**
     * The Artisan commands provided by your application.
     *
     * @var array
     */
    protected $commands = [
        'App\Console\Commands\GetMpayLog',
    ];

    /**
     * Define the application's command schedule.
     *
     * @param  \Illuminate\Console\Scheduling\Schedule  $schedule
     * @return void
     */
    protected function schedule(Schedule $schedule)
    {

        /*
        $schedule->command('mpay:get-log 1')->everyMinute()->then(function() {
            $this->call('mpay:get-log 2');
            $this->call('topup:refresh');
            //$this->call('mpay:get-log');
        })->appendOutputTo(storage_path('logs/mpay.log'));
        */

        /*
        $schedule->command('mpay:get-log 1')
            ->everyMinute()
            ->appendOutputTo(storage_path('logs/mpay.log'));
        */
        $schedule->command('topup:refresh')
            ->everyFiveMinutes()
            ->appendOutputTo(storage_path('logs/topup.log'));

        $schedule->command('mpay:get-log 1')
            ->everyThirtyMinutes()
            ->appendOutputTo(storage_path('logs/mpay.log'));

        $schedule->command('mpay:get-log 2')
            ->everyThirtyMinutes()
            ->appendOutputTo(storage_path('logs/mpay.log'));

        $schedule->command('notify:today-topup-report')
            ->dailyAt('19:35');
    }

    /**
     * Register the commands for the application.
     *
     * @return void
     */
    protected function commands()
    {
        $this->load(__DIR__.'/Commands');

        require base_path('routes/console.php');
    }
}
