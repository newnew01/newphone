<?php

namespace App\Console\Commands;

use App\Branch;
use App\Product;
use App\ProductAmount;
use App\ProductSN;
use Illuminate\Console\Command;

class ProductRefresh extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'product:refresh';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'sync product_amount amount product_sn';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $products = Product::where('type_sn',1)->get();

        $branches = Branch::all();

        foreach ($products as $product){
            foreach ($branches as $branch){
                $count = $product->getAmountInBranch($branch->id);
                $product_amount = ProductAmount::where('product_id',$product->id)->where('branch_id',$branch->id);
                if($product_amount->exists()){
                    $product_amount = $product_amount->first();
                    $product_amount->amount = $count;
                    $product_amount->save();
                }else{
                    if($count > 0){
                        $pamount = new ProductAmount();
                        $pamount->product_id = $product->id;
                        $pamount->branch_id = $branch->id;
                        $pamount->amount = $count;
                        $pamount->save();
                    }
                }
            }
        }
    }
}
