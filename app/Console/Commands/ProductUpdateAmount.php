<?php

namespace App\Console\Commands;

use App\Product;
use App\ProductAmount;
use App\ProductSN;
use Illuminate\Console\Command;

class ProductUpdateAmount extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'product:update-amount';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'refresh and update product amount';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $products = Product::all();

        foreach($products as $product){
            if($product->type_sn == 1){
                $count = ProductSN::where('product_id',$product->id)->count();
                $product->amount = $count;
                $product->save();
            }else{
                $count = ProductAmount::where('product_id',$product->id)->sum('amount');
                $product->amount = $count;
                $product->save();
            }
        }
    }
}
