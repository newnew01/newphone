<?php

namespace App\Console\Commands;

use App\TopupLog;
use App\Wepay;
use GuzzleHttp\Client;
use Illuminate\Console\Command;

class RefreshTopupStatus extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'topup:refresh';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Refresh topup pending status';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $topup_logs = TopupLog::where('status','=','1');

        if($topup_logs->exists()){
            $topup_logs = $topup_logs->get();

            foreach ($topup_logs as $topup_log){
                //echo $topup_log->orderid;

                $response = Wepay::getTopupOutput($topup_log->orderid);
                $data = json_decode($response->getBody());

                if($data->code == '00000'){
                    parse_str($data->output, $output_data);

                    $topup_log->sms = $output_data['sms'];
                    $topup_log->status = $output_data['status'];
                    $topup_log->operator_trxnsid = $output_data['operator_trxnsid'];
                    $topup_log->save();



                }

            }

        }


    }
}
