<?php

namespace App\Console\Commands;

use App\Branch;
use App\LineNotification;
use App\TopupLog;
use Carbon\Carbon;
use Illuminate\Console\Command;

class NotifyTopupReport extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'notify:today-topup-report';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'report topup via LINE';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        //$token = '5UEc26Cw3eMDkFHiWXnJSV4vfUdM9rxKdMojtkUVWaj'; //private New's Token
        $token = '6q8UvkLZ9yNlBFFyGWL7hVuiQ4C5ZIL802AJLlT7L6Z';

        $datetime = Carbon::now()->format('d/m/Y H:i:s');

        $message = "\n";
        $message = "รายงานระบบเติมเงิน\n";
        $message .= "วันที่: ".$datetime." \n\n";

        $todayReport = $this->getTodayReport();
        $todayReportTotal = $this->getTodayReportTotal();

        $message .= "[ยอดรวมทั้งหมด]\n";

        foreach ($todayReportTotal as $reportTotal){
            $branch_name = Branch::find($reportTotal->branch_id)->branch_name;
            $message .= $branch_name.': '.$reportTotal->sum."\n";
        }

        $message .= "\n[แยกตามเครือข่าย]\n";

        $branch_id = '';
        foreach ($todayReport as $report){
            if($report->branch_id != $branch_id){
                $branch_id = $report->branch_id;
                $branch_name = Branch::find($branch_id)->branch_name;
                $message .= $branch_name."\n";
            }
            $message .= '    '.$report->network.': '.$report->sum."\n";
        }



        //$this->notify($token,$message);
        LineNotification::notify($token,$message);
    }

    public function getTodayReport()
    {
        $start = (new Carbon('now'))->hour(0)->minute(0)->second(0);
        $end = (new Carbon('now'))->hour(23)->minute(59)->second(59);
        $reports = TopupLog::whereBetween('created_at', [$start , $end])->where('status','=','2')->groupBy('branch_id','network')->
        selectRaw('sum(cash) as sum, branch_id,network')->get();

        return $reports;
    }

    public function getTodayReportTotal()
    {
        $start = (new Carbon('now'))->hour(0)->minute(0)->second(0);
        $end = (new Carbon('now'))->hour(23)->minute(59)->second(59);
        $report_totals = TopupLog::whereBetween('created_at', [$start , $end])->where('status','=','2')->groupBy('branch_id')->
        selectRaw('sum(cash) as sum, branch_id')->get();

        return $report_totals;
    }
}
