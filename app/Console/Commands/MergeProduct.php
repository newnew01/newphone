<?php

namespace App\Console\Commands;

use App\InvoiceDetail;
use App\Product;
use App\ProductAmount;
use App\ProductSN;
use App\StockHistory;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\DB;

class MergeProduct extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'product:merge {source_id} {dest_id}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Merge Product';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        DB::transaction(function (){
            $source_id = $this->argument('source_id');
            $dest_id = $this->argument('dest_id');

            $product_source = Product::find($source_id);
            $product_dest = Product::find($dest_id);

            if($product_dest->type_sn == $product_source->type_sn){
                $product_dest->amount += $product_source->amount;
                $product_dest->save();
                $product_source->amount = 0;
                $product_source->save();

                if($product_dest->type_sn == 1){
                    $product_sn_source = ProductSN::where('product_id','=',$product_source->id)->get();
                    foreach ($product_sn_source as $p_sn_source){
                        $p_sn_source->product_id = $product_dest->id;
                        $p_sn_source->save();

                    }
                }else{


                    //$product_amount_dest = ProductAmount::where('product_id','=',$product_dest->id);
                    $product_amount_source = ProductAmount::where('product_id','=',$product_source->id)->get();

                    foreach ($product_amount_source as $p_amount_source){
                        $p_amount_dest = ProductAmount::where('product_id','=',$product_dest->id)->where('branch_id','=',$p_amount_source->branch_id);
                        if($p_amount_dest->exists()){
                            $p_amount_dest = $p_amount_dest->first();
                            $p_amount_dest->amount += $p_amount_source->amount;
                            $p_amount_dest->save();

                            $p_amount_source->amount = 0;
                            $p_amount_source->delete();
                        }else{
                            /*
                            $p_amount_dest = new ProductAmount();
                            $p_amount_dest->amount = $p_amount_source->amount;
                            $p_amount_dest->product_id = $product_dest->id;
                            $p_amount_dest->branch_id = $p_amount_source->branch_id;
                            $p_amount_dest->save();
                            */

                            $p_amount_source->product_id = $product_dest->id;
                            $p_amount_source->save();

                        }
                    }

                }

                $invoice_details = InvoiceDetail::where('product_id','=',$product_source->id)->get();
                foreach ($invoice_details as $inv_detail){
                    $inv_detail->product_id = $product_dest->id;
                    $inv_detail->save();
                }

                $stock_history = StockHistory::where('product_id','=',$product_source->id)->get();
                foreach ($stock_history as $st_history){
                    $st_history->product_id = $product_dest->id;
                    $st_history->save();
                }
            }
        });





    }
}
