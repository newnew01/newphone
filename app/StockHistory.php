<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class StockHistory extends Model
{
    protected $table = 'stock_history';

    public function productInfo()
    {
        return $this->belongsTo(Product::class,'product_id');
    }

    public function isAisDeal()
    {
        $ais_deal =  AisDeal::where('product_id','=',$this->product_id)->where('product_sn','=',$this->sn);
        if($ais_deal->exists())
            return true;
        else
            return false;
    }

    public function stockReference()
    {
        return $this->belongsTo(StockReference::class,'reference_id');
    }
}
