<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class UserBranch extends Model
{
    protected $table = 'user_branch';

    public function Branch()
    {
        return $this->belongsTo(Branch::class,'branch_id');
    }

}
