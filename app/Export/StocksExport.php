<?php

namespace App\Export;

use App\Product;
use App\StockReference;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\ShouldAutoSize;
use Maatwebsite\Excel\Concerns\WithColumnFormatting;
use Maatwebsite\Excel\Concerns\WithEvents;
use Maatwebsite\Excel\Events\AfterSheet;
use Maatwebsite\Excel\Classes;
use Maatwebsite\Excel\Sheet;
use PhpOffice\PhpSpreadsheet\Style\NumberFormat;

class StocksExport  implements FromCollection,ShouldAutoSize,WithEvents
{
    private $query;
    private $count;

    public function __construct($query)
    {
        $this->query = $query;
        $this->count = $query->count();
    }

    public function collection()
    {
        return $this->query;


    }


    public function registerEvents(): array
    {

        return [
            AfterSheet::class    => function(AfterSheet $event) {
                $cellRange = 'A1:F1'; // All headers
                $event->sheet->getDelegate()->getStyle($cellRange)->getFont()->setBold(true);

                $cellRange = 'A3:F3'; // All headers
                $event->sheet->getStyle($cellRange)->getFont()->setBold(true);
                $event->sheet->getDelegate()->getStyle("E1:E".$this->count)->getAlignment()->setHorizontal('left');


                $event->sheet->getStyle('A1')->getAlignment()->setWrapText(false);
            },
        ];
    }
/*
    public function columnFormats(): array
    {
        return [
            'E' => NumberFormat::FORMAT_TEXT,
        ];
    }
*/
}
