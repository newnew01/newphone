<?php

namespace App;

use bar\baz\source_with_namespace;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class Product extends Model
{
    protected $fillable = ['product_name','brand_id','model','category_id','description','amount','price','type_sn','barcode','image'];

    public function brand()
    {
        return $this->belongsTo(Brand::class);
    }

    public function category()
    {
        return $this->belongsTo(Category::class);
    }

    static public function isDuplicatedBarcode($barcode)
    {
        if(Product::where('barcode','=',$barcode)->exists())
            return true;
        else
            return false;
    }

    public function isInBranch($branch_id, $sn)
    {
        $psn = ProductSN::where('product_id','=',$this->id)->
            where('sn','=',$sn)->
            where('branch_id','=',$branch_id);

        return $psn->exists();
    }

    public function getAmount()
    {
        return $this->amount;

    }

    public function getAmountInBranch($branch_id)
    {
        if($this->type_sn == 1){
            $product_sn = ProductSN::where('product_id','=',$this->id)
                ->where('branch_id','=',$branch_id);

            if($product_sn->exists()){
                return $product_sn->count();
            }else{
                return 0;
            }
        }else{
            $product_amount = ProductAmount::where('product_id','=',$this->id)->
            where('branch_id','=',$branch_id);

            if($product_amount->exists()){
                return $product_amount->first()->amount;
            }else{
                return 0;
            }
        }

    }

    public function product_sn()
    {
        return $this->hasMany(ProductSN::class,'product_id');
    }
    public function product_amount()
    {
        return $this->hasMany(ProductAmount::class,'product_id');
    }

    static public function getAllProductInBranch($branch_id)
    {
        $x = Product::leftJoin('product_amount',function ($q) use ($branch_id){
            $q->on('product_amount.product_id', '=', 'products.id')
                ->where('product_amount.branch_id', '=', $branch_id);
            })
            ->select('products.id as id','products.product_name','products.model','products.brand_id','products.type_sn','products.model','products.category_id','products.description','products.amount as amount_all','products.price','products.cost','products.barcode','products.image',DB::raw('IFNULL(product_amount.amount, 0) as amount'),'products.amount as total_amount');

        return $x->get();
    }

    static public function getProductStock($product_id)
    {
        $product = Product::find($product_id);
        $branches = Branch::all();

        $product['brand_name'] = $product->brand->brand_name;

        if($product->type_sn == 1){


            $product_sn = $product->product_sn;
            foreach ($product_sn as $psn){
                $psn['amount'] = 1;
            }
            $data = [
                'product_info' => $product,
                'product_data' => $product_sn,
            ];

        }else{
            $product_amount = $product->product_amount;
            foreach ($product_amount as $p_amount){
                $p_amount['sn'] = '';
            }

            $data = [
                'product_info' => $product,
                'product_data' => $product_amount,
            ];
        }

        $data['branches'] = $branches;

        return $data;
    }

    public function getProductSN($branch_id=null)
    {
        $p_sn = ProductSN::where('product_id',$this->id);
        if($branch_id != null)
            $p_sn->where('branch_id',$branch_id);

        return $p_sn->get();
    }
}






