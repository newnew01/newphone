<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Milon\Barcode\DNS1D;

class StockReference extends Model
{
    protected $table = 'stock_reference';

    public function detail()
    {
        return $this->hasMany(StockHistory::class,'reference_id');
    }

    public function status()
    {
        return $this->belongsTo(Status::class,'status_id');
    }

    public function sourceBranch()
    {
        return $this->belongsTo(Branch::class,'source_branch');
    }

    public function destinationBranch()
    {
        return $this->belongsTo(Branch::class,'destination_branch');
    }

    public function type()
    {
        return $this->belongsTo(StockRefType::class,'ref_type');
    }

    public function User()
    {
        return $this->belongsTo(User::class,'employee_id');
    }



    public function generateBarcode()
    {
        $ref_type = StockRefType::find($this->ref_type);
        $prefix = $ref_type->prefix;

        /*
        switch ($this->ref_type){
            case 1:
                $prefix = 'SIN';
                break;
            case 2:
                $prefix = 'STR';
                break;
            case 3:
                $prefix = 'SA';
                break;
            case 4:
                $prefix = 'SINR';
                break;
            case 5:
                $prefix = 'SOD';
                break;

        }
        */
        return DNS1D::getBarcodePNG($prefix.'-'.sprintf('%06d', $this->id), "C128");
    }

    public function getReferenceId()
    {
        $ref_type = StockRefType::find($this->ref_type);
        /*
        switch ($this->ref_type){
            case 1:
                $prefix = 'SIN';
                break;
            case 2:
                $prefix = 'STR';
                break;
            case 3:
                $prefix = 'SOUT';
                break;
            case 4:
                $prefix = 'SINR';
                break;
            case 5:
                $prefix = 'SOD';
                break;

        }
        */
        $prefix = $ref_type->prefix;

        return $prefix.'-'.sprintf('%06d',$this->id);
    }

    public function getDocumentRef()
    {
        $data = null;

        switch ($this->ref_type){
            case 3:
                $data = $this->belongsTo(Invoice::class,'document_ref');
                break;
            case 4:
                $data = $this->belongsTo(Invoice::class,'document_ref');
                break;

        }

        return $this->belongsTo(Invoice::class,'document_ref');
    }

    public function getDefectInfo()
    {
        return $this->hasOne(StockDefectInfo::class,'stock_ref_id');
    }

    static public function getStockList($branch_id=null,$latest=false)
    {
        if($branch_id!=null){
            $stock_ref = StockReference::where('source_branch','=',$branch_id)->orWhere('destination_branch','=',$branch_id);
            if($latest)
                $stock_ref = $stock_ref->latest();

            return $stock_ref->get();
        }else{
            if($latest)
                return StockReference::latest()->get();
            else
                return StockReference::get();
        }

    }

    static public function getStockTransferringList($branch_id=null,$latest=false)
    {
        if($branch_id!=null){
            $stock_ref = StockReference::where('status_id',3)->where('ref_type',2)->where(function ($query) use ($branch_id){
                $query->where('source_branch',$branch_id);
                $query->orWhere('destination_branch', $branch_id);
            });


        }else{
            $stock_ref = StockReference::where('status_id',3)->where('ref_type',2);

        }

        if($latest)
            $stock_ref = $stock_ref->latest();

        return $stock_ref->get();

    }
}
