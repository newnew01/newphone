<?php

namespace App;

use GuzzleHttp\Client;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Response;

class Wepay extends Model
{
    static public function getTopupOutput($transaction_id)
    {
        $username = env("WEPAY_USERNAME");
        $password = env("WEPAY_PASSWORD");


        $client = new Client();
        try {
            $response = $client->request('POST', 'https://www.wepay.in.th/client_api.json.php', [
                'form_params' => [
                    'username' => $username,
                    'password' => $password,
                    'type' => 'get_output',
                    'transaction_id' => $transaction_id,
                ]
            ]);

            $data = json_decode($response->getBody());

            return $response;


        } catch (\GuzzleHttp\Exception\RequestException $e) {
            $data = json_decode($e->getResponse()->getBody(true));
            return Response::json($data);
            //return json_decode($e->getResponse()->getBody(true));
        }
    }
}
