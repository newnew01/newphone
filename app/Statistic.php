<?php

namespace App;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

class Statistic extends Model
{
    static public function getThisMonthSale($branch_id=null)
    {
        $total_price = 0;

        $invoices = Invoice::whereBetween('created_at',array(Carbon::today()->startOfMonth(),Carbon::today()->endOfMonth()))
            ->where('status','=','1');

        if($branch_id != null)
            $invoices->where('branch_id','=',$branch_id);

        $invoices = $invoices->get();

        foreach ($invoices as $invoice){
            foreach ($invoice->detail as $inv_detail){
                if($inv_detail->free_gift == 0){
                    $total_price += ($inv_detail->price*$inv_detail->amount) - $inv_detail->discount;
                }
            }

        }

        return $total_price;
    }
    
    static public function getThisMonthInvoiceAmount($branch_id=null)
    {
        $invoice_amount = $invoices = Invoice::whereBetween('created_at',array(Carbon::today()->startOfMonth(),Carbon::today()->endOfMonth()))
            ->where('status','=','1');
        if($branch_id != null)
            $invoice_amount->where('branch_id','=',$branch_id);

        $invoice_amount = $invoices->count();

        return $invoice_amount;
    }

    static public function getThisMonthTopup($branch_id=null)
    {
        $total_cash = 0;
        $topup_logs = TopupLog::whereBetween('created_at',array(Carbon::today()->startOfMonth(),Carbon::today()->endOfMonth()))
            ->where('status','=','2');
        if($branch_id != null)
            $topup_logs->where('branch_id','=',$branch_id);

        $topup_data = $topup_logs->get();

        foreach ($topup_data as $topup){
            $total_cash += $topup->cash;
        }

        return $total_cash;
    }
    
    static public function getTodayInvoiceAmount($branch_id=null)
    {
        $invoice_amount = $invoices = Invoice::whereBetween('created_at',array(Carbon::today()->startOfDay(),Carbon::today()->endOfDay()));
            //->where('status','=','1');
        if($branch_id != null)
            $invoice_amount->where('branch_id','=',$branch_id);

        $invoice_amount = $invoices->count();

        return $invoice_amount;
    }
    
    static public function getTodaySale($branch_id=null)
    {
        $total_price = 0;

        $invoices = Invoice::whereBetween('created_at',array(Carbon::today()->startOfDay(),Carbon::today()->endOfDay()))
            ->where('status','=','1');
        if($branch_id != null)
            $invoices->where('branch_id','=',$branch_id);

        $invoices = $invoices->get();

        foreach ($invoices as $invoice){
            foreach ($invoice->detail as $inv_detail){
                if($inv_detail->free_gift == 0){
                    $total_price += ($inv_detail->price*$inv_detail->amount) - $inv_detail->discount;
                }
            }

        }

        return $total_price;
    }
    
    static public function getTodayTopup($branch_id=null)
    {
        $total_cash = 0;
        $topup_logs = TopupLog::whereBetween('created_at',array(Carbon::today()->startOfDay(),Carbon::today()->endOfDay()))
            ->where('status','=','2');
        if($branch_id != null)
            $topup_logs->where('branch_id','=',$branch_id);

        $topup_data = $topup_logs->get();

        foreach ($topup_data as $topup){
            $total_cash += $topup->cash;
        }

        return $total_cash;
    }

    static public function getThisMonthStatistic($branch_id=null)
    {
        $data = [];
        $current_date = Carbon::today()->startOfMonth();

        while ($current_date->copy()->endOfDay()<Carbon::today()->endOfDay()){
            $date_ = $current_date->format('Y-m-d');
            $total_price = 0;
            $invoice_count = 0;
            $product_sale_amount = 0;

            $invoices = Invoice::whereBetween('created_at',array($current_date->startOfDay(),$current_date->copy()->endOfDay()))
                ->where('status','=','1');
            if($branch_id != null)
                $invoices->where('branch_id','=',$branch_id);

            $invoice_count = $invoices->count();
            $invoices = $invoices->get();

            foreach ($invoices as $invoice){
                foreach ($invoice->detail as $inv_detail){
                    if($inv_detail->free_gift == 0){
                        $total_price += ($inv_detail->price*$inv_detail->amount) - $inv_detail->discount;

                        if($inv_detail->product_id != null)
                            $product_sale_amount += $inv_detail->amount;
                    }
                }

            }

            array_push($data,[
                'date' => $date_,
                'total_price' => $total_price,
                'invoice_count' => $invoice_count,
                'product_sale_amount' => $product_sale_amount,
            ]);

            //echo $current_date."n";
            $current_date->addDay(1);
        }

        return $data;
        //Carbon::today()->startOfMonth()->addDay(1);
    }

    static public function getTodayMpay($branch_id=null)
    {
        $total_cash = 0;
        $mpay_log = mPayLog::whereBetween('transaction_datetime',array(Carbon::today()->startOfDay(),Carbon::today()->endOfDay()))
            ->where('type','<>','Cash-in (Bank A/C)');

        if($branch_id != null)
            $mpay_log->where('branch_id','=',$branch_id);

        $mpay_data = $mpay_log->get();

        foreach ($mpay_data as $mpay){
            $total_cash += ceil($mpay->price);
        }

        return $total_cash;
    }

    static public function getTodayMpayBillAmount($branch_id=null)
    {
        $total_cash = 0;
        $mpay_log = mPayLog::whereBetween('transaction_datetime',array(Carbon::today()->startOfDay(),Carbon::today()->endOfDay()))
            ->where('type','<>','Cash-in (Bank A/C)');

        if($branch_id != null)
            $mpay_log->where('branch_id','=',$branch_id);

        $mpay_bill_amount = $mpay_log->count();


        return $mpay_bill_amount;
    }

}
