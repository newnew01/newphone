<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Milon\Barcode\DNS1D;

class Invoice extends Model
{
    public function detail()
    {
        return $this->hasMany(InvoiceDetail::class,'invoice_id');
    }

    public function branch()
    {
        return $this->belongsTo(Branch::class,'branch_id');
    }

    public function User()
    {
        return $this->belongsTo(User::class,'employee_id');
    }

    public function getTotalPrice()
    {
        //$invoice_detail = InvoiceDetail::where('invoice_id','=',$this->id)->get();
        $invoice_detail = $this->hasMany(InvoiceDetail::class,'invoice_id')->get();
        $total_price = 0;
        foreach ($invoice_detail as $inv_detail){
            if(!$inv_detail->free_gift)
                $total_price += $inv_detail->getTotalPrice();
        }

        return $total_price;
    }

    public function getStatus()
    {
        return $this->belongsTo(Status::class,'status');
    }

    public function getInvoiceCancelReason()
    {
        return $this->hasOne(InvoiceCancelReason::class,'invoice_id');
    }

    public function getTotalPriceWithoutDiscount()
    {
        //$invoice_detail = InvoiceDetail::where('invoice_id','=',$this->id)->get();
        $invoice_detail = $this->hasMany(InvoiceDetail::class,'invoice_id')->get();
        $total_price = 0;
        foreach ($invoice_detail as $inv_detail){
            if(!$inv_detail->free_gift)
                $total_price += $inv_detail->getTotalPriceWithoutDiscount();
        }

        return $total_price;
    }

    public function generateBarcode()
    {
        return DNS1D::getBarcodePNG('INV-'.sprintf('%06d', $this->id), "C128");
    }


    public function getReferenceId()
    {

        return 'INV-'.sprintf('%06d',$this->id);
    }

    public function getDistrictName()
    {
        if($this->customer_tumbol == null) return null;

        $district = District::find($this->customer_tumbol);

        if($district->province_id == 1){
            return 'แขวง'.$district->district_name;
        }else{
            return 'ต.'.$district->district_name;
        }
    }

    public function getAmpherName()
    {

        if($this->customer_ampher == null) return null;

        $ampher = Ampher::find($this->customer_ampher);

        if($ampher->province_id == 1){
            return ''.$ampher->ampher_name;
        }else{
            return 'อ.'.$ampher->ampher_name;
        }
    }

    public function getProviceName()
    {
        if($this->customer_province == null) return null;

        $province = Province::find($this->customer_province);

        if($province->id == 1){
            return $province->province_name;
        }else{
            return 'จ.'.$province->province_name;
        }
    }

    public function getEmployeeName()
    {
        $user = User::find($this->employee_id);

        return $user->name;
    }
}
