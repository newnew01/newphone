<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class Stock extends Model
{
    const REF_TYPE_STOCKIN = 1;
    const REF_TYPE_STOCKTRANSFER = 2;
    const REF_TYPE_SALE = 3;
    const REF_TYPE_CANCEL_INV = 4;
    const REF_TYPE_DEFECT =5;

    public $branch_id;
    public $dest_branch_id;
    public $emp_id;
    public $stock_ref_id;
    public $doc_ref_id;

    public $error_msg;
    public $transaction_result = false;

    private $products=[];


    protected function duplicatedSN($product){
        if($product['product_type_sn'] == 1){
            $p = ProductSN::where('product_id','=',$product['product_id'])->where('sn','=',$product['product_sn']);
            if($p->exists())
                return true;
            else
                return false;
        }
        return false;
    }

    protected function checkDuplicatedSN($product_id,$product_sn){
        $p = ProductSN::where('product_id','=',$product_id)->where('sn','=',$product_sn);
        if($p->exists())
            return true;
        else
            return false;
    }

    public function addProduct($product_id,$product_amount=1,$product_type_sn,$product_sn=null){
        if($product_type_sn == 1)
            $product_amount = 1;
        $p = [
            'product_id' => $product_id,
            'product_amount' => $product_amount,
            'product_type_sn' => $product_type_sn,
            'product_sn' => $product_sn,
        ];
        array_push($this->products,$p);
    }

    public function saveToStock(){
        DB::transaction(function() {
            $this->stock_ref_id = $this->createStockInReference($this->branch_id,$this->emp_id);
            for($i=0;$i<count($this->products);$i++){
                if($this->products[$i]['product_type_sn'] == 1){
                    if(!$this->duplicatedSN($this->products[$i])){
                        $this->addItem( $this->products[$i]['product_id'],$this->branch_id, $this->products[$i]['product_amount'], $this->products[$i]['product_type_sn'], $this->products[$i]['product_sn']);
                    }else{
                        $this->transaction_result = false;
                        $this->error_msg = 'สินค้าซ้ำในระบบ IMIE/SN : '.$this->products[$i]['product_sn'] . ' ในระบบหรือในสาขา';
                        DB::rollBack();
                        return false;
                    }


                }else{
                    $this->addItem( $this->products[$i]['product_id'],$this->branch_id, $this->products[$i]['product_amount'], false);
                }


                //$this->addTotalAmount($this->products[$i]['product_id'],$this->products[$i]['product_amount']);

                $this->createStockHistory($this->stock_ref_id,$this->products[$i]['product_id'],$this->products[$i]['product_amount'],$this->products[$i]['product_type_sn'],$this->products[$i]['product_sn'],1);
            }
            $this->transaction_result = true;
        });


        return $this->transaction_result;


    }

    public function stockOutDefect($defect_info)
    {

        DB::transaction(function() use ($defect_info){
            $this->stock_ref_id = self::createStockReference(self::REF_TYPE_DEFECT,$this->emp_id,$this->branch_id,$this->branch_id,$this->doc_ref_id,5);
            for($i=0;$i<count($this->products);$i++){
                $this->removeItem($this->products[$i]['product_id'],$this->branch_id,$this->products[$i]['product_amount'],$this->products[$i]['product_type_sn'],$this->products[$i]['product_sn']);

                $this->createStockHistory($this->stock_ref_id,$this->products[$i]['product_id'],$this->products[$i]['product_amount'],$this->products[$i]['product_type_sn'],$this->products[$i]['product_sn'],1);

            }

            $st_defect_info = new StockDefectInfo();
            $st_defect_info->stock_ref_id = $this->stock_ref_id;
            $st_defect_info->info = $defect_info;
            $st_defect_info->save();

            $this->transaction_result = true;
        });

        return $this->transaction_result;
    }



    public function returnStockCancelInvoice($invoice_id){
        DB::transaction(function() use ($invoice_id) {
            $invoice = Invoice::find($invoice_id);
            $branch_id = $invoice->branch_id;

            $invoice_details = InvoiceDetail::where('invoice_id','=',$invoice_id);
            $invoice_details = $invoice_details->get();

            $noProduct = true;
            foreach ($invoice_details as $inv_detail) {
                if($inv_detail->product_id != null)
                    $noProduct = false;
            }
            if(!$noProduct){
                $this->stock_ref_id = self::createStockReference(self::REF_TYPE_CANCEL_INV,$this->emp_id,$branch_id,$branch_id,$invoice_id);
                foreach ($invoice_details as $inv_detail){
                    if($inv_detail->product_id == null)
                        continue;

                    $type_sn = $inv_detail->productInfo->type_sn;
                    if($type_sn == 1){
                        if(!$this->checkDuplicatedSN($inv_detail->product_id,$inv_detail->sn)){
                            $this->addItem($inv_detail->product_id,$branch_id, $inv_detail->amount, $type_sn, $inv_detail->sn);
                        }else{
                            $this->transaction_result = false;
                            $this->error_msg = 'สินค้าซ้ำในระบบ IMIE/SN : '.$inv_detail->sn . ' ในระบบหรือในสาขา';
                            DB::rollBack();
                            return false;
                        }


                    }else{
                        $this->addItem($inv_detail->product_id,$branch_id, $inv_detail->amount, false);
                    }


                    //$this->addTotalAmount($this->products[$i]['product_id'],$this->products[$i]['product_amount']);

                    $this->createStockHistory($this->stock_ref_id,$inv_detail->product_id,$inv_detail->amount,$type_sn,$inv_detail->sn,1);
                }

            }




            $this->transaction_result = true;
        });


        return $this->transaction_result;


    }

    public function confirmTranferStock($stock_ref_id){

        DB::transaction(function() use ($stock_ref_id){

            $stock_ref = StockReference::find($stock_ref_id);
            if($stock_ref->status_id == 3) {
                foreach ($stock_ref->detail as $stock_history){
                    $type_sn = $stock_history->productInfo->type_sn;
                    if($type_sn == 1){
                        if(!$this->checkDuplicatedSN($stock_history->product_id,$stock_history->sn)){
                            $this->addItem($stock_history->product_id,$stock_ref->destination_branch, $stock_history->amount, $type_sn, $stock_history->sn,false);
                            $this->removeItemTransfer($stock_history->product_id,$stock_ref->source_branch,$stock_ref->destination_branch,$stock_history->amount,$type_sn,$stock_history->sn);
                        }else{
                            $this->transaction_result = false;
                            $this->error_msg = 'สินค้าซ้ำในระบบ IMIE/SN : '.$stock_history->sn . ' ในระบบหรือในสาขา';
                            DB::rollBack();
                            return false;
                        }


                    }else{
                        //dd($stock_history->amount);
                        $this->removeItemTransfer($stock_history->product_id,$stock_ref->source_branch,$stock_ref->destination_branch,$stock_history->amount,0);
                        $this->addItem($stock_history->product_id,$stock_ref->destination_branch, $stock_history->amount, false,null,false);

                    }

                }

                $stock_ref->status_id = 1;
                $stock_ref->save();

                $this->transaction_result = true;
            }

        });


        return $this->transaction_result;


    }

    public function transferStock($dest_branch_id=null,$wait_confirm=true)
    {
        if($dest_branch_id != null) $this->dest_branch_id = $dest_branch_id;

        DB::transaction(function() use ($wait_confirm) {
            if($wait_confirm)
                $this->stock_ref_id = $this->createStockTransferReference($this->branch_id,$this->dest_branch_id,$this->emp_id,3);
            else
                $this->stock_ref_id = $this->createStockTransferReference($this->branch_id,$this->dest_branch_id,$this->emp_id);

            for($i=0;$i<count($this->products);$i++){


                if($wait_confirm){
                    $this->removeItem($this->products[$i]['product_id'],$this->branch_id,$this->products[$i]['product_amount'],$this->products[$i]['product_type_sn'],$this->products[$i]['product_sn'],false);
                    $this->createItemTransfer($this->products[$i]['product_id'],$this->branch_id,$this->dest_branch_id,$this->products[$i]['product_amount'],$this->products[$i]['product_type_sn'],$this->products[$i]['product_sn']);
                    $this->createStockHistory($this->stock_ref_id,$this->products[$i]['product_id'],$this->products[$i]['product_amount'],$this->products[$i]['product_type_sn'],$this->products[$i]['product_sn'],3);
                }else{
                    $result = $this->transferItem($this->products[$i]['product_id'],$this->branch_id,$this->dest_branch_id,$this->products[$i]['product_amount'],$this->products[$i]['product_type_sn'],$this->products[$i]['product_sn']);

                    if($result == 1){
                        $this->createStockHistory($this->stock_ref_id,$this->products[$i]['product_id'],$this->products[$i]['product_amount'],$this->products[$i]['product_type_sn'],$this->products[$i]['product_sn'],1);
                    }
                    elseif ($result == 2){
                        $this->transaction_result = false;
                        $this->error_msg = 'ไม่พบ IMIE/SN : '.$this->products[$i]['product_sn'] . ' ในระบบหรือในสาขา';
                        DB::rollBack();
                        return false;
                    }elseif ($result == 3){
                        $this->transaction_result = false;
                        $this->error_msg = 'สินค้าในสาขาต้นทางมีไม่เพียงพอ , รหัสสินค้า : '.sprintf('%06d',$this->products[$i]['product_id']);
                        DB::rollBack();
                        return false;
                    }elseif ($result == 4){
                        $this->transaction_result = false;
                        $this->error_msg = 'ไม่มีสินค้าในระบบ , รหัสสินค้า : '.sprintf('%06d',$this->products[$i]['product_id']);
                        DB::rollBack();
                        return false;
                    }
                }


            }
            $this->transaction_result = true;
        });


        return $this->transaction_result;
    }

    public function saleStock($doc_ref_id=null)
    {
        if($doc_ref_id != null) $this->doc_ref_id = $doc_ref_id;

        DB::transaction(function() {
            if(count($this->products) > 0){
                $this->stock_ref_id = self::createStockReference(self::REF_TYPE_SALE,$this->emp_id,$this->branch_id,$this->branch_id,$this->doc_ref_id);
                for($i=0;$i<count($this->products);$i++){
                    $this->removeItem($this->products[$i]['product_id'],$this->branch_id,$this->products[$i]['product_amount'],$this->products[$i]['product_type_sn'],$this->products[$i]['product_sn']);

                    $this->createStockHistory($this->stock_ref_id,$this->products[$i]['product_id'],$this->products[$i]['product_amount'],$this->products[$i]['product_type_sn'],$this->products[$i]['product_sn'],1);

                }
            }

            $this->transaction_result = true;
        });

        return $this->transaction_result;
    }

    public function applySale(Sale $sale)
    {
        $this->products = [];
        $sale_products = $sale->getProducts();
        foreach ($sale_products as $product){
            if($product['item_type'] == 1)
                $this->addProduct($product['product_id'],$product['product_amount'],$product['product_type_sn'],$product['product_sn']);
        }

        return $this->saleStock($sale->invoice_id);
    }

    public static function createStockHistory($ref_id,$product_id,$amount=1,$type_sn=0,$sn=null,$status=1)
    {
        $stock_history = new StockHistory();
        $stock_history->product_id = $product_id;
        //$stock_history->ais_deal = $ais_deal;
        $stock_history->reference_id = $ref_id;
        $stock_history->status = $status;

        if($type_sn == 1){
            $stock_history->sn = $sn;
            $stock_history->amount = 1;
        }else{
            $stock_history->amount = $amount;
        }

        $stock_history->save();

        return $stock_history->id;
    }

    public static function createStockReference($ref_type,$emp_id,$source_branch_id,$dest_branch_id,$doc_ref = null,$status_id = 1)
    {
        $stock_ref = new StockReference();
        $stock_ref->employee_id = $emp_id;
        $stock_ref->source_branch = $source_branch_id;
        $stock_ref->destination_branch = $dest_branch_id;
        $stock_ref->ref_type = $ref_type;
        $stock_ref->document_ref = $doc_ref;
        $stock_ref->status_id = $status_id;
        $stock_ref->save();

        return $stock_ref->id;
    }

    public static function createStockInReference($branch_id,$emp_id)
    {
        return self::createStockReference(self::REF_TYPE_STOCKIN,$emp_id,$branch_id,$branch_id);
    }

    public static function createStockTransferReference($source_branch,$dest_branch,$emp_id,$status=1)
    {
        return self::createStockReference(self::REF_TYPE_STOCKTRANSFER,$emp_id,$source_branch,$dest_branch,null,$status);
    }



    public static function createStockHistoryWithSN($ref_id, $product_id, $sn)
    {
        return self::createStockHistory($ref_id,$product_id,1,1,$sn);
    }

    public function addItem($product_id,$branch_id,$amount,$type_sn,$sn=null,$addTotalAmount=true)
    {
        if($type_sn == 1) $amount = 1;

        if($type_sn == 1){
            $productsn = new ProductSN();
            $productsn->product_id = $product_id;
            $productsn->sn = $sn;
            $productsn->branch_id = $branch_id;
            $productsn->save();
        }
        $this->addProductAmount($product_id,$branch_id,$amount);

        if($addTotalAmount)
            $this->addTotalAmount($product_id,$amount);

    }

    public function removeItem($product_id,$branch_id,$amount,$type_sn,$sn=null,$subTotalAmount=true)
    {
        //return code
        // 1 = success
        // 2 = no ProductSN record found
        if($type_sn == 1) $amount = 1;

        if($type_sn == 1){
            $productsn = ProductSN::where('product_id','=',$product_id)->where('branch_id','=',$branch_id)->where('sn','=',$sn);
            if($productsn->exists()){
                $productsn = $productsn->first();
                $productsn->delete();
            }else{
                //no ProductSN record found
                return 2;
            }
        }

        $this->subProductAmount($product_id,$branch_id,$amount);

        if($subTotalAmount)
            $this->subTotalAmount($product_id,$amount);

        return 1;

    }

    public function createItemTransfer($product_id,$source_branch_id,$dest_branch_id,$amount,$type_sn,$sn=null)
    {
        if($type_sn == 1){
            $product_transfer = new ProductTransfer();
            $product_transfer->product_id = $product_id;
            $product_transfer->source_branch_id = $source_branch_id;
            $product_transfer->dest_branch_id = $dest_branch_id;
            $product_transfer->amount = 1;
            $product_transfer->type_sn = $type_sn;
            $product_transfer->sn = $sn;
            $product_transfer->save();
        }else{
            $product_transfer = ProductTransfer::where('product_id',$product_id)
                ->where('source_branch_id',$source_branch_id)
                ->where('dest_branch_id',$dest_branch_id);

            if($product_transfer->exists()){
                $product_transfer = $product_transfer->first();
                $product_transfer->amount += $amount;
                $product_transfer->save();
            }else{
                $product_transfer = new ProductTransfer();
                $product_transfer->product_id = $product_id;
                $product_transfer->source_branch_id = $source_branch_id;
                $product_transfer->dest_branch_id = $dest_branch_id;
                $product_transfer->amount = $amount;
                $product_transfer->type_sn = $type_sn;
                $product_transfer->sn = $sn;
                $product_transfer->save();
            }
        }

    }

    public function removeItemTransfer($product_id,$source_branch_id,$dest_branch_id,$amount,$type_sn,$sn=null)
    {
        if($type_sn == 1){
            $product_transfer = ProductTransfer::where('product_id',$product_id)
                ->where('source_branch_id',$source_branch_id)
                ->where('dest_branch_id',$dest_branch_id)
                ->where('sn',$sn);

            if($product_transfer->exists()){
                $product_transfer = $product_transfer->first();
                $product_transfer->delete();
            }

        }else{
            $product_transfer = ProductTransfer::where('product_id',$product_id)
                ->where('source_branch_id',$source_branch_id)
                ->where('dest_branch_id',$dest_branch_id);

            if($product_transfer->exists()){
                $product_transfer = $product_transfer->first();
                if(($product_transfer->amount-$amount) <= 0){
                    $product_transfer->delete();
                }else{
                    $product_transfer->amount -= $amount;
                    $product_transfer->save();
                }
            }
        }

    }

    public function addProductAmount($product_id,$branch_id,$amount)
    {

        $q2 = ProductAmount::where('product_id','=',$product_id)->where('branch_id','=',$branch_id);
        if($q2->exists()) {
            $product_amount_dest = $q2->first();
            $product_amount_dest->amount += $amount;
            $product_amount_dest->save();
        }else{
            $product_amount_dest = new ProductAmount();
            $product_amount_dest->amount = $amount;
            $product_amount_dest->branch_id = $branch_id;
            $product_amount_dest->product_id = $product_id;
            $product_amount_dest->save();
        }
    }

    public function subProductAmount($product_id,$branch_id,$amount)
    {

        $q2 = ProductAmount::where('product_id','=',$product_id)->where('branch_id','=',$branch_id);
        if($q2->exists()) {
            $product_amount_dest = $q2->first();
            $product_amount_dest->amount -= $amount;
            $product_amount_dest->save();
        }else{
            $product_amount_dest = new ProductAmount();
            $product_amount_dest->amount = -$amount;
            $product_amount_dest->branch_id = $branch_id;
            $product_amount_dest->product_id = $product_id;
            $product_amount_dest->save();
        }
    }

    public function addTotalAmount($product_id, $amount)
    {
        $product = Product::find($product_id);
        $product->amount += $amount;
        $product->save();
    }

    public function subTotalAmount($product_id, $amount)
    {
        $product = Product::find($product_id);
        $product->amount -= $amount;
        $product->save();
    }


    public function transferItem($product_id, $source_branch_id,$dest_branch_id, $amount, $type_sn, $sn = null)
    {
        //return code
        // 1 = success
        // 2 = error SN not found in branch or in the system
        // 3 = source ProductAmount is insufficient
        // 4 = no source ProductAmount record


        if($type_sn == 1) $amount = 1;

        if($type_sn == 1){
            $productsn = ProductSN::where('product_id','=',$product_id)->where('branch_id','=',$source_branch_id)->where('sn','=',$sn);
            if($productsn->exists()){
                $productsn = $productsn->first();
                $productsn->branch_id = $dest_branch_id;
                $productsn->save();
            }else{
                //SN not found
                return 2;
            }
        }

        $q = ProductAmount::where('product_id','=',$product_id)->where('branch_id','=',$source_branch_id);

        if($q->exists()){
            $product_amount_source = $q->first();

            if($product_amount_source->amount >= $amount){
                $product_amount_source->amount -= $amount;
                $product_amount_source->save();

                $q2 = ProductAmount::where('product_id','=',$product_id)->where('branch_id','=',$dest_branch_id);
                if($q2->exists()) {
                    $product_amount_dest = $q2->first();
                    $product_amount_dest->amount += $amount;
                    $product_amount_dest->save();
                }else{
                    $product_amount_dest = new ProductAmount();
                    $product_amount_dest->amount = $amount;
                    $product_amount_dest->branch_id = $dest_branch_id;
                    $product_amount_dest->product_id = $product_id;
                    $product_amount_dest->save();
                }
            }else{
                //source ProductAmount is insufficient
                return 3;
            }
        }else{
            //no source ProductAmount record
            return 4;
        }

        return 1;
    }

    public function setProducts($products)
    {
        $this->products = $products;
    }
}
