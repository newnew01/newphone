<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class InvoiceCancelReason extends Model
{
    protected $table = 'invoice_cancel_reason';
}
