<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class District extends Model
{
    public function zipcode(){
        return $this->hasOne(Zipcode::class,'district_code','district_code');
    }

    public function ampher()
    {
        return $this->belongsTo(Ampher::class,'ampher_id');
    }

    public function province()
    {
        return $this->belongsTo(Province::class,'province_id');
    }

    public function geography()
    {
        return $this->belongsTo(Geography::class,'geo_id');
    }
}
