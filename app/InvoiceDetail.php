<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class InvoiceDetail extends Model
{
    protected $table="invoice_detail";

    public function productInfo()
    {
        return $this->belongsTo(Product::class,'product_id');
    }

    public function getTotalPrice()
    {
        return ($this->amount * $this->price)-$this->discount;
    }

    public function getTotalPriceWithoutDiscount()
    {
        return ($this->amount * $this->price);
    }

    public function serviceInfo()
    {
        return $this->belongsTo(Service::class,'service_id');
    }

    public function invoice()
    {
        return $this->belongsTo(Invoice::class,'invoice_id');
    }

}
