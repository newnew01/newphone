<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class StockDefectInfo extends Model
{
    protected $table = 'stock_defect_info';
}
