<?php
header('Access-Control-Allow-Origin: *');
header('Access-Control-Allow-Headers: Origin, X-Requested-With, Content-Type, Accept');

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

Route::post('/wepay/balance', 'WepayController@getBalance');
Route::post('/wepay/refund', 'WepayController@refund');
Route::post('/wepay/topup', 'WepayController@topup');
Route::post('/wepay/get-operator', 'WepayController@getOperator');
Route::post('/wepay/topup-status', 'WepayController@getTopupStatus');
Route::post('/wepay/get-output', 'WepayController@getTopupOutput');

Route::post('/log/today/','TopupLogController@getTodayLog');
Route::post('/log/today/report','TopupLogController@getTodayReport');
Route::post('/log/today/report-total','TopupLogController@getTodayReportTotal');
Route::post('/log/monthly/report','TopupLogController@getMonthlyReport');
Route::post('/log/monthly/report-total','TopupLogController@getMonthlyReportTotal');
Route::post('/log/entire/report-total','TopupLogController@getEntireReportTotal');
Route::post('/log/entire/report','TopupLogController@getEntireReport');

Route::post('/wepay/callback/topup', 'WepayController@callbackTopup');
Route::post('/wepay/callback/refund',  'WepayController@callbackRefund');

Route::post('/mpay/log/add',  'mPayLogController@addLog');

//test
Route::get('/wepay/balance', 'WepayController@getBalance');