<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

use App\Product;
use Carbon\Carbon;
use Intervention\Image\Facades\Image;
use Milon\Barcode\DNS1D;

Route::get('/', function () {
    return redirect('/login');
});


Route::group(['middleware' => 'auth'], function () {

    Route::get('/dashboard','DashboardController@view_index');
    Route::get('/home','DashboardController@view_index');
    Route::get('/web-service/get-dashboard-statistic','WebServiceController@getDashboardChartData');
    Route::get('/web-service/get-dashboard-statistic/{branch_id}','WebServiceController@getDashboardChartData');

    Route::get('/web-service/user/change-branch/{branch_id}','WebServiceController@changeUserBranch');




    Route::get('/product/list','ProductController@view_productList');


    Route::group(['middleware' => ['permission:edit-product']], function () {
        Route::get('/product/edit/{id}','ProductController@view_editProduct');
        Route::post('/product/edit/{id}','ProductController@editProduct');
    });

    Route::group(['middleware' => ['permission:delete-product']], function () {
        Route::get('/product/delete/{id}','ProductController@delete');
    });


    Route::group(['middleware' => ['permission:create-product']], function () {
        Route::get('/product/new','ProductController@view_newProduct');
        Route::post('/product/new','ProductController@newProduct');
    });


    Route::get('/product-catebrand','ProductCateBrandController@index');

    Route::post('/brand/new','BrandController@newBrand');
    Route::post('/category/new','CategoryController@newCategory');
    Route::get('/brand/delete/{id}','BrandController@deleteBrand');
    Route::get('/category/delete/{id}','CategoryController@deleteCategory');


    Route::group(['middleware' => ['permission:stock-in']], function () {
        Route::get('/stock/in','StockController@view_stockIn');
        Route::post('/stock/in','StockController@stockIn');


    });

    Route::group(['middleware' => ['permission:stock-transfer-confirm']], function () {
        Route::get('/stock/confirm-transfer/{stock_ref_id}','StockController@confirmTransfer');
    });

    Route::group(['middleware' => ['permission:stock-transfer']], function () {
        Route::get('/stock/transfer','StockController@view_stockTransfer');
        Route::post('/stock/transfer','StockController@stockTransfer');
    });








    Route::get('/stock/list','StockController@view_stockList');
    Route::get('/stock/list/{reference_id}','StockController@view_stockReference');

    Route::group(['middleware' => ['permission:stock-defect']], function () {
        Route::get('/stock/defect','StockController@view_stockDefect');
        Route::post('/stock/defect','StockController@stockOutDefect');
    });


    Route::get('/user/list','UserController@view_userList');

    Route::group(['middleware' => ['permission:create-user']], function () {
        Route::get('/user/new','UserController@view_userNew');
        Route::post('/user/new','UserController@newUser');
    });

    Route::group(['middleware' => ['permission:edit-user']], function () {
        Route::get('/user/edit/{id}','UserController@view_editUser');
        Route::post('/user/edit/{id}','UserController@editUser');
    });

    Route::group(['middleware' => ['permission:set-user-permission']], function () {
        Route::get('/user/permission/{id}','UserController@view_Permission');
        Route::post('/user/permission/{id}','UserController@setPermission');
    });

    //Route::get('/user/info/{id}','UserController@view_userDetail');





    Route::get('/barcode/custom','BarcodeController@view_Custom');

    Route::post('/barcode/print','BarcodeController@view_Print');
    Route::get('/barcode/print-service','BarcodeController@view_PrintService');
    Route::post('/barcode/print-service','BarcodeController@addPrintService');

    Route::group(['middleware' => ['permission:print-label']], function () {
        Route::get('/barcode/print-label','BarcodeController@view_PrintLabel');
    });

    Route::get('/web-service/magic-barcode/search-sn/{sn}','WebServiceController@searchSNbyMagicBarcode');
    Route::get('/web-service/magic-barcode/search-barcode/{barcode}','WebServiceController@searchBarcodeByMagicBarcode');

    Route::get('/web-service/label/get-print-services','WebServiceController@getPrinServices');

    Route::get('/web-service/stock/get-doc-prefix','StockController@getAllDocPrefix');

    Route::get('/web-service/thai-location/search-by-district/{district_name}','WebServiceController@getThaiLocationBydistrict');

    Route::get('/sale/list', 'SaleController@view_orderList');
    Route::get('/sale/neworder','SaleController@view_newOrder');
    Route::get('/sale/invoice/{id}','SaleController@view_invoice');
    Route::get('/web-service/get-services','WebServiceController@getAllServices');

    Route::get('/web-service/get-zipcode-by-name/{province_name},{ampher_name},{district_name}','WebServiceController@getThaiLocationByName');


    Route::group(['middleware' => ['permission:cancel-invoice']], function () {
        Route::post('/sale/cancel-invoice/{invoice_id}','SaleController@cancelInvoice');
    });

    Route::post('/sale/neworder','SaleController@newOrder');

    Route::get('/ais/new-ais-deal','AisController@view_newAisDeal');
    Route::post('/ais/new-ais-deal','AisController@addAisDeal');

    Route::get('/report/daily','ReportController@view_dailyReport');

    Route::group(['middleware' => ['permission:export-stock']], function () {
        Route::get('/report/stock','ReportStockController@view_reportStock');
        Route::post('/report/stock','ReportStockController@exportStock');
    });

    Route::get('/service-product/find-by-id/{id}','ServiceProductController@findProductById');
    Route::get('/service-product/find-by-barcode/{barcode}','ServiceProductController@findProductByBarcode');
    Route::get('/service-product/find-productsn-by-id-sn/{id},{sn}','ServiceProductController@findProductSNbyIdSN');
    Route::get('/service-product/gen-barcode','ServiceProductController@getGenBarcode');
    Route::get('/service-product/check-duplicated-sn/{id},{sn}','ServiceProductController@checkDuplicatedSN');
    Route::get('/service-product/find-productsn-in-branch/{id},{sn},{branch_id}','ServiceProductController@findProductSnInBranch');
    Route::get('/service-product/find-product-amount-in-branch/{id},{branch_id}','ServiceProductController@findProductAmountInBranch');
    Route::get('/service-product/search-product/{keyword}','ServiceProductController@searchProduct');
    Route::get('/service-product/get-product-stock/{product_id}','ServiceProductController@getProductStock');
    Route::get('/service-product/get-product-transferring/{product_id}','ServiceProductController@findTransferringProduct');

    Route::get('/heart-beat',function (){
        return 1;
    });

});



Route::get('/test-session',function (){
    $x['title'] = 'my title';
    $x['text'] = 'my text';
    \Session::flash('flash_test',$x);
    dd(session('flash_test'));
});


Route::get('/test_image', function()
{
    $img = Image::make('foo.jpg')->resize(300, 200);

    return $img->response('jpg');
});

Route::get('/test_barcode', function()
{
    //echo DNS1D::getBarcodeHTML("4445645656", "PHARMA2T",3,33,"green", true);
    //echo '<img src="data:image/png;base64,' . DNS1D::getBarcodePNG("4", "C39+",3,33,array(1,1,1), true) . '" alt="barcode"   />';
    //return DNS1D::getBarcodeHTML("11113333000009", "C128",1,16);
    //echo '<img width="3cm" src="data:image/png;base64,' . DNS1D::getBarcodePNG("11113333000009", "C128") . '" alt="barcode"   />';
    //return '<img src="' . DNS1D::getBarcodePNG("4", "C39+",3,33,array(1,1,1)) . '" alt="barcode"   />';
    $barcode_number = '11113333000009';
    $barcode =  DNS1D::getBarcodePNG($barcode_number, "C128");

    return view('template.barcode-print')->with(compact('barcode'))->with(compact('barcode_number'));
});

Route::get('/test_invoice', function()
{

    $invoice = \App\Invoice::find(1);
    $ref_id_barcode = DNS1D::getBarcodePNG(sprintf('%06d', 1), "C128");

    return view('pages.sale-invoice-document')
        ->with(compact('invoice'))
        ->with(compact('ref_id_barcode'));
        //->with(['invoice_full_price' => 'on']);
});

Auth::routes();

//Route::get('/home', 'HomeController@index')->name('home');

Route::get('/phpinfo',function (){
    echo phpinfo();
});

Route::get('/test-decode/{data}',function ($data){
    $date_time = Carbon::now();
    $date_time_access = Carbon::createFromFormat('dmYHi', base64_decode($data));

    $lower_bound = $date_time_access->copy()->subMinute(2);
    $upper_bound = $date_time_access->copy()->addMinute(2);
    if($upper_bound > $date_time){
        echo $upper_bound.'ok'.$lower_bound;

    }else{
        echo 'not ok';
    }
});

Route::get('/test-page',function (){
    //return view('pages.test');

    $vivos = Product::where('category_id',1)->get();
    return view('pages.test')->with(compact('vivos'));
});


Route::get('/test-export','ReportStockController@export');