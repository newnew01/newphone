app.controller('PrintServiceController', function($scope,$http) {

    $scope.print_services = '';

    $scope.getPrintServices =  function () {
        $http.get('/web-service/label/get-print-services').then(function (response) {
            $scope.print_services = response.data;
            $scope.print_services.forEach($scope.checkService);
        });
    }

    $scope.checkService = function (item,index) {
        //alert(uri);
        //return true;
        $http.get(item.service_status_uri).then(function (response) {
            if(response.status == 200){
                data = response.data;
                //alert(data);
                //return true;
                if(data.printer_offline == "0"){
                    if(data.printer_error == "0"){
                        item.service_status = 1;
                    }
                    else {
                        item.service_status = 2;
                    }
                }else{
                    item.service_status = 0;
                }
            }else{
                item.service_status = 3;
            }

        },function (data) {
            item.service_status = 3;
        });
    }



    $scope.pad = function(num, size) {
        var s = num+"";
        while (s.length < size) s = "0" + s;
        return s;
    }

    $scope.number_format = function(num) {
        if(num == null) return '';
        return num.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
    };

    $scope.getPrintServices();
});