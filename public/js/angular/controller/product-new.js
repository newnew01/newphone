app.controller('ProductNewController', function($scope,$sce,$http) {
    $scope.product = {"image":"/assets/images/no-image.png"};
    $scope.barcode_ = '';

    $scope.run_barcode_checkbox = false;

    $scope.convertBarcodeInput = function (input) {
        var th_char = 'ๅ/-ภถุึคตจขชๆไำพะัีรนยบลฟหกดเ้่าสวงฃผปแอิืทมใฝ+๑๒๓๔ู฿๕๖๗๘๙๐"ฎฑธํ๊ณฯญฐ,ฤฆฏโฌ็๋ษศซ.ฅ()ฉฮฺ์?ฒฬฦ';
        var en_char = '1234567890-=qwertyuiop[]asdfghjkl;\'\\zxcvbnm,./1234567890-=QWERTYUIOP[]ASDFGHJKL;\'\\ZXCVBNM,./';


        var barcode = '';

        for(var i=0;i<input.length;i++){
            var converted_char = input.charAt(i);
            for(var x=0;x<th_char.length;x++){
                if(th_char.charAt(x) == input.charAt(i)){
                    converted_char =  en_char.charAt(x);
                    break;
                }
            }
            barcode +=  converted_char;
        }

        return barcode;
    }



    $scope.checkDuplicatedBarcode = function () {
        $scope.barcode_ = $scope.convertBarcodeInput($scope.barcode_);

        $http.get("/service-product/find-by-barcode/"+$scope.barcode_)
            .then(function(response) {
                if(response.data != 'none'){
                    $scope.barcode_ = '';
                    alert('ผิดพลาด!! บาร์โค้ดซ้ำในระบบหรือมีสินค้านี้อยู่แล้ว\n- '+response.data.product_name+' '+response.data.brand_name+' '+response.data.model);
                }
            });
    }

    $scope.submitData = function () {
        //alert($scope.price);
        var err_msg = '';
        if($scope.category_id == undefined || $scope.category_id == '')
            err_msg = 'กรุณาเลือกหมวดหมู่';
        else if($scope.brand_id == undefined || $scope.brand_id == '')
            err_msg = 'กรุณาเลือกยี่ห้อ';
        else if(($scope.barcode_ == undefined || $scope.barcode_ == '') && $scope.run_barcode_checkbox == false){
            err_msg = 'กรุณากรอกบาร์โค้ด หรือเลือก [รันบาร์โค้ดอัตโนมัติ] หากสินค้าไม่มีบาร์โค้ด';
        }
        else if($scope.product_name == undefined || $scope.product_name == '')
            err_msg = 'กรุณาใส่ชื่อสินค้า';

        else if($scope.price == undefined || $scope.price == '')
            err_msg = 'กรุณาใส่ราคา';

        if(err_msg == ''){
            document.getElementById("addProductForm").submit();
        }else {
            alert(err_msg);
        }

    }

    $scope.getGenBarcode = function () {
        $http.get("/service-product/gen-barcode/")
            .then(function(response) {
                $scope.barcode_ = response.data;

            });
    }

    $scope.startWebcam = function(){
        //alert();
        $scope.image_input = "";
        document.getElementById('webcam_input').innerHTML =
            '<div id="webcam"></div>' +
            '<button type="button" class="btn btn-block btn-lg btn-success" onclick="captureWebcam()" ><i class="mdi mdi-camera-iris"> จับภาพ</button>';
        //$scope.webcam_input = $sce.trustAsHtml('<div id="webcam"></div>' +
        //    '<button type="button" class="btn  waves-effect waves-light btn-rounded btn-info pull-right m-t-20" onclick="captureWebcam()" >จับภาพ</button>');
        $('#image_input').html('');

        setTimeout(function(){
            Webcam.set({
                width: 640,
                height: 480,
                image_format: 'jpeg',
                jpeg_quality: 90
            });
            Webcam.attach( '#webcam' );
        }, 1000)




    }



});

