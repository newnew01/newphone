app.controller('StockTransferController', function($scope,$sce,$http) {
    $scope.barcode_input = '';
    $scope.imei_sn_input = '';
    $scope.amount_barcode_input = '';
    $scope.amount_input = '';

    $scope.product_info = '';
    $scope.products = [];
    //$scope.product_tmp = null;

    $scope.source_branch = source_branch;
    $scope.discount = [];
    $scope.free_gift = [];
    //$scope.ais_deal = [];

    $scope.search_products = [];
    $scope.search_product_input = '';
    $scope.search_amount_input = [];

    $scope.isSubmit = false;


    $scope.convertBarcodeInput = function (input) {
        var english = /^[A-Za-z0-9\-*]*$/;

        var th_char = 'ๅ/-ภถุึคตจขชๆไำพะัีรนยบลฟหกดเ้่าสวงฃผปแอิืทมใฝ+๑๒๓๔ู฿๕๖๗๘๙๐"ฎฑธํ๊ณฯญฐ,ฤฆฏโฌ็๋ษศซ.ฅ()ฉฮฺ์?ฒฬฦ';
        var en_char = '1234567890-=qwertyuiop[]asdfghjkl;\'\\zxcvbnm,./1234567890-=QWERTYUIOP[]ASDFGHJKL;\'\\ZXCVBNM,./';

        var isEng = english.test(input);
        var barcode = '';

        if(isEng){
            barcode = input;
        }else{
            for(var i=0;i<input.length;i++){
                var converted_char = input.charAt(i);
                for(var x=0;x<th_char.length;x++){
                    if(th_char.charAt(x) == input.charAt(i)){
                        converted_char =  en_char.charAt(x);
                        break;
                    }
                }
                barcode +=  converted_char;
            }
        }



        return barcode;

    }

    $scope.searchProduct = function () {
        if($scope.search_product_input == '') {
            alert('กรุณากรอกข้อมูล');
            return false;
        }
        $http.get('/service-product/search-product/'+$scope.search_product_input).then(function (response) {
            $scope.search_products = response.data;

        });
        $scope.search_product_input = '';
    }

    $scope.clearSearchResult = function () {
        $scope.search_products = [];
        setTimeout(function(){ document.getElementById('search_product_input').focus(); }, 100);

        $scope.$apply();
    }

    /*
    $scope.addProductBySearch = function (barcode) {
        if(barcode == '') {
            alert('กรุณากรอกบาร์โค้ด');
        }else{
            $http.get("/service-product/find-by-barcode/"+barcode).then(function (response) {
                result = response.data;
                if(result == 'null'){
                    alert('ไม่พบสินค้านี้หรือไม่มีบาร์โค้ดนี้ในระบบ!!!');
                }else{
                    $('#modal_search_product').modal('hide');
                    if(result.type_sn == 1){
                        $scope.product_tmp = result;
                        $('#modal_sn').modal('show');
                    }else{
                        isInList =false;
                        for (i = 0; i < $scope.products.length; ++i) {
                            //alert(result.id+' '+$scope.products[i].id);
                            if(result.id == $scope.products[i].id){
                                $scope.products[i].count++;
                                isInList = true;
                                break;
                            }
                        }
                        if(!isInList)
                            $scope.products.push({'id':result.id,'product_name':result.product_name,'description':result.description,'brand':result.brand.brand_name,'model':result.model,'type_sn':result.type_sn,'sn':'','count':1,'price':result.price});

                    }

                }
            });
        }
    }
    */

    $scope.addItemToListBySearch = function (product,amount) {
        $scope.product_info = product;
        if($scope.product_info.type_sn == 1){
            $scope.hideSearchProduct();
            $scope.showInputSN();
        }else{
            $scope.getProductAmountInBranch($scope.product_info.id,$scope.source_branch).then(function (result) {
                var amount_in_branch = parseInt(result.amount);
                var amount_in_list =  parseInt($scope.getProductAmountInList($scope.product_info.id));

                amount =  parseInt(amount);

                //console.log(amount_in_branch+' '+(amount_in_list+amount));
                if(amount_in_branch >= (amount_in_list+amount)){
                    $scope.addItemToList(
                        $scope.product_info.id,
                        $scope.product_info.product_name,
                        $scope.product_info.description,
                        $scope.product_info.brand_name,
                        $scope.product_info.model,
                        $scope.product_info.cate_name,
                        amount,
                        0,
                        null,
                        $scope.product_info.price
                    );
                    $scope.hideSearchProduct();
                }else{
                    //product amount in branch is not enough
                    alert('สินค้ามีไม่เพียงพอ (คงเหลือ: '+ amount_in_branch+')');
                }
            });
        }
    }

    $scope.showInputSN = function () {
        $('#modal_sn').modal('show');
    }

    $scope.hideInputSN = function () {
        $('#modal_sn').modal('hide');
    }

    $scope.showAmountBarcodeInput = function () {
        $('#modal_amount_barcode').modal('show');
    }

    $scope.hideAmountBarcodeInput = function () {
        $('#modal_amount_barcode').modal('hide');
    }

    $scope.showSearchProduct = function () {
        $('#modal_search_product').modal('show');
    }

    $scope.hideSearchProduct = function () {
        $('#modal_search_product').modal('hide');
    }

    $scope.resetBarcodeInput = function () {
        $scope.barcode_input = '';
    }

    $scope.resetAmountBarcodeInput = function () {
        $scope.amount_barcode_input = '';
    }

    $scope.resetAmountInput = function () {
        $scope.amount_input = '';
    }

    $scope.resetSNinput = function () {
        $scope.imei_sn_input = '';
    }


    $scope.enterBarcode = function () {
        $scope.barcode_input = $scope.convertBarcodeInput($scope.barcode_input);

        if($scope.barcode_input == '') {
            alert('กรุณากรอกบาร์โค้ด');
        }else{
            $scope.getProductInfoByBarcode($scope.barcode_input).then(function (result) {
                $scope.product_info = result;
                //console.log(result);
                if($scope.product_info != 'none'){
                    if($scope.product_info.type_sn == 1){
                        $scope.showInputSN();
                    }else{
                        $scope.getProductAmountInBranch($scope.product_info.id,$scope.source_branch).then(function (result) {
                            var amount_in_branch = parseInt(result.amount);
                            var amount_in_list = parseInt($scope.getProductAmountInList($scope.product_info.id));

                            if(amount_in_branch >= (amount_in_list+1)){
                                $scope.addItemToList(
                                    $scope.product_info.id,
                                    $scope.product_info.product_name,
                                    $scope.product_info.description,
                                    $scope.product_info.brand_name,
                                    $scope.product_info.model,
                                    $scope.product_info.cate_name,
                                    1,
                                    0,
                                    null,
                                    $scope.product_info.price
                                );
                            }else{
                                //product amount in branch is not enough
                                alert('สินค้ามีไม่เพียงพอ (คงเหลือ: '+ amount_in_branch+')');
                            }
                        });
                    }
                }else{
                    //barcode not found in the system
                    alert('ไม่มีบาร์โค้ดนี้ในระบบ : '+ $scope.barcode_input);
                }
                $scope.resetBarcodeInput();
            });
        }
    }

    $scope.enterBarcodeWithAmount = function () {
        $scope.amount_barcode_input = $scope.convertBarcodeInput($scope.amount_barcode_input);

        if($scope.amount_input == ''){
            alert('กรุณาใส่จำนวน!!');
            $scope.resetAmountInput();
            document.getElementById('amount_input').focus();
        }else{
            if($scope.amount_barcode_input == ''){
                alert('กรุณากรอกบาร์โค้ด');
                $scope.resetAmountBarcodeInput();
                document.getElementById('amount_barcode_input').focus();
            }else{
                $scope.getProductInfoByBarcode($scope.amount_barcode_input).then(function (result) {
                    $scope.product_info = result;
                    //console.log(result);
                    if($scope.product_info != 'none'){
                        if($scope.product_info.type_sn == 1){
                            alert('สินค้านี้ต้องมี IMEI/SN : '+$scope.product_info.product_name+' ยี่ห้อ '+$scope.product_info.brand_name+'  รุ่น '+$scope.product_info.model);
                            $scope.resetAmountBarcodeInput();
                            document.getElementById('amount_barcode_input').focus();
                        }else{
                            $scope.getProductAmountInBranch($scope.product_info.id,$scope.source_branch).then(function (result) {
                                var amount_in_branch = parseInt(result.amount);
                                var amount_in_list = parseInt($scope.getProductAmountInList($scope.product_info.id));

                                if(amount_in_branch >= amount_in_list+parseInt($scope.amount_input)){
                                    $scope.addItemToList(
                                        $scope.product_info.id,
                                        $scope.product_info.product_name,
                                        $scope.product_info.description,
                                        $scope.product_info.brand_name,
                                        $scope.product_info.model,
                                        $scope.product_info.cate_name,
                                        parseInt($scope.amount_input),
                                        0,
                                        null,
                                        $scope.product_info.price
                                    );
                                    $scope.hideAmountBarcodeInput();
                                    $scope.resetAmountBarcodeInput();
                                    $scope.resetAmountInput();
                                }else{
                                    //product amount in branch is not enough
                                    alert('สินค้ามีไม่เพียงพอ (คงเหลือ: '+ amount_in_branch+')');
                                    $scope.resetAmountBarcodeInput();
                                }
                            });
                        }
                    }else{
                        //barcode not found in the system
                        alert('ไม่มีบาร์โค้ดนี้ในระบบ : '+ $scope.amount_barcode_input);
                        $scope.resetAmountBarcodeInput();
                    }
                });
            }
        }
    }

    $scope.enterSN = function () {
        $scope.imei_sn_input = $scope.convertBarcodeInput($scope.imei_sn_input);

        if($scope.imei_sn_input.length != 15) {
            alert('เลข IMEI ต้องมีทั้งหมด 15 หลัก: ' + $scope.imei_sn_input);
            $scope.resetSNinput();
            document.getElementById('imei_sn_input').focus();
        }else {
            if($scope.imei_sn_input.length == ''){
                alert('กรุณากรอก IMEI/SN');
                document.getElementById('imei_sn_input').focus();
            } else {
                $scope.checkProductSNinBranch($scope.product_info.id,$scope.source_branch,$scope.imei_sn_input).then(function (result) {
                    if(result == true){
                        if($scope.checkProductSNinList($scope.product_info.id,$scope.imei_sn_input)){
                            alert('SN/IMEI: '+$scope.imei_sn_input+' มีอยู่ในรายการแล้ว');
                            document.getElementById('imei_sn_input').focus();
                        }else{
                            $scope.addItemToList(
                                $scope.product_info.id,
                                $scope.product_info.product_name,
                                $scope.product_info.description,
                                $scope.product_info.brand_name,
                                $scope.product_info.model,
                                $scope.product_info.cate_name,
                                1,
                                1,
                                $scope.imei_sn_input,
                                $scope.product_info.price

                            );
                            $scope.hideInputSN();
                        }
                    }else{
                        alert('ไม่มี SN/IMEI: '+$scope.imei_sn_input+' ในระบบหรือในสาขาของคุณ');
                        document.getElementById('imei_sn_input').focus();
                    }

                    $scope.resetSNinput();
                });
            }
        }

    }

    $scope.getProductInfoByBarcode = function (barcode) {
        return $http.get("/service-product/find-by-barcode/"+barcode).then(function (response) {
            return response.data;
        });
    }

    $scope.getProductAmountInBranch = function (product_id, branch_id) {
        return $http.get("/service-product/find-product-amount-in-branch/"+product_id+','+branch_id).then(function (response) {
            return response.data;

        });
    }

    $scope.checkProductSNinBranch = function (product_id, branch_id, product_sn) {
        return $http.get("/service-product/find-productsn-in-branch/"+product_id+','+product_sn+','+branch_id).then(function (response) {
            if(response.data != 'none'){
                return true;
            }
            else{
                return false;
            }
        });
    }

    $scope.checkProductSNinList = function (product_id, product_sn) {
        var isInList = false;
        for (i = 0; i < $scope.products.length; ++i) {
            if(product_id == $scope.products[i].id && product_sn == $scope.products[i].sn){
                isInList = true;
                break;
            }
        }

        return isInList;
    }

    $scope.getProductAmountInList = function (product_id) {
        for (i = 0; i < $scope.products.length; ++i) {
            if(product_id == $scope.products[i].id){
                return $scope.products[i].count;
            }
        }

        return 0;
    }



    $scope.addItemToList = function (product_id,product_name,product_description,brand_name,product_model,product_cate_name,product_amount,product_type_sn,product_sn,product_price) {
        isInList =false;
        if(product_type_sn == 1) {
            product_amount = 1;
        }else{
            for (i = 0; i < $scope.products.length; ++i) {
                //alert(result.id+' '+$scope.products[i].id);
                if(product_id == $scope.products[i].id){
                    $scope.products[i].count += product_amount;
                    isInList = true;
                    break;
                }
            }
        }

        if(!isInList){
            $scope.products.push({
                'id':product_id,
                'product_name':product_name,
                'description':product_description,
                'brand':brand_name,
                'model':product_model,
                'cate_name':product_cate_name,
                'type_sn':product_type_sn,
                'sn':product_sn,
                'count':product_amount,
                'price':product_price
            });
        }

    }



    $scope.getTotal = function(){
        var total = 0;
        for(var i = 0; i < $scope.products.length; i++){
            var product = $scope.products[i];
            //alert($scope.discount[i]);
            if($scope.discount[i] == null)
                $scope.discount[i] = 0;
            //alert($scope.free_gift[i]);
            if(!$scope.free_gift[i])
                total += (product.price * product.count)-$scope.discount[i];

        }
        return total;
    }

    $scope.getTotalDiscount = function(){
        var total = 0;

        var length= 0;
        for(var key in $scope.discount) {
            if($scope.discount.hasOwnProperty(key)){
                length++;
            }
        }

        //alert($scope.discount[0]);
        for(var i=0;i < length;i++){
            //alert($scope.discount);
            var discount = $scope.discount[i];
            if(discount == null)
                discount = 0;
            //alert($scope.free_gift[i]);
            total += parseInt(discount);

        }
        return parseInt(total);
    }

    /*
    $scope.addProductToList = function () {
        if($scope.barcode_input == '') {
            alert('กรุณากรอกบาร์โค้ด');
        }else{
            $http.get("/service-product/find-by-barcode/"+$scope.barcode_input).then(function (response) {
                result = response.data;
                if(result == 'null'){
                    alert('ไม่พบสินค้านี้หรือไม่มีบาร์โค้ดนี้ในระบบ!!!');
                }else{
                    if(result.type_sn == 1){
                        $scope.product_tmp = result;
                        $('#modal_sn').modal('show');
                    }else{
                        $http.get("/service-product/find-product-amount-in-branch/"+result.id+','+$scope.source_branch).then(function (response) {
                            result2 =  response.data;
                            console.log(result);
                            console.log(result2);
                            if(result2 == 'null'){
                                alert('ไม่มีสินค้านี้ในสาขาของคุณ');
                            }else{

                                isInList =false;
                                for (i = 0; i < $scope.products.length; ++i) {
                                    //alert(result.id+' '+$scope.products[i].id);
                                    if(result.id == $scope.products[i].id){
                                        if($scope.products[i].count >= result2.amount){
                                            alert('สินค้าคงเหลือในสาขาไม่เพียงพอ (คงเหลือ: '+result2.amount+')');
                                        }else{
                                            $scope.products[i].count++;
                                        }
                                        isInList = true;
                                        break;
                                    }
                                }
                                if(!isInList){
                                    if(result2.amount >= 1){
                                        $scope.products.push({'id':result.id,'product_name':result.product_name,'description':result.description,'brand':result.brand.brand_name,'model':result.model,'type_sn':result.type_sn,'sn':'','count':1,'price':result.price});
                                    }else{
                                        alert('สินค้าคงเหลือในสาขาไม่เพียงพอ (คงเหลือ: '+result2.amount+')');
                                    }
                                }

                            }
                        });


                    }
                    $scope.barcode_input = '';


                }
            });
        }

        document.getElementById('barcode_input').value = '';
        document.getElementById('barcode_input').focus();

    }
    */

    $scope.removeFromList = function (index) {
        $scope.products.splice(index, 1 );
        $scope.discount.splice(index,1);
        $scope.free_gift.splice(index,1);
        document.getElementById('barcode_input').value = '';
        document.getElementById('barcode_input').focus();
    }

    $scope.clearList = function () {
        if(confirm('ต้องการเคลียร์รายการ?'))
            $scope.products = [];

        document.getElementById('barcode_input').value = '';
        document.getElementById('barcode_input').focus();
    }

    /*
    $scope.addProductToListSN = function () {
        if($scope.imei_sn_input == ''){
            alert('กรุณากรอก IMEI/SN');
            document.getElementById('imei_sn_input').focus();
        } else {
            $http.get("/service-product/find-productsn-in-branch/"+$scope.product_tmp.id+','+$scope.imei_sn_input+','+$scope.source_branch).then(function (response) {
                if(response.data != 'null' && response.data.branch_id == $scope.source_branch){
                    $scope.product_tmp.sn = $scope.imei_sn_input;
                    $scope.product_tmp.ais_deal = response.data.ais_deal;
                    isInList =false;

                    for (i = 0; i < $scope.products.length; i++) {
                        //alert(result.id+' '+$scope.products[i].id);
                        if($scope.product_tmp.id == $scope.products[i].id && $scope.product_tmp.sn == $scope.products[i].sn){
                            isInList = true;
                            break;
                        }
                    }

                    if(!isInList){
                        $scope.products.push({'id':$scope.product_tmp.id,'product_name':$scope.product_tmp.product_name,'description':$scope.product_tmp.description,'brand':$scope.product_tmp.brand.brand_name,'model':$scope.product_tmp.model,'type_sn':$scope.product_tmp.type_sn,'sn':$scope.product_tmp.sn,'count':1,'ais_deal':$scope.product_tmp.ais_deal,'price':$scope.product_tmp.price});
                        $scope.imei_sn_input = '';
                        $('#modal_sn').modal('hide');
                        //$scope.$apply();
                    } else {
                        alert('มี IMEI/SN นี้อยู่ในรายการแล้ว');
                        $scope.imei_sn_input = '';
                        document.getElementById('imei_sn_input').focus();

                    }
                }
                else{
                    alert('ไม่มี SN/IMEI ในระบบ หรือในสาขาของคุณ');
                    $scope.imei_sn_input = '';
                    document.getElementById('imei_sn_input').focus();
                }
            });




            //alert($scope.imei_sn_input);

        }


    }


    $scope.addProductToListAmount = function () {
        if($scope.amount == ''){
            alert('กรุณาใส่จำนวน!!');
            document.getElementById('amount').value = '';
            document.getElementById('amount').focus();
        }else{
            if($scope.amount_barcode == ''){
                alert('กรุณากรอกบาร์โค้ด');
                document.getElementById('amount_barcode').value = '';
                document.getElementById('amount_barcode').focus();
            }else{
                $http.get("/service-product/find-by-barcode/"+$scope.amount_barcode).then(function (response) {
                    result = response.data;
                    if(result == 'null'){
                        alert('ไม่พบสินค้านี้หรือไม่มีบาร์โค้ดนี้ในระบบ!!!');
                        document.getElementById('amount_barcode').value = '';
                        document.getElementById('amount_barcode').focus();
                    }else{
                        if(result.type_sn == 1){
                            alert('ไม่สามารถเพิ่มจำนวนได้เนื่องจากเป็นสินค้ามี IMEI/SN')
                            document.getElementById('amount_barcode').value = '';
                            document.getElementById('amount_barcode').focus();
                        }else{

                            $http.get("/service-product/find-product-amount-in-branch/"+result.id+','+$scope.source_branch).then(function (response) {

                                result2 =  response.data;
                                console.log(result);
                                console.log(result2);
                                if(result2 == 'null'){
                                    alert('ไม่มีสินค้านี้ในสาขาของคุณ');
                                }else{

                                    isInList =false;
                                    for (i = 0; i < $scope.products.length; ++i) {
                                        //alert(result.id+' '+$scope.products[i].id);
                                        if(result.id == $scope.products[i].id){
                                            if(result2.amount >= (parseInt($scope.products[i].count)+parseInt($scope.amount))){
                                                $scope.products[i].count = parseInt($scope.products[i].count) + parseInt($scope.amount);
                                            }else{
                                                alert('สินค้าคงเหลือในสาขาไม่เพียงพอ (คงเหลือ: '+result2.amount+')');
                                                //alert($scope.products[i].count+' '+$scope.amount);
                                            }
                                            isInList = true;
                                            break;
                                        }
                                    }
                                    if(!isInList){
                                        if(result2.amount >= $scope.amount){
                                            $scope.products.push({'id':result.id,'product_name':result.product_name,'description':result.description,'brand':result.brand.brand_name,'model':result.model,'sn':'','count':$scope.amount,'price':result.price});
                                        }else{
                                            alert('สินค้าคงเหลือในสาขาไม่เพียงพอ (คงเหลือ: '+result2.amount+')');
                                        }
                                    }

                                }
                            });

                            $('#modal_amount_barcode').modal('hide');


                        }

                    }
                });
            }
        }
    }
    */

    $scope.submitData = function () {

        //alert();
        if(parseInt($scope.dest_branch_id) > 0){
            $scope.isSubmit = true;
            document.getElementById('stock_form').submit();
        }else{
            alert('กรุณาเเลือกสาขาปลายทาง');
        }

    }

    $scope.checkAmountInput = function () {
        if($scope.amount_input == '') {
            alert('กรุณากรอกจำนวน');
        }else {
            document.getElementById('amount_barcode_input').focus();
        }
    }

    $scope.setSourceBranch = function (branch_id) {
        //$scope.source_branch = branch_id;
        alert(branch_id);
    }


    $('#modal_sn').on('shown.bs.modal', function (e) {
        document.getElementById('imei_sn_input').focus();
        $scope.$apply();

    });

    $('#modal_sn').on('hidden.bs.modal', function (e) {
        document.getElementById('imei_sn_input').value = '';
        document.getElementById('barcode_input').focus();
        //$('.tooltip_description').tooltip();
        $scope.$apply();
    });

    $('#modal_amount_barcode').on('shown.bs.modal', function (e) {
        document.getElementById('amount_input').focus();
        $scope.$apply();

    });

    $('#modal_amount_barcode').on('hidden.bs.modal', function (e) {
        document.getElementById('amount_input').value = '';
        document.getElementById('amount_barcode_input').value = '';
        setTimeout(function(){ document.getElementById('barcode_input').focus(); }, 100);
        //document.getElementById('barcode_input').focus();
        $scope.$apply();
    });

    $('#modal_search_product').on('shown.bs.modal', function (e) {
        setTimeout(function(){ document.getElementById('search_product_input').focus(); }, 100);

        $scope.$apply();

    });

    $('#modal_search_product').on('hidden.bs.modal', function (e) {
        setTimeout(function(){ document.getElementById('barcode_input').focus(); }, 100);
        //document.getElementById('barcode_input').focus();
        $scope.$apply();
    });

    $scope.pad = function(num, size) {
        var s = num+"";
        while (s.length < size) s = "0" + s;
        return s;
    }

    $scope.number_format = function(num) {
        if(num == null) return '';
        return num.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
    }


});
