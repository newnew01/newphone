app.controller('PrintLabelController', function($scope,$http) {
    $scope.product_modal = '';
    $scope.product_modal.image = '';

    $scope.product_print = '';
    $scope.product_id_print = '';

    $scope.print_quantity = 1;

    $scope.print_services = '';

    $scope.getPrintServices =  function () {
        $http.get('/web-service/label/get-print-services').then(function (response) {
            $scope.print_services = response.data;
            $scope.print_services.forEach($scope.checkService);
        });
    }

    $scope.checkService = function (item,index) {
        //alert(uri);
        //return true;
        $http.get(item.service_status_uri).then(function (response) {
            if(response.status == 200){
                data = response.data;
                //alert(data);
                //return true;
                if(data.printer_offline == "0"){
                    if(data.printer_error == "0"){
                        item.service_status = 1;
                    }
                    else {
                        item.service_status = 2;
                    }
                }else{
                    item.service_status = 0;
                }
            }else{
                item.service_status = 3;
            }

        },function (data) {
            item.service_status = 3;
        });
    }

    $scope.viewDetail = function (id) {
        //alert(id);
        $http.get("/service-product/find-by-id/"+id)
            .then(function(response) {
                if(response.data == 'null'){
                    alert("ไม่พบสินค้าในระบบ!!");
                }else{
                    if(response.data.image == null)
                        response.data.image = '/assets/images/no-image.png';
                    $scope.product_modal = response.data;


                    $('#viewImageModal').modal('show');
                    //$scope.$apply();

                }

            });
    }

    $scope.viewPrintOption = function (id) {
        $scope.print_services = '';
        $scope.product_id_print = id;
        $scope.print_quantity = 1;
        $scope.getPrintServices();
        $('#viewPrintOptionModal').modal('show');
    }
    $scope.printLabel = function () {
        //alert($scope.print_service);
        $http.get("/service-product/find-by-id/"+$scope.product_id_print)
            .then(function(response) {
                if(response.data == 'null'){
                    alert("ไม่พบสินค้าในระบบ!!");
                }else {
                    if (response.data.image == null)
                        response.data.image = '/assets/images/no-image.png';
                    $scope.product_print = response.data;


                    //$('#viewPrintOptionModal').modal('show');
                    //$scope.$apply();

                    uri_data = '?barcode=' + $scope.product_print.barcode + '&brand=' + $scope.product_print.brand_name + '&model=' + $scope.product_print.model + '&price=' + $scope.number_format($scope.product_print.price) + '&product_id=' + $scope.pad($scope.product_print.id, 6) + '&product_name=' + $scope.product_print.product_name + '&quantity=' + $scope.print_quantity;
                    //alert($scope.print_service+uri_data);
                    $http.get($scope.print_service + uri_data).then(function (response2) {

                    });
                }
            });
    }

    $scope.printLabel2 = function () {
        $http.get("/service-product/find-by-id/"+$scope.product_id_print)
            .then(function(response) {
                if(response.data == 'null'){
                    alert("ไม่พบสินค้าในระบบ!!");
                }else{
                    if(response.data.image == null)
                        response.data.image = '/assets/images/no-image.png';
                    $scope.product_print = response.data;


                    //$('#viewPrintOptionModal').modal('show');
                    //$scope.$apply();

                    $http.get("/service-product/find-by-id/"+$scope.product_id_print).then(function (response2) {

                    });
                }

            });
    }

    $scope.pad = function(num, size) {
        var s = num+"";
        while (s.length < size) s = "0" + s;
        return s;
    }

    $scope.number_format = function(num) {
        if(num == null) return '';
        return num.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
    };
});