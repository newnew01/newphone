app.controller('ProductListController', function($scope,$http) {
    $scope.source_branch = source_branch;
    $scope.product_modal = '';
    $scope.product_modal.image = '';

    $scope.product_stock = null;
    $scope.product_transferring = null;

    $scope.viewDetail = function (id) {
        //alert(id);
        $http.get("/service-product/find-by-id/"+id)
            .then(function(response) {
                if(response.data == 'null'){
                    alert("ไม่พบสินค้าในระบบ!!");
                }else{
                    if(response.data.image == null)
                        response.data.image = '/assets/images/no-image.png';
                    $scope.product_modal = response.data;


                    $('#viewImageModal').modal('show');
                    //$scope.$apply();

                }

            });
    }

    $scope.getProductStock = function (product_id) {
        return $http.get('/service-product/get-product-stock/'+product_id).then(function (response) {
            var result = response.data;
            return result;
        });
    }

    $scope.getProductTransferring = function (product_id) {
        return $http.get('/service-product/get-product-transferring/'+product_id).then(function (response) {
            var result = response.data;
            return result;
        });
    }

    $scope.viewStock = function (product_id) {
        $scope.getProductStock(product_id).then(function (result) {

            result.product_data.forEach(function (item, index) {
                    for(var i=0;i<result.branches.length;i++){
                        if(item.branch_id == result.branches[i].id){
                            item.branch_name = result.branches[i].branch_name;
                            break;
                        }
                    }
            });
            $scope.product_stock = result;

            $scope.getProductTransferring(product_id).then(function (result2) {
                result2.forEach(function (item, index) {
                    for(var i=0;i<result.branches.length;i++){
                        if(item.source_branch_id == result.branches[i].id){
                            item.source_branch_name = result.branches[i].branch_name;
                            break;
                        }
                    }

                    for(var i=0;i<result.branches.length;i++){
                        if(item.dest_branch_id == result.branches[i].id){
                            item.dest_branch_name = result.branches[i].branch_name;
                            break;
                        }
                    }
                });
                $scope.product_transferring = result2;
                $('#viewStockModal').modal('show');
            });

        });


        //alert(id);
        /*
        $http.get("/service-product/find-by-id/"+id)
            .then(function(response) {
                if(response.data == 'null'){
                    alert("ไม่พบสินค้าในระบบ!!");
                }else{
                    if(response.data.image == null)
                        response.data.image = '/assets/images/no-image.png';
                    $scope.product_modal = response.data;


                    $('#viewImageModal').modal('show');
                    //$scope.$apply();

                }

            });
            */
    }

    $scope.pad = function(num, size) {
        var s = num+"";
        while (s.length < size) s = "0" + s;
        return s;
    }

    $scope.number_format = function(num) {
        if(num == null) return '';
        return num.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
    };
});