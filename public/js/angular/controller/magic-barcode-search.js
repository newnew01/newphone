app.controller('MagicBarcodeController', function($scope,$sce,$http) {
    $scope.input_magic_barcode = '';
    $scope.product_ = {"image":"/assets/images/no-image.png"};
    $scope.isBarcode = false;
    $scope.isSN = false;

    $scope.product_stock = null;

    $scope.status_name = ['','มีในสต๊อค','โอน (กำลังขนส่ง)','ขายแล้ว','เสีย'];

    $scope.openMagicBarcode = function () {
        $scope.input_magic_barcode = '';
        $scope.isBarcode = false;
        $scope.isSN = false;
        setTimeout(function () {
            document.getElementById('input_magic_barcode').focus();
        },100);
    }

    $scope.enterMagicBarcode = function () {
        $scope.isBarcode = false;
        $scope.isSN = false;
        $scope.product_stock = null;

        var input_magic_barcode = $scope.convertBarcodeInput($scope.input_magic_barcode);

        if(input_magic_barcode.length == 10 && input_magic_barcode.substring(0, 4) === "INV-"){
            window.location.href = "/sale/invoice/"+input_magic_barcode;
        }else{
            var indexOfdash = input_magic_barcode.indexOf("-");
            if(input_magic_barcode.length >= 9 && input_magic_barcode.length <= 11 && indexOfdash >= 2 && indexOfdash <= 4){

                $http.get('/web-service/stock/get-doc-prefix').then(function (response) {
                    var data = response.data;


                    for(var i=0;i<data.length;i++){
                        if(input_magic_barcode.substring(0, data[i].prefix.length+1) === data[i].prefix+"-"){

                            window.location.href = "/stock/list/"+input_magic_barcode;
                            break;
                        }

                    }
                });
            } else{
                if(input_magic_barcode.length == 15){
                    //suppose to be IMEI > barcode
                    $http.get("/web-service/magic-barcode/search-sn/"+input_magic_barcode).then(function (response) {
                        if(response.data == 'none'){
                            alert("ไม่พบข้อมูลที่คุณต้องการ!!");
                        }else{
                            $scope.isSN = true;

                            if(response.data.image == null)
                                response.data.image = '/assets/images/no-image.png';

                            $scope.product_ = response.data;
                        }
                    });
                }else{
                    //barcode
                    $http.get("/web-service/magic-barcode/search-barcode/"+input_magic_barcode).then(function (response) {
                        if(response.data == 'none'){
                            alert("ไม่พบข้อมูลที่คุณต้องการ!!");
                        }else{
                            $scope.isBarcode = true;

                            if(response.data.image == null)
                                response.data.image = '/assets/images/no-image.png';

                            $scope.product_ = response.data;

                            $scope.viewStock($scope.product_.id);
                        }
                    });
                }

            }
        }
        $scope.input_magic_barcode = '';
        document.getElementById('input_magic_barcode').focus();
    }

    $scope.convertBarcodeInput = function (input) {
        var english = /^[A-Za-z0-9\-*]*$/;

        var th_char = 'ๅ/-ภถุึคตจขชๆไำพะัีรนยบลฟหกดเ้่าสวงฃผปแอิืทมใฝ+๑๒๓๔ู฿๕๖๗๘๙๐"ฎฑธํ๊ณฯญฐ,ฤฆฏโฌ็๋ษศซ.ฅ()ฉฮฺ์?ฒฬฦ';
        var en_char = '1234567890-=qwertyuiop[]asdfghjkl;\'\\zxcvbnm,./1234567890-=QWERTYUIOP[]ASDFGHJKL;\'\\ZXCVBNM,./';

        var isEng = english.test(input);
        var barcode = '';

        if(isEng){
            barcode = input;
        }else{
            for(var i=0;i<input.length;i++){
                var converted_char = input.charAt(i);
                for(var x=0;x<th_char.length;x++){
                    if(th_char.charAt(x) == input.charAt(i)){
                        converted_char =  en_char.charAt(x);
                        break;
                    }
                }
                barcode +=  converted_char;
            }
        }



        return barcode;

    }

    $scope.getProductStock = function (product_id) {
        return $http.get('/service-product/get-product-stock/'+product_id).then(function (response) {
            var result = response.data;
            return result;
        });
    }

    $scope.viewStock = function (product_id) {
        $scope.getProductStock(product_id).then(function (result) {

            result.product_data.forEach(function (item, index) {
                for(var i=0;i<result.branches.length;i++){
                    if(item.branch_id == result.branches[i].id){
                        item.branch_name = result.branches[i].branch_name;
                        break;
                    }
                }
            });
            $scope.product_stock = result;
        });

    }


    $scope.pad = function(num, size) {
        if(num == null) return '';
        var s = num+"";
        while (s.length < size) s = "0" + s;
        return s;
    }

});

