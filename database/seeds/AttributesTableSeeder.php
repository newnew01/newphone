<?php

use Illuminate\Database\Seeder;

class AttributesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        \DB::table('attributes')->delete();

        $attributes = [
            array (
                'id' => 1,
                'attribute_name' => 'สี',
            ),
            array (
                'id' => 2,
                'attribute_name' => 'ความจุ',
            ),
            ];

        for($i=0;$i<count($attributes);$i++){
            DB::table('attributes')->insert([
                'id' => $attributes[$i]['id'],
                'attribute_name' => $attributes[$i]['attribute_name']
            ]);
        }

    }
}
