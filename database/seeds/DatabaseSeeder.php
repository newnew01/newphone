<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // $this->call(UsersTableSeeder::class);
        $this->call(BrandsTableSeeder::class);
        $this->call(CategoriesTableSeeder::class);
        $this->call(ProductsTableSeeder::class);
        $this->call(BranchesTableSeeder::class);
        $this->call(StatusTableSeeder::class);
        $this->call(StockRefTypeTableSeeder::class);
        $this->call(UsersTableSeeder::class);
        $this->call(RolesSeeder::class);
        $this->call(BuddyBranchTableSeeder::class);
        $this->call(PrintServicesTableSeeder::class);
        $this->call(DistrictsTableSeeder::class);
        $this->call(GeographyTableSeeder::class);
        $this->call(ProvincesTableSeeder::class);
        $this->call(ZipcodesTableSeeder::class);
        $this->call(AmphersTableSeeder::class);
        $this->call(PermissionsTableSeeder::class);
    }
}
