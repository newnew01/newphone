<?php

use Illuminate\Database\Seeder;

class StockRefTypeTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {
        

        \DB::table('stock_ref_type')->delete();
        
        \DB::table('stock_ref_type')->insert(array (
            0 => 
            array (
                'id' => 1,
                'created_at' => NULL,
                'updated_at' => NULL,
                'type_name' => 'รับสินค้าเข้า',
                'prefix' => 'SIN',
            ),
            1 => 
            array (
                'id' => 2,
                'created_at' => NULL,
                'updated_at' => NULL,
                'type_name' => 'โอนสินค้า',
                'prefix' => 'STR',
            ),
            2 => 
            array (
                'id' => 3,
                'created_at' => NULL,
                'updated_at' => NULL,
                'type_name' => 'ขาย',
                'prefix' => 'SA',
            ),
            3 => 
            array (
                'id' => 4,
                'created_at' => NULL,
                'updated_at' => NULL,
            'type_name' => 'รับสินค้า (ยกเลิกใบเสร็จ)',
                'prefix' => 'SINR',
            ),
            4 => 
            array (
                'id' => 5,
                'created_at' => NULL,
                'updated_at' => NULL,
                'type_name' => 'ส่งซ่อม',
                'prefix' => 'SRP',
            ),
        ));
        
        
    }
}