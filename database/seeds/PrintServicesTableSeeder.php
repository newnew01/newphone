<?php

use Illuminate\Database\Seeder;

class PrintServicesTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {
        

        \DB::table('print_services')->delete();
        
        \DB::table('print_services')->insert(array (
            0 => 
            array (
                'id' => 1,
                'created_at' => '2018-05-03 22:46:34',
                'updated_at' => '2018-05-03 22:46:34',
                'service_name' => 'NewphoneBT',
                'service_uri' => 'http://127.0.0.1:56425/',
                'service_status_uri' => 'http://127.0.0.1:56425/service_status/',
                'service_enable' => 1,
            ),
        ));
        
        
    }
}