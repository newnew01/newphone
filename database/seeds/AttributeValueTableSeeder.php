<?php

use Illuminate\Database\Seeder;

class AttributeValueTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        \DB::table('attributes')->delete();

        $attributes = [
            array (
                'attribute_id' => 1,
                'attribute_value' => 'ดำ (Black)',
            ),
            array (
                'attribute_id' => 1,
                'attribute_value' => 'ทอง (Gold)',
            ),
            array (
                'attribute_id' => 1,
                'attribute_value' => 'ชมพู (Rose Gold)',
            ),
            array (
                'attribute_id' => 1,
                'attribute_value' => 'ขาว (White)',
            ),
            array (
                'attribute_id' => 1,
                'attribute_value' => 'แดง (Red)',
            ),
            array (
                'attribute_id' => 1,
                'attribute_value' => 'เทา (Gray)',
            ),
            array (
                'attribute_id' => 1,
                'attribute_value' => 'เงิน (Silver)',
            ),
            array (
                'attribute_id' => 1,
                'attribute_value' => 'น้ำเงิน (Blue)',
            ),
            array (
                'attribute_id' => 1,
                'attribute_value' => 'เหลือง (Yellow)',
            ),
            array (
                'attribute_id' => 1,
                'attribute_value' => 'ม่วง (Purple)',
            ),
            array (
                'attribute_id' => 1,
                'attribute_value' => 'น้ำตาล (Brown)',
            ),
            array (
                'attribute_id' => 1,
                'attribute_value' => 'เขียวบลีน (Bleen)',
            ),
            array (
                'attribute_id' => 1,
                'attribute_value' => 'เขียวไลม์ (Lime)',
            ),

        ];

        for($i=0;$i<count($attributes);$i++){
            DB::table('attributes')->insert([
                'attribute_name' => $attributes[$i]
            ]);
        }
    }
}
