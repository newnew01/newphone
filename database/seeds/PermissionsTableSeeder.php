<?php

use Illuminate\Database\Seeder;

class PermissionsTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {
        

        \DB::table('permissions')->delete();
        
        \DB::table('permissions')->insert(array (
            0 => 
            array (
                'id' => 1,
                'name' => 'create-product',
                'guard_name' => 'web',
                'created_at' => '2018-05-25 05:02:10',
                'updated_at' => '2018-05-25 05:02:10',
            ),
            1 => 
            array (
                'id' => 2,
                'name' => 'create-category',
                'guard_name' => 'web',
                'created_at' => '2018-05-25 05:03:30',
                'updated_at' => '2018-05-25 05:03:30',
            ),
            2 => 
            array (
                'id' => 3,
                'name' => 'create-brand',
                'guard_name' => 'web',
                'created_at' => '2018-05-25 05:03:43',
                'updated_at' => '2018-05-25 05:03:43',
            ),
            3 => 
            array (
                'id' => 4,
                'name' => 'view-cost',
                'guard_name' => 'web',
                'created_at' => '2018-05-25 05:04:15',
                'updated_at' => '2018-05-25 05:04:15',
            ),
            4 => 
            array (
                'id' => 5,
                'name' => 'edit-product',
                'guard_name' => 'web',
                'created_at' => '2018-05-25 05:04:22',
                'updated_at' => '2018-05-25 05:04:22',
            ),
            5 => 
            array (
                'id' => 6,
                'name' => 'delete-product',
                'guard_name' => 'web',
                'created_at' => '2018-05-25 05:04:27',
                'updated_at' => '2018-05-25 05:04:27',
            ),
            6 => 
            array (
                'id' => 7,
                'name' => 'edit-category',
                'guard_name' => 'web',
                'created_at' => '2018-05-25 05:04:47',
                'updated_at' => '2018-05-25 05:04:47',
            ),
            7 => 
            array (
                'id' => 8,
                'name' => 'edit-brand',
                'guard_name' => 'web',
                'created_at' => '2018-05-25 05:04:52',
                'updated_at' => '2018-05-25 05:04:52',
            ),
            8 => 
            array (
                'id' => 9,
                'name' => 'stock-in',
                'guard_name' => 'web',
                'created_at' => '2018-05-25 05:05:29',
                'updated_at' => '2018-05-25 05:05:29',
            ),
            9 => 
            array (
                'id' => 10,
                'name' => 'stock-transfer',
                'guard_name' => 'web',
                'created_at' => '2018-05-25 05:07:53',
                'updated_at' => '2018-05-25 05:07:53',
            ),
            10 => 
            array (
                'id' => 11,
                'name' => 'stock-defect',
                'guard_name' => 'web',
                'created_at' => '2018-05-25 05:08:07',
                'updated_at' => '2018-05-25 05:08:07',
            ),
            11 => 
            array (
                'id' => 12,
                'name' => 'view-stock-transaction',
                'guard_name' => 'web',
                'created_at' => '2018-05-25 05:09:17',
                'updated_at' => '2018-05-25 05:09:17',
            ),
            12 => 
            array (
                'id' => 13,
                'name' => 'view-stock-transaction-all',
                'guard_name' => 'web',
                'created_at' => '2018-05-25 05:09:36',
                'updated_at' => '2018-05-25 05:09:36',
            ),
            13 => 
            array (
                'id' => 14,
                'name' => 'new-order',
                'guard_name' => 'web',
                'created_at' => '2018-05-25 05:10:12',
                'updated_at' => '2018-05-25 05:10:12',
            ),
            14 => 
            array (
                'id' => 15,
                'name' => 'view-invoice-history',
                'guard_name' => 'web',
                'created_at' => '2018-05-25 05:11:04',
                'updated_at' => '2018-05-25 05:11:04',
            ),
            15 => 
            array (
                'id' => 16,
                'name' => 'topup',
                'guard_name' => 'web',
                'created_at' => '2018-05-25 05:11:32',
                'updated_at' => '2018-05-25 05:11:32',
            ),
            16 => 
            array (
                'id' => 17,
                'name' => 'create-ais-deal',
                'guard_name' => 'web',
                'created_at' => '2018-05-25 05:12:13',
                'updated_at' => '2018-05-25 05:12:13',
            ),
            17 => 
            array (
                'id' => 18,
                'name' => 'print-label',
                'guard_name' => 'web',
                'created_at' => '2018-05-25 05:12:40',
                'updated_at' => '2018-05-25 05:12:40',
            ),
            18 => 
            array (
                'id' => 19,
                'name' => 'create-print-service',
                'guard_name' => 'web',
                'created_at' => '2018-05-25 05:13:25',
                'updated_at' => '2018-05-25 05:13:25',
            ),
            19 => 
            array (
                'id' => 20,
                'name' => 'view-today-report',
                'guard_name' => 'web',
                'created_at' => '2018-05-25 05:13:54',
                'updated_at' => '2018-05-25 05:13:54',
            ),
            20 => 
            array (
                'id' => 21,
                'name' => 'create-user',
                'guard_name' => 'web',
                'created_at' => '2018-05-25 05:14:21',
                'updated_at' => '2018-05-25 05:14:21',
            ),
            21 => 
            array (
                'id' => 22,
                'name' => 'edit-user',
                'guard_name' => 'web',
                'created_at' => '2018-06-03 00:51:39',
                'updated_at' => '2018-06-03 00:51:39',
            ),
            22 => 
            array (
                'id' => 23,
                'name' => 'set-user-permission',
                'guard_name' => 'web',
                'created_at' => '2018-06-03 00:53:32',
                'updated_at' => '2018-06-03 00:53:32',
            ),
            23 => 
            array (
                'id' => 24,
                'name' => 'delete-user',
                'guard_name' => 'web',
                'created_at' => '2018-06-03 01:22:24',
                'updated_at' => '2018-06-03 01:22:24',
            ),
        ));
        
        
    }
}