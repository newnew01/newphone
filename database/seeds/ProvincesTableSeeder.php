<?php

use Illuminate\Database\Seeder;

class ProvincesTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {
        

        \DB::table('provinces')->delete();
        
        \DB::table('provinces')->insert(array (
            0 => 
            array (
                'id' => 1,
                'province_code' => '10',
                'province_name' => 'กรุงเทพมหานคร   ',
                'province_name_eng' => 'Bangkok',
                'geo_id' => 2,
            ),
            1 => 
            array (
                'id' => 2,
                'province_code' => '11',
                'province_name' => 'สมุทรปราการ   ',
                'province_name_eng' => 'Samut Prakan',
                'geo_id' => 2,
            ),
            2 => 
            array (
                'id' => 3,
                'province_code' => '12',
                'province_name' => 'นนทบุรี   ',
                'province_name_eng' => 'Nonthaburi',
                'geo_id' => 2,
            ),
            3 => 
            array (
                'id' => 4,
                'province_code' => '13',
                'province_name' => 'ปทุมธานี   ',
                'province_name_eng' => 'Pathum Thani',
                'geo_id' => 2,
            ),
            4 => 
            array (
                'id' => 5,
                'province_code' => '14',
                'province_name' => 'พระนครศรีอยุธยา   ',
                'province_name_eng' => 'Phra Nakhon Si Ayutthaya',
                'geo_id' => 2,
            ),
            5 => 
            array (
                'id' => 6,
                'province_code' => '15',
                'province_name' => 'อ่างทอง   ',
                'province_name_eng' => 'Ang Thong',
                'geo_id' => 2,
            ),
            6 => 
            array (
                'id' => 7,
                'province_code' => '16',
                'province_name' => 'ลพบุรี   ',
                'province_name_eng' => 'Loburi',
                'geo_id' => 2,
            ),
            7 => 
            array (
                'id' => 8,
                'province_code' => '17',
                'province_name' => 'สิงห์บุรี   ',
                'province_name_eng' => 'Sing Buri',
                'geo_id' => 2,
            ),
            8 => 
            array (
                'id' => 9,
                'province_code' => '18',
                'province_name' => 'ชัยนาท   ',
                'province_name_eng' => 'Chai Nat',
                'geo_id' => 2,
            ),
            9 => 
            array (
                'id' => 10,
                'province_code' => '19',
                'province_name' => 'สระบุรี',
                'province_name_eng' => 'Saraburi',
                'geo_id' => 2,
            ),
            10 => 
            array (
                'id' => 11,
                'province_code' => '20',
                'province_name' => 'ชลบุรี   ',
                'province_name_eng' => 'Chon Buri',
                'geo_id' => 5,
            ),
            11 => 
            array (
                'id' => 12,
                'province_code' => '21',
                'province_name' => 'ระยอง   ',
                'province_name_eng' => 'Rayong',
                'geo_id' => 5,
            ),
            12 => 
            array (
                'id' => 13,
                'province_code' => '22',
                'province_name' => 'จันทบุรี   ',
                'province_name_eng' => 'Chanthaburi',
                'geo_id' => 5,
            ),
            13 => 
            array (
                'id' => 14,
                'province_code' => '23',
                'province_name' => 'ตราด   ',
                'province_name_eng' => 'Trat',
                'geo_id' => 5,
            ),
            14 => 
            array (
                'id' => 15,
                'province_code' => '24',
                'province_name' => 'ฉะเชิงเทรา   ',
                'province_name_eng' => 'Chachoengsao',
                'geo_id' => 5,
            ),
            15 => 
            array (
                'id' => 16,
                'province_code' => '25',
                'province_name' => 'ปราจีนบุรี   ',
                'province_name_eng' => 'Prachin Buri',
                'geo_id' => 5,
            ),
            16 => 
            array (
                'id' => 17,
                'province_code' => '26',
                'province_name' => 'นครนายก   ',
                'province_name_eng' => 'Nakhon Nayok',
                'geo_id' => 2,
            ),
            17 => 
            array (
                'id' => 18,
                'province_code' => '27',
                'province_name' => 'สระแก้ว   ',
                'province_name_eng' => 'Sa Kaeo',
                'geo_id' => 5,
            ),
            18 => 
            array (
                'id' => 19,
                'province_code' => '30',
                'province_name' => 'นครราชสีมา   ',
                'province_name_eng' => 'Nakhon Ratchasima',
                'geo_id' => 3,
            ),
            19 => 
            array (
                'id' => 20,
                'province_code' => '31',
                'province_name' => 'บุรีรัมย์   ',
                'province_name_eng' => 'Buri Ram',
                'geo_id' => 3,
            ),
            20 => 
            array (
                'id' => 21,
                'province_code' => '32',
                'province_name' => 'สุรินทร์   ',
                'province_name_eng' => 'Surin',
                'geo_id' => 3,
            ),
            21 => 
            array (
                'id' => 22,
                'province_code' => '33',
                'province_name' => 'ศรีสะเกษ   ',
                'province_name_eng' => 'Si Sa Ket',
                'geo_id' => 3,
            ),
            22 => 
            array (
                'id' => 23,
                'province_code' => '34',
                'province_name' => 'อุบลราชธานี   ',
                'province_name_eng' => 'Ubon Ratchathani',
                'geo_id' => 3,
            ),
            23 => 
            array (
                'id' => 24,
                'province_code' => '35',
                'province_name' => 'ยโสธร   ',
                'province_name_eng' => 'Yasothon',
                'geo_id' => 3,
            ),
            24 => 
            array (
                'id' => 25,
                'province_code' => '36',
                'province_name' => 'ชัยภูมิ   ',
                'province_name_eng' => 'Chaiyaphum',
                'geo_id' => 3,
            ),
            25 => 
            array (
                'id' => 26,
                'province_code' => '37',
                'province_name' => 'อำนาจเจริญ   ',
                'province_name_eng' => 'Amnat Charoen',
                'geo_id' => 3,
            ),
            26 => 
            array (
                'id' => 27,
                'province_code' => '39',
                'province_name' => 'หนองบัวลำภู   ',
                'province_name_eng' => 'Nong Bua Lam Phu',
                'geo_id' => 3,
            ),
            27 => 
            array (
                'id' => 28,
                'province_code' => '40',
                'province_name' => 'ขอนแก่น   ',
                'province_name_eng' => 'Khon Kaen',
                'geo_id' => 3,
            ),
            28 => 
            array (
                'id' => 29,
                'province_code' => '41',
                'province_name' => 'อุดรธานี   ',
                'province_name_eng' => 'Udon Thani',
                'geo_id' => 3,
            ),
            29 => 
            array (
                'id' => 30,
                'province_code' => '42',
                'province_name' => 'เลย   ',
                'province_name_eng' => 'Loei',
                'geo_id' => 3,
            ),
            30 => 
            array (
                'id' => 31,
                'province_code' => '43',
                'province_name' => 'หนองคาย   ',
                'province_name_eng' => 'Nong Khai',
                'geo_id' => 3,
            ),
            31 => 
            array (
                'id' => 32,
                'province_code' => '44',
                'province_name' => 'มหาสารคาม   ',
                'province_name_eng' => 'Maha Sarakham',
                'geo_id' => 3,
            ),
            32 => 
            array (
                'id' => 33,
                'province_code' => '45',
                'province_name' => 'ร้อยเอ็ด   ',
                'province_name_eng' => 'Roi Et',
                'geo_id' => 3,
            ),
            33 => 
            array (
                'id' => 34,
                'province_code' => '46',
                'province_name' => 'กาฬสินธุ์   ',
                'province_name_eng' => 'Kalasin',
                'geo_id' => 3,
            ),
            34 => 
            array (
                'id' => 35,
                'province_code' => '47',
                'province_name' => 'สกลนคร   ',
                'province_name_eng' => 'Sakon Nakhon',
                'geo_id' => 3,
            ),
            35 => 
            array (
                'id' => 36,
                'province_code' => '48',
                'province_name' => 'นครพนม   ',
                'province_name_eng' => 'Nakhon Phanom',
                'geo_id' => 3,
            ),
            36 => 
            array (
                'id' => 37,
                'province_code' => '49',
                'province_name' => 'มุกดาหาร   ',
                'province_name_eng' => 'Mukdahan',
                'geo_id' => 3,
            ),
            37 => 
            array (
                'id' => 38,
                'province_code' => '50',
                'province_name' => 'เชียงใหม่   ',
                'province_name_eng' => 'Chiang Mai',
                'geo_id' => 1,
            ),
            38 => 
            array (
                'id' => 39,
                'province_code' => '51',
                'province_name' => 'ลำพูน   ',
                'province_name_eng' => 'Lamphun',
                'geo_id' => 1,
            ),
            39 => 
            array (
                'id' => 40,
                'province_code' => '52',
                'province_name' => 'ลำปาง   ',
                'province_name_eng' => 'Lampang',
                'geo_id' => 1,
            ),
            40 => 
            array (
                'id' => 41,
                'province_code' => '53',
                'province_name' => 'อุตรดิตถ์   ',
                'province_name_eng' => 'Uttaradit',
                'geo_id' => 1,
            ),
            41 => 
            array (
                'id' => 42,
                'province_code' => '54',
                'province_name' => 'แพร่   ',
                'province_name_eng' => 'Phrae',
                'geo_id' => 1,
            ),
            42 => 
            array (
                'id' => 43,
                'province_code' => '55',
                'province_name' => 'น่าน   ',
                'province_name_eng' => 'Nan',
                'geo_id' => 1,
            ),
            43 => 
            array (
                'id' => 44,
                'province_code' => '56',
                'province_name' => 'พะเยา   ',
                'province_name_eng' => 'Phayao',
                'geo_id' => 1,
            ),
            44 => 
            array (
                'id' => 45,
                'province_code' => '57',
                'province_name' => 'เชียงราย   ',
                'province_name_eng' => 'Chiang Rai',
                'geo_id' => 1,
            ),
            45 => 
            array (
                'id' => 46,
                'province_code' => '58',
                'province_name' => 'แม่ฮ่องสอน   ',
                'province_name_eng' => 'Mae Hong Son',
                'geo_id' => 1,
            ),
            46 => 
            array (
                'id' => 47,
                'province_code' => '60',
                'province_name' => 'นครสวรรค์   ',
                'province_name_eng' => 'Nakhon Sawan',
                'geo_id' => 2,
            ),
            47 => 
            array (
                'id' => 48,
                'province_code' => '61',
                'province_name' => 'อุทัยธานี   ',
                'province_name_eng' => 'Uthai Thani',
                'geo_id' => 2,
            ),
            48 => 
            array (
                'id' => 49,
                'province_code' => '62',
                'province_name' => 'กำแพงเพชร   ',
                'province_name_eng' => 'Kamphaeng Phet',
                'geo_id' => 2,
            ),
            49 => 
            array (
                'id' => 50,
                'province_code' => '63',
                'province_name' => 'ตาก   ',
                'province_name_eng' => 'Tak',
                'geo_id' => 4,
            ),
            50 => 
            array (
                'id' => 51,
                'province_code' => '64',
                'province_name' => 'สุโขทัย   ',
                'province_name_eng' => 'Sukhothai',
                'geo_id' => 2,
            ),
            51 => 
            array (
                'id' => 52,
                'province_code' => '65',
                'province_name' => 'พิษณุโลก   ',
                'province_name_eng' => 'Phitsanulok',
                'geo_id' => 2,
            ),
            52 => 
            array (
                'id' => 53,
                'province_code' => '66',
                'province_name' => 'พิจิตร   ',
                'province_name_eng' => 'Phichit',
                'geo_id' => 2,
            ),
            53 => 
            array (
                'id' => 54,
                'province_code' => '67',
                'province_name' => 'เพชรบูรณ์   ',
                'province_name_eng' => 'Phetchabun',
                'geo_id' => 2,
            ),
            54 => 
            array (
                'id' => 55,
                'province_code' => '70',
                'province_name' => 'ราชบุรี   ',
                'province_name_eng' => 'Ratchaburi',
                'geo_id' => 4,
            ),
            55 => 
            array (
                'id' => 56,
                'province_code' => '71',
                'province_name' => 'กาญจนบุรี   ',
                'province_name_eng' => 'Kanchanaburi',
                'geo_id' => 4,
            ),
            56 => 
            array (
                'id' => 57,
                'province_code' => '72',
                'province_name' => 'สุพรรณบุรี   ',
                'province_name_eng' => 'Suphan Buri',
                'geo_id' => 2,
            ),
            57 => 
            array (
                'id' => 58,
                'province_code' => '73',
                'province_name' => 'นครปฐม   ',
                'province_name_eng' => 'Nakhon Pathom',
                'geo_id' => 2,
            ),
            58 => 
            array (
                'id' => 59,
                'province_code' => '74',
                'province_name' => 'สมุทรสาคร   ',
                'province_name_eng' => 'Samut Sakhon',
                'geo_id' => 2,
            ),
            59 => 
            array (
                'id' => 60,
                'province_code' => '75',
                'province_name' => 'สมุทรสงคราม   ',
                'province_name_eng' => 'Samut Songkhram',
                'geo_id' => 2,
            ),
            60 => 
            array (
                'id' => 61,
                'province_code' => '76',
                'province_name' => 'เพชรบุรี   ',
                'province_name_eng' => 'Phetchaburi',
                'geo_id' => 4,
            ),
            61 => 
            array (
                'id' => 62,
                'province_code' => '77',
                'province_name' => 'ประจวบคีรีขันธ์   ',
                'province_name_eng' => 'Prachuap Khiri Khan',
                'geo_id' => 4,
            ),
            62 => 
            array (
                'id' => 63,
                'province_code' => '80',
                'province_name' => 'นครศรีธรรมราช   ',
                'province_name_eng' => 'Nakhon Si Thammarat',
                'geo_id' => 6,
            ),
            63 => 
            array (
                'id' => 64,
                'province_code' => '81',
                'province_name' => 'กระบี่   ',
                'province_name_eng' => 'Krabi',
                'geo_id' => 6,
            ),
            64 => 
            array (
                'id' => 65,
                'province_code' => '82',
                'province_name' => 'พังงา   ',
                'province_name_eng' => 'Phangnga',
                'geo_id' => 6,
            ),
            65 => 
            array (
                'id' => 66,
                'province_code' => '83',
                'province_name' => 'ภูเก็ต   ',
                'province_name_eng' => 'Phuket',
                'geo_id' => 6,
            ),
            66 => 
            array (
                'id' => 67,
                'province_code' => '84',
                'province_name' => 'สุราษฎร์ธานี   ',
                'province_name_eng' => 'Surat Thani',
                'geo_id' => 6,
            ),
            67 => 
            array (
                'id' => 68,
                'province_code' => '85',
                'province_name' => 'ระนอง   ',
                'province_name_eng' => 'Ranong',
                'geo_id' => 6,
            ),
            68 => 
            array (
                'id' => 69,
                'province_code' => '86',
                'province_name' => 'ชุมพร   ',
                'province_name_eng' => 'Chumphon',
                'geo_id' => 6,
            ),
            69 => 
            array (
                'id' => 70,
                'province_code' => '90',
                'province_name' => 'สงขลา   ',
                'province_name_eng' => 'Songkhla',
                'geo_id' => 6,
            ),
            70 => 
            array (
                'id' => 71,
                'province_code' => '91',
                'province_name' => 'สตูล   ',
                'province_name_eng' => 'Satun',
                'geo_id' => 6,
            ),
            71 => 
            array (
                'id' => 72,
                'province_code' => '92',
                'province_name' => 'ตรัง   ',
                'province_name_eng' => 'Trang',
                'geo_id' => 6,
            ),
            72 => 
            array (
                'id' => 73,
                'province_code' => '93',
                'province_name' => 'พัทลุง   ',
                'province_name_eng' => 'Phatthalung',
                'geo_id' => 6,
            ),
            73 => 
            array (
                'id' => 74,
                'province_code' => '94',
                'province_name' => 'ปัตตานี   ',
                'province_name_eng' => 'Pattani',
                'geo_id' => 6,
            ),
            74 => 
            array (
                'id' => 75,
                'province_code' => '95',
                'province_name' => 'ยะลา   ',
                'province_name_eng' => 'Yala',
                'geo_id' => 6,
            ),
            75 => 
            array (
                'id' => 76,
                'province_code' => '96',
                'province_name' => 'นราธิวาส   ',
                'province_name_eng' => 'Narathiwat',
                'geo_id' => 6,
            ),
            76 => 
            array (
                'id' => 77,
                'province_code' => '97',
                'province_name' => 'บึงกาฬ',
                'province_name_eng' => 'buogkan',
                'geo_id' => 3,
            ),
        ));
        
        
    }
}