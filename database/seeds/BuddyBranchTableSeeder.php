<?php

use Illuminate\Database\Seeder;

class BuddyBranchTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {
        

        \DB::table('buddy_branch')->delete();
        
        \DB::table('buddy_branch')->insert(array (
            0 => 
            array (
                'id' => 1,
                'created_at' => NULL,
                'updated_at' => NULL,
                'branch_name' => 'นิวโฟนบ้านทา',
            ),
            1 => 
            array (
                'id' => 2,
                'created_at' => NULL,
                'updated_at' => NULL,
                'branch_name' => 'นิวโฟนบ้านโฮ่ง',
            ),
        ));
        
        
    }
}