<?php

use Illuminate\Database\Seeder;

class BranchesTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {
        

        \DB::table('branches')->delete();
        
        \DB::table('branches')->insert(array (
            0 => 
            array (
                'id' => 1,
                'created_at' => NULL,
                'updated_at' => NULL,
                'branch_name' => 'นิวโฟนแม่ทา',
                'branch_address' => '120/3 หมู่6 ต.ทากาศ อ.แม่ทา จ.ลำพูน 51170',
                'branch_tel' => '061-3613824',
                'branch_line' => '@newphone',
                'branch_facebook' => 'fb.com/th.newphone',
            ),
            1 => 
            array (
                'id' => 2,
                'created_at' => NULL,
                'updated_at' => NULL,
                'branch_name' => 'นิวโฟนบ้านโฮ่ง',
                'branch_address' => '131 หมู่ 1 ต.ทาทุ่งหลวง อ.แม่ทา จ.ลำพูน 51170',
                'branch_tel' => '063-6632701',
                'branch_line' => '@newphone',
                'branch_facebook' => 'fb.com/newphone.banhong',
            ),
            2 => 
            array (
                'id' => 3,
                'created_at' => NULL,
                'updated_at' => NULL,
                'branch_name' => 'เอสเคแม่ทา',
                'branch_address' => '131 หมู่ 1 ต.ทาทุ่งหลวง อ.แม่ทา จ.ลำพูน 51170',
                'branch_tel' => '053-574533',
                'branch_line' => NULL,
                'branch_facebook' => 'fb.com/sk.saleandservice',
            ),
            3 => 
            array (
                'id' => 4,
                'created_at' => NULL,
                'updated_at' => NULL,
                'branch_name' => 'เอสเคบ้านแซม',
                'branch_address' => '131 หมู่ 1 ต.ทาทุ่งหลวง อ.แม่ทา จ.ลำพูน 51170',
                'branch_tel' => '053-005282',
                'branch_line' => NULL,
                'branch_facebook' => 'fb.com/sk.saleandservice',
            ),
            4 => 
            array (
                'id' => 5,
                'created_at' => NULL,
                'updated_at' => NULL,
                'branch_name' => 'เอสเคบ้านโฮ่ง',
                'branch_address' => '131 หมู่ 1 ต.ทาทุ่งหลวง อ.แม่ทา จ.ลำพูน 51170',
                'branch_tel' => '053-980933',
                'branch_line' => NULL,
                'branch_facebook' => 'fb.com/sk.saleandservice',
            ),
            5 => 
            array (
                'id' => 6,
                'created_at' => NULL,
                'updated_at' => NULL,
                'branch_name' => 'นิวโฟนหล่ายแก้ว',
                'branch_address' => '131 หมู่ 1 ต.ทาทุ่งหลวง อ.แม่ทา จ.ลำพูน 51170',
                'branch_tel' => '093-2198287',
                'branch_line' => '@newphone',
                'branch_facebook' => 'fb.com/newphone.laikeaw',
            ),
        ));
        
        
    }
}