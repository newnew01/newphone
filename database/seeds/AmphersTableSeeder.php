<?php

use Illuminate\Database\Seeder;

class AmphersTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {
        

        \DB::table('amphers')->delete();
        
        \DB::table('amphers')->insert(array (
            0 => 
            array (
                'id' => 1,
                'ampher_code' => '1001',
                'ampher_name' => 'เขตพระนคร   ',
                'ampher_name_eng' => 'Khet Phra Nakhon',
                'geo_id' => 2,
                'province_id' => 1,
            ),
            1 => 
            array (
                'id' => 2,
                'ampher_code' => '1002',
                'ampher_name' => 'เขตดุสิต   ',
                'ampher_name_eng' => 'Khet Dusit',
                'geo_id' => 2,
                'province_id' => 1,
            ),
            2 => 
            array (
                'id' => 3,
                'ampher_code' => '1003',
                'ampher_name' => 'เขตหนองจอก   ',
                'ampher_name_eng' => 'Khet  Nong Chok',
                'geo_id' => 2,
                'province_id' => 1,
            ),
            3 => 
            array (
                'id' => 4,
                'ampher_code' => '1004',
                'ampher_name' => 'เขตบางรัก   ',
                'ampher_name_eng' => 'Khet Bang Rak',
                'geo_id' => 2,
                'province_id' => 1,
            ),
            4 => 
            array (
                'id' => 5,
                'ampher_code' => '1005',
                'ampher_name' => 'เขตบางเขน   ',
                'ampher_name_eng' => 'Khet Bang Khen',
                'geo_id' => 2,
                'province_id' => 1,
            ),
            5 => 
            array (
                'id' => 6,
                'ampher_code' => '1006',
                'ampher_name' => 'เขตบางกะปิ   ',
                'ampher_name_eng' => 'Khet Bang Kapi',
                'geo_id' => 2,
                'province_id' => 1,
            ),
            6 => 
            array (
                'id' => 7,
                'ampher_code' => '1007',
                'ampher_name' => 'เขตปทุมวัน   ',
                'ampher_name_eng' => 'Khet Pathum Wan',
                'geo_id' => 2,
                'province_id' => 1,
            ),
            7 => 
            array (
                'id' => 8,
                'ampher_code' => '1008',
                'ampher_name' => 'เขตป้อมปราบศัตรูพ่าย   ',
                'ampher_name_eng' => 'Khet Pom Prap Sattru Phai',
                'geo_id' => 2,
                'province_id' => 1,
            ),
            8 => 
            array (
                'id' => 9,
                'ampher_code' => '1009',
                'ampher_name' => 'เขตพระโขนง   ',
                'ampher_name_eng' => 'Khet Phra Khanong',
                'geo_id' => 2,
                'province_id' => 1,
            ),
            9 => 
            array (
                'id' => 10,
                'ampher_code' => '1010',
                'ampher_name' => 'เขตมีนบุรี   ',
                'ampher_name_eng' => 'Khet Min Buri',
                'geo_id' => 2,
                'province_id' => 1,
            ),
            10 => 
            array (
                'id' => 11,
                'ampher_code' => '1011',
                'ampher_name' => 'เขตลาดกระบัง   ',
                'ampher_name_eng' => 'Khet Lat Krabang',
                'geo_id' => 2,
                'province_id' => 1,
            ),
            11 => 
            array (
                'id' => 12,
                'ampher_code' => '1012',
                'ampher_name' => 'เขตยานนาวา   ',
                'ampher_name_eng' => 'Khet Yan Nawa',
                'geo_id' => 2,
                'province_id' => 1,
            ),
            12 => 
            array (
                'id' => 13,
                'ampher_code' => '1013',
                'ampher_name' => 'เขตสัมพันธวงศ์   ',
                'ampher_name_eng' => 'Khet Samphanthawong',
                'geo_id' => 2,
                'province_id' => 1,
            ),
            13 => 
            array (
                'id' => 14,
                'ampher_code' => '1014',
                'ampher_name' => 'เขตพญาไท   ',
                'ampher_name_eng' => 'Khet Phaya Thai',
                'geo_id' => 2,
                'province_id' => 1,
            ),
            14 => 
            array (
                'id' => 15,
                'ampher_code' => '1015',
                'ampher_name' => 'เขตธนบุรี   ',
                'ampher_name_eng' => 'Khet Thon Buri',
                'geo_id' => 2,
                'province_id' => 1,
            ),
            15 => 
            array (
                'id' => 16,
                'ampher_code' => '1016',
                'ampher_name' => 'เขตบางกอกใหญ่   ',
                'ampher_name_eng' => 'Khet Bangkok Yai',
                'geo_id' => 2,
                'province_id' => 1,
            ),
            16 => 
            array (
                'id' => 17,
                'ampher_code' => '1017',
                'ampher_name' => 'เขตห้วยขวาง   ',
                'ampher_name_eng' => 'Khet Huai Khwang',
                'geo_id' => 2,
                'province_id' => 1,
            ),
            17 => 
            array (
                'id' => 18,
                'ampher_code' => '1018',
                'ampher_name' => 'เขตคลองสาน   ',
                'ampher_name_eng' => 'Khet Khlong San',
                'geo_id' => 2,
                'province_id' => 1,
            ),
            18 => 
            array (
                'id' => 19,
                'ampher_code' => '1019',
                'ampher_name' => 'เขตตลิ่งชัน   ',
                'ampher_name_eng' => 'Khet Taling Chan',
                'geo_id' => 2,
                'province_id' => 1,
            ),
            19 => 
            array (
                'id' => 20,
                'ampher_code' => '1020',
                'ampher_name' => 'เขตบางกอกน้อย   ',
                'ampher_name_eng' => 'Khet Bangkok Noi',
                'geo_id' => 2,
                'province_id' => 1,
            ),
            20 => 
            array (
                'id' => 21,
                'ampher_code' => '1021',
                'ampher_name' => 'เขตบางขุนเทียน   ',
                'ampher_name_eng' => 'Khet Bang Khun Thian',
                'geo_id' => 2,
                'province_id' => 1,
            ),
            21 => 
            array (
                'id' => 22,
                'ampher_code' => '1022',
                'ampher_name' => 'เขตภาษีเจริญ   ',
                'ampher_name_eng' => 'Khet Phasi Charoen',
                'geo_id' => 2,
                'province_id' => 1,
            ),
            22 => 
            array (
                'id' => 23,
                'ampher_code' => '1023',
                'ampher_name' => 'เขตหนองแขม   ',
                'ampher_name_eng' => 'Khet Nong Khaem',
                'geo_id' => 2,
                'province_id' => 1,
            ),
            23 => 
            array (
                'id' => 24,
                'ampher_code' => '1024',
                'ampher_name' => 'เขตราษฎร์บูรณะ   ',
                'ampher_name_eng' => 'Khet Rat Burana',
                'geo_id' => 2,
                'province_id' => 1,
            ),
            24 => 
            array (
                'id' => 25,
                'ampher_code' => '1025',
                'ampher_name' => 'เขตบางพลัด   ',
                'ampher_name_eng' => 'Khet Bang Phlat',
                'geo_id' => 2,
                'province_id' => 1,
            ),
            25 => 
            array (
                'id' => 26,
                'ampher_code' => '1026',
                'ampher_name' => 'เขตดินแดง   ',
                'ampher_name_eng' => 'Khet Din Daeng',
                'geo_id' => 2,
                'province_id' => 1,
            ),
            26 => 
            array (
                'id' => 27,
                'ampher_code' => '1027',
                'ampher_name' => 'เขตบึงกุ่ม   ',
                'ampher_name_eng' => 'Khet Bueng Kum',
                'geo_id' => 2,
                'province_id' => 1,
            ),
            27 => 
            array (
                'id' => 28,
                'ampher_code' => '1028',
                'ampher_name' => 'เขตสาทร   ',
                'ampher_name_eng' => 'Khet Sathon',
                'geo_id' => 2,
                'province_id' => 1,
            ),
            28 => 
            array (
                'id' => 29,
                'ampher_code' => '1029',
                'ampher_name' => 'เขตบางซื่อ   ',
                'ampher_name_eng' => 'Khet Bang Sue',
                'geo_id' => 2,
                'province_id' => 1,
            ),
            29 => 
            array (
                'id' => 30,
                'ampher_code' => '1030',
                'ampher_name' => 'เขตจตุจักร   ',
                'ampher_name_eng' => 'Khet Chatuchak',
                'geo_id' => 2,
                'province_id' => 1,
            ),
            30 => 
            array (
                'id' => 31,
                'ampher_code' => '1031',
                'ampher_name' => 'เขตบางคอแหลม   ',
                'ampher_name_eng' => 'Khet Bang Kho Laem',
                'geo_id' => 2,
                'province_id' => 1,
            ),
            31 => 
            array (
                'id' => 32,
                'ampher_code' => '1032',
                'ampher_name' => 'เขตประเวศ   ',
                'ampher_name_eng' => 'Khet Prawet',
                'geo_id' => 2,
                'province_id' => 1,
            ),
            32 => 
            array (
                'id' => 33,
                'ampher_code' => '1033',
                'ampher_name' => 'เขตคลองเตย   ',
                'ampher_name_eng' => 'Khet Khlong Toei',
                'geo_id' => 2,
                'province_id' => 1,
            ),
            33 => 
            array (
                'id' => 34,
                'ampher_code' => '1034',
                'ampher_name' => 'เขตสวนหลวง   ',
                'ampher_name_eng' => 'Khet Suan Luang',
                'geo_id' => 2,
                'province_id' => 1,
            ),
            34 => 
            array (
                'id' => 35,
                'ampher_code' => '1035',
                'ampher_name' => 'เขตจอมทอง   ',
                'ampher_name_eng' => 'Khet Chom Thong',
                'geo_id' => 2,
                'province_id' => 1,
            ),
            35 => 
            array (
                'id' => 36,
                'ampher_code' => '1036',
                'ampher_name' => 'เขตดอนเมือง   ',
                'ampher_name_eng' => 'Khet Don Mueang',
                'geo_id' => 2,
                'province_id' => 1,
            ),
            36 => 
            array (
                'id' => 37,
                'ampher_code' => '1037',
                'ampher_name' => 'เขตราชเทวี   ',
                'ampher_name_eng' => 'Khet Ratchathewi',
                'geo_id' => 2,
                'province_id' => 1,
            ),
            37 => 
            array (
                'id' => 38,
                'ampher_code' => '1038',
                'ampher_name' => 'เขตลาดพร้าว   ',
                'ampher_name_eng' => 'Khet Lat Phrao',
                'geo_id' => 2,
                'province_id' => 1,
            ),
            38 => 
            array (
                'id' => 39,
                'ampher_code' => '1039',
                'ampher_name' => 'เขตวัฒนา   ',
                'ampher_name_eng' => 'Khet Watthana',
                'geo_id' => 2,
                'province_id' => 1,
            ),
            39 => 
            array (
                'id' => 40,
                'ampher_code' => '1040',
                'ampher_name' => 'เขตบางแค   ',
                'ampher_name_eng' => 'Khet Bang Khae',
                'geo_id' => 2,
                'province_id' => 1,
            ),
            40 => 
            array (
                'id' => 41,
                'ampher_code' => '1041',
                'ampher_name' => 'เขตหลักสี่   ',
                'ampher_name_eng' => 'Khet Lak Si',
                'geo_id' => 2,
                'province_id' => 1,
            ),
            41 => 
            array (
                'id' => 42,
                'ampher_code' => '1042',
                'ampher_name' => 'เขตสายไหม   ',
                'ampher_name_eng' => 'Khet Sai Mai',
                'geo_id' => 2,
                'province_id' => 1,
            ),
            42 => 
            array (
                'id' => 43,
                'ampher_code' => '1043',
                'ampher_name' => 'เขตคันนายาว   ',
                'ampher_name_eng' => 'Khet Khan Na Yao',
                'geo_id' => 2,
                'province_id' => 1,
            ),
            43 => 
            array (
                'id' => 44,
                'ampher_code' => '1044',
                'ampher_name' => 'เขตสะพานสูง   ',
                'ampher_name_eng' => 'Khet Saphan Sung',
                'geo_id' => 2,
                'province_id' => 1,
            ),
            44 => 
            array (
                'id' => 45,
                'ampher_code' => '1045',
                'ampher_name' => 'เขตวังทองหลาง   ',
                'ampher_name_eng' => 'Khet Wang Thonglang',
                'geo_id' => 2,
                'province_id' => 1,
            ),
            45 => 
            array (
                'id' => 46,
                'ampher_code' => '1046',
                'ampher_name' => 'เขตคลองสามวา   ',
                'ampher_name_eng' => 'Khet Khlong Sam Wa',
                'geo_id' => 2,
                'province_id' => 1,
            ),
            46 => 
            array (
                'id' => 47,
                'ampher_code' => '1047',
                'ampher_name' => 'เขตบางนา   ',
                'ampher_name_eng' => 'Khet Bang Na',
                'geo_id' => 2,
                'province_id' => 1,
            ),
            47 => 
            array (
                'id' => 48,
                'ampher_code' => '1048',
                'ampher_name' => 'เขตทวีวัฒนา   ',
                'ampher_name_eng' => 'Khet Thawi Watthana',
                'geo_id' => 2,
                'province_id' => 1,
            ),
            48 => 
            array (
                'id' => 49,
                'ampher_code' => '1049',
                'ampher_name' => 'เขตทุ่งครุ   ',
                'ampher_name_eng' => 'Khet Thung Khru',
                'geo_id' => 2,
                'province_id' => 1,
            ),
            49 => 
            array (
                'id' => 50,
                'ampher_code' => '1050',
                'ampher_name' => 'เขตบางบอน   ',
                'ampher_name_eng' => 'Khet Bang Bon',
                'geo_id' => 2,
                'province_id' => 1,
            ),
            50 => 
            array (
                'id' => 51,
                'ampher_code' => '1081',
                'ampher_name' => '*บ้านทะวาย   ',
                'ampher_name_eng' => '*Bantawai',
                'geo_id' => 2,
                'province_id' => 1,
            ),
            51 => 
            array (
                'id' => 52,
                'ampher_code' => '1101',
                'ampher_name' => 'เมืองสมุทรปราการ   ',
                'ampher_name_eng' => 'Mueang Samut Prakan',
                'geo_id' => 2,
                'province_id' => 2,
            ),
            52 => 
            array (
                'id' => 53,
                'ampher_code' => '1102',
                'ampher_name' => 'บางบ่อ   ',
                'ampher_name_eng' => 'Bang Bo',
                'geo_id' => 2,
                'province_id' => 2,
            ),
            53 => 
            array (
                'id' => 54,
                'ampher_code' => '1103',
                'ampher_name' => 'บางพลี   ',
                'ampher_name_eng' => 'Bang Phli',
                'geo_id' => 2,
                'province_id' => 2,
            ),
            54 => 
            array (
                'id' => 55,
                'ampher_code' => '1104',
                'ampher_name' => 'พระประแดง   ',
                'ampher_name_eng' => 'Phra Pradaeng',
                'geo_id' => 2,
                'province_id' => 2,
            ),
            55 => 
            array (
                'id' => 56,
                'ampher_code' => '1105',
                'ampher_name' => 'พระสมุทรเจดีย์   ',
                'ampher_name_eng' => 'Phra Samut Chedi',
                'geo_id' => 2,
                'province_id' => 2,
            ),
            56 => 
            array (
                'id' => 57,
                'ampher_code' => '1106',
                'ampher_name' => 'บางเสาธง   ',
                'ampher_name_eng' => 'Bang Sao Thong',
                'geo_id' => 2,
                'province_id' => 2,
            ),
            57 => 
            array (
                'id' => 58,
                'ampher_code' => '1201',
                'ampher_name' => 'เมืองนนทบุรี   ',
                'ampher_name_eng' => 'Mueang Nonthaburi',
                'geo_id' => 2,
                'province_id' => 3,
            ),
            58 => 
            array (
                'id' => 59,
                'ampher_code' => '1202',
                'ampher_name' => 'บางกรวย   ',
                'ampher_name_eng' => 'Bang Kruai',
                'geo_id' => 2,
                'province_id' => 3,
            ),
            59 => 
            array (
                'id' => 60,
                'ampher_code' => '1203',
                'ampher_name' => 'บางใหญ่   ',
                'ampher_name_eng' => 'Bang Yai',
                'geo_id' => 2,
                'province_id' => 3,
            ),
            60 => 
            array (
                'id' => 61,
                'ampher_code' => '1204',
                'ampher_name' => 'บางบัวทอง   ',
                'ampher_name_eng' => 'Bang Bua Thong',
                'geo_id' => 2,
                'province_id' => 3,
            ),
            61 => 
            array (
                'id' => 62,
                'ampher_code' => '1205',
                'ampher_name' => 'ไทรน้อย   ',
                'ampher_name_eng' => 'Sai Noi',
                'geo_id' => 2,
                'province_id' => 3,
            ),
            62 => 
            array (
                'id' => 63,
                'ampher_code' => '1206',
                'ampher_name' => 'ปากเกร็ด   ',
                'ampher_name_eng' => 'Pak Kret',
                'geo_id' => 2,
                'province_id' => 3,
            ),
            63 => 
            array (
                'id' => 64,
                'ampher_code' => '1251',
            'ampher_name' => 'เทศบาลนครนนทบุรี (สาขาแขวงท่าทราย)*   ',
                'ampher_name_eng' => 'Tetsaban Nonthaburi',
                'geo_id' => 2,
                'province_id' => 3,
            ),
            64 => 
            array (
                'id' => 65,
                'ampher_code' => '1297',
                'ampher_name' => 'เทศบาลเมืองปากเกร็ด*   ',
                'ampher_name_eng' => 'Tetsaban Pak Kret',
                'geo_id' => 2,
                'province_id' => 3,
            ),
            65 => 
            array (
                'id' => 66,
                'ampher_code' => '1301',
                'ampher_name' => 'เมืองปทุมธานี   ',
                'ampher_name_eng' => 'Mueang Pathum Thani',
                'geo_id' => 2,
                'province_id' => 4,
            ),
            66 => 
            array (
                'id' => 67,
                'ampher_code' => '1302',
                'ampher_name' => 'คลองหลวง   ',
                'ampher_name_eng' => 'Khlong Luang',
                'geo_id' => 2,
                'province_id' => 4,
            ),
            67 => 
            array (
                'id' => 68,
                'ampher_code' => '1303',
                'ampher_name' => 'ธัญบุรี   ',
                'ampher_name_eng' => 'Thanyaburi',
                'geo_id' => 2,
                'province_id' => 4,
            ),
            68 => 
            array (
                'id' => 69,
                'ampher_code' => '1304',
                'ampher_name' => 'หนองเสือ   ',
                'ampher_name_eng' => 'Nong Suea',
                'geo_id' => 2,
                'province_id' => 4,
            ),
            69 => 
            array (
                'id' => 70,
                'ampher_code' => '1305',
                'ampher_name' => 'ลาดหลุมแก้ว   ',
                'ampher_name_eng' => 'Lat Lum Kaeo',
                'geo_id' => 2,
                'province_id' => 4,
            ),
            70 => 
            array (
                'id' => 71,
                'ampher_code' => '1306',
                'ampher_name' => 'ลำลูกกา   ',
                'ampher_name_eng' => 'Lam Luk Ka',
                'geo_id' => 2,
                'province_id' => 4,
            ),
            71 => 
            array (
                'id' => 72,
                'ampher_code' => '1307',
                'ampher_name' => 'สามโคก   ',
                'ampher_name_eng' => 'Sam Khok',
                'geo_id' => 2,
                'province_id' => 4,
            ),
            72 => 
            array (
                'id' => 73,
                'ampher_code' => '1351',
            'ampher_name' => 'ลำลูกกา (สาขาตำบลคูคต)*   ',
            'ampher_name_eng' => 'Khlong Luang(Kukod)',
                'geo_id' => 2,
                'province_id' => 4,
            ),
            73 => 
            array (
                'id' => 74,
                'ampher_code' => '1401',
                'ampher_name' => 'พระนครศรีอยุธยา   ',
                'ampher_name_eng' => 'Phra Nakhon Si Ayutthaya',
                'geo_id' => 2,
                'province_id' => 5,
            ),
            74 => 
            array (
                'id' => 75,
                'ampher_code' => '1402',
                'ampher_name' => 'ท่าเรือ   ',
                'ampher_name_eng' => 'Tha Ruea',
                'geo_id' => 2,
                'province_id' => 5,
            ),
            75 => 
            array (
                'id' => 76,
                'ampher_code' => '1403',
                'ampher_name' => 'นครหลวง   ',
                'ampher_name_eng' => 'Nakhon Luang',
                'geo_id' => 2,
                'province_id' => 5,
            ),
            76 => 
            array (
                'id' => 77,
                'ampher_code' => '1404',
                'ampher_name' => 'บางไทร   ',
                'ampher_name_eng' => 'Bang Sai',
                'geo_id' => 2,
                'province_id' => 5,
            ),
            77 => 
            array (
                'id' => 78,
                'ampher_code' => '1405',
                'ampher_name' => 'บางบาล   ',
                'ampher_name_eng' => 'Bang Ban',
                'geo_id' => 2,
                'province_id' => 5,
            ),
            78 => 
            array (
                'id' => 79,
                'ampher_code' => '1406',
                'ampher_name' => 'บางปะอิน   ',
                'ampher_name_eng' => 'Bang Pa-in',
                'geo_id' => 2,
                'province_id' => 5,
            ),
            79 => 
            array (
                'id' => 80,
                'ampher_code' => '1407',
                'ampher_name' => 'บางปะหัน   ',
                'ampher_name_eng' => 'Bang Pahan',
                'geo_id' => 2,
                'province_id' => 5,
            ),
            80 => 
            array (
                'id' => 81,
                'ampher_code' => '1408',
                'ampher_name' => 'ผักไห่   ',
                'ampher_name_eng' => 'Phak Hai',
                'geo_id' => 2,
                'province_id' => 5,
            ),
            81 => 
            array (
                'id' => 82,
                'ampher_code' => '1409',
                'ampher_name' => 'ภาชี   ',
                'ampher_name_eng' => 'Phachi',
                'geo_id' => 2,
                'province_id' => 5,
            ),
            82 => 
            array (
                'id' => 83,
                'ampher_code' => '1410',
                'ampher_name' => 'ลาดบัวหลวง   ',
                'ampher_name_eng' => 'Lat Bua Luang',
                'geo_id' => 2,
                'province_id' => 5,
            ),
            83 => 
            array (
                'id' => 84,
                'ampher_code' => '1411',
                'ampher_name' => 'วังน้อย   ',
                'ampher_name_eng' => 'Wang Noi',
                'geo_id' => 2,
                'province_id' => 5,
            ),
            84 => 
            array (
                'id' => 85,
                'ampher_code' => '1412',
                'ampher_name' => 'เสนา   ',
                'ampher_name_eng' => 'Sena',
                'geo_id' => 2,
                'province_id' => 5,
            ),
            85 => 
            array (
                'id' => 86,
                'ampher_code' => '1413',
                'ampher_name' => 'บางซ้าย   ',
                'ampher_name_eng' => 'Bang Sai',
                'geo_id' => 2,
                'province_id' => 5,
            ),
            86 => 
            array (
                'id' => 87,
                'ampher_code' => '1414',
                'ampher_name' => 'อุทัย   ',
                'ampher_name_eng' => 'Uthai',
                'geo_id' => 2,
                'province_id' => 5,
            ),
            87 => 
            array (
                'id' => 88,
                'ampher_code' => '1415',
                'ampher_name' => 'มหาราช   ',
                'ampher_name_eng' => 'Maha Rat',
                'geo_id' => 2,
                'province_id' => 5,
            ),
            88 => 
            array (
                'id' => 89,
                'ampher_code' => '1416',
                'ampher_name' => 'บ้านแพรก   ',
                'ampher_name_eng' => 'Ban Phraek',
                'geo_id' => 2,
                'province_id' => 5,
            ),
            89 => 
            array (
                'id' => 90,
                'ampher_code' => '1501',
                'ampher_name' => 'เมืองอ่างทอง   ',
                'ampher_name_eng' => 'Mueang Ang Thong',
                'geo_id' => 2,
                'province_id' => 6,
            ),
            90 => 
            array (
                'id' => 91,
                'ampher_code' => '1502',
                'ampher_name' => 'ไชโย   ',
                'ampher_name_eng' => 'Chaiyo',
                'geo_id' => 2,
                'province_id' => 6,
            ),
            91 => 
            array (
                'id' => 92,
                'ampher_code' => '1503',
                'ampher_name' => 'ป่าโมก   ',
                'ampher_name_eng' => 'Pa Mok',
                'geo_id' => 2,
                'province_id' => 6,
            ),
            92 => 
            array (
                'id' => 93,
                'ampher_code' => '1504',
                'ampher_name' => 'โพธิ์ทอง   ',
                'ampher_name_eng' => 'Pho Thong',
                'geo_id' => 2,
                'province_id' => 6,
            ),
            93 => 
            array (
                'id' => 94,
                'ampher_code' => '1505',
                'ampher_name' => 'แสวงหา   ',
                'ampher_name_eng' => 'Sawaeng Ha',
                'geo_id' => 2,
                'province_id' => 6,
            ),
            94 => 
            array (
                'id' => 95,
                'ampher_code' => '1506',
                'ampher_name' => 'วิเศษชัยชาญ   ',
                'ampher_name_eng' => 'Wiset Chai Chan',
                'geo_id' => 2,
                'province_id' => 6,
            ),
            95 => 
            array (
                'id' => 96,
                'ampher_code' => '1507',
                'ampher_name' => 'สามโก้   ',
                'ampher_name_eng' => 'Samko',
                'geo_id' => 2,
                'province_id' => 6,
            ),
            96 => 
            array (
                'id' => 97,
                'ampher_code' => '1601',
                'ampher_name' => 'เมืองลพบุรี   ',
                'ampher_name_eng' => 'Mueang Lop Buri',
                'geo_id' => 2,
                'province_id' => 7,
            ),
            97 => 
            array (
                'id' => 98,
                'ampher_code' => '1602',
                'ampher_name' => 'พัฒนานิคม   ',
                'ampher_name_eng' => 'Phatthana Nikhom',
                'geo_id' => 2,
                'province_id' => 7,
            ),
            98 => 
            array (
                'id' => 99,
                'ampher_code' => '1603',
                'ampher_name' => 'โคกสำโรง   ',
                'ampher_name_eng' => 'Khok Samrong',
                'geo_id' => 2,
                'province_id' => 7,
            ),
            99 => 
            array (
                'id' => 100,
                'ampher_code' => '1604',
                'ampher_name' => 'ชัยบาดาล   ',
                'ampher_name_eng' => 'Chai Badan',
                'geo_id' => 2,
                'province_id' => 7,
            ),
            100 => 
            array (
                'id' => 101,
                'ampher_code' => '1605',
                'ampher_name' => 'ท่าวุ้ง   ',
                'ampher_name_eng' => 'Tha Wung',
                'geo_id' => 2,
                'province_id' => 7,
            ),
            101 => 
            array (
                'id' => 102,
                'ampher_code' => '1606',
                'ampher_name' => 'บ้านหมี่   ',
                'ampher_name_eng' => 'Ban Mi',
                'geo_id' => 2,
                'province_id' => 7,
            ),
            102 => 
            array (
                'id' => 103,
                'ampher_code' => '1607',
                'ampher_name' => 'ท่าหลวง   ',
                'ampher_name_eng' => 'Tha Luang',
                'geo_id' => 2,
                'province_id' => 7,
            ),
            103 => 
            array (
                'id' => 104,
                'ampher_code' => '1608',
                'ampher_name' => 'สระโบสถ์   ',
                'ampher_name_eng' => 'Sa Bot',
                'geo_id' => 2,
                'province_id' => 7,
            ),
            104 => 
            array (
                'id' => 105,
                'ampher_code' => '1609',
                'ampher_name' => 'โคกเจริญ   ',
                'ampher_name_eng' => 'Khok Charoen',
                'geo_id' => 2,
                'province_id' => 7,
            ),
            105 => 
            array (
                'id' => 106,
                'ampher_code' => '1610',
                'ampher_name' => 'ลำสนธิ   ',
                'ampher_name_eng' => 'Lam Sonthi',
                'geo_id' => 2,
                'province_id' => 7,
            ),
            106 => 
            array (
                'id' => 107,
                'ampher_code' => '1611',
                'ampher_name' => 'หนองม่วง   ',
                'ampher_name_eng' => 'Nong Muang',
                'geo_id' => 2,
                'province_id' => 7,
            ),
            107 => 
            array (
                'id' => 108,
                'ampher_code' => '1681',
                'ampher_name' => '*อ.บ้านเช่า  จ.ลพบุรี   ',
                'ampher_name_eng' => '*Amphoe Ban Chao',
                'geo_id' => 2,
                'province_id' => 7,
            ),
            108 => 
            array (
                'id' => 109,
                'ampher_code' => '1701',
                'ampher_name' => 'เมืองสิงห์บุรี   ',
                'ampher_name_eng' => 'Mueang Sing Buri',
                'geo_id' => 2,
                'province_id' => 8,
            ),
            109 => 
            array (
                'id' => 110,
                'ampher_code' => '1702',
                'ampher_name' => 'บางระจัน   ',
                'ampher_name_eng' => 'Bang Rachan',
                'geo_id' => 2,
                'province_id' => 8,
            ),
            110 => 
            array (
                'id' => 111,
                'ampher_code' => '1703',
                'ampher_name' => 'ค่ายบางระจัน   ',
                'ampher_name_eng' => 'Khai Bang Rachan',
                'geo_id' => 2,
                'province_id' => 8,
            ),
            111 => 
            array (
                'id' => 112,
                'ampher_code' => '1704',
                'ampher_name' => 'พรหมบุรี   ',
                'ampher_name_eng' => 'Phrom Buri',
                'geo_id' => 2,
                'province_id' => 8,
            ),
            112 => 
            array (
                'id' => 113,
                'ampher_code' => '1705',
                'ampher_name' => 'ท่าช้าง   ',
                'ampher_name_eng' => 'Tha Chang',
                'geo_id' => 2,
                'province_id' => 8,
            ),
            113 => 
            array (
                'id' => 114,
                'ampher_code' => '1706',
                'ampher_name' => 'อินทร์บุรี   ',
                'ampher_name_eng' => 'In Buri',
                'geo_id' => 2,
                'province_id' => 8,
            ),
            114 => 
            array (
                'id' => 115,
                'ampher_code' => '1801',
                'ampher_name' => 'เมืองชัยนาท   ',
                'ampher_name_eng' => 'Mueang Chai Nat',
                'geo_id' => 2,
                'province_id' => 9,
            ),
            115 => 
            array (
                'id' => 116,
                'ampher_code' => '1802',
                'ampher_name' => 'มโนรมย์   ',
                'ampher_name_eng' => 'Manorom',
                'geo_id' => 2,
                'province_id' => 9,
            ),
            116 => 
            array (
                'id' => 117,
                'ampher_code' => '1803',
                'ampher_name' => 'วัดสิงห์   ',
                'ampher_name_eng' => 'Wat Sing',
                'geo_id' => 2,
                'province_id' => 9,
            ),
            117 => 
            array (
                'id' => 118,
                'ampher_code' => '1804',
                'ampher_name' => 'สรรพยา   ',
                'ampher_name_eng' => 'Sapphaya',
                'geo_id' => 2,
                'province_id' => 9,
            ),
            118 => 
            array (
                'id' => 119,
                'ampher_code' => '1805',
                'ampher_name' => 'สรรคบุรี   ',
                'ampher_name_eng' => 'Sankhaburi',
                'geo_id' => 2,
                'province_id' => 9,
            ),
            119 => 
            array (
                'id' => 120,
                'ampher_code' => '1806',
                'ampher_name' => 'หันคา   ',
                'ampher_name_eng' => 'Hankha',
                'geo_id' => 2,
                'province_id' => 9,
            ),
            120 => 
            array (
                'id' => 121,
                'ampher_code' => '1807',
                'ampher_name' => 'หนองมะโมง   ',
                'ampher_name_eng' => 'Nong Mamong',
                'geo_id' => 2,
                'province_id' => 9,
            ),
            121 => 
            array (
                'id' => 122,
                'ampher_code' => '1808',
                'ampher_name' => 'เนินขาม   ',
                'ampher_name_eng' => 'Noen Kham',
                'geo_id' => 2,
                'province_id' => 9,
            ),
            122 => 
            array (
                'id' => 123,
                'ampher_code' => '1901',
                'ampher_name' => 'เมืองสระบุรี   ',
                'ampher_name_eng' => 'Mueang Saraburi',
                'geo_id' => 2,
                'province_id' => 10,
            ),
            123 => 
            array (
                'id' => 124,
                'ampher_code' => '1902',
                'ampher_name' => 'แก่งคอย   ',
                'ampher_name_eng' => 'Kaeng Khoi',
                'geo_id' => 2,
                'province_id' => 10,
            ),
            124 => 
            array (
                'id' => 125,
                'ampher_code' => '1903',
                'ampher_name' => 'หนองแค   ',
                'ampher_name_eng' => 'Nong Khae',
                'geo_id' => 2,
                'province_id' => 10,
            ),
            125 => 
            array (
                'id' => 126,
                'ampher_code' => '1904',
                'ampher_name' => 'วิหารแดง   ',
                'ampher_name_eng' => 'Wihan Daeng',
                'geo_id' => 2,
                'province_id' => 10,
            ),
            126 => 
            array (
                'id' => 127,
                'ampher_code' => '1905',
                'ampher_name' => 'หนองแซง   ',
                'ampher_name_eng' => 'Nong Saeng',
                'geo_id' => 2,
                'province_id' => 10,
            ),
            127 => 
            array (
                'id' => 128,
                'ampher_code' => '1906',
                'ampher_name' => 'บ้านหมอ   ',
                'ampher_name_eng' => 'Ban Mo',
                'geo_id' => 2,
                'province_id' => 10,
            ),
            128 => 
            array (
                'id' => 129,
                'ampher_code' => '1907',
                'ampher_name' => 'ดอนพุด   ',
                'ampher_name_eng' => 'Don Phut',
                'geo_id' => 2,
                'province_id' => 10,
            ),
            129 => 
            array (
                'id' => 130,
                'ampher_code' => '1908',
                'ampher_name' => 'หนองโดน   ',
                'ampher_name_eng' => 'Nong Don',
                'geo_id' => 2,
                'province_id' => 10,
            ),
            130 => 
            array (
                'id' => 131,
                'ampher_code' => '1909',
                'ampher_name' => 'พระพุทธบาท   ',
                'ampher_name_eng' => 'Phra Phutthabat',
                'geo_id' => 2,
                'province_id' => 10,
            ),
            131 => 
            array (
                'id' => 132,
                'ampher_code' => '1910',
                'ampher_name' => 'เสาไห้   ',
                'ampher_name_eng' => 'Sao Hai',
                'geo_id' => 2,
                'province_id' => 10,
            ),
            132 => 
            array (
                'id' => 133,
                'ampher_code' => '1911',
                'ampher_name' => 'มวกเหล็ก   ',
                'ampher_name_eng' => 'Muak Lek',
                'geo_id' => 2,
                'province_id' => 10,
            ),
            133 => 
            array (
                'id' => 134,
                'ampher_code' => '1912',
                'ampher_name' => 'วังม่วง   ',
                'ampher_name_eng' => 'Wang Muang',
                'geo_id' => 2,
                'province_id' => 10,
            ),
            134 => 
            array (
                'id' => 135,
                'ampher_code' => '1913',
                'ampher_name' => 'เฉลิมพระเกียรติ   ',
                'ampher_name_eng' => 'Chaloem Phra Kiat',
                'geo_id' => 2,
                'province_id' => 10,
            ),
            135 => 
            array (
                'id' => 136,
                'ampher_code' => '2001',
                'ampher_name' => 'เมืองชลบุรี   ',
                'ampher_name_eng' => 'Mueang Chon Buri',
                'geo_id' => 5,
                'province_id' => 11,
            ),
            136 => 
            array (
                'id' => 137,
                'ampher_code' => '2002',
                'ampher_name' => 'บ้านบึง   ',
                'ampher_name_eng' => 'Ban Bueng',
                'geo_id' => 5,
                'province_id' => 11,
            ),
            137 => 
            array (
                'id' => 138,
                'ampher_code' => '2003',
                'ampher_name' => 'หนองใหญ่   ',
                'ampher_name_eng' => 'Nong Yai',
                'geo_id' => 5,
                'province_id' => 11,
            ),
            138 => 
            array (
                'id' => 139,
                'ampher_code' => '2004',
                'ampher_name' => 'บางละมุง   ',
                'ampher_name_eng' => 'Bang Lamung',
                'geo_id' => 5,
                'province_id' => 11,
            ),
            139 => 
            array (
                'id' => 140,
                'ampher_code' => '2005',
                'ampher_name' => 'พานทอง   ',
                'ampher_name_eng' => 'Phan Thong',
                'geo_id' => 5,
                'province_id' => 11,
            ),
            140 => 
            array (
                'id' => 141,
                'ampher_code' => '2006',
                'ampher_name' => 'พนัสนิคม   ',
                'ampher_name_eng' => 'Phanat Nikhom',
                'geo_id' => 5,
                'province_id' => 11,
            ),
            141 => 
            array (
                'id' => 142,
                'ampher_code' => '2007',
                'ampher_name' => 'ศรีราชา   ',
                'ampher_name_eng' => 'Si Racha',
                'geo_id' => 5,
                'province_id' => 11,
            ),
            142 => 
            array (
                'id' => 143,
                'ampher_code' => '2008',
                'ampher_name' => 'เกาะสีชัง   ',
                'ampher_name_eng' => 'Ko Sichang',
                'geo_id' => 5,
                'province_id' => 11,
            ),
            143 => 
            array (
                'id' => 144,
                'ampher_code' => '2009',
                'ampher_name' => 'สัตหีบ   ',
                'ampher_name_eng' => 'Sattahip',
                'geo_id' => 5,
                'province_id' => 11,
            ),
            144 => 
            array (
                'id' => 145,
                'ampher_code' => '2010',
                'ampher_name' => 'บ่อทอง   ',
                'ampher_name_eng' => 'Bo Thong',
                'geo_id' => 5,
                'province_id' => 11,
            ),
            145 => 
            array (
                'id' => 146,
                'ampher_code' => '2011',
                'ampher_name' => 'เกาะจันทร์   ',
                'ampher_name_eng' => 'Ko Chan',
                'geo_id' => 5,
                'province_id' => 11,
            ),
            146 => 
            array (
                'id' => 147,
                'ampher_code' => '2051',
            'ampher_name' => 'สัตหีบ (สาขาตำบลบางเสร่)*   ',
            'ampher_name_eng' => 'Sattahip(Bang Sre)*',
                'geo_id' => 5,
                'province_id' => 11,
            ),
            147 => 
            array (
                'id' => 148,
                'ampher_code' => '2072',
                'ampher_name' => 'ท้องถิ่นเทศบาลเมืองหนองปรือ*   ',
                'ampher_name_eng' => 'Tong Tin Tetsaban Mueang Nong Prue*',
                'geo_id' => 5,
                'province_id' => 11,
            ),
            148 => 
            array (
                'id' => 149,
                'ampher_code' => '2093',
                'ampher_name' => 'เทศบาลตำบลแหลมฉบัง*   ',
                'ampher_name_eng' => 'Tetsaban Tambon Lamsabang*',
                'geo_id' => 5,
                'province_id' => 11,
            ),
            149 => 
            array (
                'id' => 150,
                'ampher_code' => '2099',
                'ampher_name' => 'เทศบาลเมืองชลบุรี*   ',
                'ampher_name_eng' => 'Mueang Chon Buri',
                'geo_id' => 5,
                'province_id' => 11,
            ),
            150 => 
            array (
                'id' => 151,
                'ampher_code' => '2101',
                'ampher_name' => 'เมืองระยอง   ',
                'ampher_name_eng' => 'Mueang Rayong',
                'geo_id' => 5,
                'province_id' => 12,
            ),
            151 => 
            array (
                'id' => 152,
                'ampher_code' => '2102',
                'ampher_name' => 'บ้านฉาง   ',
                'ampher_name_eng' => 'Ban Chang',
                'geo_id' => 5,
                'province_id' => 12,
            ),
            152 => 
            array (
                'id' => 153,
                'ampher_code' => '2103',
                'ampher_name' => 'แกลง   ',
                'ampher_name_eng' => 'Klaeng',
                'geo_id' => 5,
                'province_id' => 12,
            ),
            153 => 
            array (
                'id' => 154,
                'ampher_code' => '2104',
                'ampher_name' => 'วังจันทร์   ',
                'ampher_name_eng' => 'Wang Chan',
                'geo_id' => 5,
                'province_id' => 12,
            ),
            154 => 
            array (
                'id' => 155,
                'ampher_code' => '2105',
                'ampher_name' => 'บ้านค่าย   ',
                'ampher_name_eng' => 'Ban Khai',
                'geo_id' => 5,
                'province_id' => 12,
            ),
            155 => 
            array (
                'id' => 156,
                'ampher_code' => '2106',
                'ampher_name' => 'ปลวกแดง   ',
                'ampher_name_eng' => 'Pluak Daeng',
                'geo_id' => 5,
                'province_id' => 12,
            ),
            156 => 
            array (
                'id' => 157,
                'ampher_code' => '2107',
                'ampher_name' => 'เขาชะเมา   ',
                'ampher_name_eng' => 'Khao Chamao',
                'geo_id' => 5,
                'province_id' => 12,
            ),
            157 => 
            array (
                'id' => 158,
                'ampher_code' => '2108',
                'ampher_name' => 'นิคมพัฒนา   ',
                'ampher_name_eng' => 'Nikhom Phatthana',
                'geo_id' => 5,
                'province_id' => 12,
            ),
            158 => 
            array (
                'id' => 159,
                'ampher_code' => '2151',
                'ampher_name' => 'สาขาตำบลมาบข่า*   ',
                'ampher_name_eng' => 'Saka Tambon Mabka',
                'geo_id' => 5,
                'province_id' => 12,
            ),
            159 => 
            array (
                'id' => 160,
                'ampher_code' => '2201',
                'ampher_name' => 'เมืองจันทบุรี   ',
                'ampher_name_eng' => 'Mueang Chanthaburi',
                'geo_id' => 5,
                'province_id' => 13,
            ),
            160 => 
            array (
                'id' => 161,
                'ampher_code' => '2202',
                'ampher_name' => 'ขลุง   ',
                'ampher_name_eng' => 'Khlung',
                'geo_id' => 5,
                'province_id' => 13,
            ),
            161 => 
            array (
                'id' => 162,
                'ampher_code' => '2203',
                'ampher_name' => 'ท่าใหม่   ',
                'ampher_name_eng' => 'Tha Mai',
                'geo_id' => 5,
                'province_id' => 13,
            ),
            162 => 
            array (
                'id' => 163,
                'ampher_code' => '2204',
                'ampher_name' => 'โป่งน้ำร้อน   ',
                'ampher_name_eng' => 'Pong Nam Ron',
                'geo_id' => 5,
                'province_id' => 13,
            ),
            163 => 
            array (
                'id' => 164,
                'ampher_code' => '2205',
                'ampher_name' => 'มะขาม   ',
                'ampher_name_eng' => 'Makham',
                'geo_id' => 5,
                'province_id' => 13,
            ),
            164 => 
            array (
                'id' => 165,
                'ampher_code' => '2206',
                'ampher_name' => 'แหลมสิงห์   ',
                'ampher_name_eng' => 'Laem Sing',
                'geo_id' => 5,
                'province_id' => 13,
            ),
            165 => 
            array (
                'id' => 166,
                'ampher_code' => '2207',
                'ampher_name' => 'สอยดาว   ',
                'ampher_name_eng' => 'Soi Dao',
                'geo_id' => 5,
                'province_id' => 13,
            ),
            166 => 
            array (
                'id' => 167,
                'ampher_code' => '2208',
                'ampher_name' => 'แก่งหางแมว   ',
                'ampher_name_eng' => 'Kaeng Hang Maeo',
                'geo_id' => 5,
                'province_id' => 13,
            ),
            167 => 
            array (
                'id' => 168,
                'ampher_code' => '2209',
                'ampher_name' => 'นายายอาม   ',
                'ampher_name_eng' => 'Na Yai Am',
                'geo_id' => 5,
                'province_id' => 13,
            ),
            168 => 
            array (
                'id' => 169,
                'ampher_code' => '2210',
                'ampher_name' => 'เขาคิชฌกูฏ   ',
                'ampher_name_eng' => 'Khoa Khitchakut',
                'geo_id' => 5,
                'province_id' => 13,
            ),
            169 => 
            array (
                'id' => 170,
                'ampher_code' => '2281',
                'ampher_name' => '*กิ่ง อ.กำพุธ  จ.จันทบุรี   ',
                'ampher_name_eng' => '*King Amphoe Kampud',
                'geo_id' => 5,
                'province_id' => 13,
            ),
            170 => 
            array (
                'id' => 171,
                'ampher_code' => '2301',
                'ampher_name' => 'เมืองตราด   ',
                'ampher_name_eng' => 'Mueang Trat',
                'geo_id' => 5,
                'province_id' => 14,
            ),
            171 => 
            array (
                'id' => 172,
                'ampher_code' => '2302',
                'ampher_name' => 'คลองใหญ่   ',
                'ampher_name_eng' => 'Khlong Yai',
                'geo_id' => 5,
                'province_id' => 14,
            ),
            172 => 
            array (
                'id' => 173,
                'ampher_code' => '2303',
                'ampher_name' => 'เขาสมิง   ',
                'ampher_name_eng' => 'Khao Saming',
                'geo_id' => 5,
                'province_id' => 14,
            ),
            173 => 
            array (
                'id' => 174,
                'ampher_code' => '2304',
                'ampher_name' => 'บ่อไร่   ',
                'ampher_name_eng' => 'Bo Rai',
                'geo_id' => 5,
                'province_id' => 14,
            ),
            174 => 
            array (
                'id' => 175,
                'ampher_code' => '2305',
                'ampher_name' => 'แหลมงอบ   ',
                'ampher_name_eng' => 'Laem Ngop',
                'geo_id' => 5,
                'province_id' => 14,
            ),
            175 => 
            array (
                'id' => 176,
                'ampher_code' => '2306',
                'ampher_name' => 'เกาะกูด   ',
                'ampher_name_eng' => 'Ko Kut',
                'geo_id' => 5,
                'province_id' => 14,
            ),
            176 => 
            array (
                'id' => 177,
                'ampher_code' => '2307',
                'ampher_name' => 'เกาะช้าง   ',
                'ampher_name_eng' => 'Ko Chang',
                'geo_id' => 5,
                'province_id' => 14,
            ),
            177 => 
            array (
                'id' => 178,
                'ampher_code' => '2401',
                'ampher_name' => 'เมืองฉะเชิงเทรา   ',
                'ampher_name_eng' => 'Mueang Chachoengsao',
                'geo_id' => 5,
                'province_id' => 15,
            ),
            178 => 
            array (
                'id' => 179,
                'ampher_code' => '2402',
                'ampher_name' => 'บางคล้า   ',
                'ampher_name_eng' => 'Bang Khla',
                'geo_id' => 5,
                'province_id' => 15,
            ),
            179 => 
            array (
                'id' => 180,
                'ampher_code' => '2403',
                'ampher_name' => 'บางน้ำเปรี้ยว   ',
                'ampher_name_eng' => 'Bang Nam Priao',
                'geo_id' => 5,
                'province_id' => 15,
            ),
            180 => 
            array (
                'id' => 181,
                'ampher_code' => '2404',
                'ampher_name' => 'บางปะกง   ',
                'ampher_name_eng' => 'Bang Pakong',
                'geo_id' => 5,
                'province_id' => 15,
            ),
            181 => 
            array (
                'id' => 182,
                'ampher_code' => '2405',
                'ampher_name' => 'บ้านโพธิ์   ',
                'ampher_name_eng' => 'Ban Pho',
                'geo_id' => 5,
                'province_id' => 15,
            ),
            182 => 
            array (
                'id' => 183,
                'ampher_code' => '2406',
                'ampher_name' => 'พนมสารคาม   ',
                'ampher_name_eng' => 'Phanom Sarakham',
                'geo_id' => 5,
                'province_id' => 15,
            ),
            183 => 
            array (
                'id' => 184,
                'ampher_code' => '2407',
                'ampher_name' => 'ราชสาส์น   ',
                'ampher_name_eng' => 'Ratchasan',
                'geo_id' => 5,
                'province_id' => 15,
            ),
            184 => 
            array (
                'id' => 185,
                'ampher_code' => '2408',
                'ampher_name' => 'สนามชัยเขต   ',
                'ampher_name_eng' => 'Sanam Chai Khet',
                'geo_id' => 5,
                'province_id' => 15,
            ),
            185 => 
            array (
                'id' => 186,
                'ampher_code' => '2409',
                'ampher_name' => 'แปลงยาว   ',
                'ampher_name_eng' => 'Plaeng Yao',
                'geo_id' => 5,
                'province_id' => 15,
            ),
            186 => 
            array (
                'id' => 187,
                'ampher_code' => '2410',
                'ampher_name' => 'ท่าตะเกียบ   ',
                'ampher_name_eng' => 'Tha Takiap',
                'geo_id' => 5,
                'province_id' => 15,
            ),
            187 => 
            array (
                'id' => 188,
                'ampher_code' => '2411',
                'ampher_name' => 'คลองเขื่อน   ',
                'ampher_name_eng' => 'Khlong Khuean',
                'geo_id' => 5,
                'province_id' => 15,
            ),
            188 => 
            array (
                'id' => 189,
                'ampher_code' => '2501',
                'ampher_name' => 'เมืองปราจีนบุรี   ',
                'ampher_name_eng' => 'Mueang Prachin Buri',
                'geo_id' => 5,
                'province_id' => 16,
            ),
            189 => 
            array (
                'id' => 190,
                'ampher_code' => '2502',
                'ampher_name' => 'กบินทร์บุรี   ',
                'ampher_name_eng' => 'Kabin Buri',
                'geo_id' => 5,
                'province_id' => 16,
            ),
            190 => 
            array (
                'id' => 191,
                'ampher_code' => '2503',
                'ampher_name' => 'นาดี   ',
                'ampher_name_eng' => 'Na Di',
                'geo_id' => 5,
                'province_id' => 16,
            ),
            191 => 
            array (
                'id' => 192,
                'ampher_code' => '2504',
                'ampher_name' => '*สระแก้ว   ',
                'ampher_name_eng' => 'Sa Kaeo',
                'geo_id' => 5,
                'province_id' => 16,
            ),
            192 => 
            array (
                'id' => 193,
                'ampher_code' => '2505',
                'ampher_name' => '*วังน้ำเย็น   ',
                'ampher_name_eng' => 'Wang Nam Yen',
                'geo_id' => 5,
                'province_id' => 16,
            ),
            193 => 
            array (
                'id' => 194,
                'ampher_code' => '2506',
                'ampher_name' => 'บ้านสร้าง   ',
                'ampher_name_eng' => 'Ban Sang',
                'geo_id' => 5,
                'province_id' => 16,
            ),
            194 => 
            array (
                'id' => 195,
                'ampher_code' => '2507',
                'ampher_name' => 'ประจันตคาม   ',
                'ampher_name_eng' => 'Prachantakham',
                'geo_id' => 5,
                'province_id' => 16,
            ),
            195 => 
            array (
                'id' => 196,
                'ampher_code' => '2508',
                'ampher_name' => 'ศรีมหาโพธิ   ',
                'ampher_name_eng' => 'Si Maha Phot',
                'geo_id' => 5,
                'province_id' => 16,
            ),
            196 => 
            array (
                'id' => 197,
                'ampher_code' => '2509',
                'ampher_name' => 'ศรีมโหสถ   ',
                'ampher_name_eng' => 'Si Mahosot',
                'geo_id' => 5,
                'province_id' => 16,
            ),
            197 => 
            array (
                'id' => 198,
                'ampher_code' => '2510',
                'ampher_name' => '*อรัญประเทศ   ',
                'ampher_name_eng' => 'Aranyaprathet',
                'geo_id' => 5,
                'province_id' => 16,
            ),
            198 => 
            array (
                'id' => 199,
                'ampher_code' => '2511',
                'ampher_name' => '*ตาพระยา   ',
                'ampher_name_eng' => 'Ta Phraya',
                'geo_id' => 5,
                'province_id' => 16,
            ),
            199 => 
            array (
                'id' => 200,
                'ampher_code' => '2512',
                'ampher_name' => '*วัฒนานคร   ',
                'ampher_name_eng' => 'Watthana Nakhon',
                'geo_id' => 5,
                'province_id' => 16,
            ),
            200 => 
            array (
                'id' => 201,
                'ampher_code' => '2513',
                'ampher_name' => '*คลองหาด   ',
                'ampher_name_eng' => 'Khlong Hat',
                'geo_id' => 5,
                'province_id' => 16,
            ),
            201 => 
            array (
                'id' => 202,
                'ampher_code' => '2601',
                'ampher_name' => 'เมืองนครนายก   ',
                'ampher_name_eng' => 'Mueang Nakhon Nayok',
                'geo_id' => 2,
                'province_id' => 17,
            ),
            202 => 
            array (
                'id' => 203,
                'ampher_code' => '2602',
                'ampher_name' => 'ปากพลี   ',
                'ampher_name_eng' => 'Pak Phli',
                'geo_id' => 2,
                'province_id' => 17,
            ),
            203 => 
            array (
                'id' => 204,
                'ampher_code' => '2603',
                'ampher_name' => 'บ้านนา   ',
                'ampher_name_eng' => 'Ban Na',
                'geo_id' => 2,
                'province_id' => 17,
            ),
            204 => 
            array (
                'id' => 205,
                'ampher_code' => '2604',
                'ampher_name' => 'องครักษ์   ',
                'ampher_name_eng' => 'Ongkharak',
                'geo_id' => 2,
                'province_id' => 17,
            ),
            205 => 
            array (
                'id' => 206,
                'ampher_code' => '2701',
                'ampher_name' => 'เมืองสระแก้ว   ',
                'ampher_name_eng' => 'Mueang Sa Kaeo',
                'geo_id' => 5,
                'province_id' => 18,
            ),
            206 => 
            array (
                'id' => 207,
                'ampher_code' => '2702',
                'ampher_name' => 'คลองหาด   ',
                'ampher_name_eng' => 'Khlong Hat',
                'geo_id' => 5,
                'province_id' => 18,
            ),
            207 => 
            array (
                'id' => 208,
                'ampher_code' => '2703',
                'ampher_name' => 'ตาพระยา   ',
                'ampher_name_eng' => 'Ta Phraya',
                'geo_id' => 5,
                'province_id' => 18,
            ),
            208 => 
            array (
                'id' => 209,
                'ampher_code' => '2704',
                'ampher_name' => 'วังน้ำเย็น   ',
                'ampher_name_eng' => 'Wang Nam Yen',
                'geo_id' => 5,
                'province_id' => 18,
            ),
            209 => 
            array (
                'id' => 210,
                'ampher_code' => '2705',
                'ampher_name' => 'วัฒนานคร   ',
                'ampher_name_eng' => 'Watthana Nakhon',
                'geo_id' => 5,
                'province_id' => 18,
            ),
            210 => 
            array (
                'id' => 211,
                'ampher_code' => '2706',
                'ampher_name' => 'อรัญประเทศ   ',
                'ampher_name_eng' => 'Aranyaprathet',
                'geo_id' => 5,
                'province_id' => 18,
            ),
            211 => 
            array (
                'id' => 212,
                'ampher_code' => '2707',
                'ampher_name' => 'เขาฉกรรจ์   ',
                'ampher_name_eng' => 'Khao Chakan',
                'geo_id' => 5,
                'province_id' => 18,
            ),
            212 => 
            array (
                'id' => 213,
                'ampher_code' => '2708',
                'ampher_name' => 'โคกสูง   ',
                'ampher_name_eng' => 'Khok Sung',
                'geo_id' => 5,
                'province_id' => 18,
            ),
            213 => 
            array (
                'id' => 214,
                'ampher_code' => '2709',
                'ampher_name' => 'วังสมบูรณ์   ',
                'ampher_name_eng' => 'Wang Sombun',
                'geo_id' => 5,
                'province_id' => 18,
            ),
            214 => 
            array (
                'id' => 215,
                'ampher_code' => '3001',
                'ampher_name' => 'เมืองนครราชสีมา   ',
                'ampher_name_eng' => 'Mueang Nakhon Ratchasima',
                'geo_id' => 3,
                'province_id' => 19,
            ),
            215 => 
            array (
                'id' => 216,
                'ampher_code' => '3002',
                'ampher_name' => 'ครบุรี   ',
                'ampher_name_eng' => 'Khon Buri',
                'geo_id' => 3,
                'province_id' => 19,
            ),
            216 => 
            array (
                'id' => 217,
                'ampher_code' => '3003',
                'ampher_name' => 'เสิงสาง   ',
                'ampher_name_eng' => 'Soeng Sang',
                'geo_id' => 3,
                'province_id' => 19,
            ),
            217 => 
            array (
                'id' => 218,
                'ampher_code' => '3004',
                'ampher_name' => 'คง   ',
                'ampher_name_eng' => 'Khong',
                'geo_id' => 3,
                'province_id' => 19,
            ),
            218 => 
            array (
                'id' => 219,
                'ampher_code' => '3005',
                'ampher_name' => 'บ้านเหลื่อม   ',
                'ampher_name_eng' => 'Ban Lueam',
                'geo_id' => 3,
                'province_id' => 19,
            ),
            219 => 
            array (
                'id' => 220,
                'ampher_code' => '3006',
                'ampher_name' => 'จักราช   ',
                'ampher_name_eng' => 'Chakkarat',
                'geo_id' => 3,
                'province_id' => 19,
            ),
            220 => 
            array (
                'id' => 221,
                'ampher_code' => '3007',
                'ampher_name' => 'โชคชัย   ',
                'ampher_name_eng' => 'Chok Chai',
                'geo_id' => 3,
                'province_id' => 19,
            ),
            221 => 
            array (
                'id' => 222,
                'ampher_code' => '3008',
                'ampher_name' => 'ด่านขุนทด   ',
                'ampher_name_eng' => 'Dan Khun Thot',
                'geo_id' => 3,
                'province_id' => 19,
            ),
            222 => 
            array (
                'id' => 223,
                'ampher_code' => '3009',
                'ampher_name' => 'โนนไทย   ',
                'ampher_name_eng' => 'Non Thai',
                'geo_id' => 3,
                'province_id' => 19,
            ),
            223 => 
            array (
                'id' => 224,
                'ampher_code' => '3010',
                'ampher_name' => 'โนนสูง   ',
                'ampher_name_eng' => 'Non Sung',
                'geo_id' => 3,
                'province_id' => 19,
            ),
            224 => 
            array (
                'id' => 225,
                'ampher_code' => '3011',
                'ampher_name' => 'ขามสะแกแสง   ',
                'ampher_name_eng' => 'Kham Sakaesaeng',
                'geo_id' => 3,
                'province_id' => 19,
            ),
            225 => 
            array (
                'id' => 226,
                'ampher_code' => '3012',
                'ampher_name' => 'บัวใหญ่   ',
                'ampher_name_eng' => 'Bua Yai',
                'geo_id' => 3,
                'province_id' => 19,
            ),
            226 => 
            array (
                'id' => 227,
                'ampher_code' => '3013',
                'ampher_name' => 'ประทาย   ',
                'ampher_name_eng' => 'Prathai',
                'geo_id' => 3,
                'province_id' => 19,
            ),
            227 => 
            array (
                'id' => 228,
                'ampher_code' => '3014',
                'ampher_name' => 'ปักธงชัย   ',
                'ampher_name_eng' => 'Pak Thong Chai',
                'geo_id' => 3,
                'province_id' => 19,
            ),
            228 => 
            array (
                'id' => 229,
                'ampher_code' => '3015',
                'ampher_name' => 'พิมาย   ',
                'ampher_name_eng' => 'Phimai',
                'geo_id' => 3,
                'province_id' => 19,
            ),
            229 => 
            array (
                'id' => 230,
                'ampher_code' => '3016',
                'ampher_name' => 'ห้วยแถลง   ',
                'ampher_name_eng' => 'Huai Thalaeng',
                'geo_id' => 3,
                'province_id' => 19,
            ),
            230 => 
            array (
                'id' => 231,
                'ampher_code' => '3017',
                'ampher_name' => 'ชุมพวง   ',
                'ampher_name_eng' => 'Chum Phuang',
                'geo_id' => 3,
                'province_id' => 19,
            ),
            231 => 
            array (
                'id' => 232,
                'ampher_code' => '3018',
                'ampher_name' => 'สูงเนิน   ',
                'ampher_name_eng' => 'Sung Noen',
                'geo_id' => 3,
                'province_id' => 19,
            ),
            232 => 
            array (
                'id' => 233,
                'ampher_code' => '3019',
                'ampher_name' => 'ขามทะเลสอ   ',
                'ampher_name_eng' => 'Kham Thale So',
                'geo_id' => 3,
                'province_id' => 19,
            ),
            233 => 
            array (
                'id' => 234,
                'ampher_code' => '3020',
                'ampher_name' => 'สีคิ้ว   ',
                'ampher_name_eng' => 'Sikhio',
                'geo_id' => 3,
                'province_id' => 19,
            ),
            234 => 
            array (
                'id' => 235,
                'ampher_code' => '3021',
                'ampher_name' => 'ปากช่อง   ',
                'ampher_name_eng' => 'Pak Chong',
                'geo_id' => 3,
                'province_id' => 19,
            ),
            235 => 
            array (
                'id' => 236,
                'ampher_code' => '3022',
                'ampher_name' => 'หนองบุญมาก   ',
                'ampher_name_eng' => 'Nong Bunnak',
                'geo_id' => 3,
                'province_id' => 19,
            ),
            236 => 
            array (
                'id' => 237,
                'ampher_code' => '3023',
                'ampher_name' => 'แก้งสนามนาง   ',
                'ampher_name_eng' => 'Kaeng Sanam Nang',
                'geo_id' => 3,
                'province_id' => 19,
            ),
            237 => 
            array (
                'id' => 238,
                'ampher_code' => '3024',
                'ampher_name' => 'โนนแดง   ',
                'ampher_name_eng' => 'Non Daeng',
                'geo_id' => 3,
                'province_id' => 19,
            ),
            238 => 
            array (
                'id' => 239,
                'ampher_code' => '3025',
                'ampher_name' => 'วังน้ำเขียว   ',
                'ampher_name_eng' => 'Wang Nam Khiao',
                'geo_id' => 3,
                'province_id' => 19,
            ),
            239 => 
            array (
                'id' => 240,
                'ampher_code' => '3026',
                'ampher_name' => 'เทพารักษ์   ',
                'ampher_name_eng' => 'Thepharak',
                'geo_id' => 3,
                'province_id' => 19,
            ),
            240 => 
            array (
                'id' => 241,
                'ampher_code' => '3027',
                'ampher_name' => 'เมืองยาง   ',
                'ampher_name_eng' => 'Mueang Yang',
                'geo_id' => 3,
                'province_id' => 19,
            ),
            241 => 
            array (
                'id' => 242,
                'ampher_code' => '3028',
                'ampher_name' => 'พระทองคำ   ',
                'ampher_name_eng' => 'Phra Thong Kham',
                'geo_id' => 3,
                'province_id' => 19,
            ),
            242 => 
            array (
                'id' => 243,
                'ampher_code' => '3029',
                'ampher_name' => 'ลำทะเมนชัย   ',
                'ampher_name_eng' => 'Lam Thamenchai',
                'geo_id' => 3,
                'province_id' => 19,
            ),
            243 => 
            array (
                'id' => 244,
                'ampher_code' => '3030',
                'ampher_name' => 'บัวลาย   ',
                'ampher_name_eng' => 'Bua Lai',
                'geo_id' => 3,
                'province_id' => 19,
            ),
            244 => 
            array (
                'id' => 245,
                'ampher_code' => '3031',
                'ampher_name' => 'สีดา   ',
                'ampher_name_eng' => 'Sida',
                'geo_id' => 3,
                'province_id' => 19,
            ),
            245 => 
            array (
                'id' => 246,
                'ampher_code' => '3032',
                'ampher_name' => 'เฉลิมพระเกียรติ   ',
                'ampher_name_eng' => 'Chaloem Phra Kiat',
                'geo_id' => 3,
                'province_id' => 19,
            ),
            246 => 
            array (
                'id' => 247,
                'ampher_code' => '3049',
                'ampher_name' => 'ท้องถิ่นเทศบาลตำบลโพธิ์กลาง*   ',
                'ampher_name_eng' => 'Pho Krang',
                'geo_id' => 3,
                'province_id' => 19,
            ),
            247 => 
            array (
                'id' => 248,
                'ampher_code' => '3051',
                'ampher_name' => 'สาขาตำบลมะค่า-พลสงคราม*   ',
                'ampher_name_eng' => 'Ma Ka-Pon Songkram*',
                'geo_id' => 3,
                'province_id' => 19,
            ),
            248 => 
            array (
                'id' => 249,
                'ampher_code' => '3081',
                'ampher_name' => '*โนนลาว   ',
                'ampher_name_eng' => '*Non Lao',
                'geo_id' => 3,
                'province_id' => 19,
            ),
            249 => 
            array (
                'id' => 250,
                'ampher_code' => '3101',
                'ampher_name' => 'เมืองบุรีรัมย์   ',
                'ampher_name_eng' => 'Mueang Buri Ram',
                'geo_id' => 3,
                'province_id' => 20,
            ),
            250 => 
            array (
                'id' => 251,
                'ampher_code' => '3102',
                'ampher_name' => 'คูเมือง   ',
                'ampher_name_eng' => 'Khu Mueang',
                'geo_id' => 3,
                'province_id' => 20,
            ),
            251 => 
            array (
                'id' => 252,
                'ampher_code' => '3103',
                'ampher_name' => 'กระสัง',
                'ampher_name_eng' => 'Krasang',
                'geo_id' => 3,
                'province_id' => 20,
            ),
            252 => 
            array (
                'id' => 253,
                'ampher_code' => '3104',
                'ampher_name' => 'นางรอง   ',
                'ampher_name_eng' => 'Nang Rong',
                'geo_id' => 3,
                'province_id' => 20,
            ),
            253 => 
            array (
                'id' => 254,
                'ampher_code' => '3105',
                'ampher_name' => 'หนองกี่   ',
                'ampher_name_eng' => 'Nong Ki',
                'geo_id' => 3,
                'province_id' => 20,
            ),
            254 => 
            array (
                'id' => 255,
                'ampher_code' => '3106',
                'ampher_name' => 'ละหานทราย   ',
                'ampher_name_eng' => 'Lahan Sai',
                'geo_id' => 3,
                'province_id' => 20,
            ),
            255 => 
            array (
                'id' => 256,
                'ampher_code' => '3107',
                'ampher_name' => 'ประโคนชัย   ',
                'ampher_name_eng' => 'Prakhon Chai',
                'geo_id' => 3,
                'province_id' => 20,
            ),
            256 => 
            array (
                'id' => 257,
                'ampher_code' => '3108',
                'ampher_name' => 'บ้านกรวด   ',
                'ampher_name_eng' => 'Ban Kruat',
                'geo_id' => 3,
                'province_id' => 20,
            ),
            257 => 
            array (
                'id' => 258,
                'ampher_code' => '3109',
                'ampher_name' => 'พุทไธสง   ',
                'ampher_name_eng' => 'Phutthaisong',
                'geo_id' => 3,
                'province_id' => 20,
            ),
            258 => 
            array (
                'id' => 259,
                'ampher_code' => '3110',
                'ampher_name' => 'ลำปลายมาศ   ',
                'ampher_name_eng' => 'Lam Plai Mat',
                'geo_id' => 3,
                'province_id' => 20,
            ),
            259 => 
            array (
                'id' => 260,
                'ampher_code' => '3111',
                'ampher_name' => 'สตึก   ',
                'ampher_name_eng' => 'Satuek',
                'geo_id' => 3,
                'province_id' => 20,
            ),
            260 => 
            array (
                'id' => 261,
                'ampher_code' => '3112',
                'ampher_name' => 'ปะคำ   ',
                'ampher_name_eng' => 'Pakham',
                'geo_id' => 3,
                'province_id' => 20,
            ),
            261 => 
            array (
                'id' => 262,
                'ampher_code' => '3113',
                'ampher_name' => 'นาโพธิ์   ',
                'ampher_name_eng' => 'Na Pho',
                'geo_id' => 3,
                'province_id' => 20,
            ),
            262 => 
            array (
                'id' => 263,
                'ampher_code' => '3114',
                'ampher_name' => 'หนองหงส์   ',
                'ampher_name_eng' => 'Nong Hong',
                'geo_id' => 3,
                'province_id' => 20,
            ),
            263 => 
            array (
                'id' => 264,
                'ampher_code' => '3115',
                'ampher_name' => 'พลับพลาชัย   ',
                'ampher_name_eng' => 'Phlapphla Chai',
                'geo_id' => 3,
                'province_id' => 20,
            ),
            264 => 
            array (
                'id' => 265,
                'ampher_code' => '3116',
                'ampher_name' => 'ห้วยราช   ',
                'ampher_name_eng' => 'Huai Rat',
                'geo_id' => 3,
                'province_id' => 20,
            ),
            265 => 
            array (
                'id' => 266,
                'ampher_code' => '3117',
                'ampher_name' => 'โนนสุวรรณ   ',
                'ampher_name_eng' => 'Non Suwan',
                'geo_id' => 3,
                'province_id' => 20,
            ),
            266 => 
            array (
                'id' => 267,
                'ampher_code' => '3118',
                'ampher_name' => 'ชำนิ   ',
                'ampher_name_eng' => 'Chamni',
                'geo_id' => 3,
                'province_id' => 20,
            ),
            267 => 
            array (
                'id' => 268,
                'ampher_code' => '3119',
                'ampher_name' => 'บ้านใหม่ไชยพจน์   ',
                'ampher_name_eng' => 'Ban Mai Chaiyaphot',
                'geo_id' => 3,
                'province_id' => 20,
            ),
            268 => 
            array (
                'id' => 269,
                'ampher_code' => '3120',
                'ampher_name' => 'โนนดินแดง   ',
                'ampher_name_eng' => 'Din Daeng',
                'geo_id' => 3,
                'province_id' => 20,
            ),
            269 => 
            array (
                'id' => 270,
                'ampher_code' => '3121',
                'ampher_name' => 'บ้านด่าน   ',
                'ampher_name_eng' => 'Ban Dan',
                'geo_id' => 3,
                'province_id' => 20,
            ),
            270 => 
            array (
                'id' => 271,
                'ampher_code' => '3122',
                'ampher_name' => 'แคนดง   ',
                'ampher_name_eng' => 'Khaen Dong',
                'geo_id' => 3,
                'province_id' => 20,
            ),
            271 => 
            array (
                'id' => 272,
                'ampher_code' => '3123',
                'ampher_name' => 'เฉลิมพระเกียรติ   ',
                'ampher_name_eng' => 'Chaloem Phra Kiat',
                'geo_id' => 3,
                'province_id' => 20,
            ),
            272 => 
            array (
                'id' => 273,
                'ampher_code' => '3201',
                'ampher_name' => 'เมืองสุรินทร์   ',
                'ampher_name_eng' => 'Mueang Surin',
                'geo_id' => 3,
                'province_id' => 21,
            ),
            273 => 
            array (
                'id' => 274,
                'ampher_code' => '3202',
                'ampher_name' => 'ชุมพลบุรี   ',
                'ampher_name_eng' => 'Chumphon Buri',
                'geo_id' => 3,
                'province_id' => 21,
            ),
            274 => 
            array (
                'id' => 275,
                'ampher_code' => '3203',
                'ampher_name' => 'ท่าตูม   ',
                'ampher_name_eng' => 'Tha Tum',
                'geo_id' => 3,
                'province_id' => 21,
            ),
            275 => 
            array (
                'id' => 276,
                'ampher_code' => '3204',
                'ampher_name' => 'จอมพระ   ',
                'ampher_name_eng' => 'Chom Phra',
                'geo_id' => 3,
                'province_id' => 21,
            ),
            276 => 
            array (
                'id' => 277,
                'ampher_code' => '3205',
                'ampher_name' => 'ปราสาท   ',
                'ampher_name_eng' => 'Prasat',
                'geo_id' => 3,
                'province_id' => 21,
            ),
            277 => 
            array (
                'id' => 278,
                'ampher_code' => '3206',
                'ampher_name' => 'กาบเชิง   ',
                'ampher_name_eng' => 'Kap Choeng',
                'geo_id' => 3,
                'province_id' => 21,
            ),
            278 => 
            array (
                'id' => 279,
                'ampher_code' => '3207',
                'ampher_name' => 'รัตนบุรี   ',
                'ampher_name_eng' => 'Rattanaburi',
                'geo_id' => 3,
                'province_id' => 21,
            ),
            279 => 
            array (
                'id' => 280,
                'ampher_code' => '3208',
                'ampher_name' => 'สนม   ',
                'ampher_name_eng' => 'Sanom',
                'geo_id' => 3,
                'province_id' => 21,
            ),
            280 => 
            array (
                'id' => 281,
                'ampher_code' => '3209',
                'ampher_name' => 'ศีขรภูมิ   ',
                'ampher_name_eng' => 'Sikhoraphum',
                'geo_id' => 3,
                'province_id' => 21,
            ),
            281 => 
            array (
                'id' => 282,
                'ampher_code' => '3210',
                'ampher_name' => 'สังขะ   ',
                'ampher_name_eng' => 'Sangkha',
                'geo_id' => 3,
                'province_id' => 21,
            ),
            282 => 
            array (
                'id' => 283,
                'ampher_code' => '3211',
                'ampher_name' => 'ลำดวน   ',
                'ampher_name_eng' => 'Lamduan',
                'geo_id' => 3,
                'province_id' => 21,
            ),
            283 => 
            array (
                'id' => 284,
                'ampher_code' => '3212',
                'ampher_name' => 'สำโรงทาบ   ',
                'ampher_name_eng' => 'Samrong Thap',
                'geo_id' => 3,
                'province_id' => 21,
            ),
            284 => 
            array (
                'id' => 285,
                'ampher_code' => '3213',
                'ampher_name' => 'บัวเชด   ',
                'ampher_name_eng' => 'Buachet',
                'geo_id' => 3,
                'province_id' => 21,
            ),
            285 => 
            array (
                'id' => 286,
                'ampher_code' => '3214',
                'ampher_name' => 'พนมดงรัก   ',
                'ampher_name_eng' => 'Phanom Dong Rak',
                'geo_id' => 3,
                'province_id' => 21,
            ),
            286 => 
            array (
                'id' => 287,
                'ampher_code' => '3215',
                'ampher_name' => 'ศรีณรงค์   ',
                'ampher_name_eng' => 'Si Narong',
                'geo_id' => 3,
                'province_id' => 21,
            ),
            287 => 
            array (
                'id' => 288,
                'ampher_code' => '3216',
                'ampher_name' => 'เขวาสินรินทร์   ',
                'ampher_name_eng' => 'Khwao Sinarin',
                'geo_id' => 3,
                'province_id' => 21,
            ),
            288 => 
            array (
                'id' => 289,
                'ampher_code' => '3217',
                'ampher_name' => 'โนนนารายณ์   ',
                'ampher_name_eng' => 'Non Narai',
                'geo_id' => 3,
                'province_id' => 21,
            ),
            289 => 
            array (
                'id' => 290,
                'ampher_code' => '3301',
                'ampher_name' => 'เมืองศรีสะเกษ   ',
                'ampher_name_eng' => 'Mueang Si Sa Ket',
                'geo_id' => 3,
                'province_id' => 22,
            ),
            290 => 
            array (
                'id' => 291,
                'ampher_code' => '3302',
                'ampher_name' => 'ยางชุมน้อย   ',
                'ampher_name_eng' => 'Yang Chum Noi',
                'geo_id' => 3,
                'province_id' => 22,
            ),
            291 => 
            array (
                'id' => 292,
                'ampher_code' => '3303',
                'ampher_name' => 'กันทรารมย์   ',
                'ampher_name_eng' => 'Kanthararom',
                'geo_id' => 3,
                'province_id' => 22,
            ),
            292 => 
            array (
                'id' => 293,
                'ampher_code' => '3304',
                'ampher_name' => 'กันทรลักษ์   ',
                'ampher_name_eng' => 'Kantharalak',
                'geo_id' => 3,
                'province_id' => 22,
            ),
            293 => 
            array (
                'id' => 294,
                'ampher_code' => '3305',
                'ampher_name' => 'ขุขันธ์   ',
                'ampher_name_eng' => 'Khukhan',
                'geo_id' => 3,
                'province_id' => 22,
            ),
            294 => 
            array (
                'id' => 295,
                'ampher_code' => '3306',
                'ampher_name' => 'ไพรบึง   ',
                'ampher_name_eng' => 'Phrai Bueng',
                'geo_id' => 3,
                'province_id' => 22,
            ),
            295 => 
            array (
                'id' => 296,
                'ampher_code' => '3307',
                'ampher_name' => 'ปรางค์กู่   ',
                'ampher_name_eng' => 'Prang Ku',
                'geo_id' => 3,
                'province_id' => 22,
            ),
            296 => 
            array (
                'id' => 297,
                'ampher_code' => '3308',
                'ampher_name' => 'ขุนหาญ   ',
                'ampher_name_eng' => 'Khun Han',
                'geo_id' => 3,
                'province_id' => 22,
            ),
            297 => 
            array (
                'id' => 298,
                'ampher_code' => '3309',
                'ampher_name' => 'ราษีไศล   ',
                'ampher_name_eng' => 'Rasi Salai',
                'geo_id' => 3,
                'province_id' => 22,
            ),
            298 => 
            array (
                'id' => 299,
                'ampher_code' => '3310',
                'ampher_name' => 'อุทุมพรพิสัย   ',
                'ampher_name_eng' => 'Uthumphon Phisai',
                'geo_id' => 3,
                'province_id' => 22,
            ),
            299 => 
            array (
                'id' => 300,
                'ampher_code' => '3311',
                'ampher_name' => 'บึงบูรพ์   ',
                'ampher_name_eng' => 'Bueng Bun',
                'geo_id' => 3,
                'province_id' => 22,
            ),
            300 => 
            array (
                'id' => 301,
                'ampher_code' => '3312',
                'ampher_name' => 'ห้วยทับทัน   ',
                'ampher_name_eng' => 'Huai Thap Than',
                'geo_id' => 3,
                'province_id' => 22,
            ),
            301 => 
            array (
                'id' => 302,
                'ampher_code' => '3313',
                'ampher_name' => 'โนนคูณ   ',
                'ampher_name_eng' => 'Non Khun',
                'geo_id' => 3,
                'province_id' => 22,
            ),
            302 => 
            array (
                'id' => 303,
                'ampher_code' => '3314',
                'ampher_name' => 'ศรีรัตนะ   ',
                'ampher_name_eng' => 'Si Rattana',
                'geo_id' => 3,
                'province_id' => 22,
            ),
            303 => 
            array (
                'id' => 304,
                'ampher_code' => '3315',
                'ampher_name' => 'น้ำเกลี้ยง   ',
                'ampher_name_eng' => 'Si Rattana',
                'geo_id' => 3,
                'province_id' => 22,
            ),
            304 => 
            array (
                'id' => 305,
                'ampher_code' => '3316',
                'ampher_name' => 'วังหิน   ',
                'ampher_name_eng' => 'Wang Hin',
                'geo_id' => 3,
                'province_id' => 22,
            ),
            305 => 
            array (
                'id' => 306,
                'ampher_code' => '3317',
                'ampher_name' => 'ภูสิงห์   ',
                'ampher_name_eng' => 'Phu Sing',
                'geo_id' => 3,
                'province_id' => 22,
            ),
            306 => 
            array (
                'id' => 307,
                'ampher_code' => '3318',
                'ampher_name' => 'เมืองจันทร์   ',
                'ampher_name_eng' => 'Mueang Chan',
                'geo_id' => 3,
                'province_id' => 22,
            ),
            307 => 
            array (
                'id' => 308,
                'ampher_code' => '3319',
                'ampher_name' => 'เบญจลักษ์   ',
                'ampher_name_eng' => 'Benchalak',
                'geo_id' => 3,
                'province_id' => 22,
            ),
            308 => 
            array (
                'id' => 309,
                'ampher_code' => '3320',
                'ampher_name' => 'พยุห์   ',
                'ampher_name_eng' => 'Phayu',
                'geo_id' => 3,
                'province_id' => 22,
            ),
            309 => 
            array (
                'id' => 310,
                'ampher_code' => '3321',
                'ampher_name' => 'โพธิ์ศรีสุวรรณ   ',
                'ampher_name_eng' => 'Pho Si Suwan',
                'geo_id' => 3,
                'province_id' => 22,
            ),
            310 => 
            array (
                'id' => 311,
                'ampher_code' => '3322',
                'ampher_name' => 'ศิลาลาด   ',
                'ampher_name_eng' => 'Sila Lat',
                'geo_id' => 3,
                'province_id' => 22,
            ),
            311 => 
            array (
                'id' => 312,
                'ampher_code' => '3401',
                'ampher_name' => 'เมืองอุบลราชธานี   ',
                'ampher_name_eng' => 'Mueang Ubon Ratchathani',
                'geo_id' => 3,
                'province_id' => 23,
            ),
            312 => 
            array (
                'id' => 313,
                'ampher_code' => '3402',
                'ampher_name' => 'ศรีเมืองใหม่   ',
                'ampher_name_eng' => 'Si Mueang Mai',
                'geo_id' => 3,
                'province_id' => 23,
            ),
            313 => 
            array (
                'id' => 314,
                'ampher_code' => '3403',
                'ampher_name' => 'โขงเจียม   ',
                'ampher_name_eng' => 'Khong Chiam',
                'geo_id' => 3,
                'province_id' => 23,
            ),
            314 => 
            array (
                'id' => 315,
                'ampher_code' => '3404',
                'ampher_name' => 'เขื่องใน   ',
                'ampher_name_eng' => 'Khueang Nai',
                'geo_id' => 3,
                'province_id' => 23,
            ),
            315 => 
            array (
                'id' => 316,
                'ampher_code' => '3405',
                'ampher_name' => 'เขมราฐ   ',
                'ampher_name_eng' => 'Khemarat',
                'geo_id' => 3,
                'province_id' => 23,
            ),
            316 => 
            array (
                'id' => 317,
                'ampher_code' => '3406',
                'ampher_name' => '*ชานุมาน   ',
                'ampher_name_eng' => '*Shanuman',
                'geo_id' => 3,
                'province_id' => 23,
            ),
            317 => 
            array (
                'id' => 318,
                'ampher_code' => '3407',
                'ampher_name' => 'เดชอุดม   ',
                'ampher_name_eng' => 'Det Udom',
                'geo_id' => 3,
                'province_id' => 23,
            ),
            318 => 
            array (
                'id' => 319,
                'ampher_code' => '3408',
                'ampher_name' => 'นาจะหลวย   ',
                'ampher_name_eng' => 'Na Chaluai',
                'geo_id' => 3,
                'province_id' => 23,
            ),
            319 => 
            array (
                'id' => 320,
                'ampher_code' => '3409',
                'ampher_name' => 'น้ำยืน   ',
                'ampher_name_eng' => 'Nam Yuen',
                'geo_id' => 3,
                'province_id' => 23,
            ),
            320 => 
            array (
                'id' => 321,
                'ampher_code' => '3410',
                'ampher_name' => 'บุณฑริก   ',
                'ampher_name_eng' => 'Buntharik',
                'geo_id' => 3,
                'province_id' => 23,
            ),
            321 => 
            array (
                'id' => 322,
                'ampher_code' => '3411',
                'ampher_name' => 'ตระการพืชผล   ',
                'ampher_name_eng' => 'Trakan Phuet Phon',
                'geo_id' => 3,
                'province_id' => 23,
            ),
            322 => 
            array (
                'id' => 323,
                'ampher_code' => '3412',
                'ampher_name' => 'กุดข้าวปุ้น   ',
                'ampher_name_eng' => 'Kut Khaopun',
                'geo_id' => 3,
                'province_id' => 23,
            ),
            323 => 
            array (
                'id' => 324,
                'ampher_code' => '3413',
                'ampher_name' => '*พนา   ',
                'ampher_name_eng' => '*Phana',
                'geo_id' => 3,
                'province_id' => 23,
            ),
            324 => 
            array (
                'id' => 325,
                'ampher_code' => '3414',
                'ampher_name' => 'ม่วงสามสิบ   ',
                'ampher_name_eng' => 'Muang Sam Sip',
                'geo_id' => 3,
                'province_id' => 23,
            ),
            325 => 
            array (
                'id' => 326,
                'ampher_code' => '3415',
                'ampher_name' => 'วารินชำราบ   ',
                'ampher_name_eng' => 'Warin Chamrap',
                'geo_id' => 3,
                'province_id' => 23,
            ),
            326 => 
            array (
                'id' => 327,
                'ampher_code' => '3416',
                'ampher_name' => '*อำนาจเจริญ   ',
                'ampher_name_eng' => '*Amnat Charoen',
                'geo_id' => 3,
                'province_id' => 23,
            ),
            327 => 
            array (
                'id' => 328,
                'ampher_code' => '3417',
                'ampher_name' => '*เสนางคนิคม   ',
                'ampher_name_eng' => '*Senangkhanikhom',
                'geo_id' => 3,
                'province_id' => 23,
            ),
            328 => 
            array (
                'id' => 329,
                'ampher_code' => '3418',
                'ampher_name' => '*หัวตะพาน   ',
                'ampher_name_eng' => '*Hua Taphan',
                'geo_id' => 3,
                'province_id' => 23,
            ),
            329 => 
            array (
                'id' => 330,
                'ampher_code' => '3419',
                'ampher_name' => 'พิบูลมังสาหาร   ',
                'ampher_name_eng' => 'Phibun Mangsahan',
                'geo_id' => 3,
                'province_id' => 23,
            ),
            330 => 
            array (
                'id' => 331,
                'ampher_code' => '3420',
                'ampher_name' => 'ตาลสุม   ',
                'ampher_name_eng' => 'Tan Sum',
                'geo_id' => 3,
                'province_id' => 23,
            ),
            331 => 
            array (
                'id' => 332,
                'ampher_code' => '3421',
                'ampher_name' => 'โพธิ์ไทร   ',
                'ampher_name_eng' => 'Pho Sai',
                'geo_id' => 3,
                'province_id' => 23,
            ),
            332 => 
            array (
                'id' => 333,
                'ampher_code' => '3422',
                'ampher_name' => 'สำโรง   ',
                'ampher_name_eng' => 'Samrong',
                'geo_id' => 3,
                'province_id' => 23,
            ),
            333 => 
            array (
                'id' => 334,
                'ampher_code' => '3423',
                'ampher_name' => '*กิ่งอำเภอลืออำนาจ   ',
                'ampher_name_eng' => '*King Amphoe Lue Amnat',
                'geo_id' => 3,
                'province_id' => 23,
            ),
            334 => 
            array (
                'id' => 335,
                'ampher_code' => '3424',
                'ampher_name' => 'ดอนมดแดง   ',
                'ampher_name_eng' => 'Don Mot Daeng',
                'geo_id' => 3,
                'province_id' => 23,
            ),
            335 => 
            array (
                'id' => 336,
                'ampher_code' => '3425',
                'ampher_name' => 'สิรินธร   ',
                'ampher_name_eng' => 'Sirindhorn',
                'geo_id' => 3,
                'province_id' => 23,
            ),
            336 => 
            array (
                'id' => 337,
                'ampher_code' => '3426',
                'ampher_name' => 'ทุ่งศรีอุดม   ',
                'ampher_name_eng' => 'Thung Si Udom',
                'geo_id' => 3,
                'province_id' => 23,
            ),
            337 => 
            array (
                'id' => 338,
                'ampher_code' => '3427',
                'ampher_name' => '*ปทุมราชวงศา   ',
                'ampher_name_eng' => '*Pathum Ratchawongsa',
                'geo_id' => 3,
                'province_id' => 23,
            ),
            338 => 
            array (
                'id' => 339,
                'ampher_code' => '3428',
                'ampher_name' => '*กิ่งอำเภอศรีหลักชัย   ',
                'ampher_name_eng' => '*King Amphoe Sri Lunk Chai',
                'geo_id' => 3,
                'province_id' => 23,
            ),
            339 => 
            array (
                'id' => 340,
                'ampher_code' => '3429',
                'ampher_name' => 'นาเยีย   ',
                'ampher_name_eng' => 'Na Yia',
                'geo_id' => 3,
                'province_id' => 23,
            ),
            340 => 
            array (
                'id' => 341,
                'ampher_code' => '3430',
                'ampher_name' => 'นาตาล   ',
                'ampher_name_eng' => 'Na Tan',
                'geo_id' => 3,
                'province_id' => 23,
            ),
            341 => 
            array (
                'id' => 342,
                'ampher_code' => '3431',
                'ampher_name' => 'เหล่าเสือโก้ก   ',
                'ampher_name_eng' => 'Lao Suea Kok',
                'geo_id' => 3,
                'province_id' => 23,
            ),
            342 => 
            array (
                'id' => 343,
                'ampher_code' => '3432',
                'ampher_name' => 'สว่างวีระวงศ์   ',
                'ampher_name_eng' => 'Sawang Wirawong',
                'geo_id' => 3,
                'province_id' => 23,
            ),
            343 => 
            array (
                'id' => 344,
                'ampher_code' => '3433',
                'ampher_name' => 'น้ำขุ่น   ',
                'ampher_name_eng' => 'Nam Khun',
                'geo_id' => 3,
                'province_id' => 23,
            ),
            344 => 
            array (
                'id' => 345,
                'ampher_code' => '3481',
                'ampher_name' => '*อ.สุวรรณวารี  จ.อุบลราชธานี   ',
                'ampher_name_eng' => '*Suwan Wari',
                'geo_id' => 3,
                'province_id' => 23,
            ),
            345 => 
            array (
                'id' => 346,
                'ampher_code' => '3501',
                'ampher_name' => 'เมืองยโสธร   ',
                'ampher_name_eng' => 'Mueang Yasothon',
                'geo_id' => 3,
                'province_id' => 24,
            ),
            346 => 
            array (
                'id' => 347,
                'ampher_code' => '3502',
                'ampher_name' => 'ทรายมูล   ',
                'ampher_name_eng' => 'Sai Mun',
                'geo_id' => 3,
                'province_id' => 24,
            ),
            347 => 
            array (
                'id' => 348,
                'ampher_code' => '3503',
                'ampher_name' => 'กุดชุม   ',
                'ampher_name_eng' => 'Kut Chum',
                'geo_id' => 3,
                'province_id' => 24,
            ),
            348 => 
            array (
                'id' => 349,
                'ampher_code' => '3504',
                'ampher_name' => 'คำเขื่อนแก้ว   ',
                'ampher_name_eng' => 'Kham Khuean Kaeo',
                'geo_id' => 3,
                'province_id' => 24,
            ),
            349 => 
            array (
                'id' => 350,
                'ampher_code' => '3505',
                'ampher_name' => 'ป่าติ้ว   ',
                'ampher_name_eng' => 'Pa Tio',
                'geo_id' => 3,
                'province_id' => 24,
            ),
            350 => 
            array (
                'id' => 351,
                'ampher_code' => '3506',
                'ampher_name' => 'มหาชนะชัย   ',
                'ampher_name_eng' => 'Maha Chana Chai',
                'geo_id' => 3,
                'province_id' => 24,
            ),
            351 => 
            array (
                'id' => 352,
                'ampher_code' => '3507',
                'ampher_name' => 'ค้อวัง   ',
                'ampher_name_eng' => 'Kho Wang',
                'geo_id' => 3,
                'province_id' => 24,
            ),
            352 => 
            array (
                'id' => 353,
                'ampher_code' => '3508',
                'ampher_name' => 'เลิงนกทา   ',
                'ampher_name_eng' => 'Loeng Nok Tha',
                'geo_id' => 3,
                'province_id' => 24,
            ),
            353 => 
            array (
                'id' => 354,
                'ampher_code' => '3509',
                'ampher_name' => 'ไทยเจริญ   ',
                'ampher_name_eng' => 'Thai Charoen',
                'geo_id' => 3,
                'province_id' => 24,
            ),
            354 => 
            array (
                'id' => 355,
                'ampher_code' => '3601',
                'ampher_name' => 'เมืองชัยภูมิ   ',
                'ampher_name_eng' => 'Mueang Chaiyaphum',
                'geo_id' => 3,
                'province_id' => 25,
            ),
            355 => 
            array (
                'id' => 356,
                'ampher_code' => '3602',
                'ampher_name' => 'บ้านเขว้า   ',
                'ampher_name_eng' => 'Ban Khwao',
                'geo_id' => 3,
                'province_id' => 25,
            ),
            356 => 
            array (
                'id' => 357,
                'ampher_code' => '3603',
                'ampher_name' => 'คอนสวรรค์   ',
                'ampher_name_eng' => 'Khon Sawan',
                'geo_id' => 3,
                'province_id' => 25,
            ),
            357 => 
            array (
                'id' => 358,
                'ampher_code' => '3604',
                'ampher_name' => 'เกษตรสมบูรณ์   ',
                'ampher_name_eng' => 'Kaset Sombun',
                'geo_id' => 3,
                'province_id' => 25,
            ),
            358 => 
            array (
                'id' => 359,
                'ampher_code' => '3605',
                'ampher_name' => 'หนองบัวแดง   ',
                'ampher_name_eng' => 'Nong Bua Daeng',
                'geo_id' => 3,
                'province_id' => 25,
            ),
            359 => 
            array (
                'id' => 360,
                'ampher_code' => '3606',
                'ampher_name' => 'จัตุรัส   ',
                'ampher_name_eng' => 'Chatturat',
                'geo_id' => 3,
                'province_id' => 25,
            ),
            360 => 
            array (
                'id' => 361,
                'ampher_code' => '3607',
                'ampher_name' => 'บำเหน็จณรงค์   ',
                'ampher_name_eng' => 'Bamnet Narong',
                'geo_id' => 3,
                'province_id' => 25,
            ),
            361 => 
            array (
                'id' => 362,
                'ampher_code' => '3608',
                'ampher_name' => 'หนองบัวระเหว   ',
                'ampher_name_eng' => 'Nong Bua Rawe',
                'geo_id' => 3,
                'province_id' => 25,
            ),
            362 => 
            array (
                'id' => 363,
                'ampher_code' => '3609',
                'ampher_name' => 'เทพสถิต   ',
                'ampher_name_eng' => 'Thep Sathit',
                'geo_id' => 3,
                'province_id' => 25,
            ),
            363 => 
            array (
                'id' => 364,
                'ampher_code' => '3610',
                'ampher_name' => 'ภูเขียว   ',
                'ampher_name_eng' => 'Phu Khiao',
                'geo_id' => 3,
                'province_id' => 25,
            ),
            364 => 
            array (
                'id' => 365,
                'ampher_code' => '3611',
                'ampher_name' => 'บ้านแท่น   ',
                'ampher_name_eng' => 'Ban Thaen',
                'geo_id' => 3,
                'province_id' => 25,
            ),
            365 => 
            array (
                'id' => 366,
                'ampher_code' => '3612',
                'ampher_name' => 'แก้งคร้อ   ',
                'ampher_name_eng' => 'Kaeng Khro',
                'geo_id' => 3,
                'province_id' => 25,
            ),
            366 => 
            array (
                'id' => 367,
                'ampher_code' => '3613',
                'ampher_name' => 'คอนสาร   ',
                'ampher_name_eng' => 'Khon San',
                'geo_id' => 3,
                'province_id' => 25,
            ),
            367 => 
            array (
                'id' => 368,
                'ampher_code' => '3614',
                'ampher_name' => 'ภักดีชุมพล   ',
                'ampher_name_eng' => 'Phakdi Chumphon',
                'geo_id' => 3,
                'province_id' => 25,
            ),
            368 => 
            array (
                'id' => 369,
                'ampher_code' => '3615',
                'ampher_name' => 'เนินสง่า   ',
                'ampher_name_eng' => 'Noen Sa-nga',
                'geo_id' => 3,
                'province_id' => 25,
            ),
            369 => 
            array (
                'id' => 370,
                'ampher_code' => '3616',
                'ampher_name' => 'ซับใหญ่   ',
                'ampher_name_eng' => 'Sap Yai',
                'geo_id' => 3,
                'province_id' => 25,
            ),
            370 => 
            array (
                'id' => 371,
                'ampher_code' => '3651',
            'ampher_name' => 'เมืองชัยภูมิ (สาขาตำบลโนนสำราญ)*   ',
            'ampher_name_eng' => 'Mueang Chaiyaphum(Non Sumran)*',
                'geo_id' => 3,
                'province_id' => 25,
            ),
            371 => 
            array (
                'id' => 372,
                'ampher_code' => '3652',
                'ampher_name' => 'สาขาตำบลบ้านหว่าเฒ่า*   ',
                'ampher_name_eng' => 'Ban Wha Tao*',
                'geo_id' => 3,
                'province_id' => 25,
            ),
            372 => 
            array (
                'id' => 373,
                'ampher_code' => '3653',
            'ampher_name' => 'หนองบัวแดง (สาขาตำบลวังชมภู)*   ',
                'ampher_name_eng' => 'Nong Bua Daeng',
                'geo_id' => 3,
                'province_id' => 25,
            ),
            373 => 
            array (
                'id' => 374,
                'ampher_code' => '3654',
            'ampher_name' => 'กิ่งอำเภอซับใหญ่ (สาขาตำบลซับใหญ่)*   ',
                'ampher_name_eng' => 'King Amphoe Sap Yai*',
                'geo_id' => 3,
                'province_id' => 25,
            ),
            374 => 
            array (
                'id' => 375,
                'ampher_code' => '3655',
                'ampher_name' => 'สาขาตำบลโคกเพชร*   ',
                'ampher_name_eng' => 'Coke Phet*',
                'geo_id' => 3,
                'province_id' => 25,
            ),
            375 => 
            array (
                'id' => 376,
                'ampher_code' => '3656',
            'ampher_name' => 'เทพสถิต (สาขาตำบลนายางกลัก)*   ',
                'ampher_name_eng' => 'Thep Sathit*',
                'geo_id' => 3,
                'province_id' => 25,
            ),
            376 => 
            array (
                'id' => 377,
                'ampher_code' => '3657',
            'ampher_name' => 'บ้านแท่น (สาขาตำบลบ้านเต่า)*   ',
                'ampher_name_eng' => 'Ban Thaen',
                'geo_id' => 3,
                'province_id' => 25,
            ),
            377 => 
            array (
                'id' => 378,
                'ampher_code' => '3658',
            'ampher_name' => 'แก้งคร้อ (สาขาตำบลท่ามะไฟหวาน)*   ',
                'ampher_name_eng' => 'Kaeng Khro*',
                'geo_id' => 3,
                'province_id' => 25,
            ),
            378 => 
            array (
                'id' => 379,
                'ampher_code' => '3659',
            'ampher_name' => 'คอนสาร (สาขาตำบลโนนคูณ)*   ',
                'ampher_name_eng' => 'Khon San*',
                'geo_id' => 3,
                'province_id' => 25,
            ),
            379 => 
            array (
                'id' => 380,
                'ampher_code' => '3701',
                'ampher_name' => 'เมืองอำนาจเจริญ   ',
                'ampher_name_eng' => 'Mueang Amnat Charoen',
                'geo_id' => 3,
                'province_id' => 26,
            ),
            380 => 
            array (
                'id' => 381,
                'ampher_code' => '3702',
                'ampher_name' => 'ชานุมาน   ',
                'ampher_name_eng' => 'Chanuman',
                'geo_id' => 3,
                'province_id' => 26,
            ),
            381 => 
            array (
                'id' => 382,
                'ampher_code' => '3703',
                'ampher_name' => 'ปทุมราชวงศา   ',
                'ampher_name_eng' => 'Pathum Ratchawongsa',
                'geo_id' => 3,
                'province_id' => 26,
            ),
            382 => 
            array (
                'id' => 383,
                'ampher_code' => '3704',
                'ampher_name' => 'พนา   ',
                'ampher_name_eng' => 'Phana',
                'geo_id' => 3,
                'province_id' => 26,
            ),
            383 => 
            array (
                'id' => 384,
                'ampher_code' => '3705',
                'ampher_name' => 'เสนางคนิคม   ',
                'ampher_name_eng' => 'Senangkhanikhom',
                'geo_id' => 3,
                'province_id' => 26,
            ),
            384 => 
            array (
                'id' => 385,
                'ampher_code' => '3706',
                'ampher_name' => 'หัวตะพาน   ',
                'ampher_name_eng' => 'Hua Taphan',
                'geo_id' => 3,
                'province_id' => 26,
            ),
            385 => 
            array (
                'id' => 386,
                'ampher_code' => '3707',
                'ampher_name' => 'ลืออำนาจ   ',
                'ampher_name_eng' => 'Lue Amnat',
                'geo_id' => 3,
                'province_id' => 26,
            ),
            386 => 
            array (
                'id' => 387,
                'ampher_code' => '3901',
                'ampher_name' => 'เมืองหนองบัวลำภู   ',
                'ampher_name_eng' => 'Mueang Nong Bua Lam Phu',
                'geo_id' => 3,
                'province_id' => 27,
            ),
            387 => 
            array (
                'id' => 388,
                'ampher_code' => '3902',
                'ampher_name' => 'นากลาง   ',
                'ampher_name_eng' => 'Na Klang',
                'geo_id' => 3,
                'province_id' => 27,
            ),
            388 => 
            array (
                'id' => 389,
                'ampher_code' => '3903',
                'ampher_name' => 'โนนสัง   ',
                'ampher_name_eng' => 'Non Sang',
                'geo_id' => 3,
                'province_id' => 27,
            ),
            389 => 
            array (
                'id' => 390,
                'ampher_code' => '3904',
                'ampher_name' => 'ศรีบุญเรือง   ',
                'ampher_name_eng' => 'Si Bun Rueang',
                'geo_id' => 3,
                'province_id' => 27,
            ),
            390 => 
            array (
                'id' => 391,
                'ampher_code' => '3905',
                'ampher_name' => 'สุวรรณคูหา   ',
                'ampher_name_eng' => 'Suwannakhuha',
                'geo_id' => 3,
                'province_id' => 27,
            ),
            391 => 
            array (
                'id' => 392,
                'ampher_code' => '3906',
                'ampher_name' => 'นาวัง   ',
                'ampher_name_eng' => 'Na Wang',
                'geo_id' => 3,
                'province_id' => 27,
            ),
            392 => 
            array (
                'id' => 393,
                'ampher_code' => '4001',
                'ampher_name' => 'เมืองขอนแก่น   ',
                'ampher_name_eng' => 'Mueang Khon Kaen',
                'geo_id' => 3,
                'province_id' => 28,
            ),
            393 => 
            array (
                'id' => 394,
                'ampher_code' => '4002',
                'ampher_name' => 'บ้านฝาง   ',
                'ampher_name_eng' => 'Ban Fang',
                'geo_id' => 3,
                'province_id' => 28,
            ),
            394 => 
            array (
                'id' => 395,
                'ampher_code' => '4003',
                'ampher_name' => 'พระยืน   ',
                'ampher_name_eng' => 'Phra Yuen',
                'geo_id' => 3,
                'province_id' => 28,
            ),
            395 => 
            array (
                'id' => 396,
                'ampher_code' => '4004',
                'ampher_name' => 'หนองเรือ   ',
                'ampher_name_eng' => 'Nong Ruea',
                'geo_id' => 3,
                'province_id' => 28,
            ),
            396 => 
            array (
                'id' => 397,
                'ampher_code' => '4005',
                'ampher_name' => 'ชุมแพ   ',
                'ampher_name_eng' => 'Chum Phae',
                'geo_id' => 3,
                'province_id' => 28,
            ),
            397 => 
            array (
                'id' => 398,
                'ampher_code' => '4006',
                'ampher_name' => 'สีชมพู   ',
                'ampher_name_eng' => 'Si Chomphu',
                'geo_id' => 3,
                'province_id' => 28,
            ),
            398 => 
            array (
                'id' => 399,
                'ampher_code' => '4007',
                'ampher_name' => 'น้ำพอง   ',
                'ampher_name_eng' => 'Nam Phong',
                'geo_id' => 3,
                'province_id' => 28,
            ),
            399 => 
            array (
                'id' => 400,
                'ampher_code' => '4008',
                'ampher_name' => 'อุบลรัตน์   ',
                'ampher_name_eng' => 'Ubolratana',
                'geo_id' => 3,
                'province_id' => 28,
            ),
            400 => 
            array (
                'id' => 401,
                'ampher_code' => '4009',
                'ampher_name' => 'กระนวน   ',
                'ampher_name_eng' => 'Kranuan',
                'geo_id' => 3,
                'province_id' => 28,
            ),
            401 => 
            array (
                'id' => 402,
                'ampher_code' => '4010',
                'ampher_name' => 'บ้านไผ่   ',
                'ampher_name_eng' => 'Ban Phai',
                'geo_id' => 3,
                'province_id' => 28,
            ),
            402 => 
            array (
                'id' => 403,
                'ampher_code' => '4011',
                'ampher_name' => 'เปือยน้อย   ',
                'ampher_name_eng' => 'Pueai Noi',
                'geo_id' => 3,
                'province_id' => 28,
            ),
            403 => 
            array (
                'id' => 404,
                'ampher_code' => '4012',
                'ampher_name' => 'พล   ',
                'ampher_name_eng' => 'Phon',
                'geo_id' => 3,
                'province_id' => 28,
            ),
            404 => 
            array (
                'id' => 405,
                'ampher_code' => '4013',
                'ampher_name' => 'แวงใหญ่   ',
                'ampher_name_eng' => 'Waeng Yai',
                'geo_id' => 3,
                'province_id' => 28,
            ),
            405 => 
            array (
                'id' => 406,
                'ampher_code' => '4014',
                'ampher_name' => 'แวงน้อย   ',
                'ampher_name_eng' => 'Waeng Noi',
                'geo_id' => 3,
                'province_id' => 28,
            ),
            406 => 
            array (
                'id' => 407,
                'ampher_code' => '4015',
                'ampher_name' => 'หนองสองห้อง   ',
                'ampher_name_eng' => 'Nong Song Hong',
                'geo_id' => 3,
                'province_id' => 28,
            ),
            407 => 
            array (
                'id' => 408,
                'ampher_code' => '4016',
                'ampher_name' => 'ภูเวียง   ',
                'ampher_name_eng' => 'Phu Wiang',
                'geo_id' => 3,
                'province_id' => 28,
            ),
            408 => 
            array (
                'id' => 409,
                'ampher_code' => '4017',
                'ampher_name' => 'มัญจาคีรี   ',
                'ampher_name_eng' => 'Mancha Khiri',
                'geo_id' => 3,
                'province_id' => 28,
            ),
            409 => 
            array (
                'id' => 410,
                'ampher_code' => '4018',
                'ampher_name' => 'ชนบท   ',
                'ampher_name_eng' => 'Chonnabot',
                'geo_id' => 3,
                'province_id' => 28,
            ),
            410 => 
            array (
                'id' => 411,
                'ampher_code' => '4019',
                'ampher_name' => 'เขาสวนกวาง   ',
                'ampher_name_eng' => 'Khao Suan Kwang',
                'geo_id' => 3,
                'province_id' => 28,
            ),
            411 => 
            array (
                'id' => 412,
                'ampher_code' => '4020',
                'ampher_name' => 'ภูผาม่าน   ',
                'ampher_name_eng' => 'Phu Pha Man',
                'geo_id' => 3,
                'province_id' => 28,
            ),
            412 => 
            array (
                'id' => 413,
                'ampher_code' => '4021',
                'ampher_name' => 'ซำสูง   ',
                'ampher_name_eng' => 'Sam Sung',
                'geo_id' => 3,
                'province_id' => 28,
            ),
            413 => 
            array (
                'id' => 414,
                'ampher_code' => '4022',
                'ampher_name' => 'โคกโพธิ์ไชย   ',
                'ampher_name_eng' => 'Khok Pho Chai',
                'geo_id' => 3,
                'province_id' => 28,
            ),
            414 => 
            array (
                'id' => 415,
                'ampher_code' => '4023',
                'ampher_name' => 'หนองนาคำ   ',
                'ampher_name_eng' => 'Nong Na Kham',
                'geo_id' => 3,
                'province_id' => 28,
            ),
            415 => 
            array (
                'id' => 416,
                'ampher_code' => '4024',
                'ampher_name' => 'บ้านแฮด   ',
                'ampher_name_eng' => 'Ban Haet',
                'geo_id' => 3,
                'province_id' => 28,
            ),
            416 => 
            array (
                'id' => 417,
                'ampher_code' => '4025',
                'ampher_name' => 'โนนศิลา   ',
                'ampher_name_eng' => 'Non Sila',
                'geo_id' => 3,
                'province_id' => 28,
            ),
            417 => 
            array (
                'id' => 418,
                'ampher_code' => '4029',
                'ampher_name' => 'เวียงเก่า   ',
                'ampher_name_eng' => 'Wiang Kao',
                'geo_id' => 3,
                'province_id' => 28,
            ),
            418 => 
            array (
                'id' => 419,
                'ampher_code' => '4068',
                'ampher_name' => 'ท้องถิ่นเทศบาลตำบลบ้านเป็ด*   ',
                'ampher_name_eng' => 'Ban Pet*',
                'geo_id' => 3,
                'province_id' => 28,
            ),
            419 => 
            array (
                'id' => 420,
                'ampher_code' => '4098',
                'ampher_name' => 'เทศบาลตำบลเมืองพล*   ',
                'ampher_name_eng' => 'Tet Saban Tambon Muang Phon*',
                'geo_id' => 3,
                'province_id' => 28,
            ),
            420 => 
            array (
                'id' => 421,
                'ampher_code' => '4101',
                'ampher_name' => 'เมืองอุดรธานี   ',
                'ampher_name_eng' => 'Mueang Udon Thani',
                'geo_id' => 3,
                'province_id' => 29,
            ),
            421 => 
            array (
                'id' => 422,
                'ampher_code' => '4102',
                'ampher_name' => 'กุดจับ   ',
                'ampher_name_eng' => 'Kut Chap',
                'geo_id' => 3,
                'province_id' => 29,
            ),
            422 => 
            array (
                'id' => 423,
                'ampher_code' => '4103',
                'ampher_name' => 'หนองวัวซอ   ',
                'ampher_name_eng' => 'Nong Wua So',
                'geo_id' => 3,
                'province_id' => 29,
            ),
            423 => 
            array (
                'id' => 424,
                'ampher_code' => '4104',
                'ampher_name' => 'กุมภวาปี   ',
                'ampher_name_eng' => 'Kumphawapi',
                'geo_id' => 3,
                'province_id' => 29,
            ),
            424 => 
            array (
                'id' => 425,
                'ampher_code' => '4105',
                'ampher_name' => 'โนนสะอาด   ',
                'ampher_name_eng' => 'Non Sa-at',
                'geo_id' => 3,
                'province_id' => 29,
            ),
            425 => 
            array (
                'id' => 426,
                'ampher_code' => '4106',
                'ampher_name' => 'หนองหาน   ',
                'ampher_name_eng' => 'Nong Han',
                'geo_id' => 3,
                'province_id' => 29,
            ),
            426 => 
            array (
                'id' => 427,
                'ampher_code' => '4107',
                'ampher_name' => 'ทุ่งฝน   ',
                'ampher_name_eng' => 'Thung Fon',
                'geo_id' => 3,
                'province_id' => 29,
            ),
            427 => 
            array (
                'id' => 428,
                'ampher_code' => '4108',
                'ampher_name' => 'ไชยวาน   ',
                'ampher_name_eng' => 'Chai Wan',
                'geo_id' => 3,
                'province_id' => 29,
            ),
            428 => 
            array (
                'id' => 429,
                'ampher_code' => '4109',
                'ampher_name' => 'ศรีธาตุ   ',
                'ampher_name_eng' => 'Si That',
                'geo_id' => 3,
                'province_id' => 29,
            ),
            429 => 
            array (
                'id' => 430,
                'ampher_code' => '4110',
                'ampher_name' => 'วังสามหมอ   ',
                'ampher_name_eng' => 'Wang Sam Mo',
                'geo_id' => 3,
                'province_id' => 29,
            ),
            430 => 
            array (
                'id' => 431,
                'ampher_code' => '4111',
                'ampher_name' => 'บ้านดุง   ',
                'ampher_name_eng' => 'Ban Dung',
                'geo_id' => 3,
                'province_id' => 29,
            ),
            431 => 
            array (
                'id' => 432,
                'ampher_code' => '4112',
                'ampher_name' => '*หนองบัวลำภู   ',
                'ampher_name_eng' => '*Nong Bua Lam Phu',
                'geo_id' => 3,
                'province_id' => 29,
            ),
            432 => 
            array (
                'id' => 433,
                'ampher_code' => '4113',
                'ampher_name' => '*ศรีบุญเรือง   ',
                'ampher_name_eng' => '*Si Bun Rueang',
                'geo_id' => 3,
                'province_id' => 29,
            ),
            433 => 
            array (
                'id' => 434,
                'ampher_code' => '4114',
                'ampher_name' => '*นากลาง   ',
                'ampher_name_eng' => '*Na Klang',
                'geo_id' => 3,
                'province_id' => 29,
            ),
            434 => 
            array (
                'id' => 435,
                'ampher_code' => '4115',
                'ampher_name' => '*สุวรรณคูหา   ',
                'ampher_name_eng' => '*Suwannakhuha',
                'geo_id' => 3,
                'province_id' => 29,
            ),
            435 => 
            array (
                'id' => 436,
                'ampher_code' => '4116',
                'ampher_name' => '*โนนสัง   ',
                'ampher_name_eng' => '*Non Sang',
                'geo_id' => 3,
                'province_id' => 29,
            ),
            436 => 
            array (
                'id' => 437,
                'ampher_code' => '4117',
                'ampher_name' => 'บ้านผือ   ',
                'ampher_name_eng' => 'Ban Phue',
                'geo_id' => 3,
                'province_id' => 29,
            ),
            437 => 
            array (
                'id' => 438,
                'ampher_code' => '4118',
                'ampher_name' => 'น้ำโสม   ',
                'ampher_name_eng' => 'Nam Som',
                'geo_id' => 3,
                'province_id' => 29,
            ),
            438 => 
            array (
                'id' => 439,
                'ampher_code' => '4119',
                'ampher_name' => 'เพ็ญ   ',
                'ampher_name_eng' => 'Phen',
                'geo_id' => 3,
                'province_id' => 29,
            ),
            439 => 
            array (
                'id' => 440,
                'ampher_code' => '4120',
                'ampher_name' => 'สร้างคอม   ',
                'ampher_name_eng' => 'Sang Khom',
                'geo_id' => 3,
                'province_id' => 29,
            ),
            440 => 
            array (
                'id' => 441,
                'ampher_code' => '4121',
                'ampher_name' => 'หนองแสง   ',
                'ampher_name_eng' => 'Nong Saeng',
                'geo_id' => 3,
                'province_id' => 29,
            ),
            441 => 
            array (
                'id' => 442,
                'ampher_code' => '4122',
                'ampher_name' => 'นายูง   ',
                'ampher_name_eng' => 'Na Yung',
                'geo_id' => 3,
                'province_id' => 29,
            ),
            442 => 
            array (
                'id' => 443,
                'ampher_code' => '4123',
                'ampher_name' => 'พิบูลย์รักษ์   ',
                'ampher_name_eng' => 'Phibun Rak',
                'geo_id' => 3,
                'province_id' => 29,
            ),
            443 => 
            array (
                'id' => 444,
                'ampher_code' => '4124',
                'ampher_name' => 'กู่แก้ว   ',
                'ampher_name_eng' => 'Ku Kaeo',
                'geo_id' => 3,
                'province_id' => 29,
            ),
            444 => 
            array (
                'id' => 445,
                'ampher_code' => '4125',
                'ampher_name' => 'ประจักษ์ศิลปาคม   ',
                'ampher_name_eng' => 'rachak-sinlapakhom',
                'geo_id' => 3,
                'province_id' => 29,
            ),
            445 => 
            array (
                'id' => 446,
                'ampher_code' => '4201',
                'ampher_name' => 'เมืองเลย   ',
                'ampher_name_eng' => 'Mueang Loei',
                'geo_id' => 3,
                'province_id' => 30,
            ),
            446 => 
            array (
                'id' => 447,
                'ampher_code' => '4202',
                'ampher_name' => 'นาด้วง   ',
                'ampher_name_eng' => 'Na Duang',
                'geo_id' => 3,
                'province_id' => 30,
            ),
            447 => 
            array (
                'id' => 448,
                'ampher_code' => '4203',
                'ampher_name' => 'เชียงคาน   ',
                'ampher_name_eng' => 'Chiang Khan',
                'geo_id' => 3,
                'province_id' => 30,
            ),
            448 => 
            array (
                'id' => 449,
                'ampher_code' => '4204',
                'ampher_name' => 'ปากชม   ',
                'ampher_name_eng' => 'Pak Chom',
                'geo_id' => 3,
                'province_id' => 30,
            ),
            449 => 
            array (
                'id' => 450,
                'ampher_code' => '4205',
                'ampher_name' => 'ด่านซ้าย   ',
                'ampher_name_eng' => 'Dan Sai',
                'geo_id' => 3,
                'province_id' => 30,
            ),
            450 => 
            array (
                'id' => 451,
                'ampher_code' => '4206',
                'ampher_name' => 'นาแห้ว   ',
                'ampher_name_eng' => 'Na Haeo',
                'geo_id' => 3,
                'province_id' => 30,
            ),
            451 => 
            array (
                'id' => 452,
                'ampher_code' => '4207',
                'ampher_name' => 'ภูเรือ   ',
                'ampher_name_eng' => 'Phu Ruea',
                'geo_id' => 3,
                'province_id' => 30,
            ),
            452 => 
            array (
                'id' => 453,
                'ampher_code' => '4208',
                'ampher_name' => 'ท่าลี่   ',
                'ampher_name_eng' => 'Tha Li',
                'geo_id' => 3,
                'province_id' => 30,
            ),
            453 => 
            array (
                'id' => 454,
                'ampher_code' => '4209',
                'ampher_name' => 'วังสะพุง   ',
                'ampher_name_eng' => 'Wang Saphung',
                'geo_id' => 3,
                'province_id' => 30,
            ),
            454 => 
            array (
                'id' => 455,
                'ampher_code' => '4210',
                'ampher_name' => 'ภูกระดึง   ',
                'ampher_name_eng' => 'Phu Kradueng',
                'geo_id' => 3,
                'province_id' => 30,
            ),
            455 => 
            array (
                'id' => 456,
                'ampher_code' => '4211',
                'ampher_name' => 'ภูหลวง   ',
                'ampher_name_eng' => 'Phu Luang',
                'geo_id' => 3,
                'province_id' => 30,
            ),
            456 => 
            array (
                'id' => 457,
                'ampher_code' => '4212',
                'ampher_name' => 'ผาขาว   ',
                'ampher_name_eng' => 'Pha Khao',
                'geo_id' => 3,
                'province_id' => 30,
            ),
            457 => 
            array (
                'id' => 458,
                'ampher_code' => '4213',
                'ampher_name' => 'เอราวัณ   ',
                'ampher_name_eng' => 'Erawan',
                'geo_id' => 3,
                'province_id' => 30,
            ),
            458 => 
            array (
                'id' => 459,
                'ampher_code' => '4214',
                'ampher_name' => 'หนองหิน   ',
                'ampher_name_eng' => 'Nong Hin',
                'geo_id' => 3,
                'province_id' => 30,
            ),
            459 => 
            array (
                'id' => 460,
                'ampher_code' => '4301',
                'ampher_name' => 'เมืองหนองคาย   ',
                'ampher_name_eng' => 'Mueang Nong Khai',
                'geo_id' => 3,
                'province_id' => 31,
            ),
            460 => 
            array (
                'id' => 461,
                'ampher_code' => '4302',
                'ampher_name' => 'ท่าบ่อ   ',
                'ampher_name_eng' => 'Tha Bo',
                'geo_id' => 3,
                'province_id' => 31,
            ),
            461 => 
            array (
                'id' => 462,
                'ampher_code' => '4303',
                'ampher_name' => 'เมืองบึงกาฬ   ',
                'ampher_name_eng' => 'Mueang Bueng Kan',
                'geo_id' => 3,
                'province_id' => 97,
            ),
            462 => 
            array (
                'id' => 463,
                'ampher_code' => '4304',
                'ampher_name' => 'พรเจริญ   ',
                'ampher_name_eng' => 'Phon Charoen',
                'geo_id' => 3,
                'province_id' => 97,
            ),
            463 => 
            array (
                'id' => 464,
                'ampher_code' => '4305',
                'ampher_name' => 'โพนพิสัย   ',
                'ampher_name_eng' => 'Phon Phisai',
                'geo_id' => 3,
                'province_id' => 31,
            ),
            464 => 
            array (
                'id' => 465,
                'ampher_code' => '4306',
                'ampher_name' => 'โซ่พิสัย   ',
                'ampher_name_eng' => 'So Phisai',
                'geo_id' => 3,
                'province_id' => 97,
            ),
            465 => 
            array (
                'id' => 466,
                'ampher_code' => '4307',
                'ampher_name' => 'ศรีเชียงใหม่   ',
                'ampher_name_eng' => 'Si Chiang Mai',
                'geo_id' => 3,
                'province_id' => 31,
            ),
            466 => 
            array (
                'id' => 467,
                'ampher_code' => '4308',
                'ampher_name' => 'สังคม   ',
                'ampher_name_eng' => 'Sangkhom',
                'geo_id' => 3,
                'province_id' => 31,
            ),
            467 => 
            array (
                'id' => 468,
                'ampher_code' => '4309',
                'ampher_name' => 'เซกา   ',
                'ampher_name_eng' => 'Seka',
                'geo_id' => 3,
                'province_id' => 97,
            ),
            468 => 
            array (
                'id' => 469,
                'ampher_code' => '4310',
                'ampher_name' => 'ปากคาด   ',
                'ampher_name_eng' => 'Pak Khat',
                'geo_id' => 3,
                'province_id' => 97,
            ),
            469 => 
            array (
                'id' => 470,
                'ampher_code' => '4311',
                'ampher_name' => 'บึงโขงหลง   ',
                'ampher_name_eng' => 'Bueng Khong Long',
                'geo_id' => 3,
                'province_id' => 97,
            ),
            470 => 
            array (
                'id' => 471,
                'ampher_code' => '4312',
                'ampher_name' => 'ศรีวิไล   ',
                'ampher_name_eng' => 'Si Wilai',
                'geo_id' => 3,
                'province_id' => 97,
            ),
            471 => 
            array (
                'id' => 472,
                'ampher_code' => '4313',
                'ampher_name' => 'บุ่งคล้า   ',
                'ampher_name_eng' => 'Bung Khla',
                'geo_id' => 3,
                'province_id' => 97,
            ),
            472 => 
            array (
                'id' => 473,
                'ampher_code' => '4314',
                'ampher_name' => 'สระใคร   ',
                'ampher_name_eng' => 'Sakhrai',
                'geo_id' => 3,
                'province_id' => 31,
            ),
            473 => 
            array (
                'id' => 474,
                'ampher_code' => '4315',
                'ampher_name' => 'เฝ้าไร่   ',
                'ampher_name_eng' => 'Fao Rai',
                'geo_id' => 3,
                'province_id' => 31,
            ),
            474 => 
            array (
                'id' => 475,
                'ampher_code' => '4316',
                'ampher_name' => 'รัตนวาปี   ',
                'ampher_name_eng' => 'Rattanawapi',
                'geo_id' => 3,
                'province_id' => 31,
            ),
            475 => 
            array (
                'id' => 476,
                'ampher_code' => '4317',
                'ampher_name' => 'โพธิ์ตาก   ',
                'ampher_name_eng' => 'Pho Tak',
                'geo_id' => 3,
                'province_id' => 31,
            ),
            476 => 
            array (
                'id' => 477,
                'ampher_code' => '4401',
                'ampher_name' => 'เมืองมหาสารคาม   ',
                'ampher_name_eng' => 'Mueang Maha Sarakham',
                'geo_id' => 3,
                'province_id' => 32,
            ),
            477 => 
            array (
                'id' => 478,
                'ampher_code' => '4402',
                'ampher_name' => 'แกดำ   ',
                'ampher_name_eng' => 'Kae Dam',
                'geo_id' => 3,
                'province_id' => 32,
            ),
            478 => 
            array (
                'id' => 479,
                'ampher_code' => '4403',
                'ampher_name' => 'โกสุมพิสัย   ',
                'ampher_name_eng' => 'Kosum Phisai',
                'geo_id' => 3,
                'province_id' => 32,
            ),
            479 => 
            array (
                'id' => 480,
                'ampher_code' => '4404',
                'ampher_name' => 'กันทรวิชัย   ',
                'ampher_name_eng' => 'Kantharawichai',
                'geo_id' => 3,
                'province_id' => 32,
            ),
            480 => 
            array (
                'id' => 481,
                'ampher_code' => '4405',
                'ampher_name' => 'เชียงยืน   ',
                'ampher_name_eng' => 'Kantharawichai',
                'geo_id' => 3,
                'province_id' => 32,
            ),
            481 => 
            array (
                'id' => 482,
                'ampher_code' => '4406',
                'ampher_name' => 'บรบือ   ',
                'ampher_name_eng' => 'Borabue',
                'geo_id' => 3,
                'province_id' => 32,
            ),
            482 => 
            array (
                'id' => 483,
                'ampher_code' => '4407',
                'ampher_name' => 'นาเชือก   ',
                'ampher_name_eng' => 'Na Chueak',
                'geo_id' => 3,
                'province_id' => 32,
            ),
            483 => 
            array (
                'id' => 484,
                'ampher_code' => '4408',
                'ampher_name' => 'พยัคฆภูมิพิสัย   ',
                'ampher_name_eng' => 'Phayakkhaphum Phisai',
                'geo_id' => 3,
                'province_id' => 32,
            ),
            484 => 
            array (
                'id' => 485,
                'ampher_code' => '4409',
                'ampher_name' => 'วาปีปทุม   ',
                'ampher_name_eng' => 'Wapi Pathum',
                'geo_id' => 3,
                'province_id' => 32,
            ),
            485 => 
            array (
                'id' => 486,
                'ampher_code' => '4410',
                'ampher_name' => 'นาดูน   ',
                'ampher_name_eng' => 'Na Dun',
                'geo_id' => 3,
                'province_id' => 32,
            ),
            486 => 
            array (
                'id' => 487,
                'ampher_code' => '4411',
                'ampher_name' => 'ยางสีสุราช   ',
                'ampher_name_eng' => 'Yang Sisurat',
                'geo_id' => 3,
                'province_id' => 32,
            ),
            487 => 
            array (
                'id' => 488,
                'ampher_code' => '4412',
                'ampher_name' => 'กุดรัง   ',
                'ampher_name_eng' => 'Kut Rang',
                'geo_id' => 3,
                'province_id' => 32,
            ),
            488 => 
            array (
                'id' => 489,
                'ampher_code' => '4413',
                'ampher_name' => 'ชื่นชม   ',
                'ampher_name_eng' => 'Chuen Chom',
                'geo_id' => 3,
                'province_id' => 32,
            ),
            489 => 
            array (
                'id' => 490,
                'ampher_code' => '4481',
                'ampher_name' => '*หลุบ   ',
                'ampher_name_eng' => '*Lub',
                'geo_id' => 3,
                'province_id' => 32,
            ),
            490 => 
            array (
                'id' => 491,
                'ampher_code' => '4501',
                'ampher_name' => 'เมืองร้อยเอ็ด   ',
                'ampher_name_eng' => 'Mueang Roi Et',
                'geo_id' => 3,
                'province_id' => 33,
            ),
            491 => 
            array (
                'id' => 492,
                'ampher_code' => '4502',
                'ampher_name' => 'เกษตรวิสัย   ',
                'ampher_name_eng' => 'Kaset Wisai',
                'geo_id' => 3,
                'province_id' => 33,
            ),
            492 => 
            array (
                'id' => 493,
                'ampher_code' => '4503',
                'ampher_name' => 'ปทุมรัตต์   ',
                'ampher_name_eng' => 'Pathum Rat',
                'geo_id' => 3,
                'province_id' => 33,
            ),
            493 => 
            array (
                'id' => 494,
                'ampher_code' => '4504',
                'ampher_name' => 'จตุรพักตรพิมาน   ',
                'ampher_name_eng' => 'Chaturaphak Phiman',
                'geo_id' => 3,
                'province_id' => 33,
            ),
            494 => 
            array (
                'id' => 495,
                'ampher_code' => '4505',
                'ampher_name' => 'ธวัชบุรี   ',
                'ampher_name_eng' => 'Thawat Buri',
                'geo_id' => 3,
                'province_id' => 33,
            ),
            495 => 
            array (
                'id' => 496,
                'ampher_code' => '4506',
                'ampher_name' => 'พนมไพร   ',
                'ampher_name_eng' => 'Phanom Phrai',
                'geo_id' => 3,
                'province_id' => 33,
            ),
            496 => 
            array (
                'id' => 497,
                'ampher_code' => '4507',
                'ampher_name' => 'โพนทอง   ',
                'ampher_name_eng' => 'Phon Thong',
                'geo_id' => 3,
                'province_id' => 33,
            ),
            497 => 
            array (
                'id' => 498,
                'ampher_code' => '4508',
                'ampher_name' => 'โพธิ์ชัย   ',
                'ampher_name_eng' => 'Pho Chai',
                'geo_id' => 3,
                'province_id' => 33,
            ),
            498 => 
            array (
                'id' => 499,
                'ampher_code' => '4509',
                'ampher_name' => 'หนองพอก   ',
                'ampher_name_eng' => 'Nong Phok',
                'geo_id' => 3,
                'province_id' => 33,
            ),
            499 => 
            array (
                'id' => 500,
                'ampher_code' => '4510',
                'ampher_name' => 'เสลภูมิ   ',
                'ampher_name_eng' => 'Selaphum',
                'geo_id' => 3,
                'province_id' => 33,
            ),
        ));
        \DB::table('amphers')->insert(array (
            0 => 
            array (
                'id' => 501,
                'ampher_code' => '4511',
                'ampher_name' => 'สุวรรณภูมิ   ',
                'ampher_name_eng' => 'Suwannaphum',
                'geo_id' => 3,
                'province_id' => 33,
            ),
            1 => 
            array (
                'id' => 502,
                'ampher_code' => '4512',
                'ampher_name' => 'เมืองสรวง   ',
                'ampher_name_eng' => 'Mueang Suang',
                'geo_id' => 3,
                'province_id' => 33,
            ),
            2 => 
            array (
                'id' => 503,
                'ampher_code' => '4513',
                'ampher_name' => 'โพนทราย   ',
                'ampher_name_eng' => 'Phon Sai',
                'geo_id' => 3,
                'province_id' => 33,
            ),
            3 => 
            array (
                'id' => 504,
                'ampher_code' => '4514',
                'ampher_name' => 'อาจสามารถ   ',
                'ampher_name_eng' => 'At Samat',
                'geo_id' => 3,
                'province_id' => 33,
            ),
            4 => 
            array (
                'id' => 505,
                'ampher_code' => '4515',
                'ampher_name' => 'เมยวดี   ',
                'ampher_name_eng' => 'Moei Wadi',
                'geo_id' => 3,
                'province_id' => 33,
            ),
            5 => 
            array (
                'id' => 506,
                'ampher_code' => '4516',
                'ampher_name' => 'ศรีสมเด็จ   ',
                'ampher_name_eng' => 'Si Somdet',
                'geo_id' => 3,
                'province_id' => 33,
            ),
            6 => 
            array (
                'id' => 507,
                'ampher_code' => '4517',
                'ampher_name' => 'จังหาร   ',
                'ampher_name_eng' => 'Changhan',
                'geo_id' => 3,
                'province_id' => 33,
            ),
            7 => 
            array (
                'id' => 508,
                'ampher_code' => '4518',
                'ampher_name' => 'เชียงขวัญ   ',
                'ampher_name_eng' => 'Chiang Khwan',
                'geo_id' => 3,
                'province_id' => 33,
            ),
            8 => 
            array (
                'id' => 509,
                'ampher_code' => '4519',
                'ampher_name' => 'หนองฮี   ',
                'ampher_name_eng' => 'Nong Hi',
                'geo_id' => 3,
                'province_id' => 33,
            ),
            9 => 
            array (
                'id' => 510,
                'ampher_code' => '4520',
                'ampher_name' => 'ทุ่งเขาหลวง   ',
                'ampher_name_eng' => 'Thung Khao Luangกิ่',
                'geo_id' => 3,
                'province_id' => 33,
            ),
            10 => 
            array (
                'id' => 511,
                'ampher_code' => '4601',
                'ampher_name' => 'เมืองกาฬสินธุ์   ',
                'ampher_name_eng' => 'Mueang Kalasin',
                'geo_id' => 3,
                'province_id' => 34,
            ),
            11 => 
            array (
                'id' => 512,
                'ampher_code' => '4602',
                'ampher_name' => 'นามน   ',
                'ampher_name_eng' => 'Na Mon',
                'geo_id' => 3,
                'province_id' => 34,
            ),
            12 => 
            array (
                'id' => 513,
                'ampher_code' => '4603',
                'ampher_name' => 'กมลาไสย   ',
                'ampher_name_eng' => 'Kamalasai',
                'geo_id' => 3,
                'province_id' => 34,
            ),
            13 => 
            array (
                'id' => 514,
                'ampher_code' => '4604',
                'ampher_name' => 'ร่องคำ   ',
                'ampher_name_eng' => 'Rong Kham',
                'geo_id' => 3,
                'province_id' => 34,
            ),
            14 => 
            array (
                'id' => 515,
                'ampher_code' => '4605',
                'ampher_name' => 'กุฉินารายณ์   ',
                'ampher_name_eng' => 'Kuchinarai',
                'geo_id' => 3,
                'province_id' => 34,
            ),
            15 => 
            array (
                'id' => 516,
                'ampher_code' => '4606',
                'ampher_name' => 'เขาวง   ',
                'ampher_name_eng' => 'Khao Wong',
                'geo_id' => 3,
                'province_id' => 34,
            ),
            16 => 
            array (
                'id' => 517,
                'ampher_code' => '4607',
                'ampher_name' => 'ยางตลาด   ',
                'ampher_name_eng' => 'Yang Talat',
                'geo_id' => 3,
                'province_id' => 34,
            ),
            17 => 
            array (
                'id' => 518,
                'ampher_code' => '4608',
                'ampher_name' => 'ห้วยเม็ก   ',
                'ampher_name_eng' => 'Huai Mek',
                'geo_id' => 3,
                'province_id' => 34,
            ),
            18 => 
            array (
                'id' => 519,
                'ampher_code' => '4609',
                'ampher_name' => 'สหัสขันธ์   ',
                'ampher_name_eng' => 'Sahatsakhan',
                'geo_id' => 3,
                'province_id' => 34,
            ),
            19 => 
            array (
                'id' => 520,
                'ampher_code' => '4610',
                'ampher_name' => 'คำม่วง   ',
                'ampher_name_eng' => 'Kham Muang',
                'geo_id' => 3,
                'province_id' => 34,
            ),
            20 => 
            array (
                'id' => 521,
                'ampher_code' => '4611',
                'ampher_name' => 'ท่าคันโท   ',
                'ampher_name_eng' => 'Tha Khantho',
                'geo_id' => 3,
                'province_id' => 34,
            ),
            21 => 
            array (
                'id' => 522,
                'ampher_code' => '4612',
                'ampher_name' => 'หนองกุงศรี   ',
                'ampher_name_eng' => 'Nong Kung Si',
                'geo_id' => 3,
                'province_id' => 34,
            ),
            22 => 
            array (
                'id' => 523,
                'ampher_code' => '4613',
                'ampher_name' => 'สมเด็จ   ',
                'ampher_name_eng' => 'Somdet',
                'geo_id' => 3,
                'province_id' => 34,
            ),
            23 => 
            array (
                'id' => 524,
                'ampher_code' => '4614',
                'ampher_name' => 'ห้วยผึ้ง   ',
                'ampher_name_eng' => 'Huai Phueng',
                'geo_id' => 3,
                'province_id' => 34,
            ),
            24 => 
            array (
                'id' => 525,
                'ampher_code' => '4615',
                'ampher_name' => 'สามชัย   ',
                'ampher_name_eng' => 'Sam Chai',
                'geo_id' => 3,
                'province_id' => 34,
            ),
            25 => 
            array (
                'id' => 526,
                'ampher_code' => '4616',
                'ampher_name' => 'นาคู   ',
                'ampher_name_eng' => 'Na Khu',
                'geo_id' => 3,
                'province_id' => 34,
            ),
            26 => 
            array (
                'id' => 527,
                'ampher_code' => '4617',
                'ampher_name' => 'ดอนจาน   ',
                'ampher_name_eng' => 'Don Chan',
                'geo_id' => 3,
                'province_id' => 34,
            ),
            27 => 
            array (
                'id' => 528,
                'ampher_code' => '4618',
                'ampher_name' => 'ฆ้องชัย   ',
                'ampher_name_eng' => 'Khong Chai',
                'geo_id' => 3,
                'province_id' => 34,
            ),
            28 => 
            array (
                'id' => 529,
                'ampher_code' => '4701',
                'ampher_name' => 'เมืองสกลนคร   ',
                'ampher_name_eng' => 'Mueang Sakon Nakhon',
                'geo_id' => 3,
                'province_id' => 35,
            ),
            29 => 
            array (
                'id' => 530,
                'ampher_code' => '4702',
                'ampher_name' => 'กุสุมาลย์   ',
                'ampher_name_eng' => 'Kusuman',
                'geo_id' => 3,
                'province_id' => 35,
            ),
            30 => 
            array (
                'id' => 531,
                'ampher_code' => '4703',
                'ampher_name' => 'กุดบาก   ',
                'ampher_name_eng' => 'Kut Bak',
                'geo_id' => 3,
                'province_id' => 35,
            ),
            31 => 
            array (
                'id' => 532,
                'ampher_code' => '4704',
                'ampher_name' => 'พรรณานิคม   ',
                'ampher_name_eng' => 'Phanna Nikhom',
                'geo_id' => 3,
                'province_id' => 35,
            ),
            32 => 
            array (
                'id' => 533,
                'ampher_code' => '4705',
                'ampher_name' => 'พังโคน   ',
                'ampher_name_eng' => 'Phang Khon',
                'geo_id' => 3,
                'province_id' => 35,
            ),
            33 => 
            array (
                'id' => 534,
                'ampher_code' => '4706',
                'ampher_name' => 'วาริชภูมิ   ',
                'ampher_name_eng' => 'Waritchaphum',
                'geo_id' => 3,
                'province_id' => 35,
            ),
            34 => 
            array (
                'id' => 535,
                'ampher_code' => '4707',
                'ampher_name' => 'นิคมน้ำอูน   ',
                'ampher_name_eng' => 'Nikhom Nam Un',
                'geo_id' => 3,
                'province_id' => 35,
            ),
            35 => 
            array (
                'id' => 536,
                'ampher_code' => '4708',
                'ampher_name' => 'วานรนิวาส   ',
                'ampher_name_eng' => 'Wanon Niwat',
                'geo_id' => 3,
                'province_id' => 35,
            ),
            36 => 
            array (
                'id' => 537,
                'ampher_code' => '4709',
                'ampher_name' => 'คำตากล้า   ',
                'ampher_name_eng' => 'Kham Ta Kla',
                'geo_id' => 3,
                'province_id' => 35,
            ),
            37 => 
            array (
                'id' => 538,
                'ampher_code' => '4710',
                'ampher_name' => 'บ้านม่วง   ',
                'ampher_name_eng' => 'Ban Muang',
                'geo_id' => 3,
                'province_id' => 35,
            ),
            38 => 
            array (
                'id' => 539,
                'ampher_code' => '4711',
                'ampher_name' => 'อากาศอำนวย   ',
                'ampher_name_eng' => 'Akat Amnuai',
                'geo_id' => 3,
                'province_id' => 35,
            ),
            39 => 
            array (
                'id' => 540,
                'ampher_code' => '4712',
                'ampher_name' => 'สว่างแดนดิน   ',
                'ampher_name_eng' => 'Sawang Daen Din',
                'geo_id' => 3,
                'province_id' => 35,
            ),
            40 => 
            array (
                'id' => 541,
                'ampher_code' => '4713',
                'ampher_name' => 'ส่องดาว   ',
                'ampher_name_eng' => 'Song Dao',
                'geo_id' => 3,
                'province_id' => 35,
            ),
            41 => 
            array (
                'id' => 542,
                'ampher_code' => '4714',
                'ampher_name' => 'เต่างอย   ',
                'ampher_name_eng' => 'Tao Ngoi',
                'geo_id' => 3,
                'province_id' => 35,
            ),
            42 => 
            array (
                'id' => 543,
                'ampher_code' => '4715',
                'ampher_name' => 'โคกศรีสุพรรณ   ',
                'ampher_name_eng' => 'Khok Si Suphan',
                'geo_id' => 3,
                'province_id' => 35,
            ),
            43 => 
            array (
                'id' => 544,
                'ampher_code' => '4716',
                'ampher_name' => 'เจริญศิลป์   ',
                'ampher_name_eng' => 'Charoen Sin',
                'geo_id' => 3,
                'province_id' => 35,
            ),
            44 => 
            array (
                'id' => 545,
                'ampher_code' => '4717',
                'ampher_name' => 'โพนนาแก้ว   ',
                'ampher_name_eng' => 'Phon Na Kaeo',
                'geo_id' => 3,
                'province_id' => 35,
            ),
            45 => 
            array (
                'id' => 546,
                'ampher_code' => '4718',
                'ampher_name' => 'ภูพาน   ',
                'ampher_name_eng' => 'Phu Phan',
                'geo_id' => 3,
                'province_id' => 35,
            ),
            46 => 
            array (
                'id' => 547,
                'ampher_code' => '4751',
            'ampher_name' => 'วานรนิวาส (สาขาตำบลกุดเรือคำ)*   ',
                'ampher_name_eng' => 'Wanon Niwat',
                'geo_id' => 3,
                'province_id' => 35,
            ),
            47 => 
            array (
                'id' => 548,
                'ampher_code' => '4781',
                'ampher_name' => '*อ.บ้านหัน  จ.สกลนคร   ',
                'ampher_name_eng' => '*Banhan',
                'geo_id' => 3,
                'province_id' => 35,
            ),
            48 => 
            array (
                'id' => 549,
                'ampher_code' => '4801',
                'ampher_name' => 'เมืองนครพนม   ',
                'ampher_name_eng' => 'Mueang Nakhon Phanom',
                'geo_id' => 3,
                'province_id' => 36,
            ),
            49 => 
            array (
                'id' => 550,
                'ampher_code' => '4802',
                'ampher_name' => 'ปลาปาก   ',
                'ampher_name_eng' => 'Pla Pak',
                'geo_id' => 3,
                'province_id' => 36,
            ),
            50 => 
            array (
                'id' => 551,
                'ampher_code' => '4803',
                'ampher_name' => 'ท่าอุเทน   ',
                'ampher_name_eng' => 'Tha Uthen',
                'geo_id' => 3,
                'province_id' => 36,
            ),
            51 => 
            array (
                'id' => 552,
                'ampher_code' => '4804',
                'ampher_name' => 'บ้านแพง   ',
                'ampher_name_eng' => 'Ban Phaeng',
                'geo_id' => 3,
                'province_id' => 36,
            ),
            52 => 
            array (
                'id' => 553,
                'ampher_code' => '4805',
                'ampher_name' => 'ธาตุพนม   ',
                'ampher_name_eng' => 'That Phanom',
                'geo_id' => 3,
                'province_id' => 36,
            ),
            53 => 
            array (
                'id' => 554,
                'ampher_code' => '4806',
                'ampher_name' => 'เรณูนคร   ',
                'ampher_name_eng' => 'Renu Nakhon',
                'geo_id' => 3,
                'province_id' => 36,
            ),
            54 => 
            array (
                'id' => 555,
                'ampher_code' => '4807',
                'ampher_name' => 'นาแก   ',
                'ampher_name_eng' => 'Na Kae',
                'geo_id' => 3,
                'province_id' => 36,
            ),
            55 => 
            array (
                'id' => 556,
                'ampher_code' => '4808',
                'ampher_name' => 'ศรีสงคราม   ',
                'ampher_name_eng' => 'Si Songkhram',
                'geo_id' => 3,
                'province_id' => 36,
            ),
            56 => 
            array (
                'id' => 557,
                'ampher_code' => '4809',
                'ampher_name' => 'นาหว้า   ',
                'ampher_name_eng' => 'Na Wa',
                'geo_id' => 3,
                'province_id' => 36,
            ),
            57 => 
            array (
                'id' => 558,
                'ampher_code' => '4810',
                'ampher_name' => 'โพนสวรรค์   ',
                'ampher_name_eng' => 'Phon Sawan',
                'geo_id' => 3,
                'province_id' => 36,
            ),
            58 => 
            array (
                'id' => 559,
                'ampher_code' => '4811',
                'ampher_name' => 'นาทม   ',
                'ampher_name_eng' => 'Na Thom',
                'geo_id' => 3,
                'province_id' => 36,
            ),
            59 => 
            array (
                'id' => 560,
                'ampher_code' => '4812',
                'ampher_name' => 'วังยาง   ',
                'ampher_name_eng' => 'Wang Yang',
                'geo_id' => 3,
                'province_id' => 36,
            ),
            60 => 
            array (
                'id' => 561,
                'ampher_code' => '4901',
                'ampher_name' => 'เมืองมุกดาหาร   ',
                'ampher_name_eng' => 'Mueang Mukdahan',
                'geo_id' => 3,
                'province_id' => 37,
            ),
            61 => 
            array (
                'id' => 562,
                'ampher_code' => '4902',
                'ampher_name' => 'นิคมคำสร้อย   ',
                'ampher_name_eng' => 'Nikhom Kham Soi',
                'geo_id' => 3,
                'province_id' => 37,
            ),
            62 => 
            array (
                'id' => 563,
                'ampher_code' => '4903',
                'ampher_name' => 'ดอนตาล   ',
                'ampher_name_eng' => 'Don Tan',
                'geo_id' => 3,
                'province_id' => 37,
            ),
            63 => 
            array (
                'id' => 564,
                'ampher_code' => '4904',
                'ampher_name' => 'ดงหลวง   ',
                'ampher_name_eng' => 'Dong Luang',
                'geo_id' => 3,
                'province_id' => 37,
            ),
            64 => 
            array (
                'id' => 565,
                'ampher_code' => '4905',
                'ampher_name' => 'คำชะอี   ',
                'ampher_name_eng' => 'Khamcha-i',
                'geo_id' => 3,
                'province_id' => 37,
            ),
            65 => 
            array (
                'id' => 566,
                'ampher_code' => '4906',
                'ampher_name' => 'หว้านใหญ่   ',
                'ampher_name_eng' => 'Wan Yai',
                'geo_id' => 3,
                'province_id' => 37,
            ),
            66 => 
            array (
                'id' => 567,
                'ampher_code' => '4907',
                'ampher_name' => 'หนองสูง   ',
                'ampher_name_eng' => 'Nong Sung',
                'geo_id' => 3,
                'province_id' => 37,
            ),
            67 => 
            array (
                'id' => 568,
                'ampher_code' => '5001',
                'ampher_name' => 'เมืองเชียงใหม่   ',
                'ampher_name_eng' => 'Mueang Chiang Mai',
                'geo_id' => 1,
                'province_id' => 38,
            ),
            68 => 
            array (
                'id' => 569,
                'ampher_code' => '5002',
                'ampher_name' => 'จอมทอง   ',
                'ampher_name_eng' => 'Chom Thong',
                'geo_id' => 1,
                'province_id' => 38,
            ),
            69 => 
            array (
                'id' => 570,
                'ampher_code' => '5003',
                'ampher_name' => 'แม่แจ่ม   ',
                'ampher_name_eng' => 'Mae Chaem',
                'geo_id' => 1,
                'province_id' => 38,
            ),
            70 => 
            array (
                'id' => 571,
                'ampher_code' => '5004',
                'ampher_name' => 'เชียงดาว   ',
                'ampher_name_eng' => 'Chiang Dao',
                'geo_id' => 1,
                'province_id' => 38,
            ),
            71 => 
            array (
                'id' => 572,
                'ampher_code' => '5005',
                'ampher_name' => 'ดอยสะเก็ด   ',
                'ampher_name_eng' => 'Doi Saket',
                'geo_id' => 1,
                'province_id' => 38,
            ),
            72 => 
            array (
                'id' => 573,
                'ampher_code' => '5006',
                'ampher_name' => 'แม่แตง   ',
                'ampher_name_eng' => 'Mae Taeng',
                'geo_id' => 1,
                'province_id' => 38,
            ),
            73 => 
            array (
                'id' => 574,
                'ampher_code' => '5007',
                'ampher_name' => 'แม่ริม   ',
                'ampher_name_eng' => 'Mae Rim',
                'geo_id' => 1,
                'province_id' => 38,
            ),
            74 => 
            array (
                'id' => 575,
                'ampher_code' => '5008',
                'ampher_name' => 'สะเมิง   ',
                'ampher_name_eng' => 'Samoeng',
                'geo_id' => 1,
                'province_id' => 38,
            ),
            75 => 
            array (
                'id' => 576,
                'ampher_code' => '5009',
                'ampher_name' => 'ฝาง   ',
                'ampher_name_eng' => 'Fang',
                'geo_id' => 1,
                'province_id' => 38,
            ),
            76 => 
            array (
                'id' => 577,
                'ampher_code' => '5010',
                'ampher_name' => 'แม่อาย   ',
                'ampher_name_eng' => 'Mae Ai',
                'geo_id' => 1,
                'province_id' => 38,
            ),
            77 => 
            array (
                'id' => 578,
                'ampher_code' => '5011',
                'ampher_name' => 'พร้าว   ',
                'ampher_name_eng' => 'Phrao',
                'geo_id' => 1,
                'province_id' => 38,
            ),
            78 => 
            array (
                'id' => 579,
                'ampher_code' => '5012',
                'ampher_name' => 'สันป่าตอง   ',
                'ampher_name_eng' => 'San Pa Tong',
                'geo_id' => 1,
                'province_id' => 38,
            ),
            79 => 
            array (
                'id' => 580,
                'ampher_code' => '5013',
                'ampher_name' => 'สันกำแพง   ',
                'ampher_name_eng' => 'San Kamphaeng',
                'geo_id' => 1,
                'province_id' => 38,
            ),
            80 => 
            array (
                'id' => 581,
                'ampher_code' => '5014',
                'ampher_name' => 'สันทราย   ',
                'ampher_name_eng' => 'San Sai',
                'geo_id' => 1,
                'province_id' => 38,
            ),
            81 => 
            array (
                'id' => 582,
                'ampher_code' => '5015',
                'ampher_name' => 'หางดง   ',
                'ampher_name_eng' => 'Hang Dong',
                'geo_id' => 1,
                'province_id' => 38,
            ),
            82 => 
            array (
                'id' => 583,
                'ampher_code' => '5016',
                'ampher_name' => 'ฮอด   ',
                'ampher_name_eng' => 'Hot',
                'geo_id' => 1,
                'province_id' => 38,
            ),
            83 => 
            array (
                'id' => 584,
                'ampher_code' => '5017',
                'ampher_name' => 'ดอยเต่า   ',
                'ampher_name_eng' => 'Doi Tao',
                'geo_id' => 1,
                'province_id' => 38,
            ),
            84 => 
            array (
                'id' => 585,
                'ampher_code' => '5018',
                'ampher_name' => 'อมก๋อย   ',
                'ampher_name_eng' => 'Omkoi',
                'geo_id' => 1,
                'province_id' => 38,
            ),
            85 => 
            array (
                'id' => 586,
                'ampher_code' => '5019',
                'ampher_name' => 'สารภี   ',
                'ampher_name_eng' => 'Saraphi',
                'geo_id' => 1,
                'province_id' => 38,
            ),
            86 => 
            array (
                'id' => 587,
                'ampher_code' => '5020',
                'ampher_name' => 'เวียงแหง   ',
                'ampher_name_eng' => 'Wiang Haeng',
                'geo_id' => 1,
                'province_id' => 38,
            ),
            87 => 
            array (
                'id' => 588,
                'ampher_code' => '5021',
                'ampher_name' => 'ไชยปราการ   ',
                'ampher_name_eng' => 'Chai Prakan',
                'geo_id' => 1,
                'province_id' => 38,
            ),
            88 => 
            array (
                'id' => 589,
                'ampher_code' => '5022',
                'ampher_name' => 'แม่วาง   ',
                'ampher_name_eng' => 'Mae Wang',
                'geo_id' => 1,
                'province_id' => 38,
            ),
            89 => 
            array (
                'id' => 590,
                'ampher_code' => '5023',
                'ampher_name' => 'แม่ออน   ',
                'ampher_name_eng' => 'Mae On',
                'geo_id' => 1,
                'province_id' => 38,
            ),
            90 => 
            array (
                'id' => 591,
                'ampher_code' => '5024',
                'ampher_name' => 'ดอยหล่อ   ',
                'ampher_name_eng' => 'Doi Lo',
                'geo_id' => 1,
                'province_id' => 38,
            ),
            91 => 
            array (
                'id' => 592,
                'ampher_code' => '5051',
            'ampher_name' => 'เทศบาลนครเชียงใหม่ (สาขาแขวงกาลวิละ)*   ',
            'ampher_name_eng' => 'Tet Saban Nakorn Chiangmai(Kan lawi la)*',
                'geo_id' => 1,
                'province_id' => 38,
            ),
            92 => 
            array (
                'id' => 593,
                'ampher_code' => '5052',
            'ampher_name' => 'เทศบาลนครเชียงใหม่ (สาขาแขวงศรีวิชั)*   ',
            'ampher_name_eng' => 'Tet Saban Nakorn Chiangmai(Sri Wi)*',
                'geo_id' => 1,
                'province_id' => 38,
            ),
            93 => 
            array (
                'id' => 594,
                'ampher_code' => '5053',
            'ampher_name' => 'เทศบาลนครเชียงใหม่ (สาขาเม็งราย)*   ',
            'ampher_name_eng' => 'Tet Saban Nakorn Chiangmai(Meng Rai)*',
                'geo_id' => 1,
                'province_id' => 38,
            ),
            94 => 
            array (
                'id' => 595,
                'ampher_code' => '5101',
                'ampher_name' => 'เมืองลำพูน   ',
                'ampher_name_eng' => 'Mueang Lamphun',
                'geo_id' => 1,
                'province_id' => 39,
            ),
            95 => 
            array (
                'id' => 596,
                'ampher_code' => '5102',
                'ampher_name' => 'แม่ทา   ',
                'ampher_name_eng' => 'Mae Tha',
                'geo_id' => 1,
                'province_id' => 39,
            ),
            96 => 
            array (
                'id' => 597,
                'ampher_code' => '5103',
                'ampher_name' => 'บ้านโฮ่ง   ',
                'ampher_name_eng' => 'Ban Hong',
                'geo_id' => 1,
                'province_id' => 39,
            ),
            97 => 
            array (
                'id' => 598,
                'ampher_code' => '5104',
                'ampher_name' => 'ลี้   ',
                'ampher_name_eng' => 'Li',
                'geo_id' => 1,
                'province_id' => 39,
            ),
            98 => 
            array (
                'id' => 599,
                'ampher_code' => '5105',
                'ampher_name' => 'ทุ่งหัวช้าง   ',
                'ampher_name_eng' => 'Thung Hua Chang',
                'geo_id' => 1,
                'province_id' => 39,
            ),
            99 => 
            array (
                'id' => 600,
                'ampher_code' => '5106',
                'ampher_name' => 'ป่าซาง   ',
                'ampher_name_eng' => 'Pa Sang',
                'geo_id' => 1,
                'province_id' => 39,
            ),
            100 => 
            array (
                'id' => 601,
                'ampher_code' => '5107',
                'ampher_name' => 'บ้านธิ   ',
                'ampher_name_eng' => 'Ban Thi',
                'geo_id' => 1,
                'province_id' => 39,
            ),
            101 => 
            array (
                'id' => 602,
                'ampher_code' => '5108',
                'ampher_name' => 'เวียงหนองล่อง   ',
                'ampher_name_eng' => 'Wiang Nong Long',
                'geo_id' => 1,
                'province_id' => 39,
            ),
            102 => 
            array (
                'id' => 603,
                'ampher_code' => '5201',
                'ampher_name' => 'เมืองลำปาง   ',
                'ampher_name_eng' => 'Mueang Lampang',
                'geo_id' => 1,
                'province_id' => 40,
            ),
            103 => 
            array (
                'id' => 604,
                'ampher_code' => '5202',
                'ampher_name' => 'แม่เมาะ   ',
                'ampher_name_eng' => 'Mae Mo',
                'geo_id' => 1,
                'province_id' => 40,
            ),
            104 => 
            array (
                'id' => 605,
                'ampher_code' => '5203',
                'ampher_name' => 'เกาะคา   ',
                'ampher_name_eng' => 'Ko Kha',
                'geo_id' => 1,
                'province_id' => 40,
            ),
            105 => 
            array (
                'id' => 606,
                'ampher_code' => '5204',
                'ampher_name' => 'เสริมงาม   ',
                'ampher_name_eng' => 'Soem Ngam',
                'geo_id' => 1,
                'province_id' => 40,
            ),
            106 => 
            array (
                'id' => 607,
                'ampher_code' => '5205',
                'ampher_name' => 'งาว   ',
                'ampher_name_eng' => 'Ngao',
                'geo_id' => 1,
                'province_id' => 40,
            ),
            107 => 
            array (
                'id' => 608,
                'ampher_code' => '5206',
                'ampher_name' => 'แจ้ห่ม   ',
                'ampher_name_eng' => 'Chae Hom',
                'geo_id' => 1,
                'province_id' => 40,
            ),
            108 => 
            array (
                'id' => 609,
                'ampher_code' => '5207',
                'ampher_name' => 'วังเหนือ   ',
                'ampher_name_eng' => 'Wang Nuea',
                'geo_id' => 1,
                'province_id' => 40,
            ),
            109 => 
            array (
                'id' => 610,
                'ampher_code' => '5208',
                'ampher_name' => 'เถิน   ',
                'ampher_name_eng' => 'Thoen',
                'geo_id' => 1,
                'province_id' => 40,
            ),
            110 => 
            array (
                'id' => 611,
                'ampher_code' => '5209',
                'ampher_name' => 'แม่พริก   ',
                'ampher_name_eng' => 'Mae Phrik',
                'geo_id' => 1,
                'province_id' => 40,
            ),
            111 => 
            array (
                'id' => 612,
                'ampher_code' => '5210',
                'ampher_name' => 'แม่ทะ   ',
                'ampher_name_eng' => 'Mae Tha',
                'geo_id' => 1,
                'province_id' => 40,
            ),
            112 => 
            array (
                'id' => 613,
                'ampher_code' => '5211',
                'ampher_name' => 'สบปราบ   ',
                'ampher_name_eng' => 'Sop Prap',
                'geo_id' => 1,
                'province_id' => 40,
            ),
            113 => 
            array (
                'id' => 614,
                'ampher_code' => '5212',
                'ampher_name' => 'ห้างฉัตร   ',
                'ampher_name_eng' => 'Hang Chat',
                'geo_id' => 1,
                'province_id' => 40,
            ),
            114 => 
            array (
                'id' => 615,
                'ampher_code' => '5213',
                'ampher_name' => 'เมืองปาน   ',
                'ampher_name_eng' => 'Mueang Pan',
                'geo_id' => 1,
                'province_id' => 40,
            ),
            115 => 
            array (
                'id' => 616,
                'ampher_code' => '5301',
                'ampher_name' => 'เมืองอุตรดิตถ์   ',
                'ampher_name_eng' => 'Mueang Uttaradit',
                'geo_id' => 1,
                'province_id' => 41,
            ),
            116 => 
            array (
                'id' => 617,
                'ampher_code' => '5302',
                'ampher_name' => 'ตรอน   ',
                'ampher_name_eng' => 'Tron',
                'geo_id' => 1,
                'province_id' => 41,
            ),
            117 => 
            array (
                'id' => 618,
                'ampher_code' => '5303',
                'ampher_name' => 'ท่าปลา   ',
                'ampher_name_eng' => 'Tha Pla',
                'geo_id' => 1,
                'province_id' => 41,
            ),
            118 => 
            array (
                'id' => 619,
                'ampher_code' => '5304',
                'ampher_name' => 'น้ำปาด   ',
                'ampher_name_eng' => 'Nam Pat',
                'geo_id' => 1,
                'province_id' => 41,
            ),
            119 => 
            array (
                'id' => 620,
                'ampher_code' => '5305',
                'ampher_name' => 'ฟากท่า   ',
                'ampher_name_eng' => 'Fak Tha',
                'geo_id' => 1,
                'province_id' => 41,
            ),
            120 => 
            array (
                'id' => 621,
                'ampher_code' => '5306',
                'ampher_name' => 'บ้านโคก   ',
                'ampher_name_eng' => 'Ban Khok',
                'geo_id' => 1,
                'province_id' => 41,
            ),
            121 => 
            array (
                'id' => 622,
                'ampher_code' => '5307',
                'ampher_name' => 'พิชัย   ',
                'ampher_name_eng' => 'Phichai',
                'geo_id' => 1,
                'province_id' => 41,
            ),
            122 => 
            array (
                'id' => 623,
                'ampher_code' => '5308',
                'ampher_name' => 'ลับแล   ',
                'ampher_name_eng' => 'Laplae',
                'geo_id' => 1,
                'province_id' => 41,
            ),
            123 => 
            array (
                'id' => 624,
                'ampher_code' => '5309',
                'ampher_name' => 'ทองแสนขัน   ',
                'ampher_name_eng' => 'Thong Saen Khan',
                'geo_id' => 1,
                'province_id' => 41,
            ),
            124 => 
            array (
                'id' => 625,
                'ampher_code' => '5401',
                'ampher_name' => 'เมืองแพร่   ',
                'ampher_name_eng' => 'Mueang Phrae',
                'geo_id' => 1,
                'province_id' => 42,
            ),
            125 => 
            array (
                'id' => 626,
                'ampher_code' => '5402',
                'ampher_name' => 'ร้องกวาง   ',
                'ampher_name_eng' => 'Rong Kwang',
                'geo_id' => 1,
                'province_id' => 42,
            ),
            126 => 
            array (
                'id' => 627,
                'ampher_code' => '5403',
                'ampher_name' => 'ลอง   ',
                'ampher_name_eng' => 'Long',
                'geo_id' => 1,
                'province_id' => 42,
            ),
            127 => 
            array (
                'id' => 628,
                'ampher_code' => '5404',
                'ampher_name' => 'สูงเม่น   ',
                'ampher_name_eng' => 'Sung Men',
                'geo_id' => 1,
                'province_id' => 42,
            ),
            128 => 
            array (
                'id' => 629,
                'ampher_code' => '5405',
                'ampher_name' => 'เด่นชัย   ',
                'ampher_name_eng' => 'Den Chai',
                'geo_id' => 1,
                'province_id' => 42,
            ),
            129 => 
            array (
                'id' => 630,
                'ampher_code' => '5406',
                'ampher_name' => 'สอง   ',
                'ampher_name_eng' => 'Song',
                'geo_id' => 1,
                'province_id' => 42,
            ),
            130 => 
            array (
                'id' => 631,
                'ampher_code' => '5407',
                'ampher_name' => 'วังชิ้น   ',
                'ampher_name_eng' => 'Wang Chin',
                'geo_id' => 1,
                'province_id' => 42,
            ),
            131 => 
            array (
                'id' => 632,
                'ampher_code' => '5408',
                'ampher_name' => 'หนองม่วงไข่   ',
                'ampher_name_eng' => 'Nong Muang Khai',
                'geo_id' => 1,
                'province_id' => 42,
            ),
            132 => 
            array (
                'id' => 633,
                'ampher_code' => '5501',
                'ampher_name' => 'เมืองน่าน   ',
                'ampher_name_eng' => 'Mueang Nan',
                'geo_id' => 1,
                'province_id' => 43,
            ),
            133 => 
            array (
                'id' => 634,
                'ampher_code' => '5502',
                'ampher_name' => 'แม่จริม   ',
                'ampher_name_eng' => 'Mae Charim',
                'geo_id' => 1,
                'province_id' => 43,
            ),
            134 => 
            array (
                'id' => 635,
                'ampher_code' => '5503',
                'ampher_name' => 'บ้านหลวง   ',
                'ampher_name_eng' => 'Ban Luang',
                'geo_id' => 1,
                'province_id' => 43,
            ),
            135 => 
            array (
                'id' => 636,
                'ampher_code' => '5504',
                'ampher_name' => 'นาน้อย   ',
                'ampher_name_eng' => 'Na Noi',
                'geo_id' => 1,
                'province_id' => 43,
            ),
            136 => 
            array (
                'id' => 637,
                'ampher_code' => '5505',
                'ampher_name' => 'ปัว   ',
                'ampher_name_eng' => 'Pua',
                'geo_id' => 1,
                'province_id' => 43,
            ),
            137 => 
            array (
                'id' => 638,
                'ampher_code' => '5506',
                'ampher_name' => 'ท่าวังผา   ',
                'ampher_name_eng' => 'Tha Wang Pha',
                'geo_id' => 1,
                'province_id' => 43,
            ),
            138 => 
            array (
                'id' => 639,
                'ampher_code' => '5507',
                'ampher_name' => 'เวียงสา   ',
                'ampher_name_eng' => 'Wiang Sa',
                'geo_id' => 1,
                'province_id' => 43,
            ),
            139 => 
            array (
                'id' => 640,
                'ampher_code' => '5508',
                'ampher_name' => 'ทุ่งช้าง   ',
                'ampher_name_eng' => 'Thung Chang',
                'geo_id' => 1,
                'province_id' => 43,
            ),
            140 => 
            array (
                'id' => 641,
                'ampher_code' => '5509',
                'ampher_name' => 'เชียงกลาง   ',
                'ampher_name_eng' => 'Chiang Klang',
                'geo_id' => 1,
                'province_id' => 43,
            ),
            141 => 
            array (
                'id' => 642,
                'ampher_code' => '5510',
                'ampher_name' => 'นาหมื่น   ',
                'ampher_name_eng' => 'Na Muen',
                'geo_id' => 1,
                'province_id' => 43,
            ),
            142 => 
            array (
                'id' => 643,
                'ampher_code' => '5511',
                'ampher_name' => 'สันติสุข   ',
                'ampher_name_eng' => 'Santi Suk',
                'geo_id' => 1,
                'province_id' => 43,
            ),
            143 => 
            array (
                'id' => 644,
                'ampher_code' => '5512',
                'ampher_name' => 'บ่อเกลือ   ',
                'ampher_name_eng' => 'Bo Kluea',
                'geo_id' => 1,
                'province_id' => 43,
            ),
            144 => 
            array (
                'id' => 645,
                'ampher_code' => '5513',
                'ampher_name' => 'สองแคว   ',
                'ampher_name_eng' => 'Song Khwae',
                'geo_id' => 1,
                'province_id' => 43,
            ),
            145 => 
            array (
                'id' => 646,
                'ampher_code' => '5514',
                'ampher_name' => 'ภูเพียง   ',
                'ampher_name_eng' => 'Phu Phiang',
                'geo_id' => 1,
                'province_id' => 43,
            ),
            146 => 
            array (
                'id' => 647,
                'ampher_code' => '5515',
                'ampher_name' => 'เฉลิมพระเกียรติ   ',
                'ampher_name_eng' => 'Chaloem Phra Kiat',
                'geo_id' => 1,
                'province_id' => 43,
            ),
            147 => 
            array (
                'id' => 648,
                'ampher_code' => '5601',
                'ampher_name' => 'เมืองพะเยา   ',
                'ampher_name_eng' => 'Mueang Phayao',
                'geo_id' => 1,
                'province_id' => 44,
            ),
            148 => 
            array (
                'id' => 649,
                'ampher_code' => '5602',
                'ampher_name' => 'จุน   ',
                'ampher_name_eng' => 'Chun',
                'geo_id' => 1,
                'province_id' => 44,
            ),
            149 => 
            array (
                'id' => 650,
                'ampher_code' => '5603',
                'ampher_name' => 'เชียงคำ   ',
                'ampher_name_eng' => 'Chiang Kham',
                'geo_id' => 1,
                'province_id' => 44,
            ),
            150 => 
            array (
                'id' => 651,
                'ampher_code' => '5604',
                'ampher_name' => 'เชียงม่วน   ',
                'ampher_name_eng' => 'Chiang Muan',
                'geo_id' => 1,
                'province_id' => 44,
            ),
            151 => 
            array (
                'id' => 652,
                'ampher_code' => '5605',
                'ampher_name' => 'ดอกคำใต้   ',
                'ampher_name_eng' => 'Dok Khamtai',
                'geo_id' => 1,
                'province_id' => 44,
            ),
            152 => 
            array (
                'id' => 653,
                'ampher_code' => '5606',
                'ampher_name' => 'ปง   ',
                'ampher_name_eng' => 'Pong',
                'geo_id' => 1,
                'province_id' => 44,
            ),
            153 => 
            array (
                'id' => 654,
                'ampher_code' => '5607',
                'ampher_name' => 'แม่ใจ   ',
                'ampher_name_eng' => 'Mae Chai',
                'geo_id' => 1,
                'province_id' => 44,
            ),
            154 => 
            array (
                'id' => 655,
                'ampher_code' => '5608',
                'ampher_name' => 'ภูซาง   ',
                'ampher_name_eng' => 'Phu Sang',
                'geo_id' => 1,
                'province_id' => 44,
            ),
            155 => 
            array (
                'id' => 656,
                'ampher_code' => '5609',
                'ampher_name' => 'ภูกามยาว   ',
                'ampher_name_eng' => 'Phu Kamyao',
                'geo_id' => 1,
                'province_id' => 44,
            ),
            156 => 
            array (
                'id' => 657,
                'ampher_code' => '5701',
                'ampher_name' => 'เมืองเชียงราย   ',
                'ampher_name_eng' => 'Mueang Chiang Rai',
                'geo_id' => 1,
                'province_id' => 45,
            ),
            157 => 
            array (
                'id' => 658,
                'ampher_code' => '5702',
                'ampher_name' => 'เวียงชัย   ',
                'ampher_name_eng' => 'Wiang Chai',
                'geo_id' => 1,
                'province_id' => 45,
            ),
            158 => 
            array (
                'id' => 659,
                'ampher_code' => '5703',
                'ampher_name' => 'เชียงของ   ',
                'ampher_name_eng' => 'Chiang Khong',
                'geo_id' => 1,
                'province_id' => 45,
            ),
            159 => 
            array (
                'id' => 660,
                'ampher_code' => '5704',
                'ampher_name' => 'เทิง   ',
                'ampher_name_eng' => 'Thoeng',
                'geo_id' => 1,
                'province_id' => 45,
            ),
            160 => 
            array (
                'id' => 661,
                'ampher_code' => '5705',
                'ampher_name' => 'พาน   ',
                'ampher_name_eng' => 'Phan',
                'geo_id' => 1,
                'province_id' => 45,
            ),
            161 => 
            array (
                'id' => 662,
                'ampher_code' => '5706',
                'ampher_name' => 'ป่าแดด   ',
                'ampher_name_eng' => 'Pa Daet',
                'geo_id' => 1,
                'province_id' => 45,
            ),
            162 => 
            array (
                'id' => 663,
                'ampher_code' => '5707',
                'ampher_name' => 'แม่จัน   ',
                'ampher_name_eng' => 'Mae Chan',
                'geo_id' => 1,
                'province_id' => 45,
            ),
            163 => 
            array (
                'id' => 664,
                'ampher_code' => '5708',
                'ampher_name' => 'เชียงแสน   ',
                'ampher_name_eng' => 'Chiang Saen',
                'geo_id' => 1,
                'province_id' => 45,
            ),
            164 => 
            array (
                'id' => 665,
                'ampher_code' => '5709',
                'ampher_name' => 'แม่สาย   ',
                'ampher_name_eng' => 'Mae Sai',
                'geo_id' => 1,
                'province_id' => 45,
            ),
            165 => 
            array (
                'id' => 666,
                'ampher_code' => '5710',
                'ampher_name' => 'แม่สรวย   ',
                'ampher_name_eng' => 'Mae Suai',
                'geo_id' => 1,
                'province_id' => 45,
            ),
            166 => 
            array (
                'id' => 667,
                'ampher_code' => '5711',
                'ampher_name' => 'เวียงป่าเป้า   ',
                'ampher_name_eng' => 'Wiang Pa Pao',
                'geo_id' => 1,
                'province_id' => 45,
            ),
            167 => 
            array (
                'id' => 668,
                'ampher_code' => '5712',
                'ampher_name' => 'พญาเม็งราย   ',
                'ampher_name_eng' => 'Phaya Mengrai',
                'geo_id' => 1,
                'province_id' => 45,
            ),
            168 => 
            array (
                'id' => 669,
                'ampher_code' => '5713',
                'ampher_name' => 'เวียงแก่น   ',
                'ampher_name_eng' => 'Wiang Kaen',
                'geo_id' => 1,
                'province_id' => 45,
            ),
            169 => 
            array (
                'id' => 670,
                'ampher_code' => '5714',
                'ampher_name' => 'ขุนตาล   ',
                'ampher_name_eng' => 'Khun Tan',
                'geo_id' => 1,
                'province_id' => 45,
            ),
            170 => 
            array (
                'id' => 671,
                'ampher_code' => '5715',
                'ampher_name' => 'แม่ฟ้าหลวง   ',
                'ampher_name_eng' => 'Mae Fa Luang',
                'geo_id' => 1,
                'province_id' => 45,
            ),
            171 => 
            array (
                'id' => 672,
                'ampher_code' => '5716',
                'ampher_name' => 'แม่ลาว   ',
                'ampher_name_eng' => 'Mae Lao',
                'geo_id' => 1,
                'province_id' => 45,
            ),
            172 => 
            array (
                'id' => 673,
                'ampher_code' => '5717',
                'ampher_name' => 'เวียงเชียงรุ้ง   ',
                'ampher_name_eng' => 'Wiang Chiang Rung',
                'geo_id' => 1,
                'province_id' => 45,
            ),
            173 => 
            array (
                'id' => 674,
                'ampher_code' => '5718',
                'ampher_name' => 'ดอยหลวง   ',
                'ampher_name_eng' => 'Doi Luang',
                'geo_id' => 1,
                'province_id' => 45,
            ),
            174 => 
            array (
                'id' => 675,
                'ampher_code' => '5801',
                'ampher_name' => 'เมืองแม่ฮ่องสอน   ',
                'ampher_name_eng' => 'Mueang Mae Hong Son',
                'geo_id' => 1,
                'province_id' => 46,
            ),
            175 => 
            array (
                'id' => 676,
                'ampher_code' => '5802',
                'ampher_name' => 'ขุนยวม   ',
                'ampher_name_eng' => 'Khun Yuam',
                'geo_id' => 1,
                'province_id' => 46,
            ),
            176 => 
            array (
                'id' => 677,
                'ampher_code' => '5803',
                'ampher_name' => 'ปาย   ',
                'ampher_name_eng' => 'Pai',
                'geo_id' => 1,
                'province_id' => 46,
            ),
            177 => 
            array (
                'id' => 678,
                'ampher_code' => '5804',
                'ampher_name' => 'แม่สะเรียง   ',
                'ampher_name_eng' => 'Mae Sariang',
                'geo_id' => 1,
                'province_id' => 46,
            ),
            178 => 
            array (
                'id' => 679,
                'ampher_code' => '5805',
                'ampher_name' => 'แม่ลาน้อย   ',
                'ampher_name_eng' => 'Mae La Noi',
                'geo_id' => 1,
                'province_id' => 46,
            ),
            179 => 
            array (
                'id' => 680,
                'ampher_code' => '5806',
                'ampher_name' => 'สบเมย   ',
                'ampher_name_eng' => 'Sop Moei',
                'geo_id' => 1,
                'province_id' => 46,
            ),
            180 => 
            array (
                'id' => 681,
                'ampher_code' => '5807',
                'ampher_name' => 'ปางมะผ้า   ',
                'ampher_name_eng' => 'Pang Mapha',
                'geo_id' => 1,
                'province_id' => 46,
            ),
            181 => 
            array (
                'id' => 682,
                'ampher_code' => '5881',
                'ampher_name' => '*อ.ม่วยต่อ  จ.แม่ฮ่องสอน   ',
                'ampher_name_eng' => 'Muen Tor',
                'geo_id' => 1,
                'province_id' => 46,
            ),
            182 => 
            array (
                'id' => 683,
                'ampher_code' => '6001',
                'ampher_name' => 'เมืองนครสวรรค์   ',
                'ampher_name_eng' => 'Mueang Nakhon Sawan',
                'geo_id' => 2,
                'province_id' => 47,
            ),
            183 => 
            array (
                'id' => 684,
                'ampher_code' => '6002',
                'ampher_name' => 'โกรกพระ   ',
                'ampher_name_eng' => 'Krok Phra',
                'geo_id' => 2,
                'province_id' => 47,
            ),
            184 => 
            array (
                'id' => 685,
                'ampher_code' => '6003',
                'ampher_name' => 'ชุมแสง   ',
                'ampher_name_eng' => 'Chum Saeng',
                'geo_id' => 2,
                'province_id' => 47,
            ),
            185 => 
            array (
                'id' => 686,
                'ampher_code' => '6004',
                'ampher_name' => 'หนองบัว   ',
                'ampher_name_eng' => 'Nong Bua',
                'geo_id' => 2,
                'province_id' => 47,
            ),
            186 => 
            array (
                'id' => 687,
                'ampher_code' => '6005',
                'ampher_name' => 'บรรพตพิสัย   ',
                'ampher_name_eng' => 'Banphot Phisai',
                'geo_id' => 2,
                'province_id' => 47,
            ),
            187 => 
            array (
                'id' => 688,
                'ampher_code' => '6006',
                'ampher_name' => 'เก้าเลี้ยว   ',
                'ampher_name_eng' => 'Kao Liao',
                'geo_id' => 2,
                'province_id' => 47,
            ),
            188 => 
            array (
                'id' => 689,
                'ampher_code' => '6007',
                'ampher_name' => 'ตาคลี   ',
                'ampher_name_eng' => 'Takhli',
                'geo_id' => 2,
                'province_id' => 47,
            ),
            189 => 
            array (
                'id' => 690,
                'ampher_code' => '6008',
                'ampher_name' => 'ท่าตะโก   ',
                'ampher_name_eng' => 'Takhli',
                'geo_id' => 2,
                'province_id' => 47,
            ),
            190 => 
            array (
                'id' => 691,
                'ampher_code' => '6009',
                'ampher_name' => 'ไพศาลี   ',
                'ampher_name_eng' => 'Phaisali',
                'geo_id' => 2,
                'province_id' => 47,
            ),
            191 => 
            array (
                'id' => 692,
                'ampher_code' => '6010',
                'ampher_name' => 'พยุหะคีรี   ',
                'ampher_name_eng' => 'Phayuha Khiri',
                'geo_id' => 2,
                'province_id' => 47,
            ),
            192 => 
            array (
                'id' => 693,
                'ampher_code' => '6011',
                'ampher_name' => 'ลาดยาว   ',
                'ampher_name_eng' => 'Phayuha Khiri',
                'geo_id' => 2,
                'province_id' => 47,
            ),
            193 => 
            array (
                'id' => 694,
                'ampher_code' => '6012',
                'ampher_name' => 'ตากฟ้า   ',
                'ampher_name_eng' => 'Tak Fa',
                'geo_id' => 2,
                'province_id' => 47,
            ),
            194 => 
            array (
                'id' => 695,
                'ampher_code' => '6013',
                'ampher_name' => 'แม่วงก์   ',
                'ampher_name_eng' => 'Mae Wong',
                'geo_id' => 2,
                'province_id' => 47,
            ),
            195 => 
            array (
                'id' => 696,
                'ampher_code' => '6014',
                'ampher_name' => 'แม่เปิน   ',
                'ampher_name_eng' => 'Mae Poen',
                'geo_id' => 2,
                'province_id' => 47,
            ),
            196 => 
            array (
                'id' => 697,
                'ampher_code' => '6015',
                'ampher_name' => 'ชุมตาบง   ',
                'ampher_name_eng' => 'Chum Ta Bong',
                'geo_id' => 2,
                'province_id' => 47,
            ),
            197 => 
            array (
                'id' => 698,
                'ampher_code' => '6051',
                'ampher_name' => 'สาขาตำบลห้วยน้ำหอม*   ',
                'ampher_name_eng' => 'Huen Nam Hom',
                'geo_id' => 2,
                'province_id' => 47,
            ),
            198 => 
            array (
                'id' => 699,
                'ampher_code' => '6052',
            'ampher_name' => 'กิ่งอำเภอชุมตาบง (สาขาตำบลชุมตาบง)*   ',
                'ampher_name_eng' => 'Chum Ta Bong',
                'geo_id' => 2,
                'province_id' => 47,
            ),
            199 => 
            array (
                'id' => 700,
                'ampher_code' => '6053',
            'ampher_name' => 'แม่วงก์ (สาขาตำบลแม่เล่ย์)*   ',
                'ampher_name_eng' => 'Mea Ley',
                'geo_id' => 2,
                'province_id' => 47,
            ),
            200 => 
            array (
                'id' => 701,
                'ampher_code' => '6101',
                'ampher_name' => 'เมืองอุทัยธานี   ',
                'ampher_name_eng' => 'Mueang Uthai Thani',
                'geo_id' => 2,
                'province_id' => 48,
            ),
            201 => 
            array (
                'id' => 702,
                'ampher_code' => '6102',
                'ampher_name' => 'ทัพทัน   ',
                'ampher_name_eng' => 'Thap Than',
                'geo_id' => 2,
                'province_id' => 48,
            ),
            202 => 
            array (
                'id' => 703,
                'ampher_code' => '6103',
                'ampher_name' => 'สว่างอารมณ์   ',
                'ampher_name_eng' => 'Sawang Arom',
                'geo_id' => 2,
                'province_id' => 48,
            ),
            203 => 
            array (
                'id' => 704,
                'ampher_code' => '6104',
                'ampher_name' => 'หนองฉาง   ',
                'ampher_name_eng' => 'Nong Chang',
                'geo_id' => 2,
                'province_id' => 48,
            ),
            204 => 
            array (
                'id' => 705,
                'ampher_code' => '6105',
                'ampher_name' => 'หนองขาหย่าง   ',
                'ampher_name_eng' => 'Nong Khayang',
                'geo_id' => 2,
                'province_id' => 48,
            ),
            205 => 
            array (
                'id' => 706,
                'ampher_code' => '6106',
                'ampher_name' => 'บ้านไร่   ',
                'ampher_name_eng' => 'Ban Rai',
                'geo_id' => 2,
                'province_id' => 48,
            ),
            206 => 
            array (
                'id' => 707,
                'ampher_code' => '6107',
                'ampher_name' => 'ลานสัก   ',
                'ampher_name_eng' => 'Lan Sak',
                'geo_id' => 2,
                'province_id' => 48,
            ),
            207 => 
            array (
                'id' => 708,
                'ampher_code' => '6108',
                'ampher_name' => 'ห้วยคต   ',
                'ampher_name_eng' => 'Huai Khot',
                'geo_id' => 2,
                'province_id' => 48,
            ),
            208 => 
            array (
                'id' => 709,
                'ampher_code' => '6201',
                'ampher_name' => 'เมืองกำแพงเพชร   ',
                'ampher_name_eng' => 'Mueang Kamphaeng Phet',
                'geo_id' => 2,
                'province_id' => 49,
            ),
            209 => 
            array (
                'id' => 710,
                'ampher_code' => '6202',
                'ampher_name' => 'ไทรงาม   ',
                'ampher_name_eng' => 'Sai Ngam',
                'geo_id' => 2,
                'province_id' => 49,
            ),
            210 => 
            array (
                'id' => 711,
                'ampher_code' => '6203',
                'ampher_name' => 'คลองลาน   ',
                'ampher_name_eng' => 'Khlong Lan',
                'geo_id' => 2,
                'province_id' => 49,
            ),
            211 => 
            array (
                'id' => 712,
                'ampher_code' => '6204',
                'ampher_name' => 'ขาณุวรลักษบุรี   ',
                'ampher_name_eng' => 'Khanu Woralaksaburi',
                'geo_id' => 2,
                'province_id' => 49,
            ),
            212 => 
            array (
                'id' => 713,
                'ampher_code' => '6205',
                'ampher_name' => 'คลองขลุง   ',
                'ampher_name_eng' => 'Khlong Khlung',
                'geo_id' => 2,
                'province_id' => 49,
            ),
            213 => 
            array (
                'id' => 714,
                'ampher_code' => '6206',
                'ampher_name' => 'พรานกระต่าย   ',
                'ampher_name_eng' => 'Phran Kratai',
                'geo_id' => 2,
                'province_id' => 49,
            ),
            214 => 
            array (
                'id' => 715,
                'ampher_code' => '6207',
                'ampher_name' => 'ลานกระบือ   ',
                'ampher_name_eng' => 'Lan Krabue',
                'geo_id' => 2,
                'province_id' => 49,
            ),
            215 => 
            array (
                'id' => 716,
                'ampher_code' => '6208',
                'ampher_name' => 'ทรายทองวัฒนา   ',
                'ampher_name_eng' => 'Sai Thong Watthana',
                'geo_id' => 2,
                'province_id' => 49,
            ),
            216 => 
            array (
                'id' => 717,
                'ampher_code' => '6209',
                'ampher_name' => 'ปางศิลาทอง   ',
                'ampher_name_eng' => 'Pang Sila Thong',
                'geo_id' => 2,
                'province_id' => 49,
            ),
            217 => 
            array (
                'id' => 718,
                'ampher_code' => '6210',
                'ampher_name' => 'บึงสามัคคี   ',
                'ampher_name_eng' => 'Bueng Samakkhi',
                'geo_id' => 2,
                'province_id' => 49,
            ),
            218 => 
            array (
                'id' => 719,
                'ampher_code' => '6211',
                'ampher_name' => 'โกสัมพีนคร   ',
                'ampher_name_eng' => 'Kosamphi Nakhon',
                'geo_id' => 2,
                'province_id' => 49,
            ),
            219 => 
            array (
                'id' => 720,
                'ampher_code' => '6301',
                'ampher_name' => 'เมืองตาก   ',
                'ampher_name_eng' => 'Mueang Tak',
                'geo_id' => 4,
                'province_id' => 50,
            ),
            220 => 
            array (
                'id' => 721,
                'ampher_code' => '6302',
                'ampher_name' => 'บ้านตาก   ',
                'ampher_name_eng' => 'Ban Tak',
                'geo_id' => 4,
                'province_id' => 50,
            ),
            221 => 
            array (
                'id' => 722,
                'ampher_code' => '6303',
                'ampher_name' => 'สามเงา   ',
                'ampher_name_eng' => 'Sam Ngao',
                'geo_id' => 4,
                'province_id' => 50,
            ),
            222 => 
            array (
                'id' => 723,
                'ampher_code' => '6304',
                'ampher_name' => 'แม่ระมาด   ',
                'ampher_name_eng' => 'Mae Ramat',
                'geo_id' => 4,
                'province_id' => 50,
            ),
            223 => 
            array (
                'id' => 724,
                'ampher_code' => '6305',
                'ampher_name' => 'ท่าสองยาง   ',
                'ampher_name_eng' => 'Tha Song Yang',
                'geo_id' => 4,
                'province_id' => 50,
            ),
            224 => 
            array (
                'id' => 725,
                'ampher_code' => '6306',
                'ampher_name' => 'แม่สอด   ',
                'ampher_name_eng' => 'Mae Sot',
                'geo_id' => 4,
                'province_id' => 50,
            ),
            225 => 
            array (
                'id' => 726,
                'ampher_code' => '6307',
                'ampher_name' => 'พบพระ   ',
                'ampher_name_eng' => 'Phop Phra',
                'geo_id' => 4,
                'province_id' => 50,
            ),
            226 => 
            array (
                'id' => 727,
                'ampher_code' => '6308',
                'ampher_name' => 'อุ้มผาง   ',
                'ampher_name_eng' => 'Umphang',
                'geo_id' => 4,
                'province_id' => 50,
            ),
            227 => 
            array (
                'id' => 728,
                'ampher_code' => '6309',
                'ampher_name' => 'วังเจ้า   ',
                'ampher_name_eng' => 'Wang Chao',
                'geo_id' => 4,
                'province_id' => 50,
            ),
            228 => 
            array (
                'id' => 729,
                'ampher_code' => '6381',
                'ampher_name' => '*กิ่ง อ.ท่าปุย  จ.ตาก   ',
                'ampher_name_eng' => '*King Ta Pui',
                'geo_id' => 4,
                'province_id' => 50,
            ),
            229 => 
            array (
                'id' => 730,
                'ampher_code' => '6401',
                'ampher_name' => 'เมืองสุโขทัย   ',
                'ampher_name_eng' => 'Mueang Sukhothai',
                'geo_id' => 2,
                'province_id' => 51,
            ),
            230 => 
            array (
                'id' => 731,
                'ampher_code' => '6402',
                'ampher_name' => 'บ้านด่านลานหอย   ',
                'ampher_name_eng' => 'Ban Dan Lan Hoi',
                'geo_id' => 2,
                'province_id' => 51,
            ),
            231 => 
            array (
                'id' => 732,
                'ampher_code' => '6403',
                'ampher_name' => 'คีรีมาศ   ',
                'ampher_name_eng' => 'Khiri Mat',
                'geo_id' => 2,
                'province_id' => 51,
            ),
            232 => 
            array (
                'id' => 733,
                'ampher_code' => '6404',
                'ampher_name' => 'กงไกรลาศ   ',
                'ampher_name_eng' => 'Kong Krailat',
                'geo_id' => 2,
                'province_id' => 51,
            ),
            233 => 
            array (
                'id' => 734,
                'ampher_code' => '6405',
                'ampher_name' => 'ศรีสัชนาลัย   ',
                'ampher_name_eng' => 'Si Satchanalai',
                'geo_id' => 2,
                'province_id' => 51,
            ),
            234 => 
            array (
                'id' => 735,
                'ampher_code' => '6406',
                'ampher_name' => 'ศรีสำโรง   ',
                'ampher_name_eng' => 'Si Samrong',
                'geo_id' => 2,
                'province_id' => 51,
            ),
            235 => 
            array (
                'id' => 736,
                'ampher_code' => '6407',
                'ampher_name' => 'สวรรคโลก   ',
                'ampher_name_eng' => 'Sawankhalok',
                'geo_id' => 2,
                'province_id' => 51,
            ),
            236 => 
            array (
                'id' => 737,
                'ampher_code' => '6408',
                'ampher_name' => 'ศรีนคร   ',
                'ampher_name_eng' => 'Si Nakhon',
                'geo_id' => 2,
                'province_id' => 51,
            ),
            237 => 
            array (
                'id' => 738,
                'ampher_code' => '6409',
                'ampher_name' => 'ทุ่งเสลี่ยม   ',
                'ampher_name_eng' => 'Thung Saliam',
                'geo_id' => 2,
                'province_id' => 51,
            ),
            238 => 
            array (
                'id' => 739,
                'ampher_code' => '6501',
                'ampher_name' => 'เมืองพิษณุโลก   ',
                'ampher_name_eng' => 'Mueang Phitsanulok',
                'geo_id' => 2,
                'province_id' => 52,
            ),
            239 => 
            array (
                'id' => 740,
                'ampher_code' => '6502',
                'ampher_name' => 'นครไทย   ',
                'ampher_name_eng' => 'Nakhon Thai',
                'geo_id' => 2,
                'province_id' => 52,
            ),
            240 => 
            array (
                'id' => 741,
                'ampher_code' => '6503',
                'ampher_name' => 'ชาติตระการ   ',
                'ampher_name_eng' => 'Chat Trakan',
                'geo_id' => 2,
                'province_id' => 52,
            ),
            241 => 
            array (
                'id' => 742,
                'ampher_code' => '6504',
                'ampher_name' => 'บางระกำ   ',
                'ampher_name_eng' => 'Bang Rakam',
                'geo_id' => 2,
                'province_id' => 52,
            ),
            242 => 
            array (
                'id' => 743,
                'ampher_code' => '6505',
                'ampher_name' => 'บางกระทุ่ม   ',
                'ampher_name_eng' => 'Bang Krathum',
                'geo_id' => 2,
                'province_id' => 52,
            ),
            243 => 
            array (
                'id' => 744,
                'ampher_code' => '6506',
                'ampher_name' => 'พรหมพิราม   ',
                'ampher_name_eng' => 'Phrom Phiram',
                'geo_id' => 2,
                'province_id' => 52,
            ),
            244 => 
            array (
                'id' => 745,
                'ampher_code' => '6507',
                'ampher_name' => 'วัดโบสถ์   ',
                'ampher_name_eng' => 'Wat Bot',
                'geo_id' => 2,
                'province_id' => 52,
            ),
            245 => 
            array (
                'id' => 746,
                'ampher_code' => '6508',
                'ampher_name' => 'วังทอง   ',
                'ampher_name_eng' => 'Wang Thong',
                'geo_id' => 2,
                'province_id' => 52,
            ),
            246 => 
            array (
                'id' => 747,
                'ampher_code' => '6509',
                'ampher_name' => 'เนินมะปราง   ',
                'ampher_name_eng' => 'Noen Maprang',
                'geo_id' => 2,
                'province_id' => 52,
            ),
            247 => 
            array (
                'id' => 748,
                'ampher_code' => '6601',
                'ampher_name' => 'เมืองพิจิตร   ',
                'ampher_name_eng' => 'Mueang Phichit',
                'geo_id' => 2,
                'province_id' => 53,
            ),
            248 => 
            array (
                'id' => 749,
                'ampher_code' => '6602',
                'ampher_name' => 'วังทรายพูน   ',
                'ampher_name_eng' => 'Wang Sai Phun',
                'geo_id' => 2,
                'province_id' => 53,
            ),
            249 => 
            array (
                'id' => 750,
                'ampher_code' => '6603',
                'ampher_name' => 'โพธิ์ประทับช้าง   ',
                'ampher_name_eng' => 'Pho Prathap Chang',
                'geo_id' => 2,
                'province_id' => 53,
            ),
            250 => 
            array (
                'id' => 751,
                'ampher_code' => '6604',
                'ampher_name' => 'ตะพานหิน   ',
                'ampher_name_eng' => 'Taphan Hin',
                'geo_id' => 2,
                'province_id' => 53,
            ),
            251 => 
            array (
                'id' => 752,
                'ampher_code' => '6605',
                'ampher_name' => 'บางมูลนาก   ',
                'ampher_name_eng' => 'Bang Mun Nak',
                'geo_id' => 2,
                'province_id' => 53,
            ),
            252 => 
            array (
                'id' => 753,
                'ampher_code' => '6606',
                'ampher_name' => 'โพทะเล   ',
                'ampher_name_eng' => 'Pho Thale',
                'geo_id' => 2,
                'province_id' => 53,
            ),
            253 => 
            array (
                'id' => 754,
                'ampher_code' => '6607',
                'ampher_name' => 'สามง่าม   ',
                'ampher_name_eng' => 'Sam Ngam',
                'geo_id' => 2,
                'province_id' => 53,
            ),
            254 => 
            array (
                'id' => 755,
                'ampher_code' => '6608',
                'ampher_name' => 'ทับคล้อ   ',
                'ampher_name_eng' => 'Tap Khlo',
                'geo_id' => 2,
                'province_id' => 53,
            ),
            255 => 
            array (
                'id' => 756,
                'ampher_code' => '6609',
                'ampher_name' => 'สากเหล็ก   ',
                'ampher_name_eng' => 'Sak Lek',
                'geo_id' => 2,
                'province_id' => 53,
            ),
            256 => 
            array (
                'id' => 757,
                'ampher_code' => '6610',
                'ampher_name' => 'บึงนาราง   ',
                'ampher_name_eng' => 'Bueng Na Rang',
                'geo_id' => 2,
                'province_id' => 53,
            ),
            257 => 
            array (
                'id' => 758,
                'ampher_code' => '6611',
                'ampher_name' => 'ดงเจริญ   ',
                'ampher_name_eng' => 'Dong Charoen',
                'geo_id' => 2,
                'province_id' => 53,
            ),
            258 => 
            array (
                'id' => 759,
                'ampher_code' => '6612',
                'ampher_name' => 'วชิรบารมี   ',
                'ampher_name_eng' => 'Wachirabarami',
                'geo_id' => 2,
                'province_id' => 53,
            ),
            259 => 
            array (
                'id' => 760,
                'ampher_code' => '6701',
                'ampher_name' => 'เมืองเพชรบูรณ์   ',
                'ampher_name_eng' => 'Mueang Phetchabun',
                'geo_id' => 2,
                'province_id' => 54,
            ),
            260 => 
            array (
                'id' => 761,
                'ampher_code' => '6702',
                'ampher_name' => 'ชนแดน   ',
                'ampher_name_eng' => 'Chon Daen',
                'geo_id' => 2,
                'province_id' => 54,
            ),
            261 => 
            array (
                'id' => 762,
                'ampher_code' => '6703',
                'ampher_name' => 'หล่มสัก   ',
                'ampher_name_eng' => 'Lom Sak',
                'geo_id' => 2,
                'province_id' => 54,
            ),
            262 => 
            array (
                'id' => 763,
                'ampher_code' => '6704',
                'ampher_name' => 'หล่มเก่า   ',
                'ampher_name_eng' => 'Lom Kao',
                'geo_id' => 2,
                'province_id' => 54,
            ),
            263 => 
            array (
                'id' => 764,
                'ampher_code' => '6705',
                'ampher_name' => 'วิเชียรบุรี   ',
                'ampher_name_eng' => 'Wichian Buri',
                'geo_id' => 2,
                'province_id' => 54,
            ),
            264 => 
            array (
                'id' => 765,
                'ampher_code' => '6706',
                'ampher_name' => 'ศรีเทพ   ',
                'ampher_name_eng' => 'Si Thep',
                'geo_id' => 2,
                'province_id' => 54,
            ),
            265 => 
            array (
                'id' => 766,
                'ampher_code' => '6707',
                'ampher_name' => 'หนองไผ่   ',
                'ampher_name_eng' => 'Nong Phai',
                'geo_id' => 2,
                'province_id' => 54,
            ),
            266 => 
            array (
                'id' => 767,
                'ampher_code' => '6708',
                'ampher_name' => 'บึงสามพัน   ',
                'ampher_name_eng' => 'Bueng Sam Phan',
                'geo_id' => 2,
                'province_id' => 54,
            ),
            267 => 
            array (
                'id' => 768,
                'ampher_code' => '6709',
                'ampher_name' => 'น้ำหนาว   ',
                'ampher_name_eng' => 'Nam Nao',
                'geo_id' => 2,
                'province_id' => 54,
            ),
            268 => 
            array (
                'id' => 769,
                'ampher_code' => '6710',
                'ampher_name' => 'วังโป่ง   ',
                'ampher_name_eng' => 'Wang Pong',
                'geo_id' => 2,
                'province_id' => 54,
            ),
            269 => 
            array (
                'id' => 770,
                'ampher_code' => '6711',
                'ampher_name' => 'เขาค้อ   ',
                'ampher_name_eng' => 'Khao Kho',
                'geo_id' => 2,
                'province_id' => 54,
            ),
            270 => 
            array (
                'id' => 771,
                'ampher_code' => '7001',
                'ampher_name' => 'เมืองราชบุรี   ',
                'ampher_name_eng' => 'Mueang Ratchaburi',
                'geo_id' => 4,
                'province_id' => 55,
            ),
            271 => 
            array (
                'id' => 772,
                'ampher_code' => '7002',
                'ampher_name' => 'จอมบึง   ',
                'ampher_name_eng' => 'Chom Bueng',
                'geo_id' => 4,
                'province_id' => 55,
            ),
            272 => 
            array (
                'id' => 773,
                'ampher_code' => '7003',
                'ampher_name' => 'สวนผึ้ง   ',
                'ampher_name_eng' => 'Suan Phueng',
                'geo_id' => 4,
                'province_id' => 55,
            ),
            273 => 
            array (
                'id' => 774,
                'ampher_code' => '7004',
                'ampher_name' => 'ดำเนินสะดวก   ',
                'ampher_name_eng' => 'Damnoen Saduak',
                'geo_id' => 4,
                'province_id' => 55,
            ),
            274 => 
            array (
                'id' => 775,
                'ampher_code' => '7005',
                'ampher_name' => 'บ้านโป่ง   ',
                'ampher_name_eng' => 'Ban Pong',
                'geo_id' => 4,
                'province_id' => 55,
            ),
            275 => 
            array (
                'id' => 776,
                'ampher_code' => '7006',
                'ampher_name' => 'บางแพ   ',
                'ampher_name_eng' => 'Bang Phae',
                'geo_id' => 4,
                'province_id' => 55,
            ),
            276 => 
            array (
                'id' => 777,
                'ampher_code' => '7007',
                'ampher_name' => 'โพธาราม   ',
                'ampher_name_eng' => 'Photharam',
                'geo_id' => 4,
                'province_id' => 55,
            ),
            277 => 
            array (
                'id' => 778,
                'ampher_code' => '7008',
                'ampher_name' => 'ปากท่อ   ',
                'ampher_name_eng' => 'Pak Tho',
                'geo_id' => 4,
                'province_id' => 55,
            ),
            278 => 
            array (
                'id' => 779,
                'ampher_code' => '7009',
                'ampher_name' => 'วัดเพลง   ',
                'ampher_name_eng' => 'Wat Phleng',
                'geo_id' => 4,
                'province_id' => 55,
            ),
            279 => 
            array (
                'id' => 780,
                'ampher_code' => '7010',
                'ampher_name' => 'บ้านคา   ',
                'ampher_name_eng' => 'Ban Kha',
                'geo_id' => 4,
                'province_id' => 55,
            ),
            280 => 
            array (
                'id' => 781,
                'ampher_code' => '7074',
                'ampher_name' => 'ท้องถิ่นเทศบาลตำบลบ้านฆ้อง   ',
                'ampher_name_eng' => 'Tet Saban Ban Kong',
                'geo_id' => 4,
                'province_id' => 55,
            ),
            281 => 
            array (
                'id' => 782,
                'ampher_code' => '7101',
                'ampher_name' => 'เมืองกาญจนบุรี   ',
                'ampher_name_eng' => 'Mueang Kanchanaburi',
                'geo_id' => 4,
                'province_id' => 56,
            ),
            282 => 
            array (
                'id' => 783,
                'ampher_code' => '7102',
                'ampher_name' => 'ไทรโยค   ',
                'ampher_name_eng' => 'Sai Yok',
                'geo_id' => 4,
                'province_id' => 56,
            ),
            283 => 
            array (
                'id' => 784,
                'ampher_code' => '7103',
                'ampher_name' => 'บ่อพลอย   ',
                'ampher_name_eng' => 'Bo Phloi',
                'geo_id' => 4,
                'province_id' => 56,
            ),
            284 => 
            array (
                'id' => 785,
                'ampher_code' => '7104',
                'ampher_name' => 'ศรีสวัสดิ์   ',
                'ampher_name_eng' => 'Si Sawat',
                'geo_id' => 4,
                'province_id' => 56,
            ),
            285 => 
            array (
                'id' => 786,
                'ampher_code' => '7105',
                'ampher_name' => 'ท่ามะกา   ',
                'ampher_name_eng' => 'Tha Maka',
                'geo_id' => 4,
                'province_id' => 56,
            ),
            286 => 
            array (
                'id' => 787,
                'ampher_code' => '7106',
                'ampher_name' => 'ท่าม่วง   ',
                'ampher_name_eng' => 'Tha Muang',
                'geo_id' => 4,
                'province_id' => 56,
            ),
            287 => 
            array (
                'id' => 788,
                'ampher_code' => '7107',
                'ampher_name' => 'ทองผาภูมิ   ',
                'ampher_name_eng' => 'Pha Phum',
                'geo_id' => 4,
                'province_id' => 56,
            ),
            288 => 
            array (
                'id' => 789,
                'ampher_code' => '7108',
                'ampher_name' => 'สังขละบุรี   ',
                'ampher_name_eng' => 'Sangkhla Buri',
                'geo_id' => 4,
                'province_id' => 56,
            ),
            289 => 
            array (
                'id' => 790,
                'ampher_code' => '7109',
                'ampher_name' => 'พนมทวน   ',
                'ampher_name_eng' => 'Phanom Thuan',
                'geo_id' => 4,
                'province_id' => 56,
            ),
            290 => 
            array (
                'id' => 791,
                'ampher_code' => '7110',
                'ampher_name' => 'เลาขวัญ   ',
                'ampher_name_eng' => 'Lao Khwan',
                'geo_id' => 4,
                'province_id' => 56,
            ),
            291 => 
            array (
                'id' => 792,
                'ampher_code' => '7111',
                'ampher_name' => 'ด่านมะขามเตี้ย   ',
                'ampher_name_eng' => 'Dan Makham Tia',
                'geo_id' => 4,
                'province_id' => 56,
            ),
            292 => 
            array (
                'id' => 793,
                'ampher_code' => '7112',
                'ampher_name' => 'หนองปรือ   ',
                'ampher_name_eng' => 'Nong Prue',
                'geo_id' => 4,
                'province_id' => 56,
            ),
            293 => 
            array (
                'id' => 794,
                'ampher_code' => '7113',
                'ampher_name' => 'ห้วยกระเจา   ',
                'ampher_name_eng' => 'Huai Krachao',
                'geo_id' => 4,
                'province_id' => 56,
            ),
            294 => 
            array (
                'id' => 795,
                'ampher_code' => '7151',
                'ampher_name' => 'สาขาตำบลท่ากระดาน*   ',
                'ampher_name_eng' => 'Tha Kra Dan',
                'geo_id' => 4,
                'province_id' => 56,
            ),
            295 => 
            array (
                'id' => 796,
                'ampher_code' => '7181',
                'ampher_name' => '*บ้านทวน  จ.กาญจนบุรี   ',
                'ampher_name_eng' => '*Ban Tuan',
                'geo_id' => 4,
                'province_id' => 56,
            ),
            296 => 
            array (
                'id' => 797,
                'ampher_code' => '7201',
                'ampher_name' => 'เมืองสุพรรณบุรี   ',
                'ampher_name_eng' => 'Mueang Suphan Buri',
                'geo_id' => 2,
                'province_id' => 57,
            ),
            297 => 
            array (
                'id' => 798,
                'ampher_code' => '7202',
                'ampher_name' => 'เดิมบางนางบวช   ',
                'ampher_name_eng' => 'Doem Bang Nang Buat',
                'geo_id' => 2,
                'province_id' => 57,
            ),
            298 => 
            array (
                'id' => 799,
                'ampher_code' => '7203',
                'ampher_name' => 'ด่านช้าง   ',
                'ampher_name_eng' => 'Dan Chang',
                'geo_id' => 2,
                'province_id' => 57,
            ),
            299 => 
            array (
                'id' => 800,
                'ampher_code' => '7204',
                'ampher_name' => 'บางปลาม้า   ',
                'ampher_name_eng' => 'Bang Pla Ma',
                'geo_id' => 2,
                'province_id' => 57,
            ),
            300 => 
            array (
                'id' => 801,
                'ampher_code' => '7205',
                'ampher_name' => 'ศรีประจันต์   ',
                'ampher_name_eng' => 'Si Prachan',
                'geo_id' => 2,
                'province_id' => 57,
            ),
            301 => 
            array (
                'id' => 802,
                'ampher_code' => '7206',
                'ampher_name' => 'ดอนเจดีย์   ',
                'ampher_name_eng' => 'Don Chedi',
                'geo_id' => 2,
                'province_id' => 57,
            ),
            302 => 
            array (
                'id' => 803,
                'ampher_code' => '7207',
                'ampher_name' => 'สองพี่น้อง   ',
                'ampher_name_eng' => 'Song Phi Nong',
                'geo_id' => 2,
                'province_id' => 57,
            ),
            303 => 
            array (
                'id' => 804,
                'ampher_code' => '7208',
                'ampher_name' => 'สามชุก   ',
                'ampher_name_eng' => 'Sam Chuk',
                'geo_id' => 2,
                'province_id' => 57,
            ),
            304 => 
            array (
                'id' => 805,
                'ampher_code' => '7209',
                'ampher_name' => 'อู่ทอง   ',
                'ampher_name_eng' => 'U Thong',
                'geo_id' => 2,
                'province_id' => 57,
            ),
            305 => 
            array (
                'id' => 806,
                'ampher_code' => '7210',
                'ampher_name' => 'หนองหญ้าไซ   ',
                'ampher_name_eng' => 'Nong Ya Sai',
                'geo_id' => 2,
                'province_id' => 57,
            ),
            306 => 
            array (
                'id' => 807,
                'ampher_code' => '7301',
                'ampher_name' => 'เมืองนครปฐม   ',
                'ampher_name_eng' => 'Mueang Nakhon Pathom',
                'geo_id' => 2,
                'province_id' => 58,
            ),
            307 => 
            array (
                'id' => 808,
                'ampher_code' => '7302',
                'ampher_name' => 'กำแพงแสน   ',
                'ampher_name_eng' => 'Kamphaeng Saen',
                'geo_id' => 2,
                'province_id' => 58,
            ),
            308 => 
            array (
                'id' => 809,
                'ampher_code' => '7303',
                'ampher_name' => 'นครชัยศรี   ',
                'ampher_name_eng' => 'Nakhon Chai Si',
                'geo_id' => 2,
                'province_id' => 58,
            ),
            309 => 
            array (
                'id' => 810,
                'ampher_code' => '7304',
                'ampher_name' => 'ดอนตูม   ',
                'ampher_name_eng' => 'Don Tum',
                'geo_id' => 2,
                'province_id' => 58,
            ),
            310 => 
            array (
                'id' => 811,
                'ampher_code' => '7305',
                'ampher_name' => 'บางเลน   ',
                'ampher_name_eng' => 'Bang Len',
                'geo_id' => 2,
                'province_id' => 58,
            ),
            311 => 
            array (
                'id' => 812,
                'ampher_code' => '7306',
                'ampher_name' => 'สามพราน   ',
                'ampher_name_eng' => 'Sam Phran',
                'geo_id' => 2,
                'province_id' => 58,
            ),
            312 => 
            array (
                'id' => 813,
                'ampher_code' => '7307',
                'ampher_name' => 'พุทธมณฑล   ',
                'ampher_name_eng' => 'Phutthamonthon',
                'geo_id' => 2,
                'province_id' => 58,
            ),
            313 => 
            array (
                'id' => 814,
                'ampher_code' => '7401',
                'ampher_name' => 'เมืองสมุทรสาคร   ',
                'ampher_name_eng' => 'Mueang Samut Sakhon',
                'geo_id' => 2,
                'province_id' => 59,
            ),
            314 => 
            array (
                'id' => 815,
                'ampher_code' => '7402',
                'ampher_name' => 'กระทุ่มแบน   ',
                'ampher_name_eng' => 'Krathum Baen',
                'geo_id' => 2,
                'province_id' => 59,
            ),
            315 => 
            array (
                'id' => 816,
                'ampher_code' => '7403',
                'ampher_name' => 'บ้านแพ้ว   ',
                'ampher_name_eng' => 'Ban Phaeo',
                'geo_id' => 2,
                'province_id' => 59,
            ),
            316 => 
            array (
                'id' => 817,
                'ampher_code' => '7501',
                'ampher_name' => 'เมืองสมุทรสงคราม   ',
                'ampher_name_eng' => 'Mueang Samut Songkhram',
                'geo_id' => 2,
                'province_id' => 60,
            ),
            317 => 
            array (
                'id' => 818,
                'ampher_code' => '7502',
                'ampher_name' => 'บางคนที   ',
                'ampher_name_eng' => 'Bang Khonthi',
                'geo_id' => 2,
                'province_id' => 60,
            ),
            318 => 
            array (
                'id' => 819,
                'ampher_code' => '7503',
                'ampher_name' => 'อัมพวา   ',
                'ampher_name_eng' => 'Amphawa',
                'geo_id' => 2,
                'province_id' => 60,
            ),
            319 => 
            array (
                'id' => 820,
                'ampher_code' => '7601',
                'ampher_name' => 'เมืองเพชรบุรี   ',
                'ampher_name_eng' => 'Mueang Phetchaburi',
                'geo_id' => 4,
                'province_id' => 61,
            ),
            320 => 
            array (
                'id' => 821,
                'ampher_code' => '7602',
                'ampher_name' => 'เขาย้อย   ',
                'ampher_name_eng' => 'Khao Yoi',
                'geo_id' => 4,
                'province_id' => 61,
            ),
            321 => 
            array (
                'id' => 822,
                'ampher_code' => '7603',
                'ampher_name' => 'หนองหญ้าปล้อง   ',
                'ampher_name_eng' => 'Nong Ya Plong',
                'geo_id' => 4,
                'province_id' => 61,
            ),
            322 => 
            array (
                'id' => 823,
                'ampher_code' => '7604',
                'ampher_name' => 'ชะอำ   ',
                'ampher_name_eng' => 'Cha-am',
                'geo_id' => 4,
                'province_id' => 61,
            ),
            323 => 
            array (
                'id' => 824,
                'ampher_code' => '7605',
                'ampher_name' => 'ท่ายาง   ',
                'ampher_name_eng' => 'Tha Yang',
                'geo_id' => 4,
                'province_id' => 61,
            ),
            324 => 
            array (
                'id' => 825,
                'ampher_code' => '7606',
                'ampher_name' => 'บ้านลาด   ',
                'ampher_name_eng' => 'Ban Lat',
                'geo_id' => 4,
                'province_id' => 61,
            ),
            325 => 
            array (
                'id' => 826,
                'ampher_code' => '7607',
                'ampher_name' => 'บ้านแหลม   ',
                'ampher_name_eng' => 'Ban Laem',
                'geo_id' => 4,
                'province_id' => 61,
            ),
            326 => 
            array (
                'id' => 827,
                'ampher_code' => '7608',
                'ampher_name' => 'แก่งกระจาน   ',
                'ampher_name_eng' => 'Kaeng Krachan',
                'geo_id' => 4,
                'province_id' => 61,
            ),
            327 => 
            array (
                'id' => 828,
                'ampher_code' => '7701',
                'ampher_name' => 'เมืองประจวบคีรีขันธ์   ',
                'ampher_name_eng' => 'Mueang Prachuap Khiri Khan',
                'geo_id' => 4,
                'province_id' => 62,
            ),
            328 => 
            array (
                'id' => 829,
                'ampher_code' => '7702',
                'ampher_name' => 'กุยบุรี   ',
                'ampher_name_eng' => 'Kui Buri',
                'geo_id' => 4,
                'province_id' => 62,
            ),
            329 => 
            array (
                'id' => 830,
                'ampher_code' => '7703',
                'ampher_name' => 'ทับสะแก   ',
                'ampher_name_eng' => 'Thap Sakae',
                'geo_id' => 4,
                'province_id' => 62,
            ),
            330 => 
            array (
                'id' => 831,
                'ampher_code' => '7704',
                'ampher_name' => 'บางสะพาน   ',
                'ampher_name_eng' => 'Bang Saphan',
                'geo_id' => 4,
                'province_id' => 62,
            ),
            331 => 
            array (
                'id' => 832,
                'ampher_code' => '7705',
                'ampher_name' => 'บางสะพานน้อย   ',
                'ampher_name_eng' => 'Bang Saphan Noi',
                'geo_id' => 4,
                'province_id' => 62,
            ),
            332 => 
            array (
                'id' => 833,
                'ampher_code' => '7706',
                'ampher_name' => 'ปราณบุรี   ',
                'ampher_name_eng' => 'Pran Buri',
                'geo_id' => 4,
                'province_id' => 62,
            ),
            333 => 
            array (
                'id' => 834,
                'ampher_code' => '7707',
                'ampher_name' => 'หัวหิน   ',
                'ampher_name_eng' => 'Hua Hin',
                'geo_id' => 4,
                'province_id' => 62,
            ),
            334 => 
            array (
                'id' => 835,
                'ampher_code' => '7708',
                'ampher_name' => 'สามร้อยยอด   ',
                'ampher_name_eng' => 'Sam Roi Yot',
                'geo_id' => 4,
                'province_id' => 62,
            ),
            335 => 
            array (
                'id' => 836,
                'ampher_code' => '8001',
                'ampher_name' => 'เมืองนครศรีธรรมราช   ',
                'ampher_name_eng' => 'Mueang Nakhon Si Thammarat',
                'geo_id' => 6,
                'province_id' => 63,
            ),
            336 => 
            array (
                'id' => 837,
                'ampher_code' => '8002',
                'ampher_name' => 'พรหมคีรี   ',
                'ampher_name_eng' => 'Phrom Khiri',
                'geo_id' => 6,
                'province_id' => 63,
            ),
            337 => 
            array (
                'id' => 838,
                'ampher_code' => '8003',
                'ampher_name' => 'ลานสกา   ',
                'ampher_name_eng' => 'Lan Saka',
                'geo_id' => 6,
                'province_id' => 63,
            ),
            338 => 
            array (
                'id' => 839,
                'ampher_code' => '8004',
                'ampher_name' => 'ฉวาง   ',
                'ampher_name_eng' => 'Chawang',
                'geo_id' => 6,
                'province_id' => 63,
            ),
            339 => 
            array (
                'id' => 840,
                'ampher_code' => '8005',
                'ampher_name' => 'พิปูน   ',
                'ampher_name_eng' => 'Phipun',
                'geo_id' => 6,
                'province_id' => 63,
            ),
            340 => 
            array (
                'id' => 841,
                'ampher_code' => '8006',
                'ampher_name' => 'เชียรใหญ่   ',
                'ampher_name_eng' => 'Chian Yai',
                'geo_id' => 6,
                'province_id' => 63,
            ),
            341 => 
            array (
                'id' => 842,
                'ampher_code' => '8007',
                'ampher_name' => 'ชะอวด   ',
                'ampher_name_eng' => 'Cha-uat',
                'geo_id' => 6,
                'province_id' => 63,
            ),
            342 => 
            array (
                'id' => 843,
                'ampher_code' => '8008',
                'ampher_name' => 'ท่าศาลา   ',
                'ampher_name_eng' => 'Tha Sala',
                'geo_id' => 6,
                'province_id' => 63,
            ),
            343 => 
            array (
                'id' => 844,
                'ampher_code' => '8009',
                'ampher_name' => 'ทุ่งสง   ',
                'ampher_name_eng' => 'Thung Song',
                'geo_id' => 6,
                'province_id' => 63,
            ),
            344 => 
            array (
                'id' => 845,
                'ampher_code' => '8010',
                'ampher_name' => 'นาบอน   ',
                'ampher_name_eng' => 'Na Bon',
                'geo_id' => 6,
                'province_id' => 63,
            ),
            345 => 
            array (
                'id' => 846,
                'ampher_code' => '8011',
                'ampher_name' => 'ทุ่งใหญ่   ',
                'ampher_name_eng' => 'Thung Yai',
                'geo_id' => 6,
                'province_id' => 63,
            ),
            346 => 
            array (
                'id' => 847,
                'ampher_code' => '8012',
                'ampher_name' => 'ปากพนัง   ',
                'ampher_name_eng' => 'Pak Phanang',
                'geo_id' => 6,
                'province_id' => 63,
            ),
            347 => 
            array (
                'id' => 848,
                'ampher_code' => '8013',
                'ampher_name' => 'ร่อนพิบูลย์   ',
                'ampher_name_eng' => 'Ron Phibun',
                'geo_id' => 6,
                'province_id' => 63,
            ),
            348 => 
            array (
                'id' => 849,
                'ampher_code' => '8014',
                'ampher_name' => 'สิชล   ',
                'ampher_name_eng' => 'Sichon',
                'geo_id' => 6,
                'province_id' => 63,
            ),
            349 => 
            array (
                'id' => 850,
                'ampher_code' => '8015',
                'ampher_name' => 'ขนอม   ',
                'ampher_name_eng' => 'Khanom',
                'geo_id' => 6,
                'province_id' => 63,
            ),
            350 => 
            array (
                'id' => 851,
                'ampher_code' => '8016',
                'ampher_name' => 'หัวไทร   ',
                'ampher_name_eng' => 'Hua Sai',
                'geo_id' => 6,
                'province_id' => 63,
            ),
            351 => 
            array (
                'id' => 852,
                'ampher_code' => '8017',
                'ampher_name' => 'บางขัน   ',
                'ampher_name_eng' => 'Bang Khan',
                'geo_id' => 6,
                'province_id' => 63,
            ),
            352 => 
            array (
                'id' => 853,
                'ampher_code' => '8018',
                'ampher_name' => 'ถ้ำพรรณรา   ',
                'ampher_name_eng' => 'Tham Phannara',
                'geo_id' => 6,
                'province_id' => 63,
            ),
            353 => 
            array (
                'id' => 854,
                'ampher_code' => '8019',
                'ampher_name' => 'จุฬาภรณ์   ',
                'ampher_name_eng' => 'Chulabhorn',
                'geo_id' => 6,
                'province_id' => 63,
            ),
            354 => 
            array (
                'id' => 855,
                'ampher_code' => '8020',
                'ampher_name' => 'พระพรหม   ',
                'ampher_name_eng' => 'Phra Phrom',
                'geo_id' => 6,
                'province_id' => 63,
            ),
            355 => 
            array (
                'id' => 856,
                'ampher_code' => '8021',
                'ampher_name' => 'นบพิตำ   ',
                'ampher_name_eng' => 'Nopphitam',
                'geo_id' => 6,
                'province_id' => 63,
            ),
            356 => 
            array (
                'id' => 857,
                'ampher_code' => '8022',
                'ampher_name' => 'ช้างกลาง   ',
                'ampher_name_eng' => 'Chang Klang',
                'geo_id' => 6,
                'province_id' => 63,
            ),
            357 => 
            array (
                'id' => 858,
                'ampher_code' => '8023',
                'ampher_name' => 'เฉลิมพระเกียรติ   ',
                'ampher_name_eng' => 'Chaloem Phra Kiat',
                'geo_id' => 6,
                'province_id' => 63,
            ),
            358 => 
            array (
                'id' => 859,
                'ampher_code' => '8051',
            'ampher_name' => 'เชียรใหญ่ (สาขาตำบลเสือหึง)*   ',
                'ampher_name_eng' => 'Chian Yai*',
                'geo_id' => 6,
                'province_id' => 63,
            ),
            359 => 
            array (
                'id' => 860,
                'ampher_code' => '8052',
                'ampher_name' => 'สาขาตำบลสวนหลวง**   ',
                'ampher_name_eng' => 'Suan Luang',
                'geo_id' => 6,
                'province_id' => 63,
            ),
            360 => 
            array (
                'id' => 861,
                'ampher_code' => '8053',
            'ampher_name' => 'ร่อนพิบูลย์ (สาขาตำบลหินตก)*   ',
                'ampher_name_eng' => 'Ron Phibun',
                'geo_id' => 6,
                'province_id' => 63,
            ),
            361 => 
            array (
                'id' => 862,
                'ampher_code' => '8054',
            'ampher_name' => 'หัวไทร (สาขาตำบลควนชะลิก)*   ',
                'ampher_name_eng' => 'Hua Sai',
                'geo_id' => 6,
                'province_id' => 63,
            ),
            362 => 
            array (
                'id' => 863,
                'ampher_code' => '8055',
            'ampher_name' => 'ทุ่งสง (สาขาตำบลกะปาง)*   ',
                'ampher_name_eng' => 'Thung Song',
                'geo_id' => 6,
                'province_id' => 63,
            ),
            363 => 
            array (
                'id' => 864,
                'ampher_code' => '8101',
                'ampher_name' => 'เมืองกระบี่   ',
                'ampher_name_eng' => 'Mueang Krabi',
                'geo_id' => 6,
                'province_id' => 64,
            ),
            364 => 
            array (
                'id' => 865,
                'ampher_code' => '8102',
                'ampher_name' => 'เขาพนม   ',
                'ampher_name_eng' => 'Khao Phanom',
                'geo_id' => 6,
                'province_id' => 64,
            ),
            365 => 
            array (
                'id' => 866,
                'ampher_code' => '8103',
                'ampher_name' => 'เกาะลันตา   ',
                'ampher_name_eng' => 'Ko Lanta',
                'geo_id' => 6,
                'province_id' => 64,
            ),
            366 => 
            array (
                'id' => 867,
                'ampher_code' => '8104',
                'ampher_name' => 'คลองท่อม   ',
                'ampher_name_eng' => 'Khlong Thom',
                'geo_id' => 6,
                'province_id' => 64,
            ),
            367 => 
            array (
                'id' => 868,
                'ampher_code' => '8105',
                'ampher_name' => 'อ่าวลึก   ',
                'ampher_name_eng' => 'Ao Luek',
                'geo_id' => 6,
                'province_id' => 64,
            ),
            368 => 
            array (
                'id' => 869,
                'ampher_code' => '8106',
                'ampher_name' => 'ปลายพระยา   ',
                'ampher_name_eng' => 'Plai Phraya',
                'geo_id' => 6,
                'province_id' => 64,
            ),
            369 => 
            array (
                'id' => 870,
                'ampher_code' => '8107',
                'ampher_name' => 'ลำทับ   ',
                'ampher_name_eng' => 'Lam Thap',
                'geo_id' => 6,
                'province_id' => 64,
            ),
            370 => 
            array (
                'id' => 871,
                'ampher_code' => '8108',
                'ampher_name' => 'เหนือคลอง   ',
                'ampher_name_eng' => 'Nuea Khlong',
                'geo_id' => 6,
                'province_id' => 64,
            ),
            371 => 
            array (
                'id' => 872,
                'ampher_code' => '8201',
                'ampher_name' => 'เมืองพังงา   ',
                'ampher_name_eng' => 'Mueang Phang-nga',
                'geo_id' => 6,
                'province_id' => 65,
            ),
            372 => 
            array (
                'id' => 873,
                'ampher_code' => '8202',
                'ampher_name' => 'เกาะยาว   ',
                'ampher_name_eng' => 'Ko Yao',
                'geo_id' => 6,
                'province_id' => 65,
            ),
            373 => 
            array (
                'id' => 874,
                'ampher_code' => '8203',
                'ampher_name' => 'กะปง   ',
                'ampher_name_eng' => 'Kapong',
                'geo_id' => 6,
                'province_id' => 65,
            ),
            374 => 
            array (
                'id' => 875,
                'ampher_code' => '8204',
                'ampher_name' => 'ตะกั่วทุ่ง   ',
                'ampher_name_eng' => 'Takua Thung',
                'geo_id' => 6,
                'province_id' => 65,
            ),
            375 => 
            array (
                'id' => 876,
                'ampher_code' => '8205',
                'ampher_name' => 'ตะกั่วป่า   ',
                'ampher_name_eng' => 'Takua Pa',
                'geo_id' => 6,
                'province_id' => 65,
            ),
            376 => 
            array (
                'id' => 877,
                'ampher_code' => '8206',
                'ampher_name' => 'คุระบุรี   ',
                'ampher_name_eng' => 'Khura Buri',
                'geo_id' => 6,
                'province_id' => 65,
            ),
            377 => 
            array (
                'id' => 878,
                'ampher_code' => '8207',
                'ampher_name' => 'ทับปุด   ',
                'ampher_name_eng' => 'Thap Put',
                'geo_id' => 6,
                'province_id' => 65,
            ),
            378 => 
            array (
                'id' => 879,
                'ampher_code' => '8208',
                'ampher_name' => 'ท้ายเหมือง   ',
                'ampher_name_eng' => 'Thai Mueang',
                'geo_id' => 6,
                'province_id' => 65,
            ),
            379 => 
            array (
                'id' => 880,
                'ampher_code' => '8301',
                'ampher_name' => 'เมืองภูเก็ต   ',
                'ampher_name_eng' => 'Mueang Phuket',
                'geo_id' => 6,
                'province_id' => 66,
            ),
            380 => 
            array (
                'id' => 881,
                'ampher_code' => '8302',
                'ampher_name' => 'กะทู้   ',
                'ampher_name_eng' => 'Kathu',
                'geo_id' => 6,
                'province_id' => 66,
            ),
            381 => 
            array (
                'id' => 882,
                'ampher_code' => '8303',
                'ampher_name' => 'ถลาง   ',
                'ampher_name_eng' => 'Thalang',
                'geo_id' => 6,
                'province_id' => 66,
            ),
            382 => 
            array (
                'id' => 883,
                'ampher_code' => '8381',
                'ampher_name' => '*ทุ่งคา   ',
                'ampher_name_eng' => '*Tung Ka',
                'geo_id' => 6,
                'province_id' => 66,
            ),
            383 => 
            array (
                'id' => 884,
                'ampher_code' => '8401',
                'ampher_name' => 'เมืองสุราษฎร์ธานี   ',
                'ampher_name_eng' => 'Mueang Surat Thani',
                'geo_id' => 6,
                'province_id' => 67,
            ),
            384 => 
            array (
                'id' => 885,
                'ampher_code' => '8402',
                'ampher_name' => 'กาญจนดิษฐ์   ',
                'ampher_name_eng' => 'Kanchanadit',
                'geo_id' => 6,
                'province_id' => 67,
            ),
            385 => 
            array (
                'id' => 886,
                'ampher_code' => '8403',
                'ampher_name' => 'ดอนสัก   ',
                'ampher_name_eng' => 'Don Sak',
                'geo_id' => 6,
                'province_id' => 67,
            ),
            386 => 
            array (
                'id' => 887,
                'ampher_code' => '8404',
                'ampher_name' => 'เกาะสมุย   ',
                'ampher_name_eng' => 'Ko Samui',
                'geo_id' => 6,
                'province_id' => 67,
            ),
            387 => 
            array (
                'id' => 888,
                'ampher_code' => '8405',
                'ampher_name' => 'เกาะพะงัน   ',
                'ampher_name_eng' => 'Ko Pha-ngan',
                'geo_id' => 6,
                'province_id' => 67,
            ),
            388 => 
            array (
                'id' => 889,
                'ampher_code' => '8406',
                'ampher_name' => 'ไชยา   ',
                'ampher_name_eng' => 'Chaiya',
                'geo_id' => 6,
                'province_id' => 67,
            ),
            389 => 
            array (
                'id' => 890,
                'ampher_code' => '8407',
                'ampher_name' => 'ท่าชนะ   ',
                'ampher_name_eng' => 'Tha Chana',
                'geo_id' => 6,
                'province_id' => 67,
            ),
            390 => 
            array (
                'id' => 891,
                'ampher_code' => '8408',
                'ampher_name' => 'คีรีรัฐนิคม   ',
                'ampher_name_eng' => 'Khiri Rat Nikhom',
                'geo_id' => 6,
                'province_id' => 67,
            ),
            391 => 
            array (
                'id' => 892,
                'ampher_code' => '8409',
                'ampher_name' => 'บ้านตาขุน   ',
                'ampher_name_eng' => 'Ban Ta Khun',
                'geo_id' => 6,
                'province_id' => 67,
            ),
            392 => 
            array (
                'id' => 893,
                'ampher_code' => '8410',
                'ampher_name' => 'พนม   ',
                'ampher_name_eng' => 'Phanom',
                'geo_id' => 6,
                'province_id' => 67,
            ),
            393 => 
            array (
                'id' => 894,
                'ampher_code' => '8411',
                'ampher_name' => 'ท่าฉาง   ',
                'ampher_name_eng' => 'Tha Chang',
                'geo_id' => 6,
                'province_id' => 67,
            ),
            394 => 
            array (
                'id' => 895,
                'ampher_code' => '8412',
                'ampher_name' => 'บ้านนาสาร   ',
                'ampher_name_eng' => 'Ban Na San',
                'geo_id' => 6,
                'province_id' => 67,
            ),
            395 => 
            array (
                'id' => 896,
                'ampher_code' => '8413',
                'ampher_name' => 'บ้านนาเดิม   ',
                'ampher_name_eng' => 'Ban Na Doem',
                'geo_id' => 6,
                'province_id' => 67,
            ),
            396 => 
            array (
                'id' => 897,
                'ampher_code' => '8414',
                'ampher_name' => 'เคียนซา   ',
                'ampher_name_eng' => 'Khian Sa',
                'geo_id' => 6,
                'province_id' => 67,
            ),
            397 => 
            array (
                'id' => 898,
                'ampher_code' => '8415',
                'ampher_name' => 'เวียงสระ   ',
                'ampher_name_eng' => 'Wiang Sa',
                'geo_id' => 6,
                'province_id' => 67,
            ),
            398 => 
            array (
                'id' => 899,
                'ampher_code' => '8416',
                'ampher_name' => 'พระแสง   ',
                'ampher_name_eng' => 'Phrasaeng',
                'geo_id' => 6,
                'province_id' => 67,
            ),
            399 => 
            array (
                'id' => 900,
                'ampher_code' => '8417',
                'ampher_name' => 'พุนพิน   ',
                'ampher_name_eng' => 'Phunphin',
                'geo_id' => 6,
                'province_id' => 67,
            ),
            400 => 
            array (
                'id' => 901,
                'ampher_code' => '8418',
                'ampher_name' => 'ชัยบุรี   ',
                'ampher_name_eng' => 'Chai Buri',
                'geo_id' => 6,
                'province_id' => 67,
            ),
            401 => 
            array (
                'id' => 902,
                'ampher_code' => '8419',
                'ampher_name' => 'วิภาวดี   ',
                'ampher_name_eng' => 'Vibhavadi',
                'geo_id' => 6,
                'province_id' => 67,
            ),
            402 => 
            array (
                'id' => 903,
                'ampher_code' => '8451',
            'ampher_name' => 'เกาะพงัน (สาขาตำบลเกาะเต่า)*   ',
                'ampher_name_eng' => 'Ko Pha-ngan',
                'geo_id' => 6,
                'province_id' => 67,
            ),
            403 => 
            array (
                'id' => 904,
                'ampher_code' => '8481',
                'ampher_name' => '*อ.บ้านดอน  จ.สุราษฎร์ธานี   ',
                'ampher_name_eng' => '*Ban Don',
                'geo_id' => 6,
                'province_id' => 67,
            ),
            404 => 
            array (
                'id' => 905,
                'ampher_code' => '8501',
                'ampher_name' => 'เมืองระนอง   ',
                'ampher_name_eng' => 'Mueang Ranong',
                'geo_id' => 6,
                'province_id' => 68,
            ),
            405 => 
            array (
                'id' => 906,
                'ampher_code' => '8502',
                'ampher_name' => 'ละอุ่น   ',
                'ampher_name_eng' => 'La-un',
                'geo_id' => 6,
                'province_id' => 68,
            ),
            406 => 
            array (
                'id' => 907,
                'ampher_code' => '8503',
                'ampher_name' => 'กะเปอร์   ',
                'ampher_name_eng' => 'Kapoe',
                'geo_id' => 6,
                'province_id' => 68,
            ),
            407 => 
            array (
                'id' => 908,
                'ampher_code' => '8504',
                'ampher_name' => 'กระบุรี   ',
                'ampher_name_eng' => 'Kra Buri',
                'geo_id' => 6,
                'province_id' => 68,
            ),
            408 => 
            array (
                'id' => 909,
                'ampher_code' => '8505',
                'ampher_name' => 'สุขสำราญ   ',
                'ampher_name_eng' => 'Suk Samran',
                'geo_id' => 6,
                'province_id' => 68,
            ),
            409 => 
            array (
                'id' => 910,
                'ampher_code' => '8601',
                'ampher_name' => 'เมืองชุมพร   ',
                'ampher_name_eng' => 'Mueang Chumphon',
                'geo_id' => 6,
                'province_id' => 69,
            ),
            410 => 
            array (
                'id' => 911,
                'ampher_code' => '8602',
                'ampher_name' => 'ท่าแซะ   ',
                'ampher_name_eng' => 'Tha Sae',
                'geo_id' => 6,
                'province_id' => 69,
            ),
            411 => 
            array (
                'id' => 912,
                'ampher_code' => '8603',
                'ampher_name' => 'ปะทิว   ',
                'ampher_name_eng' => 'Pathio',
                'geo_id' => 6,
                'province_id' => 69,
            ),
            412 => 
            array (
                'id' => 913,
                'ampher_code' => '8604',
                'ampher_name' => 'หลังสวน   ',
                'ampher_name_eng' => 'Lang Suan',
                'geo_id' => 6,
                'province_id' => 69,
            ),
            413 => 
            array (
                'id' => 914,
                'ampher_code' => '8605',
                'ampher_name' => 'ละแม   ',
                'ampher_name_eng' => 'Lamae',
                'geo_id' => 6,
                'province_id' => 69,
            ),
            414 => 
            array (
                'id' => 915,
                'ampher_code' => '8606',
                'ampher_name' => 'พะโต๊ะ   ',
                'ampher_name_eng' => 'Phato',
                'geo_id' => 6,
                'province_id' => 69,
            ),
            415 => 
            array (
                'id' => 916,
                'ampher_code' => '8607',
                'ampher_name' => 'สวี   ',
                'ampher_name_eng' => 'Sawi',
                'geo_id' => 6,
                'province_id' => 69,
            ),
            416 => 
            array (
                'id' => 917,
                'ampher_code' => '8608',
                'ampher_name' => 'ทุ่งตะโก   ',
                'ampher_name_eng' => 'Thung Tako',
                'geo_id' => 6,
                'province_id' => 69,
            ),
            417 => 
            array (
                'id' => 918,
                'ampher_code' => '9001',
                'ampher_name' => 'เมืองสงขลา   ',
                'ampher_name_eng' => 'Mueang Songkhla',
                'geo_id' => 6,
                'province_id' => 70,
            ),
            418 => 
            array (
                'id' => 919,
                'ampher_code' => '9002',
                'ampher_name' => 'สทิงพระ   ',
                'ampher_name_eng' => 'Sathing Phra',
                'geo_id' => 6,
                'province_id' => 70,
            ),
            419 => 
            array (
                'id' => 920,
                'ampher_code' => '9003',
                'ampher_name' => 'จะนะ   ',
                'ampher_name_eng' => 'Chana',
                'geo_id' => 6,
                'province_id' => 70,
            ),
            420 => 
            array (
                'id' => 921,
                'ampher_code' => '9004',
                'ampher_name' => 'นาทวี   ',
                'ampher_name_eng' => 'Na Thawi',
                'geo_id' => 6,
                'province_id' => 70,
            ),
            421 => 
            array (
                'id' => 922,
                'ampher_code' => '9005',
                'ampher_name' => 'เทพา   ',
                'ampher_name_eng' => 'Thepha',
                'geo_id' => 6,
                'province_id' => 70,
            ),
            422 => 
            array (
                'id' => 923,
                'ampher_code' => '9006',
                'ampher_name' => 'สะบ้าย้อย   ',
                'ampher_name_eng' => 'Saba Yoi',
                'geo_id' => 6,
                'province_id' => 70,
            ),
            423 => 
            array (
                'id' => 924,
                'ampher_code' => '9007',
                'ampher_name' => 'ระโนด   ',
                'ampher_name_eng' => 'Ranot',
                'geo_id' => 6,
                'province_id' => 70,
            ),
            424 => 
            array (
                'id' => 925,
                'ampher_code' => '9008',
                'ampher_name' => 'กระแสสินธุ์   ',
                'ampher_name_eng' => 'Krasae Sin',
                'geo_id' => 6,
                'province_id' => 70,
            ),
            425 => 
            array (
                'id' => 926,
                'ampher_code' => '9009',
                'ampher_name' => 'รัตภูมิ   ',
                'ampher_name_eng' => 'Rattaphum',
                'geo_id' => 6,
                'province_id' => 70,
            ),
            426 => 
            array (
                'id' => 927,
                'ampher_code' => '9010',
                'ampher_name' => 'สะเดา   ',
                'ampher_name_eng' => 'Sadao',
                'geo_id' => 6,
                'province_id' => 70,
            ),
            427 => 
            array (
                'id' => 928,
                'ampher_code' => '9011',
                'ampher_name' => 'หาดใหญ่   ',
                'ampher_name_eng' => 'Hat Yai',
                'geo_id' => 6,
                'province_id' => 70,
            ),
            428 => 
            array (
                'id' => 929,
                'ampher_code' => '9012',
                'ampher_name' => 'นาหม่อม   ',
                'ampher_name_eng' => 'Na Mom',
                'geo_id' => 6,
                'province_id' => 70,
            ),
            429 => 
            array (
                'id' => 930,
                'ampher_code' => '9013',
                'ampher_name' => 'ควนเนียง   ',
                'ampher_name_eng' => 'Khuan Niang',
                'geo_id' => 6,
                'province_id' => 70,
            ),
            430 => 
            array (
                'id' => 931,
                'ampher_code' => '9014',
                'ampher_name' => 'บางกล่ำ   ',
                'ampher_name_eng' => 'Bang Klam',
                'geo_id' => 6,
                'province_id' => 70,
            ),
            431 => 
            array (
                'id' => 932,
                'ampher_code' => '9015',
                'ampher_name' => 'สิงหนคร   ',
                'ampher_name_eng' => 'Singhanakhon',
                'geo_id' => 6,
                'province_id' => 70,
            ),
            432 => 
            array (
                'id' => 933,
                'ampher_code' => '9016',
                'ampher_name' => 'คลองหอยโข่ง   ',
                'ampher_name_eng' => 'Khlong Hoi Khong',
                'geo_id' => 6,
                'province_id' => 70,
            ),
            433 => 
            array (
                'id' => 934,
                'ampher_code' => '9077',
                'ampher_name' => 'ท้องถิ่นเทศบาลตำบลสำนักขาม   ',
                'ampher_name_eng' => 'Sum Nung Kam',
                'geo_id' => 6,
                'province_id' => 70,
            ),
            434 => 
            array (
                'id' => 935,
                'ampher_code' => '9096',
                'ampher_name' => 'เทศบาลตำบลบ้านพรุ*   ',
                'ampher_name_eng' => 'Ban Pru*',
                'geo_id' => 6,
                'province_id' => 70,
            ),
            435 => 
            array (
                'id' => 936,
                'ampher_code' => '9101',
                'ampher_name' => 'เมืองสตูล   ',
                'ampher_name_eng' => 'Mueang Satun',
                'geo_id' => 6,
                'province_id' => 71,
            ),
            436 => 
            array (
                'id' => 937,
                'ampher_code' => '9102',
                'ampher_name' => 'ควนโดน   ',
                'ampher_name_eng' => 'Khuan Don',
                'geo_id' => 6,
                'province_id' => 71,
            ),
            437 => 
            array (
                'id' => 938,
                'ampher_code' => '9103',
                'ampher_name' => 'ควนกาหลง   ',
                'ampher_name_eng' => 'Khuan Kalong',
                'geo_id' => 6,
                'province_id' => 71,
            ),
            438 => 
            array (
                'id' => 939,
                'ampher_code' => '9104',
                'ampher_name' => 'ท่าแพ   ',
                'ampher_name_eng' => 'Tha Phae',
                'geo_id' => 6,
                'province_id' => 71,
            ),
            439 => 
            array (
                'id' => 940,
                'ampher_code' => '9105',
                'ampher_name' => 'ละงู   ',
                'ampher_name_eng' => 'La-ngu',
                'geo_id' => 6,
                'province_id' => 71,
            ),
            440 => 
            array (
                'id' => 941,
                'ampher_code' => '9106',
                'ampher_name' => 'ทุ่งหว้า   ',
                'ampher_name_eng' => 'Thung Wa',
                'geo_id' => 6,
                'province_id' => 71,
            ),
            441 => 
            array (
                'id' => 942,
                'ampher_code' => '9107',
                'ampher_name' => 'มะนัง   ',
                'ampher_name_eng' => 'Manang',
                'geo_id' => 6,
                'province_id' => 71,
            ),
            442 => 
            array (
                'id' => 943,
                'ampher_code' => '9201',
                'ampher_name' => 'เมืองตรัง   ',
                'ampher_name_eng' => 'Mueang Trang',
                'geo_id' => 6,
                'province_id' => 72,
            ),
            443 => 
            array (
                'id' => 944,
                'ampher_code' => '9202',
                'ampher_name' => 'กันตัง   ',
                'ampher_name_eng' => 'Kantang',
                'geo_id' => 6,
                'province_id' => 72,
            ),
            444 => 
            array (
                'id' => 945,
                'ampher_code' => '9203',
                'ampher_name' => 'ย่านตาขาว   ',
                'ampher_name_eng' => 'Yan Ta Khao',
                'geo_id' => 6,
                'province_id' => 72,
            ),
            445 => 
            array (
                'id' => 946,
                'ampher_code' => '9204',
                'ampher_name' => 'ปะเหลียน   ',
                'ampher_name_eng' => 'Palian',
                'geo_id' => 6,
                'province_id' => 72,
            ),
            446 => 
            array (
                'id' => 947,
                'ampher_code' => '9205',
                'ampher_name' => 'สิเกา   ',
                'ampher_name_eng' => 'Sikao',
                'geo_id' => 6,
                'province_id' => 72,
            ),
            447 => 
            array (
                'id' => 948,
                'ampher_code' => '9206',
                'ampher_name' => 'ห้วยยอด   ',
                'ampher_name_eng' => 'Huai Yot',
                'geo_id' => 6,
                'province_id' => 72,
            ),
            448 => 
            array (
                'id' => 949,
                'ampher_code' => '9207',
                'ampher_name' => 'วังวิเศษ   ',
                'ampher_name_eng' => 'Wang Wiset',
                'geo_id' => 6,
                'province_id' => 72,
            ),
            449 => 
            array (
                'id' => 950,
                'ampher_code' => '9208',
                'ampher_name' => 'นาโยง   ',
                'ampher_name_eng' => 'Na Yong',
                'geo_id' => 6,
                'province_id' => 72,
            ),
            450 => 
            array (
                'id' => 951,
                'ampher_code' => '9209',
                'ampher_name' => 'รัษฎา   ',
                'ampher_name_eng' => 'Ratsada',
                'geo_id' => 6,
                'province_id' => 72,
            ),
            451 => 
            array (
                'id' => 952,
                'ampher_code' => '9210',
                'ampher_name' => 'หาดสำราญ   ',
                'ampher_name_eng' => 'Hat Samran',
                'geo_id' => 6,
                'province_id' => 72,
            ),
            452 => 
            array (
                'id' => 953,
                'ampher_code' => '9251',
            'ampher_name' => 'อำเภอเมืองตรัง(สาขาคลองเต็ง)**   ',
            'ampher_name_eng' => 'Mueang Trang(Krong Teng)**',
                'geo_id' => 6,
                'province_id' => 72,
            ),
            453 => 
            array (
                'id' => 954,
                'ampher_code' => '9301',
                'ampher_name' => 'เมืองพัทลุง   ',
                'ampher_name_eng' => 'Mueang Phatthalung',
                'geo_id' => 6,
                'province_id' => 73,
            ),
            454 => 
            array (
                'id' => 955,
                'ampher_code' => '9302',
                'ampher_name' => 'กงหรา   ',
                'ampher_name_eng' => 'Kong Ra',
                'geo_id' => 6,
                'province_id' => 73,
            ),
            455 => 
            array (
                'id' => 956,
                'ampher_code' => '9303',
                'ampher_name' => 'เขาชัยสน   ',
                'ampher_name_eng' => 'Khao Chaison',
                'geo_id' => 6,
                'province_id' => 73,
            ),
            456 => 
            array (
                'id' => 957,
                'ampher_code' => '9304',
                'ampher_name' => 'ตะโหมด   ',
                'ampher_name_eng' => 'Tamot',
                'geo_id' => 6,
                'province_id' => 73,
            ),
            457 => 
            array (
                'id' => 958,
                'ampher_code' => '9305',
                'ampher_name' => 'ควนขนุน   ',
                'ampher_name_eng' => 'Khuan Khanun',
                'geo_id' => 6,
                'province_id' => 73,
            ),
            458 => 
            array (
                'id' => 959,
                'ampher_code' => '9306',
                'ampher_name' => 'ปากพะยูน   ',
                'ampher_name_eng' => 'Pak Phayun',
                'geo_id' => 6,
                'province_id' => 73,
            ),
            459 => 
            array (
                'id' => 960,
                'ampher_code' => '9307',
                'ampher_name' => 'ศรีบรรพต   ',
                'ampher_name_eng' => 'Si Banphot',
                'geo_id' => 6,
                'province_id' => 73,
            ),
            460 => 
            array (
                'id' => 961,
                'ampher_code' => '9308',
                'ampher_name' => 'ป่าบอน   ',
                'ampher_name_eng' => 'Pa Bon',
                'geo_id' => 6,
                'province_id' => 73,
            ),
            461 => 
            array (
                'id' => 962,
                'ampher_code' => '9309',
                'ampher_name' => 'บางแก้ว   ',
                'ampher_name_eng' => 'Bang Kaeo',
                'geo_id' => 6,
                'province_id' => 73,
            ),
            462 => 
            array (
                'id' => 963,
                'ampher_code' => '9310',
                'ampher_name' => 'ป่าพะยอม   ',
                'ampher_name_eng' => 'Pa Phayom',
                'geo_id' => 6,
                'province_id' => 73,
            ),
            463 => 
            array (
                'id' => 964,
                'ampher_code' => '9311',
                'ampher_name' => 'ศรีนครินทร์   ',
                'ampher_name_eng' => 'Srinagarindra',
                'geo_id' => 6,
                'province_id' => 73,
            ),
            464 => 
            array (
                'id' => 965,
                'ampher_code' => '9401',
                'ampher_name' => 'เมืองปัตตานี   ',
                'ampher_name_eng' => 'Mueang Pattani',
                'geo_id' => 6,
                'province_id' => 74,
            ),
            465 => 
            array (
                'id' => 966,
                'ampher_code' => '9402',
                'ampher_name' => 'โคกโพธิ์   ',
                'ampher_name_eng' => 'Khok Pho',
                'geo_id' => 6,
                'province_id' => 74,
            ),
            466 => 
            array (
                'id' => 967,
                'ampher_code' => '9403',
                'ampher_name' => 'หนองจิก   ',
                'ampher_name_eng' => 'Nong Chik',
                'geo_id' => 6,
                'province_id' => 74,
            ),
            467 => 
            array (
                'id' => 968,
                'ampher_code' => '9404',
                'ampher_name' => 'ปะนาเระ   ',
                'ampher_name_eng' => 'Panare',
                'geo_id' => 6,
                'province_id' => 74,
            ),
            468 => 
            array (
                'id' => 969,
                'ampher_code' => '9405',
                'ampher_name' => 'มายอ   ',
                'ampher_name_eng' => 'Mayo',
                'geo_id' => 6,
                'province_id' => 74,
            ),
            469 => 
            array (
                'id' => 970,
                'ampher_code' => '9406',
                'ampher_name' => 'ทุ่งยางแดง   ',
                'ampher_name_eng' => 'Thung Yang Daeng',
                'geo_id' => 6,
                'province_id' => 74,
            ),
            470 => 
            array (
                'id' => 971,
                'ampher_code' => '9407',
                'ampher_name' => 'สายบุรี   ',
                'ampher_name_eng' => 'Sai Buri',
                'geo_id' => 6,
                'province_id' => 74,
            ),
            471 => 
            array (
                'id' => 972,
                'ampher_code' => '9408',
                'ampher_name' => 'ไม้แก่น   ',
                'ampher_name_eng' => 'Mai Kaen',
                'geo_id' => 6,
                'province_id' => 74,
            ),
            472 => 
            array (
                'id' => 973,
                'ampher_code' => '9409',
                'ampher_name' => 'ยะหริ่ง   ',
                'ampher_name_eng' => 'Yaring',
                'geo_id' => 6,
                'province_id' => 74,
            ),
            473 => 
            array (
                'id' => 974,
                'ampher_code' => '9410',
                'ampher_name' => 'ยะรัง   ',
                'ampher_name_eng' => 'Yarang',
                'geo_id' => 6,
                'province_id' => 74,
            ),
            474 => 
            array (
                'id' => 975,
                'ampher_code' => '9411',
                'ampher_name' => 'กะพ้อ   ',
                'ampher_name_eng' => 'Kapho',
                'geo_id' => 6,
                'province_id' => 74,
            ),
            475 => 
            array (
                'id' => 976,
                'ampher_code' => '9412',
                'ampher_name' => 'แม่ลาน   ',
                'ampher_name_eng' => 'Mae Lan',
                'geo_id' => 6,
                'province_id' => 74,
            ),
            476 => 
            array (
                'id' => 977,
                'ampher_code' => '9501',
                'ampher_name' => 'เมืองยะลา   ',
                'ampher_name_eng' => 'Mueang Yala',
                'geo_id' => 6,
                'province_id' => 75,
            ),
            477 => 
            array (
                'id' => 978,
                'ampher_code' => '9502',
                'ampher_name' => 'เบตง   ',
                'ampher_name_eng' => 'Betong',
                'geo_id' => 6,
                'province_id' => 75,
            ),
            478 => 
            array (
                'id' => 979,
                'ampher_code' => '9503',
                'ampher_name' => 'บันนังสตา   ',
                'ampher_name_eng' => 'Bannang Sata',
                'geo_id' => 6,
                'province_id' => 75,
            ),
            479 => 
            array (
                'id' => 980,
                'ampher_code' => '9504',
                'ampher_name' => 'ธารโต   ',
                'ampher_name_eng' => 'Than To',
                'geo_id' => 6,
                'province_id' => 75,
            ),
            480 => 
            array (
                'id' => 981,
                'ampher_code' => '9505',
                'ampher_name' => 'ยะหา   ',
                'ampher_name_eng' => 'Yaha',
                'geo_id' => 6,
                'province_id' => 75,
            ),
            481 => 
            array (
                'id' => 982,
                'ampher_code' => '9506',
                'ampher_name' => 'รามัน   ',
                'ampher_name_eng' => 'Raman',
                'geo_id' => 6,
                'province_id' => 75,
            ),
            482 => 
            array (
                'id' => 983,
                'ampher_code' => '9507',
                'ampher_name' => 'กาบัง   ',
                'ampher_name_eng' => 'Kabang',
                'geo_id' => 6,
                'province_id' => 75,
            ),
            483 => 
            array (
                'id' => 984,
                'ampher_code' => '9508',
                'ampher_name' => 'กรงปินัง   ',
                'ampher_name_eng' => 'Krong Pinang',
                'geo_id' => 6,
                'province_id' => 75,
            ),
            484 => 
            array (
                'id' => 985,
                'ampher_code' => '9601',
                'ampher_name' => 'เมืองนราธิวาส   ',
                'ampher_name_eng' => 'Mueang Narathiwat',
                'geo_id' => 6,
                'province_id' => 76,
            ),
            485 => 
            array (
                'id' => 986,
                'ampher_code' => '9602',
                'ampher_name' => 'ตากใบ   ',
                'ampher_name_eng' => 'Tak Bai',
                'geo_id' => 6,
                'province_id' => 76,
            ),
            486 => 
            array (
                'id' => 987,
                'ampher_code' => '9603',
                'ampher_name' => 'บาเจาะ   ',
                'ampher_name_eng' => 'Bacho',
                'geo_id' => 6,
                'province_id' => 76,
            ),
            487 => 
            array (
                'id' => 988,
                'ampher_code' => '9604',
                'ampher_name' => 'ยี่งอ   ',
                'ampher_name_eng' => 'Yi-ngo',
                'geo_id' => 6,
                'province_id' => 76,
            ),
            488 => 
            array (
                'id' => 989,
                'ampher_code' => '9605',
                'ampher_name' => 'ระแงะ   ',
                'ampher_name_eng' => 'Ra-ngae',
                'geo_id' => 6,
                'province_id' => 76,
            ),
            489 => 
            array (
                'id' => 990,
                'ampher_code' => '9606',
                'ampher_name' => 'รือเสาะ   ',
                'ampher_name_eng' => 'Rueso',
                'geo_id' => 6,
                'province_id' => 76,
            ),
            490 => 
            array (
                'id' => 991,
                'ampher_code' => '9607',
                'ampher_name' => 'ศรีสาคร   ',
                'ampher_name_eng' => 'Si Sakhon',
                'geo_id' => 6,
                'province_id' => 76,
            ),
            491 => 
            array (
                'id' => 992,
                'ampher_code' => '9608',
                'ampher_name' => 'แว้ง   ',
                'ampher_name_eng' => 'Waeng',
                'geo_id' => 6,
                'province_id' => 76,
            ),
            492 => 
            array (
                'id' => 993,
                'ampher_code' => '9609',
                'ampher_name' => 'สุคิริน   ',
                'ampher_name_eng' => 'Sukhirin',
                'geo_id' => 6,
                'province_id' => 76,
            ),
            493 => 
            array (
                'id' => 994,
                'ampher_code' => '9610',
                'ampher_name' => 'สุไหงโก-ลก   ',
                'ampher_name_eng' => 'Su-ngai Kolok',
                'geo_id' => 6,
                'province_id' => 76,
            ),
            494 => 
            array (
                'id' => 995,
                'ampher_code' => '9611',
                'ampher_name' => 'สุไหงปาดี   ',
                'ampher_name_eng' => 'Su-ngai Padi',
                'geo_id' => 6,
                'province_id' => 76,
            ),
            495 => 
            array (
                'id' => 996,
                'ampher_code' => '9612',
                'ampher_name' => 'จะแนะ   ',
                'ampher_name_eng' => 'Chanae',
                'geo_id' => 6,
                'province_id' => 76,
            ),
            496 => 
            array (
                'id' => 997,
                'ampher_code' => '9613',
                'ampher_name' => 'เจาะไอร้อง   ',
                'ampher_name_eng' => 'Cho-airong',
                'geo_id' => 6,
                'province_id' => 76,
            ),
            497 => 
            array (
                'id' => 998,
                'ampher_code' => '9681',
                'ampher_name' => '*อ.บางนรา  จ.นราธิวาส   ',
                'ampher_name_eng' => '*Bang Nra',
                'geo_id' => 6,
                'province_id' => 76,
            ),
            498 => 
            array (
                'id' => 1005,
                'ampher_code' => '3807',
                'ampher_name' => 'ปากคาด',
                'ampher_name_eng' => 'Pak Khat',
                'geo_id' => 3,
                'province_id' => 77,
            ),
            499 => 
            array (
                'id' => 1004,
                'ampher_code' => '3806',
                'ampher_name' => 'บึงโขงหลง',
                'ampher_name_eng' => 'Bueng Khong Long',
                'geo_id' => 3,
                'province_id' => 77,
            ),
        ));
        \DB::table('amphers')->insert(array (
            0 => 
            array (
                'id' => 1003,
                'ampher_code' => '3805',
                'ampher_name' => 'ศรีวิไล',
                'ampher_name_eng' => 'Si Wilai',
                'geo_id' => 3,
                'province_id' => 77,
            ),
            1 => 
            array (
                'id' => 1002,
                'ampher_code' => '3804',
                'ampher_name' => 'พรเจริญ',
                'ampher_name_eng' => 'Phon Charoen',
                'geo_id' => 3,
                'province_id' => 77,
            ),
            2 => 
            array (
                'id' => 1001,
                'ampher_code' => '3803',
                'ampher_name' => 'โซ่พิสัย',
                'ampher_name_eng' => 'So Phisai',
                'geo_id' => 3,
                'province_id' => 77,
            ),
            3 => 
            array (
                'id' => 1000,
                'ampher_code' => '3802',
                'ampher_name' => 'เซกา',
                'ampher_name_eng' => 'Seka',
                'geo_id' => 3,
                'province_id' => 77,
            ),
            4 => 
            array (
                'id' => 999,
                'ampher_code' => '3801',
                'ampher_name' => 'เมืองบึงกาฬ',
                'ampher_name_eng' => 'Mueang Bueng Kan',
                'geo_id' => 3,
                'province_id' => 77,
            ),
            5 => 
            array (
                'id' => 1006,
                'ampher_code' => '3808',
                'ampher_name' => 'บุ่งคล้า',
                'ampher_name_eng' => 'Bung Khla',
                'geo_id' => 3,
                'province_id' => 77,
            ),
        ));
        
        
    }
}