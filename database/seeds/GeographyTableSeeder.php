<?php

use Illuminate\Database\Seeder;

class GeographyTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {
        

        \DB::table('geography')->delete();
        
        \DB::table('geography')->insert(array (
            0 => 
            array (
                'id' => 1,
                'geo_name' => 'ภาคเหนือ',
            ),
            1 => 
            array (
                'id' => 2,
                'geo_name' => 'ภาคกลาง',
            ),
            2 => 
            array (
                'id' => 3,
                'geo_name' => 'ภาคตะวันออกเฉียงเหนือ',
            ),
            3 => 
            array (
                'id' => 4,
                'geo_name' => 'ภาคตะวันตก',
            ),
            4 => 
            array (
                'id' => 5,
                'geo_name' => 'ภาคตะวันออก',
            ),
            5 => 
            array (
                'id' => 6,
                'geo_name' => 'ภาคใต้',
            ),
        ));
        
        
    }
}