<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAmphersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('amphers', function (Blueprint $table) {
            $table->increments('id');
            $table->string('ampher_code',4);
            $table->string('ampher_name');
            $table->string('ampher_name_eng');
            $table->integer('geo_id');
            $table->integer('province_id');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('amphers');
    }
}
