<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMpayLogsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('mpay_logs', function (Blueprint $table) {
            $table->increments('id');
            $table->timestamps();
            $table->dateTime('transaction_datetime');
            $table->string('orderid');
            $table->double('price');
            $table->string('type');
            $table->integer('branch_id');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('mpay_logs');
    }
}
