<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateInvoicesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('invoices', function (Blueprint $table) {
            $table->increments('id');
            $table->timestamps();
            $table->string('customer_fname')->nullable();
            $table->string('customer_lname')->nullable();
            $table->integer('gender')->nullable();
            $table->string('customer_address')->nullable();
            $table->integer('customer_tumbol')->nullable();
            $table->integer('customer_ampher')->nullable();
            $table->integer('customer_province')->nullable();
            $table->string('customer_zipcode',5)->nullable();
            $table->string('customer_tel')->nullable();
            $table->text('remark')->nullable();
            $table->integer('employee_id');
            $table->integer('branch_id');
            $table->integer('status')->default(1);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('invoices');
    }
}
