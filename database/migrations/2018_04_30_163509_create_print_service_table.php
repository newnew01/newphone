<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePrintServiceTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('print_services', function (Blueprint $table) {
            $table->increments('id');
            $table->timestamps();
            $table->string('service_name');
            $table->string('service_uri');
            $table->string('service_status_uri');
            $table->boolean('service_enable')->default(1);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('print_services');
    }
}
