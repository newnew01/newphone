@extends('app')
@section('page-header','เซอร์วิสการพิมพ์')
@section('page-header-right')
    <li class="breadcrumb-item"><a href="javascript:void(0)">หน้าหลัก</a></li>
    <li class="breadcrumb-item">บาร์โค้ด</li>
    <li class="breadcrumb-item active">เซอร์วิสการพิมพ์</li>
@endsection

@section('content')
<!-- ============================================================== -->
<!-- Start Page Content -->
<!-- ============================================================== -->
<div class="row">
    <div class="col-12">
        <div class="card card-outline-inverse" ng-controller="PrintServiceController">
            <div class="card-header">
                <button  class="btn btn-sm waves-effect waves-light btn-rounded btn-success pull-right" data-toggle="modal" data-target="#addPrintService" data-whatever="@mdo"><i class="mdi mdi-library-plus"></i> เพิ่ม</button>
                <h4 class="m-b-0 text-white">เซอร์วิสการพิมพ์</h4>
            </div>
            <div class="card-body">
                <table class="table-bordered table-hover table" id="print_serviceTable">
                    <thead>
                    <tr>
                        <th>#</th>
                        <th>ชื่อเซอร์วิส</th>
                        <th>URI</th>
                        <th>Status URI</th>
                        <th>สถานะ</th>
                        <th></th>
                    </tr>
                    </thead>
                    <tbody>
                        <tr ng-repeat="print_sv in print_services">
                            <td><% $index+1 %></td>
                            <td><% print_sv.service_name %></td>
                            <td><% print_sv.service_uri %></td>
                            <td><% print_sv.service_status_uri %></td>
                            <td ng-switch on="print_sv.service_status">
                                <span class="label label-success" ng-switch-when="1">ใช้งานได้</span>
                                <span class="label label-danger" ng-switch-when="0">เครื่องพิมพ์ปิด</span>
                                <span class="label label-danger" ng-switch-when="2">เกิดข้อผิดพลาด</span>
                                <span class="label label-danger" ng-switch-when="3">ไม่สามารถเชื่อมต่อได้</span>
                            </td>
                            <td><a href="/ิbarcode/print-service/delete/<% print_sv.id %>" class="btn waves-effect waves-light btn-xs btn-danger" onclick="return confirm('ต้องการลบใช่หรือไม่?');">ลบ</a></td>
                        </tr>
                    @if(count($print_services) == 0)
                        <tr>
                            <td colspan="5" style="text-align: center">ไม่มีข้อมูล</td>
                        </tr>
                    @endif
                    </tbody>
                </table>

            </div>



        </div>
    </div>



</div>

<div class="modal fade" id="addPrintService" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel1">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title" id="exampleModalLabel1">เพิ่มเซอร์วิสการพิมพ์</h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            </div>
            <form class="form" method="post" action="/barcode/print-service">
                {{csrf_field()}}
                <div class="modal-body">

                    <div class="form-group">
                        <input type="text" class="form-control" placeholder="ชื่อเซอร์วิส" name="service_name">
                    </div>

                    <div class="form-group">
                        <input type="text" class="form-control" placeholder="เซอร์วิส URI" name="service_uri">
                    </div>

                    <div class="form-group">
                        <input type="text" class="form-control" placeholder="สถานะเซอร์วิส URI" name="service_status_uri">
                    </div>

                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">ยกเลิก</button>
                    <button type="submit" class="btn btn-primary">เพิ่ม</button>
                </div>
            </form>
        </div>
    </div>
</div>


<!-- ============================================================== -->
<!-- End PAge Content -->
<!-- ============================================================== -->
@endsection

@section('js-head')
    <script src="/js/angular/controller/print-service.js"></script>
@endsection

@section('js-bottom')
    <!-- jQuery peity -->
    <script src="/assets/plugins/tablesaw-master/dist/tablesaw.js"></script>
    <script src="/assets/plugins/tablesaw-master/dist/tablesaw-init.js"></script>

    <script src="/assets/plugins/toast-master/js/jquery.toast.js"></script>

    <!-- This is data table -->
    <script src="/assets/plugins/datatables/media/js/jquery.dataTables.js"></script>


    <script>
        $(document).ready(function() {
            $('#productTable').DataTable();
        });
    </script>


    @include('template.flash-msg');
@endsection

@section('css-head')
    <!-- Bootstrap responsive table CSS -->
    <link href="/assets/plugins/tablesaw-master/dist/tablesaw.css" rel="stylesheet">

    <!-- toast CSS -->
    <link href="/assets/plugins/toast-master/css/jquery.toast.css" rel="stylesheet">
    <!-- Custom CSS -->
    <link href="/css/style.css" rel="stylesheet">
    <!-- You can change the theme colors from here -->
    <link href="/css/colors/blue.css" id="theme" rel="stylesheet">
@endsection