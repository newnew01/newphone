@extends('app')
@section('page-header','ใบเสร็จรับเงิน')
@section('page-header-right')
    <li class="breadcrumb-item"><a href="javascript:void(0)">หน้าหลัก</a></li>
    <li class="breadcrumb-item">ใบเสร็จรับเงิน</li>
    <li class="breadcrumb-item active">ใบเสร็จรับเงิน</li>
@endsection

@section('content')
    <!-- ============================================================== -->
    <!-- Start Page Content -->
    <!-- ============================================================== -->
    <div class="row">
        <div class="col-md-12">
            <div class="card card-body printableArea">
                <style>
                    h1,h2,h3,h4,h5,h6 {
                        color: black !important;
                    }

                    body {
                        color: black !important;
                    }
                    .printableArea .table td,.printableArea .table th{
                        padding-left: .2rem !important;
                        padding-right: .2rem !important;
                        font-size: 0.7em;

                    }
                    .printableArea .table td{
                        padding-top: .5rem !important;
                        padding-bottom: .5rem !important;
                    }



                </style>
                <div class="row">


                    <div class="pull-left">
                        <img src="/assets/images/branch_icon/{{$invoice->branch->branch_icon}}" width="200px" class="m-b-20">
                    </div>
                    <div class="pull-right m-l-10" style="font-size: 0.7em;">
                        <p class="text-muted m-l-5 m-b-0">{{$invoice->branch->branch_name}}</p>
                        <!--
                        <p class="text-muted m-l-5 m-b-0">{{$invoice->branch->branch_address}}</p>
                        -->
                        <p class="text-muted m-l-5 m-b-0">โทร : {{$invoice->branch->branch_tel}}</p>
                        <p class="text-muted m-l-5 m-b-0">LINE: {{$invoice->branch->branch_line}} @if($invoice->branch->branch_facebook != null) FB_page: {{$invoice->branch->branch_facebook}} @endif</p>
                    </div>
                </div>

                <h3><b>ใบเสร็จรับเงิน </b><span class="pull-right">เลขที่อ้างอิง: {{$invoice->getReferenceId()}}</span></h3>
                <div class="row">
                    <div class="col-md-12">
                        <div class="pull-right text-right">
                            <address>
                                <img src="data:image/png;base64,{{$invoice->generateBarcode()}}" alt="barcode">
                            </address>
                        </div>

                        <div class="pull-left">
                            <address>
                                @if($invoice->status == 4)<h3><span class="badge badge-danger">ยกเลิกใบเสร็จ</span> <span class="text-danger">เหตุผล: {{$invoice->getInvoiceCancelReason->reason}}</span></h3>@endif
                                <p class="text-muted m-l-5">วันที่ :<i class="fa fa-calendar"></i> {{ Carbon\Carbon::parse($invoice->created_at)->format('d-m-Y') }}</p>
                                @if($invoice->customer_fname != null)
                                <p class="text-muted m-l-5 m-b-0">ชื่อลูกค้า : {{$invoice->customer_fname}} {{$invoice->customer_lname}} </p>
                                <p class="text-muted m-l-5 m-b-0">ที่อยู่ : {{$invoice->customer_address}} {{$invoice->customer_tumbol == null ? '':$prefix['district'].$address_data['district']}} {{$invoice->customer_ampher == null ? '':$prefix['ampher'].$address_data['ampher']}} {{$invoice->customer_province == null ? '':$prefix['province'].$address_data['province']}} {{$invoice->customer_zipcode == null ? '':$invoice->customer_zipcode}}</p>
                                <p class="text-muted m-l-5">โทร : {{$invoice->customer_tel}}</p>
                                @endif
                            </address>
                        </div>


                    </div>
                    <div class="col-md-12">
                        <div class="table-responsive m-t-40" style="clear: both;">
                            <table class="table table-hover">
                                <thead>
                                <tr>
                                    <th width="30px">#</th>
                                    <th width="80px">รหัสสินค้า</th>
                                    <th>สินค้า</th>
                                    <th width="40px" class="text-center">จำนวน</th>
                                    <th width="80px" class="text-right">ราคา/หน่วย</th>
                                    <th width="80px" class="text-right">ราคาสุทธิ</th>

                                </tr>
                                </thead>
                                <tbody>
                                @foreach($invoice->detail as $index=>$inv_detail)
                                <tr>
                                    <td>{{$index+1}}</td>
                                    <td>
                                        @if($inv_detail->product_id != null)
                                            {{sprintf("%06d",$inv_detail->productInfo->id)}}
                                        @else
                                            {{sprintf("%06d",$inv_detail->serviceInfo->id)}}
                                        @endif
                                    </td>
                                    <td>
                                        @if($inv_detail->product_id != null)
                                            {{$inv_detail->productInfo->product_name}} {{$inv_detail->productInfo->brand->brand_name}} {{$inv_detail->productInfo->model}} {{$inv_detail->sn == null ? '':'IMEI: '.$inv_detail->sn}}
                                        @else
                                            {{$inv_detail->serviceInfo->service_name}}
                                        @endif
                                        @if(!$inv_detail->free_gift)
                                            @if(empty($invoice_full_price))
                                                {{$inv_detail->discount > 0 ? " **ส่วนลด ".$inv_detail->discount.'.-':''}}
                                            @endif
                                        @else
                                            {{'**แถมฟรี'}}
                                        @endif
                                        @if($inv_detail->product_id != null)
                                            @if($inv_detail->sn != null)
                                                <br>
                                                <img src="data:image/png;base64,{{\Milon\Barcode\DNS1D::getBarcodePNG($inv_detail->sn, "C128",1.5,10)}}" alt="barcode">

                                            @endif
                                        @endif


                                    </td>
                                    <td class="text-center" >{{$inv_detail->amount}}</td>
                                    <td class="text-right">@if(!$inv_detail->free_gift) {{number_format($inv_detail->price)}} @endif</td>
                                    <td class="text-right">@if(!$inv_detail->free_gift) @if(empty($invoice_full_price)) {{number_format($inv_detail->getTotalPrice())}} @else {{number_format($inv_detail->getTotalPriceWithoutDiscount())}} @endif @endif</td>
                                </tr>
                                @endforeach


                                </tbody>
                            </table>
                        </div>
                    </div>
                    <div class="col-md-12 text-right">
                        <h3 class="p-r-20 font-bold">ราคาสุทธิ: @if(empty($invoice_full_price)) {{number_format($invoice->getTotalPrice())}} @else {{number_format($invoice->getTotalPriceWithoutDiscount())}} @endif บาท</h3>
                    </div>

                    <div class="col-md-12">
                        <h5 class="p-r-20 font-bold">หมายเหตุ</h5>
                        <div style="border: 1px dashed;min-height: 50px;font-size: 0.7em" class="col-md-12">{{$invoice->remark}}</div>
                    </div>
                    <div class="col-md-12">
                        <div class="pull-right m-t-30 text-right">

                            <hr>
                            <h3><b>ผู้ขาย :</b> {{$invoice->User->name}}</h3>
                        </div>
                        <div class="clearfix"></div>


                    </div>

                    <div class="col-md-12 text-center">
                        <span>{!! \Milon\Barcode\DNS1D::getBarcodeSVG($invoice->getReferenceId(), "C128",1.5,10,"#00FFFF") !!}</span>
                        <span>{!! \Milon\Barcode\DNS1D::getBarcodeSVG($invoice->getReferenceId(), "C128",1.5,10,"#ff00ff") !!}</span>
                        <span>{!! \Milon\Barcode\DNS1D::getBarcodeSVG($invoice->getReferenceId(), "C128",1.5,10,"#ffff00") !!}</span>
                    </div>

                </div>

            </div>
        </div>
        <div class="col-md-12">
            <div class="card card-body">
                <div class="row">
                    <div class="col-md-12" style="text-align: center">
                        <a href="/sale/list" class="btn btn-info"> ย้อนกลับ </a>
                        <button id="print" class="btn btn-default" type="button"> <span><i class="fa fa-print"></i> พิมพ์</span> </button>
                        @if($invoice->status != 4)
                            @can('cancel-invoice')
                                <button id="cancen_invoice_btn" class="btn btn-default btn-danger" type="button" data-toggle="modal" data-target="#modal_cancel_invoice"> <span><i class="mdi mdi-playlist-remove"></i> ยกเลิกใบเสร็จ</span> </button>
                            @endcan
                        @endif
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="modal fade" tabindex="-1" id="modal_cancel_invoice" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true" style="display: none;">
        <div class="modal-dialog">
            <form method="post" action="/sale/cancel-invoice/{{$invoice->id}}"  style="width: 100%">
                {{ csrf_field() }}
                <div class="modal-content">
                    <div class="modal-header">
                        <h4 class="modal-title" id="myLargeModalLabel"><i class="mdi mdi-playlist-remove"></i> ยกเลิกใบเสร็จ</h4>
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                    </div>


                    <div class="modal-body">
                        <div class="form-group">
                            <label for="message-text" class="control-label">เหตุผล:</label>
                            <textarea class="form-control" id="reason_input" name="reason_input"></textarea>
                        </div>
                    </div>

                    <div class="modal-footer">
                        <button type="button" class="btn btn-default waves-effect" data-dismiss="modal">ปิด</button>
                        <button type="submit" class="btn btn-danger waves-effect waves-light">ยกเลิกใบเสร็จ</button>
                    </div>
                </div>
            </form>
            <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
    </div>
    <!-- ============================================================== -->
    <!-- End PAge Content -->
    <!-- ============================================================== -->
@endsection

@section('js-bottom')
    <script src="/js/jquery.PrintArea.js" type="text/JavaScript"></script>
    <script>
        $(document).ready(function() {
            $("#print").click(function() {
                var mode = 'iframe'; //popup
                var close = mode == "popup";
                var options = {
                    mode: mode,
                    popClose: close
                };
                $("div.printableArea").printArea(options);
            });
        });
    </script>
@endsection

@section('css-head')

@endsection