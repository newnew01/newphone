@extends('app')
@section('page-header','รายการสต๊อค')
@section('page-header-right')
    <li class="breadcrumb-item"><a href="javascript:void(0)">หน้าหลัก</a></li>
    <li class="breadcrumb-item">สต๊อคสินค้า</li>
    <li class="breadcrumb-item active">รายการสต๊อค</li>
@endsection

@section('content')
<!-- ============================================================== -->
<!-- Start Page Content -->
<!-- ============================================================== -->
<div class="row">
    <div class="col-12">
        <div class="card card-outline-inverse">
            <div class="card-header">
                <h4 class="m-b-0 text-white">รายการที่อยู่ระหว่างโอน</h4>
            </div>
            <div class="card-body">
                <table class="table-bordered table-hover table"  id="stockTransferListTable">
                    <thead >
                    <tr>
                        <th>เลขที่อ้างอิง</th>
                        <th>รายการ</th>
                        <th>สาขา</th>
                        <th>สถานะ</th>
                        <th>วันที่</th>

                        <th></th>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach($stock_transferring as $s)
                            <tr>
                                <td class="title">{{$s->type->prefix}}-{{sprintf('%06d', $s->id)}}</td>
                                <td>

                                    @if($s->ref_type == 1)
                                        <i class="mdi mdi-subdirectory-arrow-right text-primary"></i>
                                    @endif
                                    @if($s->ref_type == 2)
                                        <i class="mdi mdi-swap-horizontal text-success"></i>
                                    @endif
                                    @if($s->ref_type == 3)
                                        <i class="mdi mdi-currency-usd text-warning"></i>
                                    @endif
                                    @if($s->ref_type == 4)
                                        <i class="mdi mdi-loop text-danger"></i>
                                    @endif
                                    @if($s->ref_type == 5)
                                        <i class="mdi mdi-image-broken-variant text-danger"></i>
                                    @endif

                                    {{$s->type->type_name}}</td>
                                <td>
                                    @if($s->ref_type == 2)
                                        {{$s->sourceBranch->branch_name}} ->
                                    @endif
                                    {{$s->destinationBranch->branch_name}}
                                </td>
                                <td>
                                    @if($s->status_id == 1)
                                        <span class="label label-success">{{$s->status->status_name}}</span>
                                    @endif
                                    @if($s->status_id == 3)
                                        <span class="label label-warning">{{$s->status->status_name}}</span>
                                    @endif
                                    @if($s->status_id == 4)
                                        <span class="label label-danger">{{$s->status->status_name}}</span>
                                    @endif
                                    @if($s->status_id == 5)
                                        <span class="label label-inverse">{{$s->status->status_name}}</span>
                                    @endif
                                </td>
                                <td>{{Carbon\Carbon::parse($s->created_at)->format('d-m-Y')}}</td>
                                <td>
                                    <a href="/stock/list/{{$s->getReferenceId()}}" class="btn waves-effect waves-light btn-xs btn-primary">รายละเอียด</a>
                                </td>
                            </tr>
                    @endforeach

                    @if(count($stock_transferring) == 0)
                        <tr>
                            <td colspan="6" class="text-center">ไม่มีข้อมูล</td>
                        </tr>
                    @endif


                    </tbody>
                </table>
            </div>
        </div>
    </div>

    <div class="col-12">
        <div class="card card-outline-inverse">
            <div class="card-header">
                @can('stock-in')
                    <a href="/stock/in" class="btn btn-sm waves-effect waves-light btn-rounded btn-success pull-right m-l-5"><i class="mdi mdi-library-plus"></i> รับสินค้าเข้า</a>
                @endcan
                @can('stock-transfer')
                    <a href="/stock/transfer" class="btn btn-sm waves-effect waves-light btn-rounded btn-info pull-right"><i class="mdi mdi-library-plus"></i> โอนสินค้า</a>
                @endcan
                <h4 class="m-b-0 text-white">รายการทำสต๊อค</h4>
            </div>
            <div class="card-body">
                <table class="table-bordered table-hover table"  id="stockListTable">
                    <thead >
                    <tr>
                        <th>เลขที่อ้างอิง</th>
                        <th>รายการ</th>
                        <th>สาขา</th>
                        <th>สถานะ</th>
                        <th>วันที่</th>

                        <th></th>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach($stock_reference as $s)
                        <tr>
                            <td class="title">{{$s->type->prefix}}-{{sprintf('%06d', $s->id)}}</td>
                            <td>

                                @if($s->ref_type == 1)
                                <i class="mdi mdi-subdirectory-arrow-right text-primary"></i>
                                @endif
                                @if($s->ref_type == 2)
                                    <i class="mdi mdi-swap-horizontal text-success"></i>
                                @endif
                                @if($s->ref_type == 3)
                                    <i class="mdi mdi-currency-usd text-warning"></i>
                                @endif
                                @if($s->ref_type == 4)
                                    <i class="mdi mdi-loop text-danger"></i>
                                @endif
                                @if($s->ref_type == 5)
                                    <i class="mdi mdi-image-broken-variant text-danger"></i>
                                @endif

                                {{$s->type->type_name}}</td>
                            <td>
                                @if($s->ref_type == 2)
                                    {{$s->sourceBranch->branch_name}} ->
                                @endif
                                {{$s->destinationBranch->branch_name}}
                            </td>
                            <td>
                                @if($s->status_id == 1)
                                    <span class="label label-success">{{$s->status->status_name}}</span>
                                @endif
                                @if($s->status_id == 3)
                                        <span class="label label-warning">{{$s->status->status_name}}</span>
                                @endif
                                @if($s->status_id == 4)
                                    <span class="label label-danger">{{$s->status->status_name}}</span>
                                @endif
                                @if($s->status_id == 5)
                                        <span class="label label-inverse">{{$s->status->status_name}}</span>
                                @endif
                            </td>
                            <td>{{Carbon\Carbon::parse($s->created_at)->format('d-m-Y')}}</td>
                            <td>
                                <a href="/stock/list/{{$s->getReferenceId()}}" class="btn waves-effect waves-light btn-xs btn-primary">รายละเอียด</a>
                            </td>
                        </tr>
                    @endforeach

                    @if(count($stock_reference) == 0)
                        <tr>
                            <td colspan="6" class="text-center">ไม่มีข้อมูล</td>
                        </tr>
                    @endif


                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>
<!-- ============================================================== -->
<!-- End PAge Content -->
<!-- ============================================================== -->
@endsection

@section('js-bottom')


    <!-- This is data table -->
    <script src="/assets/plugins/datatables/media/js/jquery.dataTables.js"></script>


    <script>
        $(document).ready(function() {
            $('#stockListTable').DataTable({
                "bSort" : false
            });
        });
    </script>
@endsection

@section('css-head')


@endsection
