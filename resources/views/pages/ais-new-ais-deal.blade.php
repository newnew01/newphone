@extends('app')
@section('page-header','เพิ่มเครื่องโครงการ')
@section('page-header-right')
    <li class="breadcrumb-item"><a href="javascript:void(0)">หน้าหลัก</a></li>
    <li class="breadcrumb-item">สต๊อคสินค้า</li>
    <li class="breadcrumb-item active">รับสินค้าเข้า</li>
@endsection

@section('content')
<!-- ============================================================== -->
<!-- Start Page Content -->
<!-- ============================================================== -->
<div class="row" ng-controller="StockInController">

    <form method="post" action="/ais/new-ais-deal" id="ais_deal_form" style="width: 100%">
        {{ csrf_field() }}
        <div class="col-md-12">



            <div style="display: none" ng-repeat="product in products">
                <input type="hidden" name="product_id[]" value="<% product.id %>">
                <input type="hidden" name="sn[]" value="<% product.sn %>">
            </div>


            <div class="card card-outline-inverse">
                <div class="card-header">
                    <h4 class="m-b-0 text-white">รายละเอียด</h4>
                </div>
                <div class="card-body">




                    <div class="form-group col-md-6 p-l-0">
                        <div class="input-group">
                            <div class="input-group-addon ">AIS Buddy</div>
                            <select class="form-control custom-select" name="buddy_branch_id">
                                @foreach($buddy_branches as $branch)
                                    <option value="{{$branch->id}}">{{$branch->branch_name}}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>
                </div>
            </div>

        </div>

        <div class="col-md-12">
            <div class="card card-outline-inverse">
                <div class="card-header">
                    <h4 class="m-b-0 text-white">ข้อมูลสินค้า</h4>
                </div>
                <div class="card-body">
                    <div class="row p-t-20">
                        <div class="col-md-8">
                            <div class="form-group">
                                <div class="input-group">
                                    <div class="input-group-addon"><i class="mdi mdi-barcode"></i></div>
                                    <input type="text" class="form-control" id="barcode_input" placeholder="บาร์โค้ด" style="" ng-model="barcode_input" ng-keyup="$event.keyCode == 13 && addProductToList()" autocomplete="off">

                                    <span class="input-group-btn">
                                    <button type="button" id="check-minutes" class="btn waves-effect waves-light btn-success" ng-click="addProductToList()">ตกลง</button>
                                </span>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-4">
                            <button type="button" id="check-minutes" class="btn waves-effect waves-light btn-info"><i class="mdi mdi-magnify"></i> ค้นหา</button>

                        </div>


                    </div>

                    <div class="row">
                        <table class="table color-table success-table">
                            <thead>
                            <tr>
                                <th>#</th>
                                <th>ชื่อสินค้า</th>
                                <th>ยี่ห้อ</th>
                                <th>รุ่น</th>
                                <th>SN/IMEI</th>
                                <th></th>
                            </tr>
                            </thead>
                            <tbody>

                            <tr ng-if="products.length == 0">
                                <td colspan="8" class="text-center">ไม่มีข้อมูล</td>
                            </tr>

                            <tr ng-repeat="product in products">
                                <td><% $index+1 %></td>
                                <td ><span data-toggle="tooltip" title="<% product.description %>"><% product.product_name %></span></td>
                                <td><% product.brand %></td>
                                <td><% product.model %></td>
                                <td><% product.sn %></td>
                                <td>
                                    <button type="button" class="btn waves-effect waves-light btn-xs btn-primary">รายละเอียดสินค้า</button>
                                    <button type="button" class="btn waves-effect waves-light btn-xs btn-danger" ng-click="removeFromList($index)">ลบ</button>
                                </td>
                            </tr>

                            </tbody>
                        </table>
                    </div>

                </div>
            </div>
        </div>



        <div class="col-md-12">
            <div class="card card-outline-inverse">
                <div class="card-body text-center">
                    <button type="submit" class="btn btn-success waves-effect waves-light m-r-10" onclick="document.getElementById('stock_form').submit()"><i class="mdi mdi-content-save"></i> บันทึก</button>
                    <button type="button" class="btn btn-inverse waves-effect waves-light" ng-click="clearList()"><i class="mdi mdi-delete-empty"></i> เคลียร์รายการ</button>
                </div>
            </div>
        </div>

    </form>




    <div class="modal fade" tabindex="-1" id="modal_sn" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true" style="display: none;">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title" id="myLargeModalLabel">กรอก IMEI/SN</h4>
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                </div>
                <div class="modal-body">
                    <div class="input-group">
                        <div class="input-group-addon"><i class="mdi mdi-barcode"></i></div>
                        <input type="text" class="form-control" name="imei_sn_input" id="imei_sn_input" placeholder="กรุณากรอก IMEI/SN" ng-model="imei_sn_input" ng-keyup="$event.keyCode == 13 && addProductToListSN()">
                        <span class="input-group-btn">
                                <button ng-click="addProductToListSN()"   class="btn waves-effect waves-light btn-success">ตกลง</button>
                            </span>
                    </div>
                </div>
            </div>
            <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
    </div>


</div>
<!-- ============================================================== -->
<!-- End PAge Content -->
<!-- ============================================================== -->
@endsection

@section('js-head')
    <script src="/js/angular/controller/new-ais-deal.js"></script>
@endsection

@section('js-bottom')

    <script src="/assets/plugins/toast-master/js/jquery.toast.js"></script>

    @include('template.flash-msg');


    <script>
        $(document).ready(function () {
            $('#barcode_input').focus();

            $(window).keydown(function(event){
                if(event.keyCode == 13) {
                    event.preventDefault();
                    return false;
                }
            });
        });
    </script>
@endsection



@section('css-head')
    <link rel="stylesheet" href="/assets/plugins/dropify/dist/css/dropify.min.css">
    <link href="/assets/plugins/bootstrap/css/bootstrap.min.css" rel="stylesheet">
    <!-- toast CSS -->
    <link href="/assets/plugins/toast-master/css/jquery.toast.css" rel="stylesheet">
    <!-- Custom CSS -->
    <link href="/css/style.css" rel="stylesheet">
    <!-- You can change the theme colors from here -->
    <link href="/css/colors/blue.css" id="theme" rel="stylesheet">

@endsection