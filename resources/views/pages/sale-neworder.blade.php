@extends('app')
@section('page-header','ขายใหม่')
@section('page-header-right')
    <li class="breadcrumb-item"><a href="javascript:void(0)">หน้าหลัก</a></li>
    <li class="breadcrumb-item">การขาย</li>
    <li class="breadcrumb-item active">ขายใหม่</li>
@endsection

@section('content')
<!-- ============================================================== -->
<!-- Start Page Content -->
<!-- ============================================================== -->
<div class="row" ng-controller="SaleNewOrderController">

    <form method="post" action="/sale/neworder" style="width: 100%" autocomplete="off" id="sale_form">
        {{ csrf_field() }}
        <div class="col-md-12">
            <div class="card card-outline-inverse">
                <div class="card-header">
                    <h4 class="m-b-0 text-white">ข้อมูลสินค้า</h4>
                </div>
                <div class="card-body">
                    <div class="row p-t-20">
                        <div class="col-md-8">
                            <div class="form-group">
                                <div class="input-group">
                                    <div class="input-group-addon"><i class="mdi mdi-barcode"></i></div>
                                    <input type="text" class="form-control" id="barcode_input" placeholder="บาร์โค้ด" style="" ng-model="barcode_input" ng-keyup="$event.keyCode == 13 && enterBarcode()">

                                    <span class="input-group-btn">
                                        <button type="button" id="check-minutes" class="btn waves-effect waves-light btn-success" ng-click="enterBarcode()">ตกลง</button>
                                    </span>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-4">

                            <button type="button" id="check-minutes" class="btn waves-effect waves-light btn-warning" data-toggle="modal" data-target="#modal_amount_barcode">จำนวนมาก</button>
                            <button type="button" id="check-minutes" class="btn waves-effect waves-light btn-info" data-toggle="modal" data-target="#modal_search_product"><i class="mdi mdi-magnify"></i> ค้นหา</button>
                            <button type="button" id="check-minutes" class="btn waves-effect waves-light btn-primary m-l-10" ng-click="viewAddServiceModal()"><i class="mdi mdi-shape-polygon-plus"></i> ค่าบริการ</button>

                        </div>

                        <table class="table table-bordered">
                            <thead>
                            <tr>
                                <th width="30px">#</th>
                                <th>สินค้า</th>
                                <th width="70px" class="text-center">จำนวน</th>
                                <th width="120px" class="text-center">ราคา/หน่วย</th>
                                <th width="90px" class="text-center">ราคารวม</th>
                                <th width="120px" class="text-center">ราคาสุทธิ</th>
                                <th width="90px" class="text-center">ส่วนลด</th>
                                <th width="90px" class="text-center">แถม</th>
                                <th width="80px"></th>
                            </tr>
                            </thead>
                            <tbody>

                            <tr ng-if="products.length == 0">
                                <td colspan="9" class="text-center">ไม่มีข้อมูล</td>
                            </tr>

                            <tr ng-repeat="product in products">
                                <td><% $index+1 %></td>
                                <td ><span class="tooltip_description" data-toggle="tooltip" title="<% product.description %>" tooltip><% product.product_name %> <% product.brand == 'อื่นๆ' ? '':product.brand %> <% product.model %> <span ng-if="product.ais_deal == 1">(AIS deal)</span> <span ng-if="product.sn != null">[<% product.sn %>]</span></span></td>
                                <td class="text-center"><% product.count %></td>
                                <td class="text-center"><% product.price %></td>
                                <td class="text-center"><span><% (product.price*product.count) %></span></td>
                                <td class="text-center font-bold"><span ng-if="!free_gift[$index]"><% (product.price*product.count)-discount[$index] %></span><span ng-if="free_gift[$index]">0</span></td>
                                <td class="text-center">
                                    <input value='0' type="text" style="width: 100%" id="discount_<% $index %>" ng-model="discount[$index]" ng-if="!free_gift[$index]" ng-init="discount[$index] = 0">
                                </td>
                                <td class="text-center">
                                    <input type="checkbox" id="free_gift_<% $index %>" class="filled-in chk-col-red" ng-model="free_gift[$index]"  ng-init="free_gift[$index] = false"/>
                                    <label for="free_gift_<% $index %>"></label>
                                </td>

                                <td>
                                    <button type="button" class="btn waves-effect waves-light btn-xs btn-danger" ng-click="removeFromList($index)">ลบ</button>
                                </td>
                            </tr>

                            <tr  ng-if="false">
                                <td></td>
                                <td ></td>
                                <td class="text-center"></td>
                                <td class="text-center"></td>
                                <td class="text-center font-bold"><% getTotal()+getTotalDiscount() %></td>
                                <td class="text-center font-bold"><% getTotal() %></td>
                                <td class="text-center font-bold"><% getTotalDiscount() %></td>
                                <td class="text-center"></td>
                                <td></td>
                            </tr>

                            </tbody>
                        </table>

                        <div class="col-md-12 text-right">
                            <h3 class="p-r-20 font-bold">ราคาสุทธิ: <% getTotal() %></h3>
                        </div>
                    </div>

                    <div class="form-group m-b-0 m-t-20">
                        <div class="checkbox checkbox-success " style="display: inline">
                            <input id="checkbox1" type="checkbox" name="invoice_full_price">
                            <label for="checkbox1"> ออกใบเสร็จราคาเต็ม </label>
                        </div>


                    </div>

                    <div class="form-group">
                        <label>หมายเหตุ</label>
                        <textarea class="form-control" name="remark" rows="5"></textarea>
                    </div>




                </div>
            </div>
        </div>




        <div style="display: none" ng-repeat="product in products">
            <input type="hidden" name="product_id[]" value="<% product.id %>">
            <input type="hidden" name="type_sn[]" value="<% product.type_sn %>">
            <input type="hidden" name="sn[]" value="<% product.sn %>">
            <input type="hidden" name="count[]" value="<% product.count %>">
            <input type="hidden" name="discount[]" value="<% discount[$index] %>">
            <input type="hidden" name="free_gift[]" value="<% free_gift[$index] %>">
            <input type="hidden" name="ais_deal[]" value="<% product.ais_deal %>">
            <input type="hidden" name="item_type[]" value="<% product.item_type %>">
            <input type="hidden" name="item_price[]" value="<% product.price %>">

        </div>

        <div class="col-md-12">
            <div class="card card-outline-inverse">
                <div class="card-header">
                    <button type="button" class="btn btn-default btn-sm pull-right" ng-click="readSmartCard()" ng-bind-html="txt_btn_save"></button>
                    <h4 class="m-b-0 text-white">ข้อมูลลูกค้า</h4>
                </div>
                <div class="card-body">
                    <div class="form-body">
                        <div class="row p-t-20">
                            <div class="col-md-12">
                                <div class="form-group">
                                    <div class="checkbox checkbox-success " style="display: inline">
                                        <input id="no_customer_info" name="no_customer_info" type="checkbox" ng-model="no_customer_info">
                                        <label for="no_customer_info"> ไม่ระบุข้อมูลลูกค้า </label>
                                    </div>
                                </div>
                            </div>

                            <div class="col-md-5">
                                <div class="form-group">
                                    <label class="control-label">ชื่อ</label>
                                    <input type="text" name="first_name" class="form-control" autocomplete="off" ng-model="fname" ng-disabled="no_customer_info">
                                </div>
                            </div>

                            <div class="col-md-5">
                                <div class="form-group">
                                    <label class="control-label">นามสกุล</label>
                                    <input type="text" name="last_name" class="form-control"  ng-model="lname" ng-disabled="no_customer_info">
                                </div>
                            </div>
                            <div class="col-md-2">
                                <div class="form-group">
                                    <label class="control-label">เพศ</label>
                                    <select class="form-control custom-select" name="gender"  ng-model="gender" ng-disabled="no_customer_info">
                                        <option value="">[เลือกเพศ]</option>
                                        <option value="1">ชาย</option>
                                        <option value="2">หญิง</option>
                                    </select>
                                </div>
                            </div>

                        </div>
                        <!--/row-->
                        <div class="row">
                            <div class="col-md-5">
                                <div class="form-group">
                                    <label class="control-label">เบอร์โทร</label>
                                    <input type="text" name="tel_no" class="form-control" ng-disabled="no_customer_info">
                                </div>
                            </div>
                            <div class="col-md-7">
                                <div class="form-group">
                                    <label class="control-label">วันเดือนปีเกิด</label>
                                    <input type="text" name="date_of_birth" class="form-control" ng-model="birth_date" placeholder="วว/ดด/ปปปป" ng-disabled="no_customer_info">
                                </div>
                            </div>
                            <!--/span-->
                        </div>



                        <h3 class="box-title m-t-40">ที่อยู่</h3>
                        <hr>
                        <div class="row">
                            <div class="col-md-12 ">
                                <div class="form-group">
                                    <label>รายละเอียดที่อยู่</label>
                                    <input type="text" name="address" class="form-control" placeholder="eg.บ้านเลขที่, หมู่, อาคาร, ซอย" ng-model="address" ng-disabled="no_customer_info">
                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-md-12 ">
                                <div class="form-group">
                                    <label>ค้นหาที่อยู่</label>
                                    <select  id="search_location_input" class="select2 form-control custom-select" style="width: 100%; height:36px;" ng-disabled="no_customer_info">
                                    </select>
                                </div>

                                <input type="hidden" name="tumbol" id="tumbol" ng-value="tumbol">
                                <input type="hidden" name="ampher" id="ampher" ng-value="ampher">
                                <input type="hidden" name="province" id="province" ng-value="province">
                                <input type="hidden" name="zip_code" id="zip_code" ng-value="zip_code">

                            </div>

                            <div class="col-md-3">
                                <div class="form-group">
                                    <label class="control-label">ตำบล</label>
                                    <input type="text" class="form-control" autocomplete="off" ng-model="txt_district" id="txt_district" disabled>
                                </div>
                            </div>
                            <div class="col-md-3">
                                <div class="form-group">
                                    <label class="control-label">อำเภอ</label>
                                    <input type="text"  class="form-control" autocomplete="off" ng-model="txt_ampher" id="txt_ampher" disabled>
                                </div>
                            </div>

                            <div class="col-md-3">
                                <div class="form-group">
                                    <label class="control-label">จังหวัด</label>
                                    <input type="text"  class="form-control" autocomplete="off" ng-model="txt_province" id="txt_province" disabled>
                                </div>
                            </div>

                            <div class="col-md-3">
                                <div class="form-group">
                                    <label class="control-label">รหัสไปรษณีย์</label>
                                    <input type="text" class="form-control" autocomplete="off" ng-model="txt_zip_code" id="txt_zip_code" disabled>
                                </div>
                            </div>
                        </div>
                    </div>



                </div>
            </div>

            <div class="card card-outline-inverse">
                <div class="card-body text-center">
                    <div class="form-actions">
                        <button type="button" class="btn btn-success" ng-click="submitData()" ng-disabled="isSubmit"> <i class="fa fa-check"></i> บันทึกใบเสร็จ</button>
                        <button type="button" class="btn btn-inverse">เคลียร์ข้อมูล</button>
                    </div>
                </div>
            </div>
        </div>

    </form>




    <div class="modal fade" tabindex="-1" id="modal_amount_barcode" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true" style="display: none;">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title" id="myLargeModalLabel">ขายจำนวนมาก</h4>
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                </div>
                <div class="modal-body">
                    <div class="input-group">
                        <div class="input-group-addon"><i class="mdi mdi-numeric"></i></div>
                        <input type="text" class="form-control col-md-2" name="amount" id="amount_input" ng-model="amount_input" placeholder="จำนวน" ng-keyup="$event.keyCode == 13 && checkAmountInput()">
                        <div class="input-group-addon"><i class="mdi mdi-barcode"></i></div>
                        <input type="text" class="form-control" name="amount_barcode" id="amount_barcode_input" ng-model="amount_barcode_input" placeholder="บาร์โค้ด" ng-keyup="$event.keyCode == 13 && enterBarcodeWithAmount()">
                        <span class="input-group-btn">
                                <button ng-click="enterBarcodeWithAmount()"   class="btn waves-effect waves-light btn-success">ตกลง</button>
                        </span>
                    </div>
                </div>
            </div>
            <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
    </div>

    <div class="modal fade" tabindex="-1" id="modal_sn" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true" style="display: none;">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title" id="myLargeModalLabel">กรอก IMEI/SN</h4>
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                </div>
                <div class="modal-body">
                    <div class="input-group">
                        <div class="input-group-addon"><i class="mdi mdi-barcode"></i></div>
                        <input type="text" class="form-control" name="imei_sn_input" id="imei_sn_input" placeholder="กรุณากรอก IMEI/SN" ng-model="imei_sn_input" ng-keyup="$event.keyCode == 13 && enterSN()">
                        <span class="input-group-btn">
                                <button ng-click="enterSN()"   class="btn waves-effect waves-light btn-success">ตกลง</button>
                            </span>
                    </div>
                </div>
            </div>
            <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
    </div>

    <div class="modal fade" tabindex="-1" id="modal_search_product" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true" style="display: none;">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header">
                    <div class="input-group">
                        <span class="input-group-addon" id="basic-addon1"><i class="mdi mdi-magnify"></i></span>
                        <input type="text" class="form-control col-md-6" placeholder="รหัสสินค้า,ชื่อสินค้า,ยี่ห้อ,รุ่น,ราคา" aria-describedby="basic-addon1" id="search_product_input" ng-model="search_product_input" ng-keyup="$event.keyCode == 13 && searchProduct()">
                        <span class="input-group-btn">
                            <button type="button" class="btn waves-effect waves-light btn-success" ng-click="searchProduct()">ตกลง</button>
                        </span>
                        <button type="button" class="btn waves-effect waves-light btn-secondary btn-inverse m-l-10" ng-click="clearSearchResult()"><i class="mdi mdi-delete-empty"></i> เคลียร์การค้นหา</button>
                    </div>


                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                </div>
                <div class="modal-body">
                    <table class="table-bordered table-hover table" id="search_result_table">
                        <thead>
                        <tr>
                            <th>รหัสสินค้า</th>
                            <th>ชื่อสินค้า</th>
                            <th>ยี่ห้อ</th>
                            <th>รุ่น</th>
                            <th>ราคา</th>
                            <th>คงเหลือ</th>
                            <th></th>
                        </tr>
                        </thead>
                        <tbody>
                        <tr ng-repeat="product in search_products">
                            <td><% pad(product.id,6) %></td>
                            <td><% product.product_name %></td>
                            <td><% product.brand_name %></td>
                            <td><% product.model %></td>
                            <td><% number_format(product.price) %></td>
                            <td>
                                <span ng-if="product.amount_in_branch <= 0" class="text-danger"><% product.amount_in_branch %></span>
                                <span ng-if="product.amount_in_branch > 0" class="text-success"><% product.amount_in_branch %></span>
                                (<% product.amount %>)
                            </td>
                            <td>
                                <input value='0' type="text" style="width: 40px;text-align: center;" id="search_amount_input_<% $index %>" ng-model="search_amount_input[$index]"  ng-init="search_amount_input[$index] = 1" ng-if="product.type_sn == 0">
                                <button  class="btn waves-effect waves-light btn-xs btn-info" ng-click="addItemToListBySearch(product,search_amount_input[$index])"><i class="mdi mdi-plus"></i> เพิ่ม</button>
                            </td>
                        </tr>
                        <tr ng-if="search_products.length == 0">
                            <td colspan="6" style="text-align: center">ไม่มีข้อมูล</td>
                        </tr>
                        </tbody>
                    </table>
                </div>
            </div>
            <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
    </div>

    <div class="modal fade" tabindex="-1" id="modal_add_service" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true" style="display: none;">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title" id="myLargeModalLabel">เพิ่มค่าบริการ</h4>
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                </div>
                <div class="modal-body">
                    <table class="table-bordered table-hover table" id="search_result_table">
                        <thead>
                        <tr>
                            <th>รหัส</th>
                            <th>ชื่อบริการ</th>
                            <th>ราคา</th>
                            <th></th>
                        </tr>
                        </thead>
                        <tbody>
                        <tr ng-repeat="service in services">
                            <td><% pad(service.id,6) %></td>
                            <td><% service.service_name %></td>
                            <td><input value='0' type="text" style="width: 80px;text-align: center;" id="service_price_input_<% $index %>" ng-model="service_price_input[$index]"  ng-init="service_price_input[$index] = service.price" ng-disabled="service.price_editable == 0" ></td>
                            <td>
                                <input value='0' type="text" style="width: 40px;text-align: center;" id="service_amount_input_<% $index %>" ng-model="service_amount_input[$index]"  ng-init="service_amount_input[$index] = 1">
                                <button  class="btn waves-effect waves-light btn-xs btn-info" ng-click="addServiceToList(service,service_amount_input[$index],service_price_input[$index])"><i class="mdi mdi-plus"></i> เพิ่ม</button>
                            </td>
                        </tr>
                        <tr ng-if="services.length == 0">
                            <td colspan="6" style="text-align: center">ไม่มีข้อมูล</td>
                        </tr>
                        </tbody>
                    </table>
                </div>
            </div>
            <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
    </div>


</div>
<!-- ============================================================== -->
<!-- End PAge Content -->
<!-- ============================================================== -->
@endsection

@section('js-head')
    <script src="/js/angular/controller/sale-neworder.js"></script>
@endsection

@section('js-bottom')
    <script src="/assets/plugins/toast-master/js/jquery.toast.js"></script>
    <script src="/assets/plugins/select2/dist/js/select2.full.min.js" type="text/javascript"></script>

    @include('template.flash-msg');

    <script>
        source_branch = {{Auth::user()->branch_id}};

        $(document).ready(function () {
            $('#barcode_input').focus();

            $(window).keydown(function(event){
                if(event.keyCode == 13) {
                    event.preventDefault();
                    return false;
                }
            });

            $('#search_location_input').select2({
                placeholder: "กรุณากรอกตำบล",
                minimumInputLength:3,
                ajax: {
                    url: function (params) {
                        return '/web-service/thai-location/search-by-district/' + params.term;
                    },
                    processResults: function (data) {
                        return {
                            results: $.map(data, function(obj) {
                                district_prefix = 'ต.';
                                ampher_prefix = 'อ.';
                                province_prefix = 'จ.';

                                if(obj.province_code == "10"){
                                    district_prefix = 'แขวง';
                                    ampher_prefix = '';
                                    province_prefix = '';
                                }
                                return {
                                    id: obj.district_code,
                                    text: district_prefix+obj.district_name+' '+ampher_prefix+obj.ampher_name+' '+province_prefix+obj.province_name+' '+obj.zipcode,
                                    district_id: obj.district_id,
                                    ampher_id : obj.ampher_id,
                                    province_id: obj.province_id,
                                    zipcode: obj.zipcode,
                                    district_name: obj.district_name,
                                    ampher_name : obj.ampher_name,
                                    province_name: obj.province_name
                                };
                            })
                        };
                    }

                }
            });

            $('#search_location_input').on('select2:select', function (e) {
                var data = e.params.data;
                $('#tumbol').val(data.district_id);
                $('#ampher').val(data.ampher_id);
                $('#province').val(data.province_id);
                $('#zip_code').val(data.zipcode);

                $('#txt_district').val(data.district_name);
                $('#txt_ampher').val(data.ampher_name);
                $('#txt_province').val(data.province_name);
                $('#txt_zip_code').val(data.zipcode);

                console.log(data);
            });

        });



    </script>
@endsection

@section('css-head')
    <link rel="stylesheet" href="/assets/plugins/dropify/dist/css/dropify.min.css">
    <link href="/assets/plugins/bootstrap/css/bootstrap.min.css" rel="stylesheet">
    <!-- toast CSS -->
    <link href="/assets/plugins/toast-master/css/jquery.toast.css" rel="stylesheet">
    <!-- Custom CSS -->
    <link href="/css/style.css" rel="stylesheet">
    <!-- You can change the theme colors from here -->
    <link href="/css/colors/blue.css" id="theme" rel="stylesheet">

    <link href="/assets/plugins/select2/dist/css/select2.min.css" rel="stylesheet" type="text/css" />

@endsection