@extends('app')
@section('page-header','รับสินค้าเข้า')
@section('page-header-right')
    <li class="breadcrumb-item"><a href="javascript:void(0)">หน้าหลัก</a></li>
    <li class="breadcrumb-item">สต๊อคสินค้า</li>
    <li class="breadcrumb-item active">รับสินค้าเข้า</li>
@endsection

@section('content')
<!-- ============================================================== -->
<!-- Start Page Content -->
<!-- ============================================================== -->
<div class="row" ng-controller="StockInController">

    <form method="post" action="/stock/in" id="stock_form" style="width: 100%">
        {{ csrf_field() }}
        <div class="col-md-12">



            <div style="display: none" ng-repeat="product in products">
                <input type="hidden" name="product_id[]" value="<% product.id %>">
                <input type="hidden" name="type_sn[]" value="<% product.type_sn %>">
                <input type="hidden" name="sn[]" value="<% product.sn %>">
                <input type="hidden" name="count[]" value="<% product.count %>">
            </div>


            <div class="card card-outline-inverse">
                <div class="card-header">
                    <h4 class="m-b-0 text-white">รายละเอียดการรับเข้า</h4>
                </div>
                <div class="card-body">




                    <div class="form-group col-md-6 p-l-0">
                        <div class="input-group">
                            <div class="input-group-addon ">สาขารับเข้า</div>
                            <select class="form-control custom-select" name="source_branch_id">
                                @foreach($branches as $branch)
                                    <option value="{{$branch->id}}" {{Auth::user()->branch_id == $branch->id ? "selected":"disabled"}}>{{$branch->branch_name}}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>
                </div>
            </div>

        </div>

        <div class="col-md-12">
            <div class="card card-outline-inverse">
                <div class="card-header">
                    <h4 class="m-b-0 text-white">ข้อมูลสินค้า</h4>
                </div>
                <div class="card-body">
                    <div class="row p-t-20">
                        <div class="col-md-8">
                            <div class="form-group">
                                <div class="input-group">
                                    <div class="input-group-addon"><i class="mdi mdi-barcode"></i></div>
                                    <input type="text" class="form-control" id="barcode_input" placeholder="บาร์โค้ด" style="" ng-model="barcode_input" ng-keyup="$event.keyCode == 13 && enterBarcode()" autocomplete="off">

                                    <span class="input-group-btn">
                                    <button type="button" id="check-minutes" class="btn waves-effect waves-light btn-success" ng-click="enterBarcode()">ตกลง</button>
                                </span>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-4">

                            <button type="button" id="check-minutes" class="btn waves-effect waves-light btn-warning" data-toggle="modal" data-target="#modal_amount_barcode">จำนวนมาก</button>
                            <button type="button" id="check-minutes" class="btn waves-effect waves-light btn-info" data-toggle="modal" data-target="#modal_search_product"><i class="mdi mdi-magnify"></i> ค้นหา</button>

                        </div>


                    </div>

                    <div class="row">
                        <table class="table table-bordered">
                            <thead>
                            <tr>
                                <th>#</th>
                                <th>ชื่อสินค้า</th>
                                <th>ยี่ห้อ</th>
                                <th>รุ่น</th>
                                <th>SN/IMEI</th>
                                <th>จำนวน</th>
                                <th></th>
                            </tr>
                            </thead>
                            <tbody>

                            <tr ng-if="products.length == 0">
                                <td colspan="8" class="text-center">ไม่มีข้อมูล</td>
                            </tr>

                            <tr ng-repeat="product in products">
                                <td><% $index+1 %></td>
                                <td ><span data-toggle="tooltip" title="<% product.description %>"><% product.product_name %></span></td>
                                <td><% product.brand %></td>
                                <td><% product.model %></td>
                                <td><% product.sn %></td>
                                <td><% product.count %></td>
                                <td>
                                    <button type="button" class="btn waves-effect waves-light btn-xs btn-danger" ng-click="removeFromList($index)"><i class="mdi mdi-delete"></i> ลบ</button>
                                </td>
                            </tr>

                            </tbody>
                        </table>
                    </div>

                </div>
            </div>
        </div>



        <div class="col-md-12">
            <div class="card card-outline-inverse">
                <div class="card-body text-center">
                    <button type="submit" class="btn btn-success waves-effect waves-light m-r-10" ng-click="submitData()" ng-disabled="isSubmit"><i class="mdi mdi-content-save"></i> บันทึก</button>
                    <button type="button" class="btn btn-inverse waves-effect waves-light" ng-click="clearList()"><i class="mdi mdi-delete-empty"></i> เคลียร์รายการ</button>
                </div>
            </div>
        </div>

    </form>




    <div class="modal fade" tabindex="-1" id="modal_sn" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true" style="display: none;">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title" id="myLargeModalLabel">กรอก IMEI/SN</h4>
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                </div>
                <div class="modal-body">
                    <div class="input-group">
                        <div class="input-group-addon"><i class="mdi mdi-barcode"></i></div>
                        <input type="text" class="form-control" name="imei_sn_input" id="imei_sn_input" placeholder="กรุณากรอก IMEI/SN" ng-model="imei_sn_input" ng-keyup="$event.keyCode == 13 && enterSN()">
                        <span class="input-group-btn">
                                <button ng-click="enterSN()"   class="btn waves-effect waves-light btn-success">ตกลง</button>
                            </span>
                    </div>
                </div>
            </div>
            <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
    </div>

    <div class="modal fade" tabindex="-1" id="modal_amount_barcode" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true" style="display: none;">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title" id="myLargeModalLabel">รับเข้าจำนวนมาก</h4>
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                </div>
                <div class="modal-body">
                    <div class="input-group">
                        <div class="input-group-addon"><i class="mdi mdi-numeric"></i></div>
                        <input type="text" class="form-control col-md-2" name="amount_input" id="amount_input" ng-model="amount_input" placeholder="จำนวน" ng-keyup="$event.keyCode == 13 && checkAmountInput()">
                        <div class="input-group-addon"><i class="mdi mdi-barcode"></i></div>
                        <input type="text" class="form-control" name="amount_barcode_input" id="amount_barcode_input" ng-model="amount_barcode_input" placeholder="บาร์โค้ด" ng-keyup="$event.keyCode == 13 && enterBarcodeWithAmount()">
                        <span class="input-group-btn">
                                <button ng-click="enterBarcodeWithAmount()"   class="btn waves-effect waves-light btn-success">ตกลง</button>
                        </span>
                    </div>
                </div>
            </div>
            <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
    </div>

    <div class="modal fade" tabindex="-1" id="modal_search_product" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true" style="display: none;">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header">
                    <div class="input-group">
                        <span class="input-group-addon" id="basic-addon1"><i class="mdi mdi-magnify"></i></span>
                        <input type="text" class="form-control col-md-6" placeholder="รหัสสินค้า,ชื่อสินค้า,ยี่ห้อ,รุ่น,ราคา" aria-describedby="basic-addon1" id="search_product_input" ng-model="search_product_input" ng-keyup="$event.keyCode == 13 && searchProduct()">
                        <span class="input-group-btn">
                            <button type="button" class="btn waves-effect waves-light btn-success" ng-click="searchProduct()">ตกลง</button>
                        </span>
                        <button type="button" class="btn waves-effect waves-light btn-secondary btn-inverse m-l-10" ng-click="clearSearchResult()"><i class="mdi mdi-delete-empty"></i> เคลียร์การค้นหา</button>
                    </div>


                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                </div>
                <div class="modal-body">
                    <table class="table-bordered table-hover table" id="search_result_table">
                        <thead>
                        <tr>
                            <th>รหัสสินค้า</th>
                            <th>ชื่อสินค้า</th>
                            <th>ยี่ห้อ</th>
                            <th>รุ่น</th>
                            <th>ราคา</th>
                            <th></th>
                        </tr>
                        </thead>
                        <tbody>
                            <tr ng-repeat="product in search_products">
                                <td><% pad(product.id,6) %></td>
                                <td><% product.product_name %></td>
                                <td><% product.brand_name %></td>
                                <td><% product.model %></td>
                                <td><% number_format(product.price) %></td>
                                <td>
                                    <input value='0' type="text" style="width: 40px;text-align: center;" id="search_amount_input_<% $index %>" ng-model="search_amount_input[$index]"  ng-init="search_amount_input[$index] = 1" ng-if="product.type_sn == 0">
                                    <button  class="btn waves-effect waves-light btn-xs btn-info" ng-click="addItemToListBySearch(product,search_amount_input[$index])"><i class="mdi mdi-plus"></i> เพิ่ม</button>
                                </td>
                            </tr>
                            <tr ng-if="search_products.length == 0">
                                <td colspan="6" style="text-align: center">ไม่มีข้อมูล</td>
                            </tr>
                        </tbody>
                    </table>
                </div>
            </div>
            <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
    </div>


</div>
<!-- ============================================================== -->
<!-- End PAge Content -->
<!-- ============================================================== -->
@endsection

@section('js-head')
    <script src="/js/angular/controller/stock-in.js"></script>
@endsection

@section('js-bottom')



    <script src="/assets/plugins/toast-master/js/jquery.toast.js"></script>

    @include('template.flash-msg');


    <script>
        source_branch = {{Auth::user()->branch_id}};

        $(document).ready(function () {
            $('#barcode_input').focus();

            $(window).keydown(function(event){
                if(event.keyCode == 13) {
                    event.preventDefault();
                    return false;
                }
            });

            //$('#search_result_table').DataTable();
        });
    </script>

@endsection



@section('css-head')
    <link rel="stylesheet" href="/assets/plugins/dropify/dist/css/dropify.min.css">
    <link href="/assets/plugins/bootstrap/css/bootstrap.min.css" rel="stylesheet">
    <!-- toast CSS -->
    <link href="/assets/plugins/toast-master/css/jquery.toast.css" rel="stylesheet">
    <!-- Custom CSS -->
    <link href="/css/style.css" rel="stylesheet">
    <!-- You can change the theme colors from here -->
    <link href="/css/colors/blue.css" id="theme" rel="stylesheet">


@endsection