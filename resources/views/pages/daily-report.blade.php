@extends('app')
@section('page-header','รายงานประจำวัน')
@section('page-header-right')
    <li class="breadcrumb-item"><a href="javascript:void(0)">หน้าหลัก</a></li>
    <li class="breadcrumb-item">รายงาน</li>
    <li class="breadcrumb-item active">รายงานประจำวัน</li>
@endsection

@section('content')

    <div class="row">
        <div class="col-md-12">
            <div class="card card-body printableArea">
                <style>
                    h1,h2,h3,h4,h5,h6 {
                        color: black !important;
                    }

                    body {
                        color: black !important;
                    }
                    table td {
                        padding-top:0.4rem !important;
                        padding-bottom: 0.4rem !important;
                    }
                </style>
                <h2><b>รายงานประจำวัน</b> <span class="pull-right" style="font-size: 0.7em">วันที่: {{$date}}</span></h2>
                <div class="row">
                    <div class="col-md-12">
                        <div class="pull-left">
                            <address>
                                <h3>สาขา : {{$branch_name}}</h3>
                                <p class=""><b>จำนวน :</b> {{$invoice_amount}} บิล</p>
                            </address>
                        </div>

                    </div>
                    <div class="col-md-12">
                        <div class="table-responsive" style="clear: both;">
                            <table class="table table-hover" style="font-size: 0.7em">
                                <thead>
                                <tr>
                                    <th class="text-center">#</th>
                                    <th>รายการ</th>
                                    <th class="text-right" width="100px">รายรับ</th>
                                    <th class="text-right" width="100px">รายจ่าย</th>
                                </tr>
                                </thead>
                                <tbody>
                                    @foreach($invoices as $index => $invoice)
                                        <tr>
                                            <td class="text-center">{{$index+1}}</td>
                                            <td>
                                                @if($invoice->status == 4) <span class="badge badge-danger">ยกเลิก</span> @endif
                                                {{$invoice->getReferenceId()}} |
                                                @foreach($invoice->detail as $index => $inv_detail)
                                                    @if(!$inv_detail->free_gift)
                                                        @if($index != 0) , @endif
                                                        @if($inv_detail->product_id != null)
                                                            {{$inv_detail->productInfo->product_name}} {{$inv_detail->productInfo->brand->brand_name}} {{$inv_detail->productInfo->model}}
                                                        @else
                                                            {{$inv_detail->serviceInfo->service_name}}
                                                        @endif
                                                    @endif
                                                @endforeach
                                                @if($invoice->status == 4) **{{$invoice->getInvoiceCancelReason->reason}} @endif
                                            </td>
                                            <td class="text-right">@if($invoice->status != 4) {{number_format($invoice->getTotalPrice())}} @endif</td>
                                            <td class="text-right"></td>
                                        </tr>
                                    @endforeach

                                    @if($total_topup > 0)
                                    <tr>
                                        <td class="text-center"></td>
                                        <td>เติมเงิน</td>
                                        <td class="text-right">{{number_format($total_topup)}}</td>
                                        <td class="text-right"></td>
                                    </tr>
                                    @endif

                                    @if($total_mpay > 0)
                                    <tr>
                                        <td class="text-center"></td>
                                        <td>mPay (จ่ายบิล: {{$mpay_bill_amount}})</td>
                                        <td class="text-right">{{number_format($total_mpay)}}</td>
                                        <td class="text-right"></td>
                                    </tr>
                                    @endif

                                <tr>
                                    <td class="text-center"></td>
                                    <td class="text-right"><h4>รวม</h4></td>
                                    <td class="text-right"><h4>{{$total_income}}</h4></td>
                                    <td class="text-right"><h4>{{$total_outcome}}</h4></td>
                                </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>
                    <div class="col-md-12">
                        <div class="pull-right m-t-30 text-right m-r-20" style="min-width: 150px">

                            <hr>
                            <h3 style="text-align: center;"><b>ผู้ตรวจ</b></h3>
                        </div>
                        <div class="pull-right m-t-30 text-right m-r-20" style="min-width: 150px;">

                            <hr>
                            <h3 style="text-align: center;"><b>ผู้สรุป :</b> {{\Illuminate\Support\Facades\Auth::user()->name}}</h3>
                        </div>
                        <div class="clearfix"></div>


                    </div>
                </div>
            </div>
        </div>

        <div class="col-md-12">
            <div class="card card-body">
                <div class="row">
                    <div class="col-md-12" style="text-align: center">
                        <button id="print" class="btn btn-default" type="button"> <span><i class="fa fa-print"></i> พิมพ์</span> </button>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('js-bottom')
    <script src="/js/jquery.PrintArea.js" type="text/JavaScript"></script>
    <script>
        $(document).ready(function() {
            $("#print").click(function() {
                var mode = 'iframe'; //popup
                var close = mode == "popup";
                var options = {
                    mode: mode,
                    popClose: close
                };
                $("div.printableArea").printArea(options);
            });
        });
    </script>
@endsection

@section('css-head')

@endsection