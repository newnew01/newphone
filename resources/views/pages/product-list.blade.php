@extends('app')
@section('page-header','รายการสินค้า')
@section('page-header-right')
    <li class="breadcrumb-item"><a href="javascript:void(0)">หน้าหลัก</a></li>
    <li class="breadcrumb-item">สินค้า</li>
    <li class="breadcrumb-item active">รายการสินค้า</li>
@endsection

@section('content')
<!-- ============================================================== -->
<!-- Start Page Content -->
<!-- ============================================================== -->
<div class="row">
    <div class="col-12">
        <div class="card card-outline-inverse" ng-controller="ProductListController">
            <div class="card-header">
                <a  href="/product/new" class="btn btn-sm waves-effect waves-light btn-rounded btn-success pull-right"><i class="mdi mdi-library-plus"></i> เพิ่มสินค้า</a>
                <h4 class="m-b-0 text-white">รายการสินค้า</h4>
            </div>
            <div class="card-body">
                <table class="table-bordered table-hover table" id="productTable">
                    <thead>
                    <tr>
                        <th>รหัสสินค้า</th>
                        <th>ชื่อสินค้า</th>
                        <th>ยี่ห้อ</th>
                        <th>รุ่น</th>
                        <th>ราคา</th>
                        @can('view-cost')
                            <th>ราคาทุน</th>
                        @endcan
                        <th width="150px">คงเหลือ</th>
                        <th></th>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach($products as $product)
                        <tr>
                            <td>{{sprintf('%06d',$product->id)}}</td>
                            <td>{{$product->product_name}}</td>
                            <td>{{$product->brand->brand_name}}</td>
                            <td>{{$product->model}}</td>
                            <td>{{number_format($product->price)}}</td>
                            @can('view-cost')
                                <td>{{number_format($product->cost)}}</td>
                            @endcan
                            <td>
                                @if($product->amount > 0)
                                    <span class="text-success">{{$product->amount}}</span>
                                @else
                                    <span class="text-danger">{{$product->amount}}</span>
                                @endif

                                 ({{$product->total_amount}}) <button class="btn m-l-10 waves-effect waves-light btn-xs btn-default" ng-click="viewStock({{$product->id}})"><i class="mdi mdi-eye"></i> ดู</button></td>
                            <td>
                                <button type="button" class="btn waves-effect waves-light btn-xs btn-primary" ng-click="viewDetail({{$product->id}})">รายละเอียด</button>
                                @can('edit-product')
                                    <a  href="/product/edit/{{$product->id}}" class="btn waves-effect waves-light btn-xs btn-info">แก้ไข</a>
                                @endcan

                                @can('delete-product')
                                    <a href="/product/delete/{{$product->id}}" class="btn waves-effect waves-light btn-xs btn-danger" onclick="return confirm('ต้องการลบใช่หรือไม่?');">ลบ</a>
                                @endcan
                            </td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
            </div>


            <div id="viewImageModal" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                <div class="modal-dialog modal-lg">
                    <div class="modal-content">
                        <div class="modal-header">
                            <h4 class="modal-title" id="myModalLabel"><% product_modal.product_name %></h4>
                            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                        </div>
                        <div class="modal-body">
                            <div class="row">
                                <div class="col-md-6">
                                    <img ng-src="<% product_modal.image %>" src="/assets/images/no-image.png" width="100%">
                                </div>

                                <div class="col-md-6">
                                    <table class="table">
                                        <tr>
                                            <td class="font-bold">รหัสสินค้า</td>
                                            <td><% pad(product_modal.id,6) %></td>
                                        </tr>
                                        <tr>
                                            <td class="font-bold">ชื่อสินค้า</td>
                                            <td><% product_modal.product_name %></td>
                                        </tr>
                                        <tr>
                                            <td class="font-bold">ยี่ห้อ</td>
                                            <td><% product_modal.brand_name %></td>
                                        </tr>
                                        <tr>
                                            <td class="font-bold">รุ่น</td>
                                            <td><% product_modal.model %></td>
                                        </tr>
                                        <tr>
                                            <td class="font-bold">หมวดหมู่</td>
                                            <td><% product_modal.cate_name %></td>
                                        </tr>
                                        <tr>
                                            <td class="font-bold">ราคา</td>
                                            <td><% number_format(product_modal.price) %> บาท</td>
                                        </tr>
                                        <tr>
                                            <td class="font-bold">บาร์โค้ด</td>
                                            <td><% product_modal.barcode %></td>
                                        </tr>
                                        <tr>
                                            <td class="font-bold">คำอธิบาย</td>
                                            <td><% product_modal.description %></td>
                                        </tr>
                                    </table>
                                </div>
                            </div>

                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-info waves-effect btn-block" data-dismiss="modal">ปิด</button>
                        </div>
                    </div>
                    <!-- /.modal-content -->
                </div>
                <!-- /.modal-dialog -->
            </div>

            <div id="viewStockModal" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                <div class="modal-dialog modal-lg">
                    <div class="modal-content">
                        <div class="modal-header">
                            <h4 class="modal-title" id="myModalLabel">สินค้าคงเหลือ | <% product_stock.product_info.product_name %> <% product_stock.product_info.brand_name %> <% product_stock.product_info.model %></h4>
                            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                        </div>
                        <div class="modal-body">
                            <div class="row">
                                <div class="col-lg-12">
                                    <table class="table table-bordered">
                                        <thead>
                                        <tr>
                                            <th width="50px" style="text-align: center">#</th>
                                            <th width="200px">สาขา</th>
                                            <th width="100px" style="text-align: center">คงเหลือ</th>
                                            <th>IMEI/SN</th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        <tr ng-repeat="p_stock in product_stock.product_data">
                                            <td style="text-align: center"><% $index+1 %></td>
                                            <td><% p_stock.branch_name %> <i class="mdi mdi-briefcase-check" ng-if="p_stock.branch_id == source_branch" style="color: green"></i></td>
                                            <td style="text-align: center"><% p_stock.amount %></td>
                                            <td><% p_stock.sn %></td>
                                        </tr>

                                        <tr ng-repeat="p_transferring in product_transferring">
                                            <td style="text-align: center"></td>
                                            <td><% p_transferring.source_branch_name %> > <% p_transferring.dest_branch_name %></td>
                                            <td style="text-align: center"><% p_transferring.amount %></td>
                                            <td><% p_transferring.sn %></td>
                                        </tr>

                                        <tr ng-if="product_stock.product_data.length == 0">
                                            <td colspan="4" class="text-center">ไม่มีข้อมูล</td>
                                        </tr>
                                        </tbody>
                                    </table>
                                </div>



                            </div>

                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-info waves-effect btn-block" data-dismiss="modal">ปิด</button>
                        </div>
                    </div>
                    <!-- /.modal-content -->
                </div>
                <!-- /.modal-dialog -->
            </div>
        </div>
    </div>



</div>


<!-- ============================================================== -->
<!-- End PAge Content -->
<!-- ============================================================== -->
@endsection

@section('js-head')
    <script src="/js/angular/controller/product-list.js"></script>
@endsection

@section('js-bottom')
    <!-- jQuery peity -->
    <script src="/assets/plugins/tablesaw-master/dist/tablesaw.js"></script>
    <script src="/assets/plugins/tablesaw-master/dist/tablesaw-init.js"></script>

    <script src="/assets/plugins/toast-master/js/jquery.toast.js"></script>

    <!-- This is data table -->
    <script src="/assets/plugins/datatables/media/js/jquery.dataTables.js"></script>


    <script>
        source_branch = {{Auth::user()->branch_id}};

        $(document).ready(function() {
            $('#productTable').DataTable();
        });
    </script>


    @include('template.flash-msg');
@endsection

@section('css-head')
    <!-- Bootstrap responsive table CSS -->
    <link href="/assets/plugins/tablesaw-master/dist/tablesaw.css" rel="stylesheet">

    <!-- toast CSS -->
    <link href="/assets/plugins/toast-master/css/jquery.toast.css" rel="stylesheet">
    <!-- Custom CSS -->
    <link href="/css/style.css" rel="stylesheet">
    <!-- You can change the theme colors from here -->
    <link href="/css/colors/blue.css" id="theme" rel="stylesheet">
@endsection