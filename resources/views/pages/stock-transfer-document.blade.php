@extends('app')
@section('page-header','โอนสินค้า')
@section('page-header-right')
    <li class="breadcrumb-item"><a href="javascript:void(0)">หน้าหลัก</a></li>
    <li class="breadcrumb-item">สต๊อคสินค้า</li>
    <li class="breadcrumb-item active">โอนสินค้า</li>
@endsection

@section('content')
    <!-- ============================================================== -->
    <!-- Start Page Content -->
    <!-- ============================================================== -->
    <div class="row">
        <div class="col-md-12">
            <div class="card card-body printableArea">
                <style>
                    h1,h2,h3,h4,h5,h6 {
                        color: black !important;
                    }

                    body {
                        color: black !important;
                    }
                    .printableArea .table td,.printableArea .table th{
                        padding-left: .2rem !important;
                        padding-right: .2rem !important;
                        font-size: 0.7em;

                    }
                    .printableArea .table td{
                        padding-top: .5rem !important;
                        padding-bottom: .5rem !important;
                    }
                </style>
                <h3><b>ใบโอนสินค้า</b> - {{$stock_reference->sourceBranch->branch_name}}<span class="pull-right">เลขที่อ้างอิง: {{$stock_reference->type->prefix}}-{{sprintf('%06d', $stock_reference->id)}}</span></h3>
                <div class="row">
                    <div class="col-md-12">
                        <div class="pull-left">
                            <address>
                                <p class="text-muted m-l-5"><b>วันที่ :</b> <i class="fa fa-calendar"></i> {{ Carbon\Carbon::parse($stock_reference->created_at)->format('d-m-Y') }}</p>
                                <p class="text-muted m-l-5 m-b-0"><b>สาขาต้นทาง :</b> {{ $stock_reference->sourceBranch->branch_name }}</p>
                                <p class="text-muted m-l-5"><b>โอนไปสาขา :</b> {{ $stock_reference->destinationBranch->branch_name }}</p>

                            </address>
                        </div>
                        <div class="pull-right text-right">
                            <address>
                                <img src="data:image/png;base64,{{$stock_reference->generateBarcode()}}" alt="barcode">
                            </address>
                        </div>
                    </div>
                    <div class="col-md-12">
                        <div class="table-responsive m-t-40" style="clear: both;">
                            <table class="table table-hover">
                                <thead>
                                <tr>
                                    <th class="text-center"><b>#</b></th>
                                    <th class="text-right" width="70px"><b>รหัสสินค้า</b></th>
                                    <th><b>สินค้า</b></th>
                                    <th class="text-right"><b>จำนวน</b></th>
                                    <th class="text-right"><b>SN/IMEI</b></th>

                                </tr>
                                </thead>
                                <tbody>
                                @foreach($stock_reference->detail as $index => $s )
                                <tr>
                                    <td class="text-center">{{$index+1}}</td>
                                    <td class="text-right">{{sprintf('%06d', $s->productInfo->id)}}</td>
                                    <td>{{$s->productInfo->product_name}}
                                        @if($s->productInfo->type_sn == 1)
                                            {{$s->productInfo->brand->brand_name}}
                                        @endif
                                        {{$s->productInfo->model}}
                                        @if($s->ais_deal == 1)
                                            (AIS Deal)
                                        @endif
                                    </td>
                                    <td class="text-right">{{$s->amount}} </td>
                                    <td class="text-right">{{$s->sn}}</td>
                                </tr>
                                @endforeach

                                </tbody>
                            </table>
                        </div>
                    </div>
                    <div class="col-md-12">
                        <div class="pull-right m-t-30 text-right" style="min-width: 150px;">

                            <hr>
                            <h3 style="text-align: center;"><b>ผู้รับสินค้า</b> </h3>
                        </div>
                        <div class="pull-right m-t-30 text-right m-r-20" style="min-width: 150px">

                            <hr>
                            <h3 style="text-align: center;"><b>ผู้ส่งสินค้า</b></h3>
                        </div>
                        <div class="pull-right m-t-30 text-right m-r-20" style="min-width: 150px;">

                            <hr>
                            <h3 style="text-align: center;"><b>ผู้โอน :</b> {{$stock_reference->user->name}}</h3>
                        </div>
                        <div class="clearfix"></div>


                    </div>
                </div>
            </div>
        </div>
        <div class="col-md-12">
            <div class="card card-body">
                <div class="row">
                    <div class="col-md-12" style="text-align: center">
                        <a href="/stock/list" class="btn btn-info"> ย้อนกลับ </a>
                        <button id="print" class="btn btn-default" type="button"> <span><i class="fa fa-print"></i> พิมพ์</span> </button>

                        @can('stock-transfer-confirm')
                            @if($stock_reference->status_id == 3)
                                @if($stock_reference->destination_branch == \Illuminate\Support\Facades\Auth::user()->branch_id || Gate::check('stock-transfer-confirm-all'))
                                    <a class="btn btn-warning" href="/stock/confirm-transfer/{{$stock_reference->id}}" onclick="return confirm('ยืนยันการรับเข้าสินค้า?')"> <span>ยืนยันรับเข้า</span> </a>
                                @endif
                            @endif
                        @endcan
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- ============================================================== -->
    <!-- End PAge Content -->
    <!-- ============================================================== -->
@endsection

@section('js-bottom')
    <script src="/assets/plugins/toast-master/js/jquery.toast.js"></script>
    @include('template.flash-msg');
    <script src="/js/jquery.PrintArea.js" type="text/JavaScript"></script>
    <script>
        $(document).ready(function() {
            $("#print").click(function() {
                var mode = 'iframe'; //popup
                var close = mode == "popup";
                var options = {
                    mode: mode,
                    popClose: close
                };
                $("div.printableArea").printArea(options);
            });
        });
    </script>
@endsection

@section('css-head')

    <!-- toast CSS -->
    <link href="/assets/plugins/toast-master/css/jquery.toast.css" rel="stylesheet">
    <!-- Custom CSS -->
    <link href="/css/style.css" rel="stylesheet">
    <!-- You can change the theme colors from here -->
    <link href="/css/colors/blue.css" id="theme" rel="stylesheet">

@endsection
