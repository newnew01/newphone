@extends('app')
@section('page-header','พิมพ์ลาเบลสินค้า')
@section('page-header-right')
    <li class="breadcrumb-item"><a href="javascript:void(0)">หน้าหลัก</a></li>
    <li class="breadcrumb-item">ลาเบลสินค้า</li>
    <li class="breadcrumb-item active">พิมพ์ลาเบลสินค้า</li>
@endsection

@section('content')
<!-- ============================================================== -->
<!-- Start Page Content -->
<!-- ============================================================== -->
<div class="row">
    <div class="col-12">
        <div class="card card-outline-inverse" ng-controller="PrintLabelController">
            <div class="card-header">
                <h4 class="m-b-0 text-white">รายการสินค้า</h4>
            </div>
            <div class="card-body">
                <table class="table-bordered table-hover table" id="productTable">
                    <thead>
                    <tr>
                        <th>รหัสสินค้า</th>
                        <th>ชื่อสินค้า</th>
                        <th>ยี่ห้อ</th>
                        <th>รุ่น</th>
                        <th>ราคา</th>
                        <th></th>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach($products as $product)
                        <tr>
                            <td>{{sprintf('%06d',$product->id)}}</td>
                            <td>{{$product->product_name}}</td>
                            <td>{{$product->brand->brand_name}}</td>
                            <td>{{$product->model}}</td>
                            <td>{{number_format($product->price)}}</td>
                            <td>
                                <button type="button" class="btn waves-effect waves-light btn-xs btn-primary" ng-click="viewDetail({{$product->id}})">รายละเอียด</button>
                                <button type="button" class="btn waves-effect waves-light btn-xs btn-info" ng-click="viewPrintOption({{$product->id}})">พิมพ์</button>
                            </td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
            </div>


            <div id="viewImageModal" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                <div class="modal-dialog modal-lg">
                    <div class="modal-content">
                        <div class="modal-header">
                            <h4 class="modal-title" id="myModalLabel"><% product_modal.product_name %></h4>
                            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                        </div>
                        <div class="modal-body">
                            <div class="row">
                                <div class="col-md-6">
                                    <img ng-src="<% product_modal.image %>" src="/assets/images/no-image.png" width="100%">
                                </div>

                                <div class="col-md-6">
                                    <table class="table">
                                        <tr>
                                            <td class="font-bold">รหัสสินค้า</td>
                                            <td><% pad(product_modal.id,6) %></td>
                                        </tr>
                                        <tr>
                                            <td class="font-bold">ชื่อสินค้า</td>
                                            <td><% product_modal.product_name %></td>
                                        </tr>
                                        <tr>
                                            <td class="font-bold">ยี่ห้อ</td>
                                            <td><% product_modal.brand_name %></td>
                                        </tr>
                                        <tr>
                                            <td class="font-bold">รุ่น</td>
                                            <td><% product_modal.model %></td>
                                        </tr>
                                        <tr>
                                            <td class="font-bold">หมวดหมู่</td>
                                            <td><% product_modal.cate_name %></td>
                                        </tr>
                                        <tr>
                                            <td class="font-bold">ราคา</td>
                                            <td><% number_format(product_modal.price) %> บาท</td>
                                        </tr>
                                        <tr>
                                            <td class="font-bold">บาร์โค้ด</td>
                                            <td><% product_modal.barcode %></td>
                                        </tr>
                                        <tr>
                                            <td class="font-bold">คำอธิบาย</td>
                                            <td><% product_modal.description %></td>
                                        </tr>
                                    </table>
                                </div>
                            </div>

                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-info waves-effect btn-block" data-dismiss="modal">ปิด</button>
                        </div>
                    </div>
                    <!-- /.modal-content -->
                </div>
                <!-- /.modal-dialog -->
            </div>

            <div id="viewPrintOptionModal" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                <div class="modal-dialog">
                    <div class="modal-content">
                        <div class="modal-header">
                            <h4 class="modal-title" id="myModalLabel">พิมพ์ลาเบลสินค้า</h4>
                            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                        </div>
                        <div class="modal-body">
                            <div class="row">
                                <div class="col-xl-12">

                                    <div class="form-group">
                                        <label>เลือกเซอร์วิสการพิมพ์</label>
                                        <select class="form-control custom-select" tabindex="1" name="print_service" ng-model="print_service">
                                            <option value="">[เซอร์วิสการพิมพ์]</option>
                                            <option value="<% print_svc.service_uri %>" ng-repeat="print_svc in print_services" ng-disabled="print_svc.service_status != 1">
                                                <% print_svc.service_name %> - <% print_svc.service_status == 0 ? '[เครื่องพิมพ์ปิด]':(print_svc.service_status == 1 ? '[ใช้งานได้]':(print_svc.service_status == 2 ? '[เกิดข้อผิดพลาด]':(print_svc.service_status == 3 ? '[ไม่สามารถเชื่อมต่อได้]':'[unknown]'))) %>
                                            </option>
                                        </select>
                                    </div>


                                    <div class="form-group">
                                        <label>จำนวน</label>
                                        <input type="text" class="form-control form-control-line" value="1" ng-model="print_quantity"> </div>
                                </div>
                            </div>

                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-info waves-effect" data-dismiss="modal" ng-click="printLabel()"><i class="mdi mdi-printer"></i> พิมพ์</button>


                            <button type="button" class="btn btn-default waves-effect" data-dismiss="modal">ปิด</button>
                            <div class="row">



                            </div>

                        </div>
                    </div>
                    <!-- /.modal-content -->
                </div>
                <!-- /.modal-dialog -->
            </div>
        </div>
    </div>



</div>


<!-- ============================================================== -->
<!-- End PAge Content -->
<!-- ============================================================== -->
@endsection

@section('js-head')
    <script src="/js/angular/controller/print-label.js"></script>
@endsection

@section('js-bottom')
    <!-- jQuery peity -->
    <script src="/assets/plugins/tablesaw-master/dist/tablesaw.js"></script>
    <script src="/assets/plugins/tablesaw-master/dist/tablesaw-init.js"></script>

    <script src="/assets/plugins/toast-master/js/jquery.toast.js"></script>

    <!-- This is data table -->
    <script src="/assets/plugins/datatables/media/js/jquery.dataTables.js"></script>


    <script>
        $(document).ready(function() {
            $('#productTable').DataTable();
        });
    </script>


    @include('template.flash-msg');
@endsection

@section('css-head')
    <!-- Bootstrap responsive table CSS -->
    <link href="/assets/plugins/tablesaw-master/dist/tablesaw.css" rel="stylesheet">

    <!-- toast CSS -->
    <link href="/assets/plugins/toast-master/css/jquery.toast.css" rel="stylesheet">
    <!-- Custom CSS -->
    <link href="/css/style.css" rel="stylesheet">
    <!-- You can change the theme colors from here -->
    <link href="/css/colors/blue.css" id="theme" rel="stylesheet">
@endsection