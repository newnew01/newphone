@extends('app')
@section('page-header','รายการขาย')
@section('page-header-right')
    <li class="breadcrumb-item"><a href="javascript:void(0)">หน้าหลัก</a></li>
    <li class="breadcrumb-item">การขาย</li>
    <li class="breadcrumb-item active">รายการขาย</li>
@endsection

@section('content')
<!-- ============================================================== -->
<!-- Start Page Content -->
<!-- ============================================================== -->
<div class="row">
    <div class="col-12">
        <div class="card card-outline-inverse">
            <div class="card-header">
                <button type="button" class="btn btn-sm waves-effect waves-light btn-rounded btn-success pull-right"><i class="mdi mdi-library-plus"></i> ขายใหม่</button>
                <h4 class="m-b-0 text-white">รายการ</h4>
            </div>
            <div class="card-body">
                <table class="table-bordered table-hover table" id="saleTable">
                    <thead>
                    <tr>
                        <th data-tablesaw-sortable-default-col class="tablesaw-sortable-descending">เลขที่ใบเสร็จ</th>
                        <th>ชื่อลูกค้า</th>
                        <th>สาขา</th>
                        <th>ยอดรวม</th>
                        <th>วันที่</th>
                        <th>สถานะ</th>
                        <th></th>
                    </tr>
                    </thead>
                    <tbody>

                    @foreach($invoices as $index=>$invoice)
                    <tr>
                        <td>INV-{{sprintf('%06d',$invoice->id)}}</td>
                        <td class="title"><a class="link" href="javascript:void(0)">{{$invoice->customer_fname}}  {{$invoice->customer_lname}}</a> @if($invoice->customer_fname == null) ***ไม่ระบุข้อมูลผู้ซื้อ @endif</td>
                        <td>{{$invoice->branch->branch_name}}</td>
                        <td>{{number_format($invoice->getTotalPrice())}}</td>
                        <td>{{$invoice->created_at->format('d-m-Y')}}</td>
                        <td><span class="badge {{$invoice->status == 1 ? 'badge-success':($invoice->status == 4 ? 'badge-danger':'')}}">{{$invoice->getStatus->status_name}}</span></td>
                        <td>
                            <a href="/sale/invoice/INV-{{sprintf('%06d',$invoice->id)}}" class="btn waves-effect waves-light btn-xs btn-info">ดูใบเสร็จ</a>
                        </td>
                    </tr>
                    @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>
<!-- ============================================================== -->
<!-- End PAge Content -->
<!-- ============================================================== -->
@endsection

@section('js-bottom')
    <!-- jQuery peity -->
    <script src="/assets/plugins/tablesaw-master/dist/tablesaw.js"></script>
    <script src="/assets/plugins/tablesaw-master/dist/tablesaw-init.js"></script>
    <!-- This is data table -->
    <script src="/assets/plugins/datatables/media/js/jquery.dataTables.js"></script>


    <script>
        $(document).ready(function() {
            $('#saleTable').DataTable({
                "order": [0,'desc']
            });
        });
    </script>
@endsection

@section('css-head')
    <!-- Bootstrap responsive table CSS -->
    <link href="../assets/plugins/tablesaw-master/dist/tablesaw.css" rel="stylesheet">
@endsection