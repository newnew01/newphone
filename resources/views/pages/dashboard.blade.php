@extends('app')
@section('page-header','หน้าหลัก - '.\Illuminate\Support\Facades\Auth::user()->branch->branch_name)

@section('content')
<!-- ============================================================== -->
<!-- Start Page Content -->
<!-- ============================================================== -->


<div class="row">

    <div class="col-lg-8 col-xlg-9">
        <div class="card">
            <div class="card-body">
                <div class="row">
                    <div class="col-12">
                        <div class="d-flex flex-wrap">
                            <div>
                                <h4 class="card-title">กราฟสถิติ</h4>
                            </div>
                            <div class="ml-auto">
                                <ul class="list-inline">
                                    <li>
                                        <h6 class="text-muted  text-info"><i class="fa fa-circle font-10 m-r-10"></i>ยอดขาย</h6>
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </div>
                    <div class="col-12">
                        <div id="dashboard_chart" style="height: 355px;"></div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="col-lg-3">
        <div class="card">
            <div class="card-body">
                <h4 class="card-title">ยอดขายเดือนนี้</h4>
                <div class="d-flex">
                    <div class="align-self-center">
                        <h4 class="font-medium m-b-0"><i class="mdi mdi-cash-multiple text-success"></i> {{number_format($statistics->getThisMonthSale($branch_id))}} บาท</h4></div>
                    <div class="ml-auto">
                        <div id="spark8"></div>
                    </div>
                </div>
            </div>
        </div>
        <div class="card">
            <div class="card-body">
                <h4 class="card-title">บิลขายเดือนนี้</h4>
                <div class="d-flex">
                    <div class="align-self-center">
                        <h4 class="font-medium m-b-0"><i class="mdi mdi-file-document text-danger"></i>  {{number_format($statistics->getThisMonthInvoiceAmount($branch_id))}} บิล</h4></div>
                    <div class="ml-auto">
                        <div id="spark9"></div>
                    </div>
                </div>
            </div>
        </div>
        <div class="card">
            <div class="card-body">
                <h4 class="card-title">ยอดเติมเงินเดือนนี้</h4>
                <div class="d-flex">
                    <div class="align-self-center">
                        <h4 class="font-medium m-b-0"><i class="mdi mdi-cellphone-iphone text-success"></i> {{number_format($statistics->getThisMonthTopup($branch_id))}} บาท</h4></div>
                    <div class="ml-auto">
                        <div id="spark10"></div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="card-group">
    <div class="card">
        <div class="card-body">
            <div class="row">
                <div class="col-12">
                    <h2 class="m-b-0"><i class="mdi mdi-cash-multiple text-info"></i></h2>
                    <h3 class="">{{number_format($statistics->getTodaySale($branch_id))}} บาท</h3>
                    <h6 class="card-subtitle">ยอดขาย (วันนี้)</h6></div>
                <div class="col-12">
                    <div class="progress">
                        <div class="progress-bar bg-info" role="progressbar" style="width: 85%; height: 6px;" aria-valuenow="25" aria-valuemin="0" aria-valuemax="100"></div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- Column -->
    <!-- Column -->
    <div class="card">
        <div class="card-body">
            <div class="row">
                <div class="col-12">
                    <h2 class="m-b-0"><i class="mdi mdi-file-document text-purple"></i></h2>
                    <h3 class="">{{number_format($statistics->getTodayInvoiceAmount($branch_id))}} บิล</h3>
                    <h6 class="card-subtitle">บิลขายใหม่ (วันนี้)</h6></div>
                <div class="col-12">
                    <div class="progress">
                        <div class="progress-bar bg-primary" role="progressbar" style="width: 56%; height: 6px;" aria-valuenow="25" aria-valuemin="0" aria-valuemax="100"></div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- Column -->
    <!-- Column -->
    <div class="card">
        <div class="card-body">
            <div class="row">
                <div class="col-12">
                    <h2 class="m-b-0"><i class="mdi mdi-cellphone-iphone text-success"></i></h2>
                    <h3 class="">{{number_format($statistics->getTodayTopup($branch_id))}} บาท</h3>
                    <h6 class="card-subtitle">ยอดเติมเงิน (วันนี้)</h6></div>
                <div class="col-12">
                    <div class="progress">
                        <div class="progress-bar bg-success" role="progressbar" style="width: 40%; height: 6px;" aria-valuenow="25" aria-valuemin="0" aria-valuemax="100"></div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- Column -->
    <!-- Column -->
    <div class="card">
        <div class="card-body">
            <div class="row">
                <div class="col-12">
                    <h2 class="m-b-0"><i class="mdi mdi-buffer text-warning"></i></h2>
                    <h3 class="">ไม่มีข้อมูล</h3>
                    <h6 class="card-subtitle">ค่าใช้จ่าย (วันนี้)</h6></div>
                <div class="col-12">
                    <div class="progress">
                        <div class="progress-bar bg-warning" role="progressbar" style="width: 26%; height: 6px;" aria-valuenow="25" aria-valuemin="0" aria-valuemax="100"></div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>


<!-- ============================================================== -->
<!-- End PAge Content -->
<!-- ============================================================== -->
@endsection

@section('js-bottom')
    <!--sparkline JavaScript -->
    <script src="/assets/plugins/sparkline/jquery.sparkline.min.js"></script>
    <!--morris JavaScript -->
    <script src="/assets/plugins/raphael/raphael-min.js"></script>
    <script src="/assets/plugins/morrisjs/morris.min.js"></script>

    <script>
        var branch_id = {{$branch_id}};
        function numberWithCommas(x) {
            return x.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");

        }

        $(document).ready(function() {
            var dashborad_chart = Morris.Area({
                element: 'dashboard_chart',
                xkey: 'date',
                ykeys: ['total_price'],
                labels: ['ยอดขาย'],
                pointSize: 3,
                fillOpacity: 0,
                pointStrokeColors: ['#1976d2', '#26c6da', '#1976d2'],
                behaveLikeLine: true,
                gridLineColor: '#e0e0e0',
                lineWidth: 3,
                hideHover: 'auto',
                lineColors: ['#1976d2', '#26c6da', '#1976d2'],
                resize: true,
                xLabelFormat: function (d) {
                    return ("0" + d.getDate()).slice(-2) + '/' + ("0" + (d.getMonth() + 1)).slice(-2) + '/' + d.getFullYear();
                },
                hoverCallback: function (index, options, content, row) {
                    var data = '';

                    data += '<div class="morris-hover-row-label">'+options.data[index].date+'</div>';
                    data += '<div class="morris-hover-point">ยอดขาย: '+numberWithCommas(options.data[index].total_price)+' บาท</div>';
                    data += '<div class="morris-hover-point">บิลขาย: '+numberWithCommas(options.data[index].invoice_count)+' บิล</div>';
                    data += '<div class="morris-hover-point">จำนวนสินค้า: '+numberWithCommas(options.data[index].product_sale_amount)+' ชิ้น</div>';

                    //data = row[index];

                    //return content;
                    return data;

                }

            });

            $.get( "/web-service/get-dashboard-statistic/"+branch_id, function( data ) {
                dashborad_chart.setData(data);
            });
        });
    </script>
@endsection

@section('css-head')
    <!-- morris CSS -->
    <link href="/assets/plugins/morrisjs/morris.css" rel="stylesheet">

@endsection

