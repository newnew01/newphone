@extends('app')
@section('page-header','กำหนดสิทธิ์')
@section('page-header-right')
    <li class="breadcrumb-item"><a href="javascript:void(0)">หน้าหลัก</a></li>
    <li class="breadcrumb-item">ผู้ใช้งาน</li>
    <li class="breadcrumb-item active">กำหนดสิทธิ์</li>
@endsection

@section('content')
    <div class="row">
        <form class="form" method="post" action="/user/permission/{{$user->id}}" id="userPermissionForm" style="width: 100%" autocomplete="off">
            {{csrf_field()}}
            <div class="col-md-12">
                <div class="card">
                    <div class="card-body">
                        <h4 class="card-title">ชื่อ: {{$user->name}} ({{$user->username}})</h4>
                        <div class="table-responsive">
                            <table class="table">
                                <thead>
                                <tr>
                                    <th width="350px">สิทธิ์</th>
                                    <th>คำอธิบาย</th>
                                </tr>
                                </thead>
                                <tbody>
                                @foreach($permissions as $permission)
                                    <tr>
                                        <td>
                                            <input type="checkbox" id="{{$permission->name}}" name="{{$permission->name}}" class="filled-in" @if($user->hasPermissionTo($permission->id)) checked @endif />
                                            <label class="m-b-0" for="{{$permission->name}}">{{$permission->name}}</label>
                                        </td>
                                        <td>{{$permission->description}}</td>
                                    </tr>
                                @endforeach

                                </tbody>
                            </table>
                        </div>

                        <div class="m-b-30">
                            <h5 class="box-title">สาขาที่สามารถจัดการได้ (ขวา)</h5>
                            <select id='user-permission-branch' name="user-permission-branch[]" multiple='multiple'>
                                @foreach($branches as $branch)
                                    <option value='{{$branch->id}}' @if($user->hasPermissionBranch($branch->id)) selected @endif>{{$branch->branch_name}}</option>
                                @endforeach


                            </select>
                        </div>
                        <div class="text-center">
                            <button type="submit" class="btn btn-success waves-effect waves-light m-r-10">บันทึก</button>
                            <a href="/user/list" class="btn btn-inverse waves-effect waves-light">ย้อนกลับ</a>
                        </div>

                    </div>
                </div>
            </div>
        </form>
    </div>
@endsection

@section('js-bottom')
    <script type="text/javascript" src="/assets/plugins/multiselect/js/jquery.multi-select.js"></script>
    <script>
        $(document).ready(function() {
            $('#user-permission-branch').multiSelect();
        });

    </script>

@endsection

@section('css-head')
    <link href="/assets/plugins/multiselect/css/multi-select.css" rel="stylesheet" type="text/css" />
    <style>
        h1,h2,h3,h4,h5,h6 {
            color: black !important;
        }

        body {
            color: black !important;
        }
    </style>
@endsection