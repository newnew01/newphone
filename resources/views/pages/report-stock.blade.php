@extends('app')
@section('page-header','รายงานสต๊อค')
@section('page-header-right')
    <li class="breadcrumb-item"><a href="javascript:void(0)">หน้าหลัก</a></li>
    <li class="breadcrumb-item">รายงาน</li>
    <li class="breadcrumb-item active">รายงานสต๊อค</li>
@endsection

@section('content')
<!-- ============================================================== -->
<!-- Start Page Content -->
<!-- ============================================================== -->
<div class="row">
    <div class="col-12">
        <div class="card card-outline-inverse">
            <div class="card-header">
                <h4 class="m-b-0 text-white">ส่งออกเป็นไฟล์ Excel</h4>
            </div>
            <div class="card-body">
                <form method="post" action="/report/stock">
                    {{csrf_field()}}
                    <div class="form-group row">
                        <label class="col-2 col-form-label">สาขา</label>
                        <div class="col-10">
                            @can('export-stock-all-branch')
                            <select class="custom-select col-12" name="branch_id">
                                <option selected="" value="0">[ทั้งหมด]</option>
                                @foreach($branches as $branch)
                                    <option value="{{$branch->id}}">{{$branch->branch_name}}</option>
                                @endforeach
                            </select>
                            @endcan
                            @cannot('export-stock-all-branch')
                                <label class="col-2 col-form-label">{{\Illuminate\Support\Facades\Auth::user()->branch->branch_name}}</label>

                                <input type="hidden" name="branch_id" value="{{\Illuminate\Support\Facades\Auth::user()->branch->id}}">
                            @endcannot
                        </div>
                    </div>

                    <div class="form-group row">
                        <label class="col-2 col-form-label">หมวดหมู่</label>
                        <div class="col-10">
                            <select class="custom-select col-12" name="category_id">
                                <option selected="" value="0">[ทั้งหมด]</option>
                                @foreach($categories as $category)
                                    <option value="{{$category->id}}">{{$category->cate_name}}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>

                    <div class="form-group row">
                        <label class="col-2 col-form-label">ยี่ห้อ</label>
                        <div class="col-10">
                            <select class="custom-select col-12" name="brand_id">
                                <option selected="" value="0">[ทั้งหมด]</option>
                                @foreach($brands as $brand)
                                    <option value="{{$brand->id}}">{{$brand->brand_name}}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>

                    <input type="checkbox" id="option_show_imei" name="option_show_imei" class="filled-in">
                    <label for="option_show_imei" class="m-r-20">แสดง IMEI</label>

                    <input type="checkbox" id="option_show_zero_amount" name="option_show_zero_amount" class="filled-in m-l-20">
                    <label for="option_show_zero_amount" class="m-r-20">แสดงสินค้าคงเหลือ 0</label>

                    <input type="checkbox" id="option_order_by_brand" name="option_order_by_brand" class="filled-in m-l-20">
                    <label for="option_order_by_brand" class="m-r-20">เรียงสินค้าตามยี่ห้อ</label>

                    @can('export-stock-all-branch'  )
                    <input type="checkbox" id="option_show_branch" name="option_show_branch" class="filled-in m-l-20">
                    <label for="option_show_branch">แยกสินค้าตามสาขา(*กรณีเลือกสาขาทั้งหมด)</label>
                    @endcan

                    <div class="row">
                        <div class="col-md-12 text-center">
                            <button type="submit" class="btn btn-info waves-effect waves-light m-r-10">ดาวน์โหลด</button>
                        </div>

                    </div>
                </form>




            </div>


        </div>
    </div>



</div>


<!-- ============================================================== -->
<!-- End PAge Content -->
<!-- ============================================================== -->
@endsection

@section('js-head')

@endsection

@section('js-bottom')
    <!-- jQuery peity -->
    <script src="/assets/plugins/tablesaw-master/dist/tablesaw.js"></script>
    <script src="/assets/plugins/tablesaw-master/dist/tablesaw-init.js"></script>

    <script src="/assets/plugins/toast-master/js/jquery.toast.js"></script>

    @include('template.flash-msg');
@endsection

@section('css-head')
    <!-- Bootstrap responsive table CSS -->
    <link href="/assets/plugins/tablesaw-master/dist/tablesaw.css" rel="stylesheet">

    <!-- toast CSS -->
    <link href="/assets/plugins/toast-master/css/jquery.toast.css" rel="stylesheet">
    <!-- Custom CSS -->
    <link href="/css/style.css" rel="stylesheet">
    <!-- You can change the theme colors from here -->
    <link href="/css/colors/blue.css" id="theme" rel="stylesheet">
@endsection