<!-- ============================================================== -->
<!-- Topbar header - style you can find in pages.scss -->
<!-- ============================================================== -->

<header class="topbar">
    <nav class="navbar top-navbar navbar-expand-md navbar-light">
        <!-- ============================================================== -->
        <!-- Logo -->
        <!-- ============================================================== -->
        <div class="navbar-header">
            <a class="navbar-brand" href="index.html">
                <!-- Logo icon --><b>
                    <!--You can put here icon as well // <i class="wi wi-sunset"></i> //-->
                    <!-- Dark Logo icon -->
                    <img src="/assets/images/logo-icon.png" alt="homepage" class="dark-logo" />
                    <!-- Light Logo icon -->
                    <img src="/assets/images/logo-light-icon.png" alt="homepage" class="light-logo" />
                </b>
                <!--End Logo icon -->
                <!-- Logo text --><span>
                         <!-- dark Logo text -->
                         <img src="/assets/images/logo-text.png" alt="homepage" class="dark-logo" />
                    <!-- Light Logo text -->
                         <img src="/assets/images/logo-light-text.png" class="light-logo" alt="homepage" /></span> </a>
        </div>
        <!-- ============================================================== -->
        <!-- End Logo -->
        <!-- ============================================================== -->
        <div class="navbar-collapse">
            <!-- ============================================================== -->
            <!-- toggle and nav items -->
            <!-- ============================================================== -->
            <ul class="navbar-nav mr-auto mt-md-0">
                <!-- This is  -->
                <li class="nav-item"> <a class="nav-link nav-toggler hidden-md-up text-muted waves-effect waves-dark" href="javascript:void(0)"><i class="mdi mdi-menu"></i></a> </li>
                <li class="nav-item m-l-10"> <a class="nav-link sidebartoggler hidden-sm-down text-muted waves-effect waves-dark" href="javascript:void(0)"><i class="ti-menu"></i></a> </li>
                <li class="nav-item dropdown">
                    <a class="nav-link dropdown-toggle text-muted waves-effect waves-dark" href="" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"> <i class="mdi mdi-rocket"></i></a>
                    <div class="dropdown-menu">
                        @can('new-order')
                            <a class="dropdown-item" href="/sale/neworder"><i class="mdi mdi-currency-usd text-warning"></i> ขายใหม่</a>
                        @endcan

                        @can('stock-transfer')
                            <a class="dropdown-item" href="/stock/transfer"><i class="mdi mdi-swap-horizontal text-success"></i> โอนสินค้า</a>
                        @endcan

                        @can('stock-in')
                            <a class="dropdown-item" href="/stock/in"><i class="mdi mdi-subdirectory-arrow-right text-primary"></i> รับเข้าสินค้าใหม่</a>
                        @endcan

                        @can('stock-defect')
                            <a class="dropdown-item" href="/stock/defect"><i class="mdi mdi-image-broken-variant text-danger"></i> ตัดสินค้าเสีย</a>
                        @endcan



                    </div>
                </li>

                <li class="nav-item dropdown mega-dropdown" ng-controller="MagicBarcodeController">


                    <a class="nav-link dropdown-toggle text-muted waves-effect waves-dark" href="" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" ng-click="openMagicBarcode()"><i class="mdi mdi-barcode-scan"></i></a>
                    <div class="dropdown-menu scale-up-left" style="padding-bottom: 0px">
                        <div class="row">
                            <div class="col-lg-12 col-xlg-12 m-b-10">
                                <div class="input-group">
                                    <div class="input-group-addon"><i class="mdi mdi-barcode"></i></div>
                                    <input type="text" class="form-control ng-pristine ng-valid ng-empty ng-touched" name="input_magic_barcode" id="input_magic_barcode" placeholder="บาร์โค้ด , IMEI, เลขที่ใบเสร็จ, ใบอ้างอิงการทำสต๊อค" ng-model="input_magic_barcode" ng-keyup="$event.keyCode == 13 && enterMagicBarcode()">
                                    <span class="input-group-btn">
                                        <button ng-click="enterMagicBarcode()" class="btn waves-effect waves-light btn-success">ตรวจสอบ</button>
                                    </span>
                                </div>
                            </div>

                            <div class="col-lg-3" ng-hide="!isSN && !isBarcode">
                                <!-- CAROUSEL -->
                                <div id="carouselExampleControls" class="carousel slide" data-ride="carousel">
                                    <img ng-src="/assets/images/no-image.png" src="<% product_.image %>" width="100%">
                                </div>
                                <!-- End CAROUSEL -->
                            </div>

                            <div class="col-lg-4" ng-hide="!isSN && !isBarcode">
                                <table class="table no-border lite-padding">
                                    <tbody>
                                    <tr>
                                        <td style="font-weight: bold">รหัสสินค้า</td>
                                        <td class="font-medium"><% pad(product_.id,6) %></td>
                                    </tr>
                                    <tr>
                                        <td style="font-weight: bold">ชื่อสินค้า</td>
                                        <td class="font-medium"><% product_.product_name %></td>
                                    </tr>
                                    <tr>
                                        <td style="font-weight: bold">ยี่ห้อ</td>
                                        <td class="font-medium"><% product_.brand_name %></td>
                                    </tr>
                                    <tr>
                                        <td style="font-weight: bold">รุ่น</td>
                                        <td class="font-medium"><% product_.model %></td>
                                    </tr>
                                    <tr>
                                        <td style="font-weight: bold">หมวดหมู่</td>
                                        <td class="font-medium"><% product_.cate_name %></td>
                                    </tr>
                                    <tr>
                                        <td style="font-weight: bold">ราคา</td>
                                        <td class="font-medium"><% product_.price %></td>
                                    </tr>
                                    <tr>
                                        <td style="font-weight: bold">บาร์โค้ด</td>
                                        <td class="font-medium"><% product_.barcode %></td>
                                    </tr>
                                    <tr>
                                        <td style="font-weight: bold">คงเหลือ</td>
                                        <td class="font-medium"><span class="<% product_.amount_in_branch > 0 ? 'text-success':'text-danger' %>"><% product_.amount_in_branch %></span> (<% product_.amount %>)</td>
                                    </tr>
                                    <tr>
                                        <td style="font-weight: bold">คำอธิบาย</td>
                                        <td class="font-medium"><% product_.description %></td>
                                    </tr>
                                    </tbody>
                                </table>
                            </div>

                            <div class="col-lg-5" ng-hide="!isBarcode">
                                <table class="table table-bordered">
                                    <thead>
                                    <tr>
                                        <th width="50px" style="text-align: center">#</th>
                                        <th width="200px">สาขา</th>
                                        <th width="100px" style="text-align: center">คงเหลือ</th>
                                        <th>IMEI/SN</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    <tr ng-repeat="p_stock in product_stock.product_data">
                                        <td style="text-align: center"><% $index+1 %></td>
                                        <td><% p_stock.branch_name %> <i class="mdi mdi-briefcase-check" ng-if="p_stock.branch_id == source_branch" style="color: green"></i></td>
                                        <td style="text-align: center"><% p_stock.amount %></td>
                                        <td><% p_stock.sn %></td>
                                    </tr>

                                    <tr ng-if="product_stock.product_data.length == 0">
                                        <td colspan="4" class="text-center">ไม่มีข้อมูล</td>
                                    </tr>
                                    </tbody>
                                </table>
                            </div>


                            <div class="col-lg-3" ng-hide="!isSN">
                                <table class="table no-border lite-padding">
                                    <tbody>
                                    <tr>
                                        <td style="font-weight: bold">IMEI</td>
                                        <td class="font-medium"><% product_.sn %></td>
                                    </tr>
                                    <tr>
                                        <td style="font-weight: bold">สถานะ</td>
                                        <td class="font-medium"><% status_name[product_.status] %></td>
                                    </tr>
                                    <tr>
                                        <td style="font-weight: bold">สาขา</td>
                                        <td class="font-medium"><% product_.branch_name %></td>
                                    </tr>
                                    </tbody>
                                </table>
                                <a class="btn btn-block btn-lg btn-info" ng-hide="product_.status!=3" href="/sale/invoice/INV-<% pad(product_.invoice_id,6) %>"><i class="mdi mdi-file-document"></i> ดูใบเสร็จ</a>
                                <button type="button" class="btn btn-block btn-lg btn-default" disabled="disabled"><i class="mdi mdi-history"></i> ประวัติสินค้า</button>
                            </div>

                        </div>
                    </div>
                </li>


            </ul>



        </div>
    </nav>



</header>
<!-- ============================================================== -->
<!-- End Topbar header -->
<!-- ============================================================== -->

@section('js-bottom')
    @include('template.flash-msg')
@endsection
