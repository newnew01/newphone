<!-- Sidebar navigation-->
<nav class="sidebar-nav">
    <ul id="sidebarnav">

        <li class="nav-devider"></li>
        <li><a class="waves-effect waves-dark" href="/home"><i class="mdi mdi-gauge"></i><span class="">หน้าหลัก </span></a></li>

        <li> <a class="has-arrow waves-effect waves-dark" href="#" aria-expanded="false"><i class="mdi mdi-package"></i><span class="hide-menu">สินค้า</span></a>
            <ul aria-expanded="false" class="collapse">
                @can('create-product')
                    <li><a href="/product/new">เพิ่มสินค้า</a></li>
                @endcan
                <li><a href="/product/list">รายการสินค้า</a></li>
                <li><a href="/product-catebrand">หมวดหมู่และยี่ห้อ</a></li>
            </ul>
        </li>
        <li> <a class="has-arrow waves-effect waves-dark" href="#" aria-expanded="false"><i class="mdi mdi-package-variant-closed"></i><span class="hide-menu">สต๊อคสินค้า</span></a>
            <ul aria-expanded="false" class="collapse">
                @can('stock-in')
                    <li><a href="/stock/in">รับสินค้าเข้าใหม่</a></li>
                @endcan

                @can('stock-transfer')
                    <li><a href="/stock/transfer">โอนสินค้า</a></li>
                @endcan

                @can('stock-defect')
                    <li><a href="/stock/defect">ตัดสินค้าเสีย</a></li>
                @endcan

                <li><a href="/stock/list">รายการทำสต๊อค</a></li>
            </ul>
        </li>
        <li> <a class="has-arrow waves-effect waves-dark" href="#" aria-expanded="false"><i class="mdi mdi-cart"></i><span class="hide-menu">การขาย</span></a>
            <ul aria-expanded="false" class="collapse">
                @can('new-order')
                    <li><a href="/sale/neworder">ขายใหม่ </a></li>
                @endcan
                <li><a href="/sale/list">รายการขาย</a></li>
                <li><a href="/sale/topup">เติมเงิน</a></li>
            </ul>
        </li>

        <li> <a class="has-arrow waves-effect waves-dark" href="#" aria-expanded="false"><i class="mdi mdi-cellphone"></i><span class="hide-menu">AIS</span></a>
            <ul aria-expanded="false" class="collapse">
                <li><a href="/ais/new-ais-deal">เพิ่มเครื่องโครงการ</a></li>
            </ul>
        </li>
        <li> <a class="has-arrow waves-effect waves-dark" href="#" aria-expanded="false"><i class="mdi mdi-barcode"></i><span class="hide-menu">ลาเบลสินค้า</span></a>
            <ul aria-expanded="false" class="collapse">
                @can('print-label')
                <li><a href="/barcode/print-label">พิมพ์ลาเบลสินค้า</a></li>
                @endcan
                <li><a href="/barcode/custom">กำหนดเอง</a></li>
                <li><a href="/barcode/print-service">เซอร์วิสการพิมพ์</a></li>
            </ul>
        </li>

        <li> <a class="has-arrow waves-effect waves-dark" href="#" aria-expanded="false"><i class="mdi mdi-file-chart"></i><span class="hide-menu">รายงาน</span></a>
            <ul aria-expanded="false" class="collapse">
                <li><a href="/report/daily">รายงานประจำวัน</a></li>
                @can('export-stock')
                <li><a href="/report/stock">รายงานสต๊อค</a></li>
                @endcan
            </ul>
        </li>

        @if(Gate::check('create-user') || Gate::check('edit-user') || Gate::check('set-user-permission') || Gate::check('delete-user'))
            <li> <a class="has-arrow waves-effect waves-dark" href="#" aria-expanded="false"><i class="mdi mdi-account"></i><span class="hide-menu">ผู้ใช้งาน</span></a>
                <ul aria-expanded="false" class="collapse">
                    @can('create-user')
                        <li><a href="/user/new">เพิ่มผู้ใช้งาน</a></li>
                    @endcan
                    <li><a href="/user/list">รายการผู้ใช้งาน</a></li>
                </ul>
            </li>
        @endif

            <li>
                <a class="waves-effect waves-dark" href="/logout" aria-expanded="false" onclick="event.preventDefault();document.getElementById('logout-form').submit();"><i class="mdi mdi-logout"></i><span class="hide-menu">ออกจากระบบ</span></a>
            </li>



    </ul>
</nav>
<!-- End Sidebar navigation -->

<form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
    {{ csrf_field() }}
</form>