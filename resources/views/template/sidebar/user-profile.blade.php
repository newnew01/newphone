<!-- User profile -->
<div class="user-profile">
    <!-- User profile image -->
    <div class="profile-img"> <img src="/assets/images/users/users-3-icon.png" alt="user" />
        <!-- this is blinking heartbit-->
        <div class="notify setpos"> <span class="heartbit"></span> <span class="point"></span> </div>
    </div>
    <!-- User profile text-->
    <div class="profile-text">
        <h5>ชื่อ: {{Auth::user()->name}}</h5>
        <h6>สาขา: {{Auth::user()->branch->branch_name}}</h6>
        @if(Auth::user()->getUserBranch()->count() > 1)
            <select class="col-10" id="select_user_branch">
                <option selected="">[เปลี่ยนสาขา]</option>
                @foreach(Auth::user()->getUserBranch()->get() as $user_branch)
                    <option value="{{$user_branch->branch->id}}">{{$user_branch->branch->branch_name}}</option>
                @endforeach
            </select>
        @endif

    </div>
</div>
<!-- End User profile text-->